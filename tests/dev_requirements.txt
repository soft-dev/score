paramiko
docker-py
spython
ipywidgets
nbformat
pycryptodomex
requests
IPython
importlib_resources ; python_version < "3.7.0"
pytest
sphinx
sphinx_rtd_theme
