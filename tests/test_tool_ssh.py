# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import pytest
import warnings
import time
import tempfile
import threading
import os
from shutil import rmtree
from getpass import getuser, getpass
from pathlib import Path

from score.statics.file_system import ScoreFileSystem
from score.tools import ssh

test_home_directory = os.path.join(os.path.expanduser('~'), 'SCORE_TEST_TO_BE_REMOVED')
test_remote_home_directory = '$HOME/SCORE_TEST_REMOTE_TO_BE_REMOVED'

test_remote_temp_directory = os.path.join(tempfile.gettempdir(), 'SCORE_TEST_REMOTE_TO_BE_REMOVED')
test_local_temp_directory = os.path.join(tempfile.gettempdir(), 'SCORE_TEST_LOCAL_TO_BE_REMOVED')

test_remote_temp_file = os.path.join(tempfile.gettempdir(), 'SCORE_TEST_REMOTE_TO_BE_REMOVED.tmp')
test_local_temp_file = os.path.join(tempfile.gettempdir(), 'SCORE_TEST_LOCAL_TO_BE_REMOVED.tmp')

test_remote_nested_temp_directory = os.path.join(test_remote_temp_directory, 'inside_dir')
test_local_nested_temp_directory = os.path.join(test_local_temp_directory, 'inside_dir')

ssh_keys_directory = os.path.join(test_home_directory, '.ssh')
ssh_key_name = 'score_test_id_rsa'
ssh_additional_key_name = 'score_test_additional_id_rsa'

"""
Tests must be perform on localhost with current user to be able to test remote file/folder creation and to clean 
everything at the end
"""
server = "localhost"
username = getuser()
password = ''

# Leave following global variable to their values
# Using a gitlab runner, it is not possible to deploy a key using password, the key must be created and deployed before.
KEEP_SCORE_FOLDER = False
ssh_key_file = os.getenv("GITLAB_CI_SSH_KEY", os.path.join(ssh_keys_directory, ssh_key_name))
config_directory = os.path.join(os.path.expandvars(ScoreFileSystem.MAIN_CONFIG_DIR))


def setup_module():
    """ Get user password for ssh tests, and create an empty directory """

    global password, KEEP_SCORE_FOLDER

    if os.getenv("GITLAB_CI_SSH_KEY") is None:
        password = getpass(prompt="\n\033[1m\033[34m{}@{}'s password :\033[0m".format(username, server))
    if os.path.isdir(test_home_directory):
        rmtree(test_home_directory)
    os.mkdir(test_home_directory)

    if os.path.isdir(test_remote_home_directory.replace('$HOME', os.path.expanduser('~'))):
        rmtree(test_remote_home_directory.replace('$HOME', os.path.expanduser('~')))

    if server == 'localhost' and username == getuser() and os.path.isdir(config_directory):
        KEEP_SCORE_FOLDER = True


def teardown_module():

    """ Test removing remote ssh authorization for the main key and remove created folders """

    # Removing main key
    if os.path.isfile(ssh_key_file + '.pub'):
        with open(ssh_key_file + '.pub', 'r') as fd:
            main_key = fd.read().strip()
        removed = ssh.remove_remote_authorized_key(server,
                                                   username,
                                                   ssh_key_file,
                                                   main_key)
        assert removed, "Main key has not been removed\n" \
                        "\033[31m YOU SHOULD REMOVE AUTHORIZED KEYS MANUALLY IN " \
                        "FILE {}".format(os.path.join(os.path.expanduser('~'), '.ssh', 'authorized_keys'))
    else:
        warnings.warn("\033[32mMain ssh key was not existing, this should not append !!\033[0m")

    # TEST that main key has been removed
    assert not ssh.test_ssh_connection(server, username, ssh_key_file), \
        "Test connection using removed main key succeed, meaning the key has not been removed\n" \
        "\033[31m YOU SHOULD REMOVE AUTHORIZED KEYS MANUALLY IN FILE {}".format(os.path.join(os.path.expanduser('~'),
                                                                                             '.ssh',
                                                                                             'authorized_keys'))

    if not KEEP_SCORE_FOLDER and os.path.isdir(config_directory):
        rmtree(config_directory)

    if os.path.isdir(test_home_directory):
        rmtree(test_home_directory)


def test_create_ssh_key_pair():

    """
    Test creating ssh key pair, with and without a previous ssh directory, than check :
        - if folder has been created, private and public keys exist
        - if private key has the right permission (440)
        - if public key contain the user information
    """

    # TEST key files creation in non existing folder, without username
    ssh.create_ssh_key_pair(ssh_keys_directory, ssh_key_name)
    assert os.path.isdir(ssh_keys_directory), "Keys directory has not been created"
    assert os.path.isfile(os.path.join(ssh_keys_directory, ssh_key_name)), \
        "Private key has not been created when folder didn't previously exist"
    assert os.path.isfile(os.path.join(ssh_keys_directory, ssh_key_name + '.pub')), \
        "Public key has not been created when folder didn't previously exist"
    assert os.stat(os.path.join(ssh_keys_directory, ssh_key_name)).st_mode & 0o777 == 0o400, \
        "Error in private key permissions when folder didn't previously exist"
    with open(os.path.join(ssh_keys_directory, ssh_key_name + '.pub'), 'r') as fd:
        assert 'unknown_user@SCORE' in fd.read(), "Default username not set in public key"

    # TEST key creation in existing folder, with username
    additional_key_username = 'test_additional_key'
    ssh.create_ssh_key_pair(ssh_keys_directory, ssh_additional_key_name, username=additional_key_username)
    assert os.path.isfile(os.path.join(ssh_keys_directory, ssh_additional_key_name)), \
        "Private key has not been created in existing folder"
    assert os.path.isfile(os.path.join(ssh_keys_directory, ssh_additional_key_name + '.pub')), \
        "Public key has not been created in existing folder"
    assert os.stat(os.path.join(ssh_keys_directory, ssh_additional_key_name)).st_mode & 0o777 == 0o400, \
        "Error in private key permissions in existing folder"
    with open(os.path.join(ssh_keys_directory, ssh_additional_key_name + '.pub'), 'r') as fd:
        assert f'{additional_key_username}@SCORE' in fd.read(), \
            "Filled username not set in public key"


def test_key_deployment():

    """
    Test public key deployment and removing, with some limit
    !! -> If user has already a working ssh key for localhost (in folder $HOME/.ssh), the connection test will work
    every time.
        - This test is done for the main key (using password for first connection) and
        for an additional key (using main key for connection).
        - Test removing the additional key.

    """

    # TEST deploying main key
    # That test is avoid in case of gitlab runner, because main key has already been deployed
    if os.getenv("GITLAB_CI_SSH_KEY") is None:
        deployed = ssh.deploy_main_key(server, username, password, ssh_key_file)
        assert deployed, "Main key has not been deployed"

    # TEST deploying additional key
    with open(os.path.join(ssh_keys_directory, ssh_additional_key_name + '.pub'), 'r') as fd:
        additional_key = fd.read().strip()

    deployed = ssh.deploy_additional_key(server,
                                         username,
                                         ssh_key_file,
                                         additional_key)
    assert deployed, "Additional key has not been deployed"
    assert ssh.test_ssh_connection(server, username, os.path.join(ssh_keys_directory, ssh_additional_key_name)), \
        "Test connection using deployed additional key failed\n" \
        "\033[31m YOU SHOULD REMOVE AUTHORIZED KEYS MANUALLY IN FILE {}".format(os.path.join(os.path.expanduser('~'),
                                                                                             '.ssh',
                                                                                             'authorized_keys'))

    # TEST removing additional key
    removed = ssh.remove_remote_authorized_key(server,
                                               username,
                                               ssh_key_file,
                                               additional_key)
    assert removed, "Additional key has not been removed\n" \
                    "\033[31m YOU SHOULD REMOVE AUTHORIZED KEYS MANUALLY IN " \
                    "FILE {}".format(os.path.join(os.path.expanduser('~'), '.ssh', 'authorized_keys'))

    # TEST connection is not working anymore after removing additional key
    assert not ssh.test_ssh_connection(server, username, os.path.join(ssh_keys_directory, ssh_additional_key_name)), \
        "Test connection using removed additional key succeed, meaning the key has not been removed\n" \
        "\033[31m YOU SHOULD REMOVE AUTHORIZED KEYS MANUALLY IN FILE {}".format(os.path.join(os.path.expanduser('~'),
                                                                                             '.ssh',
                                                                                             'authorized_keys'))


def test_creating_remote_directory():

    """
    Test creating a remote folder.
    This test is fully performed only if the connection is on localhost with current user.
        - function return the right remote directory full path
        - directory really exists and has the right permissions

        Finally this function try to clean local or remote folder depending on the situation.

    TODO: replace this function by SCPHandler mkdir ?
    """

    # TEST creating remote directory
    chmod = 0o440
    directory = ssh.create_remote_directory(server, username, ssh_key_file, test_remote_home_directory,
                                            chmod=oct(chmod)[-3:])
    assert directory, "Remote directory hasn't been created"
    assert directory == test_remote_home_directory, "Created directory doesn't match the requested one"
    if server == 'localhost' and username == getuser():
        assert os.path.isdir(directory.replace('$HOME', os.path.expanduser('~'))), \
            "Remote directory doesn't exist"
        assert os.stat(directory.replace('$HOME', os.path.expanduser('~'))).st_mode & 0o777 == chmod, \
            "Created remote directory doesn't have the right permissions"
        try:
            rmtree(directory.replace('$HOME', os.path.expanduser('~')))
        except:
            warnings.warn("\n\033[31mDirectory {} has not been deleted.\033[0m".format(test_remote_home_directory))
    else:
        warnings.warn("\n\033[32mThe test on remote directory creation can only be fully performed on localhost with "
                      "current user session.\n"
                      "The directory could have not been really created, or not with the right permissions.\n\033[0m")
        try:
            ssh_handler = ssh.SSHHandler(server, username, ssh_key_file)
            stdout, stderr = ssh_handler.simple_command('rmdir {}'.format(directory),
                                                        close_session=True)
            if stderr:
                warnings.warn("\n\033[31mThe remote directory may not have been deleted : "
                              "{}@{}:{}\nMessage from remote : \033[1m'{}'\033[0m".format(server, username, directory, stderr))

        except:
            warnings.warn("\n\033[31mThe remote directory may not have been deleted : "
                          "{}@{}:{}\033[0m".format(server, username, directory))


def test_creating_remote_ssh_key():

    """
    Test the creation of a remote SSH key pair.
    This test is fully performed only if the connection is on localhost with current user.
        - returned public key value is bytes and not empty
        - returned public key is not empty
        - remote directory is created, and key pair files exist
        - if key pair was previously existing, return the public key value

        TODO: be able to set up remote folder name (default is '.score')
    """

    # TEST creating remote SSH key
    remote_key = ssh.create_remote_ssh_key(server, username, ssh_key_file)
    assert isinstance(remote_key, bytes), "Remote public key should be bytes"
    assert remote_key, "Remote public key is empty"

    if server == 'localhost' and username == getuser():
        if not KEEP_SCORE_FOLDER:
            assert os.path.isdir(config_directory), "Remote configuration folder has not been created"
            assert os.path.isfile(os.path.join(config_directory, 'score_id_rsa')), "Remote private key doesn't exist"
            assert os.path.isfile(os.path.join(config_directory, 'score_id_rsa.pub')), "Remote public key doesn't exist"

            # TEST retrieving remote SSH key
            remote_key = ssh.create_remote_ssh_key(server, username, ssh_key_file)
            assert isinstance(remote_key, bytes), "Retrieved remote public key should be bytes"
            assert remote_key, "Retrieved remote public key is empty"
        else:
            warnings.warn("\n\033[32mThe configuration directory ({}) was already existing, "
                          "the test does not include : \n"
                          "\t(i) remote configuration directory creation\n"
                          "\t(ii) creation of key files (private and public)\033[0m".format(config_directory))
    else:
        warnings.warn("\n\033[32mThe test on remote ssh keys creation can only be fully performed on localhost with "
                      "current user session.\n"
                      "\033[31mThe remote configuration directory {}@{}:$HOME/.score has not been removed "
                      "after test.\033[0m".format(username, server))


@pytest.mark.parametrize("length", [0, 5, 7, 11, -1, 8])
def test_random_string_digits(length):

    """ Test retrieving random string with different lengths """

    random_str = ssh.random_string_digits(length)
    assert isinstance(random_str, str), "Random string should be a string"
    assert len(random_str) == max(length, 0), "Random string doesn't have the right length"


@pytest.fixture()
def ssh_handler():

    """ Create a SSHHandler object in setup, and kill it in teardown """

    # create SSHHandler object, return it and close it
    ssh_handler = ssh.SSHHandler(server, username, key_file=ssh_key_file)
    yield ssh_handler
    ssh_handler.kill()


def test_ssh_handler_creation(ssh_handler):
    """ Test the creation of a SSHHandler object """

    # TEST connecting to handler
    assert isinstance(ssh_handler, ssh.paramiko.SSHClient)
    assert isinstance(ssh_handler, ssh.Thread)


def test_ssh_handler_simple_command_and_kill(ssh_handler):

    """
    Test SSHHandler simple command
        - types of stdout and stderr are the expected ones (string or bytes)
        - stdout and stderr are filled when they should
        - killing the handler (with the command or independently)

    """

    simple_command_ok = 'whoami'
    simple_command_error = 'whoiam'

    # TEST simple command
    stdout, stderr = ssh_handler.simple_command(simple_command_ok)
    assert isinstance(stdout, str), "stdout should be a string"
    assert isinstance(stderr, str), "stderr should be a string"
    assert stdout == username, "stdout should be the username"
    assert not stderr, "stderr should be empty"

    # check that session has not been closed
    try:
        ssh_handler.simple_command(simple_command_ok)
    except AttributeError:
        pytest.fail("It seems that handler has been closed and should not")

    # TEST simple command returning bytes
    stdout, stderr = ssh_handler.simple_command(simple_command_ok, binary_output=True)
    assert isinstance(stdout, bytes), "stdout should be a bytes array"
    assert isinstance(stderr, str), "stderr should be a string"
    assert stdout.decode('utf8') == username, "Decoded stdout should be the username"
    assert not stderr, "stderr should be empty"

    # TEST simple command with a wrong command
    stdout, stderr = ssh_handler.simple_command(simple_command_error)
    assert isinstance(stdout, str), "stdout should be a string"
    assert isinstance(stderr, str), "stderr should be a string"
    assert not stdout, "stdout should be empty"
    assert stderr, "stderr should not be empty"

    # TEST closing session after simple command
    stdout, stderr = ssh_handler.simple_command(simple_command_ok, close_session=True)
    assert isinstance(stdout, str), "stdout should be a string (closing session)"
    assert isinstance(stderr, str), "stderr should be a string (closing session)"
    assert stdout == username, "stdout should be the username (closing session)"
    assert not stderr, "stderr should be empty (closing session)"

    # TEST that session has been closed
    # TODO: add custom message if not error is raised
    with pytest.raises(AttributeError):
        ssh_handler.simple_command(simple_command_ok)

    # TEST killing session
    ssh_handler.reconnect()
    ssh_handler.kill()
    with pytest.raises(AttributeError) as exec_info:
        ssh_handler.simple_command(simple_command_ok)


def test_ssh_handler_exec_detach_and_get_pid(ssh_handler):

    """
    Test SSHHandler in detached execution
    This test is fully performed only if the connection is on localhost with current user.
        - The returned value is a file path on remote server, and the file is created
        - Disconnection/reconnection allows retrieving process PID with command line
        - The command is really performed
    """

    exec_detach_command = 'sleep 10'
    exec_detach_command_localhost = 'mkdir {}'.format(test_remote_home_directory)
    simple_command_error = 'whoiam'

    # TEST detached execution and get_pid
    error_file = ssh_handler.exec_detach(exec_detach_command)
    assert error_file, "The detach command should return an error file path"
    pid = ssh_handler.get_pid(exec_detach_command)
    assert pid, "The PID is empty, meaning one of [exec_detach(), get_pid()) function didn't work"

    # disconnecting handler and reconnect back to see if PID still remains
    ssh_handler.kill()
    ssh_handler.reconnect()
    pid2 = ssh_handler.get_pid(exec_detach_command)
    assert pid, "The PID is empty, meaning one of [exec_detach(), get_pid()) function didn't work"
    assert pid == pid2, "The PID changes during disconnection"

    if server == "localhost":
        # TEST error file exists and is empty with previous command with no error
        error_file_created = False
        for i in range(3):
            time.sleep(1)
            error_file_created = os.path.isfile(error_file)
            if error_file_created:
                break

        assert error_file_created, \
            "Error file has not been created using command '{}', supposed without error".format(exec_detach_command)
        with open(error_file, 'r') as fd:
            error = fd.read()
            assert not len(error)

        # TEST that error file exists and is not empty with a command containing error
        error_file = ssh_handler.exec_detach(simple_command_error)
        error_file_created = False
        for i in range(3):
            time.sleep(1)
            error_file_created = os.path.isfile(error_file)
            if error_file_created:
                break

        assert error_file_created, \
            "Error file has not been created with a fake command : '{}'".format(simple_command_error)
        with open(error_file, 'r') as fd:
            error = fd.read()
            assert len(error)

        if username == getuser():
            # TEST that the command is really executed by creating a directory
            ssh_handler.exec_detach(exec_detach_command_localhost)
            directory_created = False
            for i in range(3):
                time.sleep(1)
                directory_created = os.path.isdir(test_remote_home_directory.replace('$HOME', os.path.expanduser('~')))
                if directory_created:
                    break
            assert directory_created, "Remote directory doesn't exist"
            try:
                rmtree(test_remote_home_directory.replace('$HOME', os.path.expanduser('~')))
            except:
                warnings.warn("\n\033[31mDirectory {} has not been deleted.\033[0m".format(test_remote_home_directory))
        else:
            warnings.warn("\n\033[32mYou should use current user to perform a full test on detached execution\033[0m")
    else:
        warnings.warn("\n\033[32mYou should use localhost server to perform a full test on detached execution\033[0m")


def test_ssh_handler_exec_channel(ssh_handler, capsys):

    """
    Test execution in channel, and reading channel output without waiting end of command
    This test is performed only if the connection is on localhost.
        - following a remote file works well and return file contents
        - RegexCallback works on remote file contents

    TODO: should return error when file doesn't exist
    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on execution in channel\033[0m")
        return

    exec_channel_command = 'tail -f {}'.format(test_remote_temp_file)
    pattern1 = r"^([0-9]*)abc$"
    pattern2 = r"^(\w*)$"
    text1 = "123abc"
    text2 = "123"

    def callback1(h, m):
        """ Function called by RegexCallback """
        print("Callback 1 called", end='')

    def callback2(h, m):
        """ Function called by RegexCallback """
        print("Callback 2 called", end='')

    # TEST execution in channel
    if os.path.isfile(test_remote_temp_file):
        os.remove(test_remote_temp_file)

    # TODO: should return error as file doesn't exist (maybe use recv_stderr_ready())
    # ssh_handler.exec_channel(exec_channel_command)

    # TEST following a file without RegexCallback set up
    open(test_remote_temp_file, 'w').close()
    ssh_handler.exec_channel(exec_channel_command)
    with open(test_remote_temp_file, 'w') as fd:
        fd.write(text1)

    time.sleep(1)
    out, err = capsys.readouterr()
    assert out.strip() == text1, "Written data should have been printed"
    ssh_handler.kill()

    if os.path.isfile(test_remote_temp_file):
        os.remove(test_remote_temp_file)

    # TEST following a file with RegexCallback set up
    # TODO: after kill the thread cannot been run again
    #  We could init Thread in reconnect() function, but we should do the same in RegexCallback. Do we need it ?
    # ssh_handler.reconnect()
    ssh_handler = ssh.SSHHandler(server, username, key_file=ssh_key_file)

    open(test_remote_temp_file, 'w').close()
    ssh_handler.set_regex_callbacks((pattern1, callback1), (pattern2, callback2))
    ssh_handler.exec_channel(exec_channel_command)
    with open(test_remote_temp_file, 'w') as fd:
        fd.write(text1)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback 1 called", \
        "First callback function should have been called when writing \"{}\" in watched file".format(text1)

    with open(test_remote_temp_file, 'w') as fd:
        fd.write(text2)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback 2 called", \
        "Second callback function should have been called when writing \"{}\" in watched file".format(text2)
    # TODO: not necessary if reconnect works
    ssh_handler.kill()


@pytest.fixture()
def scp_handler():

    """ Create a SCPHandler object in setup, and kill it in teardown """

    # Clean temporary files and folder before test
    if os.path.isfile(test_local_temp_file):
        os.remove(test_local_temp_file)
    if os.path.isfile(test_remote_temp_file):
        os.remove(test_remote_temp_file)
    if os.path.isdir(test_remote_temp_directory):
        rmtree(test_remote_temp_directory)
    if os.path.isdir(test_local_temp_directory):
        rmtree(test_local_temp_directory)

    # create SCPHandler object, return it and close it
    scp_handler = ssh.SCPHandler(server, username, key_file=ssh_key_file)

    yield scp_handler

    scp_handler.close()

    # Clean temporary files and folder after test
    if os.path.isfile(test_local_temp_file):
        os.remove(test_local_temp_file)
    if os.path.isfile(test_remote_temp_file):
        os.remove(test_remote_temp_file)
    if os.path.isdir(test_remote_temp_directory):
        rmtree(test_remote_temp_directory)
    if os.path.isdir(test_local_temp_directory):
        rmtree(test_local_temp_directory)


def test_scp_handler_creation(scp_handler):
    """ Test the creation of a SCPHandler object """

    assert isinstance(scp_handler, ssh.paramiko.SFTPClient)


def test_scp_handler_get(scp_handler):

    """
    Test SCP handler 'get' function
    This test is performed only if the connection is on localhost.
        - get remote file "normally", in a thread and with closing session after.

    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on SCPHandler\033[0m")
        return

    text = "tralala"

    # TEST get file
    with open(test_remote_temp_file, 'w') as fd:
        fd.write(text)
    scp_handler.get(test_remote_temp_file, test_local_temp_file)
    assert os.path.isfile(test_local_temp_file), "Get remote file didn't create local file"
    with open(test_local_temp_file, 'r') as fd:
        assert fd.read() == text, "Get remote file created local file but not with the well contents"
    os.remove(test_local_temp_file)

    # TEST get file in thread
    scp_handler.get(test_remote_temp_file, test_local_temp_file, threading=True)
    threads = threading.enumerate()
    is_thread = False
    for thread in threads:
        if thread.getName() == 'score-get':
            is_thread = True
            break
    assert is_thread, "Get remote file in thread didn't create a thread"
    thread.join(timeout=2)
    assert not thread.is_alive(), "Thread is too long to copy small file, but it might not be an error"
    assert os.path.isfile(test_local_temp_file), "Get remote file (thread) didn't create local file"
    with open(test_local_temp_file, 'r') as fd:
        assert fd.read() == text, "Get remote file (thread) created local file but not with the well contents"
    os.remove(test_local_temp_file)

    # TEST get file and close session
    scp_handler.get(test_remote_temp_file, test_local_temp_file, one_shot=True)
    assert os.path.isfile(test_local_temp_file), "Get remote file (one_shot) didn't create local file"
    with open(test_local_temp_file, 'r') as fd:
        assert fd.read() == text, "Get remote file (one_shot) created local file but not with the well contents"
    with pytest.raises(OSError):
        scp_handler.get(test_remote_temp_file, test_local_temp_file)


def test_scp_handler_put(scp_handler):

    """
    Test SCP handler 'put' function
    This test can be performed only if the connection is on localhost.
        - put local file to remote "normally", in a thread and with closing session after.

    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on SCPHandler\033[0m")
        return

    text = "tralala"

    # TEST get file
    with open(test_local_temp_file, 'w') as fd:
        fd.write(text)
    scp_handler.put(test_local_temp_file, test_remote_temp_file)
    assert os.path.isfile(test_remote_temp_file), "Put remote file didn't create file"
    with open(test_remote_temp_file, 'r') as fd:
        assert fd.read() == text, "Put remote file created remote file but not with the well contents"
    os.remove(test_remote_temp_file)

    # TEST get file in thread
    scp_handler.put(test_local_temp_file, test_remote_temp_file, threading=True)
    threads = threading.enumerate()
    is_thread = False
    for thread in threads:
        if thread.getName() == 'score-put':
            is_thread = True
            break
    assert is_thread, "Put remote file in thread didn't create a thread"
    thread.join(timeout=2)
    assert not thread.is_alive(), "Thread is too long to copy small file, but it might not be an error"
    assert os.path.isfile(test_remote_temp_file), "Put remote file (thread) didn't create file"
    with open(test_remote_temp_file, 'r') as fd:
        assert fd.read() == text, "Put remote file (thread) created remote file but not with the well contents"
    os.remove(test_remote_temp_file)

    # TEST get file and close session
    scp_handler.put(test_local_temp_file, test_remote_temp_file, one_shot=True)
    assert os.path.isfile(test_remote_temp_file), "Put remote file (one_shot) didn't create file"
    with open(test_remote_temp_file, 'r') as fd:
        assert fd.read() == text, "Put remote file (one_shot) created remote file but not with the well contents"
    with pytest.raises(OSError):
        scp_handler.put(test_local_temp_file, test_remote_temp_file)


# TODO: test SCPHandler mkdir, get_dir and put_dir
def test_scp_handler_mkdir(scp_handler):

    """
    Test remote mkdir using SCPHandler.
    This test can be performed only if the connection is on localhost.
        - create remote directory with expected mode
        - raise error or not if directory already exists
        - one_shot argument close SCP session
    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on SCPHandler\033[0m")
        return

    # TEST create a remote directory with specific mode
    chmod = 0o440
    scp_handler.mkdir(test_remote_temp_directory, mode=chmod)
    assert os.path.isdir(test_remote_temp_directory), "Remote directory has not been created"
    assert os.stat(test_remote_temp_directory).st_mode & 0o777 == chmod, \
        "Remote directory doesn't have the right permissions"

    # TEST error raises when directory exists and error is not ignored
    with pytest.raises(IOError):
        scp_handler.mkdir(test_remote_temp_directory)

    # TEST that no error is raised when directory exists and error is ignored, but mode has changed
    chmod = 0o777
    try:
        scp_handler.mkdir(test_remote_temp_directory, mode=chmod, ignore_existing=True)
    except IOError:
        pytest.fail("No error should raise when ignore_existing is True ")
    assert os.stat(test_remote_temp_directory).st_mode & 0o777 == chmod, \
        "Remote directory doesn't have the right permissions"

    # TEST that one_shot argument close SCP session
    scp_handler.mkdir(test_remote_temp_directory, mode=chmod, ignore_existing=True, one_shot=True)
    with pytest.raises(OSError):
        scp_handler.mkdir(test_remote_temp_directory, mode=chmod, ignore_existing=True)


def test_scp_handler_get_dir(scp_handler):

    """
    Test copying locally a remote directory.
    This test can be performed only if the connection is on localhost.
        - get a remote directory (with target directory previously existing or not)
        - close session after copy

    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on SCPHandler\033[0m")
        return

    text = "tralala"

    # create nested "remote" directory and create a file inside
    Path(test_remote_nested_temp_directory).mkdir(parents=True, exist_ok=True)
    with open(os.path.join(test_remote_nested_temp_directory, os.path.basename(test_remote_temp_file)), 'w') as fd:
        fd.write(text)

    # Get "remote" directory contents with target directory not existing
    scp_handler.get_dir(test_remote_temp_directory, test_local_temp_directory)
    assert os.path.isdir(test_local_temp_directory), \
        "Local main directory has not been created with SCPHandler.get_dir(), no target"
    nested_created = test_remote_nested_temp_directory.replace(test_remote_temp_directory, test_local_temp_directory)
    assert os.path.isdir(nested_created), \
        "Nested local directory has not been created with SCPHandler.get_dir(), no target"
    assert os.path.isfile(os.path.join(nested_created, os.path.basename(test_remote_temp_file))), \
        "Nested local file has not been created with SCPHandler.get_dir(), no target"
    with open(os.path.join(nested_created, os.path.basename(test_remote_temp_file)), 'r') as fd:
        assert fd.read() == text, "Nested local file has no the same contents with SCPHandler.get_dir(), no target"

    rmtree(test_local_temp_directory)

    # Get "remote" directory contents with target directory existing
    os.mkdir(test_local_temp_directory)
    scp_handler.get_dir(test_remote_temp_directory, test_local_temp_directory)
    assert os.path.isdir(test_local_temp_directory), \
        "Local main directory has not been created with SCPHandler.get_dir(), target existing"
    nested_created = test_remote_nested_temp_directory.replace(test_remote_temp_directory, test_local_temp_directory)
    assert os.path.isdir(nested_created), \
        "Nested local directory has not been created with SCPHandler.get_dir(), target existing"
    assert os.path.isfile(os.path.join(nested_created, os.path.basename(test_remote_temp_file))), \
        "Nested local file has not been created with SCPHandler.get_dir(), target existing"
    with open(os.path.join(nested_created, os.path.basename(test_remote_temp_file)), 'r') as fd:
        assert fd.read() == text, \
            "Nested local file has no the same contents with SCPHandler.get_dir(), target existing"

    rmtree(test_local_temp_directory)

    if os.geteuid() != 0:
        # raise error if target is not writable
        os.mkdir(test_local_temp_directory, mode=0o440)
        with pytest.raises(PermissionError):
            scp_handler.get_dir(test_remote_temp_directory, test_local_temp_directory)

        rmtree(test_local_temp_directory)

    # TEST closing session after getting directory
    scp_handler.get_dir(test_remote_temp_directory, test_local_temp_directory, one_shot=True)
    with pytest.raises(OSError):
        scp_handler.get_dir(test_remote_temp_directory, test_local_temp_directory)


def test_scp_handler_put_dir_and_rmtree(scp_handler):

    """
    Test copying remotely a local directory.
    This test can be performed only if the connection is on localhost.
        - put a local directory (with target directory previously existing or not)
        - close session after copy

    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on SCPHandler\033[0m")
        return

    text = "tralala"

    # create nested "remote" directory and create a file inside
    Path(test_local_nested_temp_directory).mkdir(parents=True, exist_ok=True)
    with open(os.path.join(test_local_nested_temp_directory, os.path.basename(test_local_temp_file)), 'w') as fd:
        fd.write(text)

    # Put local directory contents to remote with target directory not existing
    scp_handler.put_dir(test_local_temp_directory, test_remote_temp_directory)
    assert os.path.isdir(test_remote_temp_directory), \
        "Remote main directory has not been created with SCPHandler.put_dir(), no target"
    nested_created = test_local_nested_temp_directory.replace(test_local_temp_directory, test_remote_temp_directory)
    assert os.path.isdir(nested_created), \
        "Nested remote directory has not been created with SCPHandler.put_dir(), no target"
    assert os.path.isfile(os.path.join(nested_created, os.path.basename(test_local_temp_file))), \
        "Nested remote file has not been created with SCPHandler.put_dir(), no target"
    with open(os.path.join(nested_created, os.path.basename(test_local_temp_file)), 'r') as fd:
        assert fd.read() == text, "Nested remote file has no the same contents with SCPHandler.put_dir(), no target"

    rmtree(test_remote_temp_directory)
    assert not os.path.isdir(test_remote_temp_directory), \
        "Remote main directory (with content) has not been remove by rmtree function, "

    # Put local directory contents with target directory existing
    os.mkdir(test_remote_temp_directory)
    scp_handler.put_dir(test_local_temp_directory, test_remote_temp_directory)
    assert os.path.isdir(test_remote_temp_directory), \
        "Remote main directory has not been created with SCPHandler.put_dir(), target existing"
    nested_created = test_local_nested_temp_directory.replace(test_local_temp_directory, test_remote_temp_directory)
    assert os.path.isdir(nested_created), \
        "Nested remote directory has not been created with SCPHandler.put_dir(), target existing"
    assert os.path.isfile(os.path.join(nested_created, os.path.basename(test_local_temp_file))), \
        "Nested remote file has not been created with SCPHandler.get_dir(), target existing"
    with open(os.path.join(nested_created, os.path.basename(test_local_temp_file)), 'r') as fd:
        assert fd.read() == text, \
            "Nested remote file has no the same contents with SCPHandler.put_dir(), target existing"

    rmtree(test_remote_temp_directory)

    if os.geteuid() != 0:
        # TEST raise error if target is not writable
        scp_handler.mkdir(test_remote_temp_directory, mode=0o440)
        with pytest.raises(PermissionError):
            scp_handler.put_dir(test_local_temp_directory, test_remote_temp_directory)

        rmtree(test_remote_temp_directory)

    # TEST closing session after putting directory
    scp_handler.put_dir(test_local_temp_directory, test_remote_temp_directory, one_shot=True)
    with pytest.raises(OSError):
        scp_handler.put_dir(test_local_temp_directory, test_remote_temp_directory)


def test_scp_rmtree(scp_handler):

    """
        Test removing remote directory using rmtree method :
            - remove empty remote directory
            - remove remote directory with files
    """

    if not server == 'localhost':
        warnings.warn("\n\033[32mYou should use localhost server to perform tests on SCPHandler\033[0m")
        return

    text = "tralala"

    # create empty directory
    os.mkdir(test_remote_temp_directory)

    scp_handler.rmtree(test_remote_temp_directory)
    assert not os.path.isdir(test_remote_temp_directory), \
        "Remote main directory (empty) has not been remove by rmtree method"

    # create nested "remote" directory and create a file inside
    Path(test_remote_nested_temp_directory).mkdir(parents=True, exist_ok=True)
    with open(os.path.join(test_remote_nested_temp_directory, os.path.basename(test_remote_temp_file)), 'w') as fd:
        fd.write(text)

    scp_handler.rmtree(test_remote_temp_directory)
    assert not os.path.isdir(test_remote_temp_directory), \
        "Remote main directory (with content) has not been remove by rmtree method"
