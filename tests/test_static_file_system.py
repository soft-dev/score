# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import tempfile
from shutil import rmtree

from score.statics.file_system import ScoreFileSystem

test_temp_directory = os.path.join(tempfile.gettempdir(), 'SCORE_TEST_TO_BE_REMOVED')


def setup_module():
    if os.path.isdir(test_temp_directory):
        rmtree(test_temp_directory)
    os.mkdir(test_temp_directory)


def teardown_module():
    if os.path.isdir(test_temp_directory):
        rmtree(test_temp_directory)


def test_score_file_system():
    """ Test capturing context manager as print in file instead of in terminal """

    fs = ScoreFileSystem(root=test_temp_directory)

    fs.create_tree()

    assert os.path.isdir(fs.main), "Main directory has not been created with create_tree function"
    assert os.path.isdir(fs.inputs), "Inputs directory has not been created with create_tree function"
    assert os.path.isdir(fs.outputs), "Outputs directory has not been created with create_tree function"

    for k, v in fs.__dict__.items():
        if '_file' in k:
            assert test_temp_directory in v, f"The path of file '{k}' should be in main folder"
