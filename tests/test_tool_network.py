# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import pytest
import re
import socket

from score.tools.network import get_ip, get_free_port


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

pattern_ip = r'^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}' \
             r'(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'


def teardown_module():
    sock.close()


def test_get_ip():
    """ Test if get_ip() return an IP address pattern """

    m = re.match(pattern_ip, get_ip())
    assert type(m) == re.Match, "score.tools.network.get_ip() didn't return a valid IP address"


def test_get_free_port():
    """ Test if get_free_port() return free and not protected port """

    port = get_free_port()
    assert isinstance(port, int), "Function should return an integer"
    try:
        sock.bind(('localhost', port))
    except PermissionError:
        pytest.fail("Returned port is protected and cannot been used by normal user")
    except OSError:
        pytest.fail("Returned port is not free")

