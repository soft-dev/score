# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import pytest
import time
from score.tools.RegexCallbacks import RegexCallbacks


class UselessHandler:
    is_handler = True


def callback_with_handler_arg(m, h):
    """ Function called by RegexCallback """

    handler_exists = False
    if h is not None:
        handler_exists = h.is_handler
    print(f"Callback with handler arg called, handler : {handler_exists}", end='')


def callback_without_handler_arg(m):
    """ Function called by RegexCallback """
    print("Callback without handler arg called", end='')


rc = RegexCallbacks()
useless = UselessHandler()
rc_with_handler = RegexCallbacks(useless)
pattern1 = r"^([0-9]*)abc$"
pattern2 = r"^(\w*)$"
text1 = "123abc"
text2 = "123"


def teardown_module():
    rc.stop()
    rc_with_handler.stop()


@pytest.mark.parametrize("tuple_error", ["test", (2, 2), ("test", 2), ["test", callback_with_handler_arg]])
def test_regex_callback_param(tuple_error):
    """ Test bad arguments to set_regex_callback method """

    with pytest.raises(Exception):
        rc.set_regex_callbacks(tuple_error)


def test_regex_callback_feature_1_tuple(capsys):
    """ Test RegexCallback with one tuple of (pattern, function) """

    # Empties regex_callbacks list
    rc.regex_callbacks = list()

    rc.set_regex_callbacks((pattern1, callback_with_handler_arg))
    rc.data_queue.put(text1)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback with handler arg called, handler : False", "Callback should have been called"


def test_regex_callback_feature_2_tuples(capsys):
    """ Test RegexCallback with one tuple of (pattern, function) :
        - the first function is called when a string corresponding to pattern1 is sent
        - the second function is called when a string corresponding to pattern2 is sent
    """

    # Empties regex_callbacks list
    rc.regex_callbacks = list()

    rc.set_regex_callbacks((pattern1, callback_with_handler_arg), (pattern2, callback_without_handler_arg))
    rc.data_queue.put(text1)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback with handler arg called, handler : False", \
        "First callback function should have been called with string \"{}\"".format(text1)

    rc.data_queue.put(text2)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback without handler arg called", \
        "Second callback function should have been called with string \"{}\"".format(text2)


def test_regex_callback_feature_2_tuples_with_handler(capsys):
    """ Test RegexCallback with one tuple of (pattern, function) :
        - the first function is called when a string corresponding to pattern1 is sent
        - the second function is called when a string corresponding to pattern2 is sent
    """

    # Empties regex_callbacks list
    rc_with_handler.regex_callbacks = list()

    rc_with_handler.set_regex_callbacks((pattern1, callback_with_handler_arg), (pattern2, callback_without_handler_arg))
    rc_with_handler.data_queue.put(text1)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback with handler arg called, handler : True", \
        "First callback function should have been called with string \"{}\"".format(text1)

    rc_with_handler.data_queue.put(text2)
    time.sleep(2)
    out, err = capsys.readouterr()
    assert out == "Callback without handler arg called", \
        "Second callback function should have been called with string \"{}\"".format(text2)
