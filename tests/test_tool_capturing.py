# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os

from score.tools import Capturing

file_path = "test_capturing.txt"
write_text = "Let's try it with special châractèrs"


def setup_module():
    if os.path.isfile(file_path):
        os.remove(file_path)


def teardown_module():
    if os.path.isfile(file_path):
        os.remove(file_path)


class WorkerCommunication:
    """ A worker communication class implementing send_string function """
    @staticmethod
    def send_string(msg):
        with open(file_path, "a+") as fd:
            fd.write(msg)


def test_capturing():
    """ Test capturing context manager as print in file instead of in terminal """

    wc = WorkerCommunication
    with Capturing(wc):
        print(write_text, end='')
    assert os.path.isfile(file_path), "Output file has not been created, meaning capturing didn't worked well"
    with open(file_path, 'r') as fd:
        assert fd.read() == write_text, "Output file contents don't match written text"
