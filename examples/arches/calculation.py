# -*- coding: UTF-8 -*-

import numpy

from pylmgc90 import chipy


def calculation(load_parameters):

    Q = load_parameters["Q"]

    print("##0##Chipy initialisation")
    # Initializing
    chipy.Initialize()

    # checking/creating mandatory subfolders
    chipy.checkDirectories()

    # logMes
    chipy.utilities_DisableLogMes()

    #
    # defining some variables
    #

    # space dimension
    dim = 2

    # modeling hypothesis ( 1 = plain strain, 2 = plain stress, 3 = axi-symmetry)
    mhyp = 1

    # time evolution parameters
    dt = 1e-3
    nb_steps = 1000

    # theta integrator parameter
    theta = 0.5

    # deformable  yes=1, no=0
    deformable = 0

    # interaction parameters
    Rloc_tol = 5.e-2

    # nlgs parameters
    tol = 1e-4
    relax = 1.0
    norm = 'Quad '
    gs_it1 = 50
    gs_it2 = 10
    solver_type='Stored_Delassus_Loops         '

    # write parameter
    freq_write   = 10

    # display parameters
    freq_display = 10
    # freq_display = 1
    ref_radius = 5.e-2

    #
    # read and load
    #

    print("##0##Chipy : set space dimension")
    # Set space dimension
    chipy.SetDimension(dim,mhyp)
    #
    chipy.utilities_logMes('INIT TIME STEPPING')
    chipy.TimeEvolution_SetTimeStep(dt)
    chipy.Integrator_InitTheta(theta)
    #
    chipy.utilities_logMes('READ BEHAVIOURS')
    chipy.ReadBehaviours()
    if deformable:
        chipy.ReadModels()
    #
    chipy.utilities_logMes('READ BODIES')
    chipy.ReadBodies()
    #
    chipy.utilities_logMes('LOAD BEHAVIOURS')
    chipy.LoadBehaviours()
    if deformable:
        chipy.LoadModels()
    #
    chipy.utilities_logMes('READ INI DOF')
    chipy.ReadIniDof()
    #
    if deformable:
        chipy.utilities_logMes('READ INI GPV')
        chipy.ReadIniGPV()
    #
    chipy.utilities_logMes('READ DRIVEN DOF')
    chipy.ReadDrivenDof()
    #
    chipy.utilities_logMes('LOAD TACTORS')
    chipy.LoadTactors()
    #
    chipy.utilities_logMes('READ INI Vloc Rloc')
    chipy.ReadIniVlocRloc()

    #
    # paranoid writes
    #
    print("##0##Chipy : paranoid write")
    chipy.utilities_logMes('WRITE BODIES')
    chipy.WriteBodies()
    chipy.utilities_logMes('WRITE BEHAVIOURS')
    chipy.WriteBehaviours()
    chipy.utilities_logMes('WRITE DRIVEN DOF')
    chipy.WriteDrivenDof()

    #
    # open display & postpro
    #

    print("##0##Chipy : open display & postpro")

    chipy.utilities_logMes('DISPLAY & WRITE')
    chipy.OpenDisplayFiles()
    chipy.OpenPostproFiles()

    #
    # simulation part ...
    #

    print("##0##Chipy : compute mass")
    # ... calls a simulation time loop
    # since constant compute elementary mass once
    chipy.utilities_logMes('COMPUTE MASS')
    chipy.ComputeMass()

    for k in range(0,nb_steps):
        #
        print("##{}##Chipy : compute step {}".format(int(k*100/1000), k))
        chipy.utilities_logMes('INCREMENT STEP')
        chipy.IncrementStep()

        chipy.utilities_logMes('COMPUTE Fext')
        chipy.ComputeFext()

        #----------------------------------------------------------------------
        # Contribution des charges ponctuelles
        map_=chipy.POLYG_GetPOLYG2RBDY2()
        nb_RBDY2=chipy.RBDY2_GetNbRBDY2()
        for ibloc in range(nb_RBDY2-1):
            coorG=chipy.RBDY2_GetBodyVector('Coor_',ibloc+1)
            polyg=map_[ibloc]; polyg=int(polyg)
            vertices=chipy.POLYG_GetVertices(polyg)
            Fext=chipy.RBDY2_GetBodyVector('Fext_', ibloc+1)

            if polyg % 2 == 0: #intrados Q3, Q4
                x0=min(vertices[2][0],vertices[3][0])
                x1=max(vertices[2][0],vertices[3][0])
                y0=min(vertices[2][1],vertices[3][1])
                y1=max(vertices[2][1],vertices[3][1])
            else: #intrados Q2, Q3
                x0=min(vertices[1][0],vertices[2][0])
                x1=max(vertices[1][0],vertices[2][0])
                y0=min(vertices[1][1],vertices[2][1])
                y1=max(vertices[1][1],vertices[2][1])

            xi=(x0+x1)*0.5; Li=abs(x1-x0); yi=(y0+y1)*0.5

          # Efforts
            msg = ''
            for q in Q: # contribution des efforts ponctuels
                e=q[1]
                if x0 < e <= x1:
                    msg+='\nWARNING :: bloc '+str(ibloc)+' : '+str(Fext)+' -> '
                    Fext[1] -=q[0]
            # Couples
            xG=coorG[0]
            for q in Q:
                e=q[1]
                if x0 < e <= x1:
                    Vi =q[0]; Fext[2] -= Vi*(xG-e)
                    #print 'xG = ',xG,' / e = ',e
                    # msg+=str(Fext)+'\n'; print(msg)

            chipy.RBDY2_PutBodyVector('Fext_', ibloc+1, Fext)
      #----------------------------------------------------------------------

        chipy.utilities_logMes('COMPUTE Fint')
        chipy.ComputeBulk()
        chipy.utilities_logMes('COMPUTE Free Vlocy')
        chipy.ComputeFreeVelocity()

        chipy.utilities_logMes('SELECT PROX TACTORS')
        chipy.SelectProxTactors()

        chipy.utilities_logMes('RESOLUTION' )
        chipy.RecupRloc(Rloc_tol)

        chipy.ExSolver(solver_type, norm, tol, relax, gs_it1, gs_it2)
        chipy.UpdateTactBehav()

        chipy.StockRloc()

        chipy.utilities_logMes('COMPUTE DOF, FIELDS, etc.')
        chipy.ComputeDof()

        chipy.utilities_logMes('UPDATE DOF, FIELDS')
        chipy.UpdateStep()

        chipy.utilities_logMes('WRITE OUT DOF')
        chipy.WriteOutDof(freq_write)
        chipy.utilities_logMes('WRITE OUT Rloc')
        chipy.WriteOutVlocRloc(freq_write)

        chipy.utilities_logMes('VISU & POSTPRO')
        chipy.WriteDisplayFiles(freq_display,ref_radius)
        chipy.WritePostproFiles()

        # time.sleep(0.01)

    #
    # close display & postpro
    #
    chipy.CloseDisplayFiles()
    chipy.ClosePostproFiles()

    print("##100##Construct results array")
    # rebuild last configuration
    nb_POLYG = chipy.POLYG_GetNbPOLYG()
    points_X={}
    points_Y={}
    for bloc in range(1, nb_POLYG+1, 1):
        vertices = chipy.POLYG_GetVertices(bloc)
        pts_X=list(vertices[:,0])+[vertices[0,0]]
        points_X[bloc]=numpy.array(pts_X)
        pts_Y=list(vertices[:,1])+[vertices[0,1]]
        points_Y[bloc]=numpy.array(pts_Y)

    # this is the end
    chipy.Finalize()

    # with open("OUTBOX/BODIES.OUT") as fp:
    #     bodies = fp.readlines()
    # return [points_X, points_Y, bodies]
    return [points_X, points_Y]
