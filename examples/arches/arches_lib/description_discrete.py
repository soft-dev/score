import sys
import numpy
import math

from arches_lib import utils


def ogive(fleche, ep_bandeau, bandeau_type, ancrage, lc_bloc, generated_params):
    b2 = ep_bandeau
    L_intrados = generated_params["L_intrados"]
    L_extrados = generated_params["L_extrados"]
    R1 = generated_params["R1"]
    R2 = generated_params["R2"]
    theta1 = generated_params["theta1"]
    theta2 = generated_params["theta2"]
    O1 = generated_params["O1"]
    xO1 = generated_params["xO1"]

    VERTICES = {}
    e_joint = 0.005
    points_X = {}
    points_Y = {}

    L_fibre_moy = (L_intrados + L_extrados) / 2.
    R_fibre_moy = (R1 + R2) / 2.
    theta_moy = (theta1 + theta2) / 2.
    N = int(L_fibre_moy / lc_bloc)
    nb_blocs0 = N
    if N != 0:
        if N % 2 != 0:
            dlc1 = abs(lc_bloc - L_fibre_moy / (N - 1))
            nb1 = N - 1
            dlc2 = abs(lc_bloc - L_fibre_moy / (N + 1))
            nb2 = N + 1
            dlc = min(dlc1, dlc2)
            if dlc == dlc1:
                nb_blocs0 = nb1
            else:
                nb_blocs0 = nb2
        else:
            nb_blocs0 = N

    # nb bloc minimal
    nb_blocs0 = max(nb_blocs0, 2)

    lc_voussoir = L_fibre_moy / nb_blocs0
    print('lc voussoir retenue : ', lc_voussoir)
    print('nombre de voussoirs : ', nb_blocs0)

    # ouverture angulaire
    alpha_joint = theta_moy / ((nb_blocs0 / 2 - 1) * 10.)
    alpha_joint = 0.

    # ouverture d'un bloc, epaisseur d'un bloc et epaisseur du join
    alpha_bloc = (theta_moy - (nb_blocs0 / 2 - 1) * alpha_joint) / (nb_blocs0 / 2)
    e_bloc = b2
    # self.e_joint = 0.005

    N = nb_blocs0
    while (N / 2 - 1) * alpha_bloc > theta1:
        print('---------')
        print('WARNING : need to increase lc bloc')
        print('     N = ', N, ' : lc voussoir = ', lc_voussoir)
        N -= 2
        alpha_joint = theta_moy / ((N / 2 - 1) * 10.)
        alpha_joint = 0.
        alpha_bloc = (theta_moy - (N / 2 - 1) * alpha_joint) / (N / 2)
        lc_voussoir = L_fibre_moy / N
        print('  -> N = ', N, ' : lc voussoir = ', lc_voussoir)

    nb_blocs0 = N

    e_bloc = lc_voussoir

    # check
    if (nb_blocs0 / 2 - 1) * alpha_bloc > theta1:
        print('WARNING : need to increase lc bloc')
        sys.exit()

    if bandeau_type == 'H':
        a1 = ancrage + ep_bandeau
        print('ancrage : ', ancrage)
        b2 = a1

    alpha = 0.
    iter_bloc = 0
    # points_X = {}
    # points_Y = {}
    # on positionne les voussoirs
    # for i in range(0, nb_blocs0/2): #nb_blocs0 % 2 = 0
    for i in range(0, int(nb_blocs0 / 2) - 1):  # nb_blocs0 % 2 = 0
        # print '**** bloc  ',i
        # print 'alpha : ',alpha*180./math.pi

        ALPHA = [alpha, alpha + alpha_bloc]
        # blocs REF : cos > 0 / sin > 0
        O1Q1 = utils.droite_1Pt_alpha(O1, ALPHA[0])
        uO1Q1 = O1Q1[0]
        vO1Q1 = O1Q1[1]
        O1Q4 = utils.droite_1Pt_alpha(O1, ALPHA[1])
        uO1Q4 = O1Q4[0]
        vO1Q4 = O1Q4[1]
        Q1 = utils.intersection_Cercle_Droite_ux_v(uO1Q1, vO1Q1, O1, R1)  # print ' Q1 : ',Q1
        Q4 = utils.intersection_Cercle_Droite_ux_v(uO1Q4, vO1Q4, O1, R1)  # print ' Q4 : ',Q4
        if i % 2 == 0:  # blocs courts
            O1Q2 = utils.droite_1Pt_alpha(O1, ALPHA[0])
            uO1Q2 = O1Q2[0]
            vO1Q2 = O1Q2[1]
            O1Q3 = utils.droite_1Pt_alpha(O1, ALPHA[1])
            uO1Q3 = O1Q3[0]
            vO1Q3 = O1Q3[1]
            Q2 = utils.intersection_Cercle_Droite_ux_v(uO1Q2, vO1Q2, O1, R1 + ep_bandeau)  # print ' Q2 : ',Q2
            Q3 = utils.intersection_Cercle_Droite_ux_v(uO1Q3, vO1Q3, O1, R1 + ep_bandeau)  # print ' Q3 : ',Q3
        else:  # blocs longs
            O1Q2 = utils.droite_1Pt_alpha(O1, ALPHA[0])
            uO1Q2 = O1Q2[0]
            vO1Q2 = O1Q2[1]
            O1Q3 = utils.droite_1Pt_alpha(O1, ALPHA[1])
            uO1Q3 = O1Q3[0]
            vO1Q3 = O1Q3[1]
            Q2 = utils.intersection_Cercle_Droite_ux_v(uO1Q2, vO1Q2, O1, R1 + b2) # print ' Q2 : ',Q2
            Q3 = utils.intersection_Cercle_Droite_ux_v(uO1Q3, vO1Q3, O1, R1 + b2)  # print ' Q3 : ',Q3

        # ----------------
        if bandeau_type in ['BS', 'H']:
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = Q1[0]
            vertices[0, 1] = Q1[1]  # Q1
            vertices[1, 0] = Q2[0]
            vertices[1, 1] = Q2[1]  # Q2
            vertices[2, 0] = Q3[0]
            vertices[2, 1] = Q3[1]  # Q3
            vertices[3, 0] = Q4[0]
            vertices[3, 1] = Q4[1]  # Q4
        elif bandeau_type == 'TC':
            Q5 = (Q2[0], Q3[1])
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = Q1[0]
            vertices[0, 1] = Q1[1]  # Q1
            vertices[1, 0] = Q2[0]
            vertices[1, 1] = Q2[1]  # Q2
            vertices[2, 0] = Q5[0]
            vertices[2, 1] = Q5[1]  # Q5
            vertices[3, 0] = Q3[0]
            vertices[3, 1] = Q3[1]  # Q3
            vertices[4, 0] = Q4[0]
            vertices[4, 1] = Q4[1]  # Q4

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        # blocs symetriques
        if bandeau_type in ['BS', 'H']:
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = -Q1[0]
            vertices[0, 1] = Q1[1]
            vertices[1, 0] = -Q4[0]
            vertices[1, 1] = Q4[1]
            vertices[2, 0] = -Q3[0]
            vertices[2, 1] = Q3[1]
            vertices[3, 0] = -Q2[0]
            vertices[3, 1] = Q2[1]
        elif bandeau_type == 'TC':
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = -Q1[0]
            vertices[0, 1] = Q1[1]
            vertices[1, 0] = -Q4[0]
            vertices[1, 1] = Q4[1]
            vertices[2, 0] = -Q3[0]
            vertices[2, 1] = Q3[1]
            vertices[3, 0] = -Q5[0]
            vertices[3, 1] = Q5[1]
            vertices[4, 0] = -Q2[0]
            vertices[4, 1] = Q2[1]

            # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        alpha += alpha_bloc + alpha_joint
        # print 'alpha+ : ',alpha*180./math.pi
        # print ''

    # cles
    # blocs REF : cos > 0 / sin > 0
    O1Q1 = utils.droite_1Pt_alpha(O1, alpha)
    uO1Q1 = O1Q1[0]
    vO1Q1 = O1Q1[1]
    O1Q2 = utils.droite_1Pt_alpha(O1, alpha)
    uO1Q2 = O1Q2[0]
    vO1Q2 = O1Q2[1]
    Q1 = utils.intersection_Cercle_Droite_ux_v(uO1Q1, vO1Q1, O1, R1)  # print ' Q1 : ',Q1
    Q2 = utils.intersection_Cercle_Droite_ux_v(uO1Q2, vO1Q2, O1, R1 + b2)  # print ' Q2 : ',Q2
    Q3 = (0., math.sqrt((R1 + b2) ** 2 - xO1 ** 2))
    Q4 = (0., fleche)

    if bandeau_type in ['BS', 'H']:
        vertices = numpy.zeros([4, 2], 'd')
        vertices[0, 0] = Q1[0]
        vertices[0, 1] = Q1[1]  # Q1
        vertices[1, 0] = Q2[0]
        vertices[1, 1] = Q2[1]  # Q2
        vertices[2, 0] = Q3[0]
        vertices[2, 1] = Q3[1]  # Q3
        vertices[3, 0] = Q4[0]
        vertices[3, 1] = Q4[1]  # Q4
    elif bandeau_type == 'TC':
        Q5 = (Q2[0], Q3[1])
        vertices = numpy.zeros([5, 2], 'd')
        vertices[0, 0] = Q1[0]
        vertices[0, 1] = Q1[1]  # Q1
        vertices[1, 0] = Q2[0]
        vertices[1, 1] = Q2[1]  # Q2
        vertices[2, 0] = Q5[0]
        vertices[2, 1] = Q5[1]  # Q5
        vertices[3, 0] = Q3[0]
        vertices[3, 1] = Q3[1]  # Q3
        vertices[4, 0] = Q4[0]
        vertices[4, 1] = Q4[1]  # Q4

    # matplotlib
    iter_bloc += 1
    pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
    points_X[iter_bloc] = numpy.array(pts_X)
    pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
    points_Y[iter_bloc] = numpy.array(pts_Y)
    VERTICES[iter_bloc] = vertices

    # blocs symetriques
    if bandeau_type in ['BS', 'H']:
        vertices = numpy.zeros([4, 2], 'd')
        vertices[0, 0] = Q4[0]
        vertices[0, 1] = Q4[1]  # Q4
        vertices[1, 0] = Q3[0]
        vertices[1, 1] = Q3[1]  # Q3
        vertices[2, 0] = -Q2[0]
        vertices[2, 1] = Q2[1]  # Q3
        vertices[3, 0] = -Q1[0]
        vertices[3, 1] = Q1[1]  # Q1
    elif bandeau_type == 'TC':
        vertices = numpy.zeros([5, 2], 'd')
        vertices[0, 0] = Q4[0]
        vertices[0, 1] = Q4[1]  # Q4
        vertices[1, 0] = Q3[0]
        vertices[1, 1] = Q3[1]  # Q3
        vertices[2, 0] = -Q5[0]
        vertices[2, 1] = Q5[1]  # Q5
        vertices[3, 0] = -Q2[0]
        vertices[3, 1] = Q2[1]  # Q3
        vertices[4, 0] = -Q1[0]
        vertices[4, 1] = Q1[1]  # Q1

    # matplotlib
    iter_bloc += 1
    pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
    points_X[iter_bloc] = numpy.array(pts_X)
    pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
    points_Y[iter_bloc] = numpy.array(pts_Y)
    VERTICES[iter_bloc] = vertices

    print('\n--------------------------------------')
    print('check fleche : ')
    print('  construction : fleche = ', Q1[1])
    print('  donnee       : fleche = ', fleche)
    print('  erreur = ', abs(Q1[1] - fleche) * 100. / fleche, ' % ')
    print('--------------------------------------\n')

    description_params = {"points_X": points_X,
                          "points_Y": points_Y,
                          "VERTICES": VERTICES,
                          "e_bloc": e_bloc,
                          "e_joint": e_joint}
    return description_params


def parabole(portee, fleche, ep_bandeau, bandeau_type, ancrage, lc_bloc, generated_params):
    VERTICES={}
    points_X={}
    points_Y={}

    L_intrados = generated_params["L_intrados"]
    a = generated_params["a"]
    b = generated_params["b"]
    c = generated_params["c"]
    A = generated_params["A"]
    xA = generated_params["xA"]
    yA = generated_params["yA"]
    b2 = ep_bandeau

    N = int(L_intrados / lc_bloc)
    nb_blocs0 = N
    if N != 0:
        dlc1 = abs(lc_bloc - L_intrados / N)
        nb1 = N - 1
        dlc2 = abs(lc_bloc - L_intrados / (N + 1))
        nb2 = N + 1
        dlc = min(dlc1, dlc2)
        if dlc == dlc1:
            nb_blocs0 = nb1
        else:
            nb_blocs0 = nb2

    # nb bloc minimal
    nb_blocs0 = max(nb_blocs0, 3)

    lc_bloc = L_intrados / nb_blocs0
    print('lc voussoir retenue : ', lc_bloc)
    print('nombre de voussoirs : ', nb_blocs0)
    print('')
    e_bloc = lc_bloc

    if bandeau_type == 'H':
        a1 = ancrage + ep_bandeau
        print('ancrage : ', ancrage)
        b2 = a1

    # ouverture angulaire
    # alpha_joint = math.pi/((nb_blocs0/2 - 1)*10.)
    # alpha_joint=0.

    # ouverture d'un bloc, epaisseur d'un bloc et epaisseur du join
    # alpha_bloc = (math.pi - (nb_blocs0/2-1)*alpha_joint)/nb_blocs0
    e_bloc = b2
    e_joint = 0.005

    # print 'alpha_bloc = ',alpha_bloc*180/math.pi

    Lc_target = []
    for i in range(1, int(nb_blocs0 / 2) + 1):
        Lc_target.append(lc_bloc * i)

    n_discr = 2500
    x0 = portee / 2
    dx = portee / n_discr
    i_target = 0
    lc_target = Lc_target[i_target]
    X_voussoirs = [[portee / 2]]
    ARC0 = 0.
    ARC1 = 0.
    for i in range(0, n_discr):
        x1 = x0 - dx
        y0 = utils.P(x0, a, b, c)
        y1 = utils.P(x1, a, b, c)
        arc_01 = math.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2)
        ARC1 += arc_01

        if ARC0 < lc_target < ARC1:
            dx0 = abs(lc_target - ARC0)
            dx1 = abs(lc_target - ARC1)
            MIN = min(dx0, dx1)
            if MIN == dx0:
                xx = x0
            else:
                xx = x1
            X_voussoirs[-1].append(xx)

            if len(X_voussoirs) < len(Lc_target):
                X_voussoirs.append([xx])
                i_target += 1
                lc_target = Lc_target[i_target]

        x0 = x1
        ARC0 = ARC1
    ############################################################################

    iter_bloc = 0
    # on construit les voussoirs
    if nb_blocs0 % 2 == 0:
        nb_end = int(nb_blocs0 / 2) - 1
    else:
        nb_end = int(nb_blocs0 / 2)

    for k in range(0, nb_end):
        print('**** bloc  ', k, '  ***********************')

        # blocs REF : cos > 0 / sin > 0
        xQ1 = X_voussoirs[k][0]
        yQ1 = utils.P(xQ1, a, b, c)
        Q1 = (xQ1, yQ1)
        print(' Q1 : ', Q1)
        xQ4 = X_voussoirs[k][1]
        yQ4 = utils.P(xQ4, a, b, c)
        Q4 = (xQ4, yQ4)
        print(' Q4 : ', Q4)

        TAN_Q1 = utils.tangente_parabole(Q1, -4 * fleche / portee ** 2, 0., fleche)
        theta1 = math.atan(-1 / TAN_Q1[0])
        TAN_Q4 = utils.tangente_parabole(Q4, -4 * fleche / portee ** 2, 0., fleche)
        theta4 = math.atan(-1 / TAN_Q4[0])

        # euristique pour la gestion du harpage
        if nb_blocs0 % 2 == 1:
            if nb_blocs0 / 2 % 2 == 0:
                b2k = b2
                b2kp1 = ep_bandeau
            else:
                b2k = ep_bandeau
                b2kp1 = b2
        else:
            if nb_blocs0 / 2 % 2 == 0:
                b2k = ep_bandeau
                b2kp1 = b2
            else:
                b2k = b2
                b2kp1 = ep_bandeau

        # euristique pour la definition des points Q2 et Q3 a partir de Q1 et Q4
        if k % 2 == 0:  # blocs longueur b2k
            dx1 = b2k * math.cos(theta1)
            dy1 = b2k * math.sin(theta1)
            dx4 = b2k * math.cos(theta4)
            dy4 = b2k * math.sin(theta4)
        else:  # blocs longueur b2k+1
            dx1 = b2kp1 * math.cos(theta1)
            dy1 = b2kp1 * math.sin(theta1)
            dx4 = b2kp1 * math.cos(theta4)
            dy4 = b2kp1 * math.sin(theta4)

        xQ2 = Q1[0] + dx1
        yQ2 = Q1[1] + dy1
        Q2 = (xQ2, yQ2)  # print ' Q2 : ',Q2
        xQ3 = Q4[0] + dx4
        yQ3 = Q4[1] + dy4
        Q3 = (xQ3, yQ3)  # print ' Q3 : ',Q3

        # euristique pour la construction des cles
        if k == nb_end - 1:
            if k % 2 != 0:  # blocs longueur b2k
                Dx4 = b2k * math.cos(theta4)
                Dy4 = b2k * math.sin(theta4)
            else:  # blocs longueur b2k+1
                Dx4 = b2kp1 * math.cos(theta4)
                Dy4 = b2kp1 * math.sin(theta4)

        # ----------------
        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = Q1[0]
            vertices[0, 1] = Q1[1]  # Q1
            vertices[1, 0] = Q2[0]
            vertices[1, 1] = Q2[1]  # Q2
            vertices[2, 0] = Q3[0]
            vertices[2, 1] = Q3[1]  # Q3
            vertices[3, 0] = Q4[0]
            vertices[3, 1] = Q4[1]  # Q4
        elif bandeau_type == 'TC':
            Q5 = (Q2[0], Q3[1])
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = Q1[0]
            vertices[0, 1] = Q1[1]  # Q1
            vertices[1, 0] = Q2[0]
            vertices[1, 1] = Q2[1]  # Q2
            vertices[2, 0] = Q5[0]
            vertices[2, 1] = Q5[1]  # Q5
            vertices[3, 0] = Q3[0]
            vertices[3, 1] = Q3[1]  # Q3
            vertices[4, 0] = Q4[0]
            vertices[4, 1] = Q4[1]  # Q4

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        # blocs symetriques
        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = -Q1[0]
            vertices[0, 1] = Q1[1]
            vertices[1, 0] = -Q4[0]
            vertices[1, 1] = Q4[1]
            vertices[2, 0] = -Q3[0]
            vertices[2, 1] = Q3[1]
            vertices[3, 0] = -Q2[0]
            vertices[3, 1] = Q2[1]
        elif bandeau_type == 'TC':
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = -Q1[0]
            vertices[0, 1] = Q1[1]
            vertices[1, 0] = -Q4[0]
            vertices[1, 1] = Q4[1]
            vertices[2, 0] = -Q3[0]
            vertices[2, 1] = Q3[1]
            vertices[3, 0] = -Q5[0]
            vertices[3, 1] = Q5[1]
            vertices[4, 0] = -Q2[0]
            vertices[4, 1] = Q2[1]

            # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        # sommiers
        if k == 0:
            # sommier 1
            B2 = b2 / math.cos(theta1)
            q1 = A
            q2 = (xA + B2, 0.)
            q3 = (xA + b2 * math.cos(theta1), yA + b2 * math.sin(theta1))
            S1vertices = numpy.zeros([3, 2], 'd')
            S1vertices[0, 0] = q1[0]
            S1vertices[0, 1] = q1[1]
            S1vertices[1, 0] = q2[0]
            S1vertices[1, 1] = q2[1]
            S1vertices[2, 0] = q3[0]
            S1vertices[2, 1] = q3[1]

            # matplotlib
            iter_bloc += 1
            pts_X = list(S1vertices[:, 0]) + [S1vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(S1vertices[:, 1]) + [S1vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            # sommier 2
            S2vertices = numpy.zeros([3, 2], 'd')
            S2vertices[0, 0] = -q1[0]
            S2vertices[0, 1] = q1[1]
            S2vertices[1, 0] = -q3[0]
            S2vertices[1, 1] = q3[1]
            S2vertices[2, 0] = -q2[0]
            S2vertices[2, 1] = q2[1]

            # matplotlib
            iter_bloc += 1
            pts_X = list(S2vertices[:, 0]) + [S2vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(S2vertices[:, 1]) + [S2vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

    # cle(s)
    # blocs REF : cos > 0 / sin > 0
    q1 = Q4
    q2 = (q1[0] + Dx4, q1[1] + Dy4)  # print ' q2 : ',q2
    if nb_blocs0 % 2 == 0:
        q3 = (0., fleche + b2); q4 = (0., fleche)
    else:
        q3 = (-q2[0], q2[1]); q4 = (-q1[0], q1[1])

    if nb_blocs0 % 2 != 0:  # un seul bloc a la cle
        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([6, 2], 'd')
            vertices[0, 0] = q4[0]
            vertices[0, 1] = q4[1]  # q4
            vertices[1, 0] = q1[0]
            vertices[1, 1] = q1[1]  # q1
            vertices[2, 0] = q2[0]
            vertices[2, 1] = q2[1]  # q2
            vertices[3, 0] = q3[0]
            vertices[3, 1] = q3[1]  # q3
            vertices[4, 0] = -q2[0]
            vertices[4, 1] = q2[1]  # q2
            vertices[5, 0] = -q1[0]
            vertices[5, 1] = q1[1]  # q1
        elif bandeau_type == 'TC':
            q5 = (q2[0], q3[1])
            vertices = numpy.zeros([7, 2], 'd')
            vertices[0, 0] = q4[0]
            vertices[0, 1] = q4[1]  # q4
            vertices[1, 0] = q1[0]
            vertices[1, 1] = q1[1]  # q1
            vertices[2, 0] = q2[0]
            vertices[2, 1] = q2[1]  # q2
            vertices[3, 0] = q5[0]
            vertices[3, 1] = q5[1]  # q5
            vertices[4, 0] = -q5[0]
            vertices[4, 1] = q5[1]  # q5
            vertices[5, 0] = -q2[0]
            vertices[5, 1] = q2[1]  # q2
            vertices[6, 0] = -q1[0]
            vertices[6, 1] = q1[1]  # q1

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

    else:  # 2 blocs a la cle

        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = q1[0]
            vertices[0, 1] = q1[1]  # q1
            vertices[1, 0] = q2[0]
            vertices[1, 1] = q2[1]  # q2
            vertices[2, 0] = q3[0]
            vertices[2, 1] = q3[1]  # q3
            vertices[3, 0] = q4[0]
            vertices[3, 1] = q4[1]  # q4
        elif bandeau_type == 'TC':
            q5 = (q2[0], q3[1])
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = q1[0]
            vertices[0, 1] = q1[1]  # q1
            vertices[1, 0] = q2[0]
            vertices[1, 1] = q2[1]  # q2
            vertices[2, 0] = q5[0]
            vertices[2, 1] = q5[1]  # q5
            vertices[3, 0] = q3[0]
            vertices[3, 1] = q3[1]  # q3
            vertices[4, 0] = q4[0]
            vertices[4, 1] = q4[1]  # q4

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        # blocs symetriques
        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = q4[0]
            vertices[0, 1] = q4[1]  # q4
            vertices[1, 0] = q3[0]
            vertices[1, 1] = q3[1]  # q3
            vertices[2, 0] = -q2[0]
            vertices[2, 1] = q2[1]  # q2
            vertices[3, 0] = -q1[0]
            vertices[3, 1] = q1[1]  # q1
        elif bandeau_type == 'TC':
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = q4[0]
            vertices[0, 1] = q4[1]  # q4
            vertices[1, 0] = q3[0]
            vertices[1, 1] = q3[1]  # q3
            vertices[2, 0] = -q5[0]
            vertices[2, 1] = q5[1]  # q5
            vertices[3, 0] = -q2[0]
            vertices[3, 1] = q2[1]  # q2
            vertices[4, 0] = -q1[0]
            vertices[4, 1] = q1[1]  # q1

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

    print('\n--------------------------------------')
    print('check fleche : ')
    print('  construction : fleche = ', Q4[1])
    print('  donnee       : fleche = ', fleche)
    print('  erreur = ', abs(Q4[1] - fleche) * 100. / fleche, ' % ')
    print('--------------------------------------\n')

    description_params = {"points_X": points_X,
                          "points_Y": points_Y,
                          "VERTICES": VERTICES,
                          "e_bloc": e_bloc,
                          "e_joint": e_joint}
    return description_params


def plein_cintre(fleche, ep_bandeau, bandeau_type, ancrage, lc_bloc, generated_params):
    VERTICES={}
    points_X={}
    points_Y={}

    fleche = fleche
    ep_bandeau = ep_bandeau
    ancrage = ancrage

    # reference à double arche ??
    ep_bandeau1 = None
    ep_rein1 = None
    P2 = None

    L_intrados = generated_params["L_intrados"]
    R1 = generated_params["R1"]
    R2 = generated_params["R2"]
    R3 = generated_params["R3"]
    theta0 = generated_params["theta0"]
    P1 = generated_params["P1"]
    coor1 = generated_params["coor1"]
    coor2 = generated_params["coor2"]
    coor3 = generated_params["coor3"]

    N = int(L_intrados / lc_bloc)
    nb_blocs0 = N
    if N != 0:
        if N % 2 == 0:
            dlc1 = abs(lc_bloc - L_intrados / (N - 1))
            nb1 = N - 1
            dlc2 = abs(lc_bloc - L_intrados / (N + 1))
            nb2 = N + 1
            dlc = min(dlc1, dlc2)
            if dlc == dlc1:
                nb_blocs0 = nb1
            else:
                nb_blocs0 = nb2
        else:
            nb_blocs0 = N

    # nb bloc minimal
    nb_blocs0 = max(nb_blocs0, 3)

    lc_bloc = L_intrados / nb_blocs0
    print('lc voussoir retenue : ', lc_bloc)
    print('nombre de voussoirs : ', nb_blocs0)
    e_bloc = lc_bloc

    # print 'nb blocs : ',nb_blocs0

    double_arches = ['DB', 'H']
    if bandeau_type == 'DB':
        b2 = ep_bandeau1
        print('bandeau 2: ', ep_bandeau1)
        r2 = ep_rein1
        print('reins 2: ', ep_rein1)

    if bandeau_type == 'H':
        a1 = ancrage + ep_bandeau
        print('ancrage : ', ancrage)
        b2 = a1
        r2 = a1

    # parametres du script:
    surbaissement = True
    surb = R1 - fleche
    if abs(surb) < 1.e-03: surbaissement = False
    print('surbaissement : ', surbaissement)

    r_int = R1
    r_ext = r_int + ep_bandeau

    iter_bloc = 0

    if not surbaissement:  # arche non surbaissee
        print('ARCHE NON SURBAISSEE')
        # ouverture angulaire
        theta_joint = math.pi / ((nb_blocs0 - 1) * 10.)
        theta_joint = 0.

        # ouverture d'un bloc, epaisseur d'un bloc et epaisseur du join
        theta_bloc = (math.pi - (nb_blocs0 - 1) * theta_joint) / nb_blocs0
        e_bloc = r_ext - r_int
        e_joint = r_ext * theta_joint

        # on initialise l'angle de premier bloc a 0
        theta = 0.
    else:
        print('ARCHE SURBAISSEE')
        # nombre de blocs, ouverture angulaire et rayons int/ext
        theta_joint = math.pi / ((nb_blocs0 + 2 - 1) * 10.)
        theta_joint = 0.

        # ouverture d'un bloc, epaisseur d'un bloc et epaisseur du join
        theta_bloc = (math.pi - (nb_blocs0 + 2 - 1) * theta_joint - theta0 * 2) / nb_blocs0
        e_bloc = r_ext - r_int
        e_joint = r_ext * theta_joint

        # sommiers
        theta = theta0
        # sommier 1
        coor_Q1 = (r_int * math.cos(theta), r_int * math.sin(theta) - surb)
        if bandeau_type == 'H' or bandeau_type == 'TC' or bandeau_type == 'BS':
            coor_Q2 = (P1, coor_Q1[1])
            coor_Q3 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)
        elif bandeau_type == 'DB':
            coor_Q2 = (P2, coor_Q1[1])
            coor_Q3 = utils.intersection_Cercle(coor1, R1, theta, coor3, R3)

        vertices = numpy.zeros([3, 2], 'd')
        vertices[0, 0] = coor_Q1[0]
        vertices[0, 1] = coor_Q1[1]
        vertices[1, 0] = coor_Q2[0]
        vertices[1, 1] = coor_Q2[1]
        vertices[2, 0] = coor_Q3[0]
        vertices[2, 1] = coor_Q3[1]

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        # sommier 2
        vertices = numpy.zeros([3, 2], 'd')
        vertices[0, 0] = -coor_Q1[0]
        vertices[0, 1] = coor_Q1[1]
        vertices[1, 0] = -coor_Q3[0]
        vertices[1, 1] = coor_Q3[1]
        vertices[2, 0] = -coor_Q2[0]
        vertices[2, 1] = coor_Q2[1]

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        theta += theta_joint

    # on positionne les voussoirs
    if bandeau_type == 'BS' or bandeau_type == 'DB':
        for i in range(0, int(nb_blocs0 / 2)):  # nb_blocs % 2 = 1
            # blocs REF : cos > 0 / sin > 0
            coor_Q1 = (R1 * math.cos(theta), R1 * math.sin(theta) + coor1[1])  # ; print 'Q1 : ',coor_Q1
            coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q2 : ',coor_Q2
            coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_bloc, coor2, R2)  # ; print 'Q3 : ',coor_Q3
            coor_Q4 = (
            R1 * math.cos(theta + theta_bloc), R1 * math.sin(theta + theta_bloc) + coor1[1])  # ; print 'Q4 : ',coor_Q4

            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = coor_Q1[0]
            vertices[0, 1] = coor_Q1[1]  # Q1
            vertices[1, 0] = coor_Q2[0]
            vertices[1, 1] = coor_Q2[1]  # Q2
            vertices[2, 0] = coor_Q3[0]
            vertices[2, 1] = coor_Q3[1]  # Q3
            vertices[3, 0] = coor_Q4[0]
            vertices[3, 1] = coor_Q4[1]  # Q4

            # matplotlib
            iter_bloc += 1
            pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            # blocs symetriques
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = -coor_Q1[0]
            vertices[0, 1] = coor_Q1[1]
            vertices[1, 0] = -coor_Q4[0]
            vertices[1, 1] = coor_Q4[1]
            vertices[2, 0] = -coor_Q3[0]
            vertices[2, 1] = coor_Q3[1]
            vertices[3, 0] = -coor_Q2[0]
            vertices[3, 1] = coor_Q2[1]

            # matplotlib
            iter_bloc += 1
            pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            theta += theta_bloc + theta_joint

        # cle
        coor_Q1 = (R1 * math.cos(theta), R1 * math.sin(theta) + coor1[1])  # ; print 'Q1 : ',coor_Q1
        coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q2 : ',coor_Q2

        vertices = numpy.zeros([4, 2], 'd')
        vertices[0, 0] = coor_Q1[0]
        vertices[0, 1] = coor_Q1[1]  # Q1
        vertices[1, 0] = coor_Q2[0]
        vertices[1, 1] = coor_Q2[1]  # Q2
        vertices[2, 0] = -coor_Q2[0]
        vertices[2, 1] = coor_Q2[1]
        vertices[3, 0] = -coor_Q1[0]
        vertices[3, 1] = coor_Q1[1]

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        if bandeau_type == 'DB':

            if not surbaissement:  # arche non surbaissee
                theta = 0.
            else:
                theta = theta0 + theta_joint

            for i in range(0, int((nb_blocs0 + 1) / 2)):
                print('bloc ', i)
                # blocs REF : cos > 0 / sin > 0
                if i == 0:
                    theta_Bloc = theta_bloc * 0.5
                else:
                    theta_Bloc = theta_bloc
                coor_Q1 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q1 : ',coor_Q1
                coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor3, R3)  # ; print 'Q2 : ',coor_Q2
                coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_Bloc, coor3, R3)  # ; print 'Q3 : ',coor_Q3
                coor_Q4 = utils.intersection_Cercle(coor1, R1, theta + theta_Bloc, coor2, R2)  # ; print 'Q4 : ',coor_Q4

                if i == (nb_blocs0 + 1) / 2 - 1:
                    print('cle ', i)
                    coor_Q1 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q1 : ',coor_Q1
                    coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor3, R3)  # ; print 'Q2 : ',coor_Q2
                    coor_Q3 = (0., fleche + ep_bandeau + b2)  # ; print 'Q3 : ',coor_Q3
                    coor_Q4 = (0., fleche + ep_bandeau)  # ; print 'Q4 : ',coor_Q4

                vertices = numpy.zeros([4, 2], 'd')
                vertices[0, 0] = coor_Q1[0]
                vertices[0, 1] = coor_Q1[1]  # Q1
                vertices[1, 0] = coor_Q2[0]
                vertices[1, 1] = coor_Q2[1]  # Q2
                vertices[2, 0] = coor_Q3[0]
                vertices[2, 1] = coor_Q3[1]  # Q3
                vertices[3, 0] = coor_Q4[0]
                vertices[3, 1] = coor_Q4[1]  # Q4

                # matplotlib
                iter_bloc += 1
                pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
                points_X[iter_bloc] = numpy.array(pts_X)
                pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
                points_Y[iter_bloc] = numpy.array(pts_Y)
                VERTICES[iter_bloc] = vertices

                # blocs symetriques
                vertices = numpy.zeros([4, 2], 'd')
                vertices[0, 0] = -coor_Q1[0]
                vertices[0, 1] = coor_Q1[1]
                vertices[1, 0] = -coor_Q4[0]
                vertices[1, 1] = coor_Q4[1]
                vertices[2, 0] = -coor_Q3[0]
                vertices[2, 1] = coor_Q3[1]
                vertices[3, 0] = -coor_Q2[0]
                vertices[3, 1] = coor_Q2[1]

                # matplotlib
                iter_bloc += 1
                pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
                points_X[iter_bloc] = numpy.array(pts_X)
                pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
                points_Y[iter_bloc] = numpy.array(pts_Y)
                VERTICES[iter_bloc] = vertices

                theta += theta_Bloc + theta_joint

    elif bandeau_type == 'H':
        for i in range(0, int(nb_blocs0 / 2)):
            # blocs REF : cos > 0 / sin > 0
            coor_Q1 = (R1 * math.cos(theta), R1 * math.sin(theta) + coor1[1])  # ; print 'Q1 : ',coor_Q1
            if nb_blocs0 / 2 % 2 == 0:
                if i % 2 == 1:  # blocs courts
                    coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q2 : ',coor_Q2
                    coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_bloc, coor2, R2)  # ; print 'Q3 : ',coor_Q3
                else:  # blocs longs
                    coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor3, R3)  # ; print 'Q2 : ',coor_Q2
                    coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_bloc, coor3, R3)  # ; print 'Q3 : ',coor_Q3
            else:
                if i % 2 == 1:  # blocs longs
                    coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor3, R3)  # ; print 'Q2 : ',coor_Q2
                    coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_bloc, coor3, R3)  # ; print 'Q3 : ',coor_Q3
                else:  # blocs courts
                    coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q2 : ',coor_Q2
                    coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_bloc, coor2, R2)  # ; print 'Q3 : ',coor_Q3
            coor_Q4 = (
            R1 * math.cos(theta + theta_bloc), R1 * math.sin(theta + theta_bloc) + coor1[1])  # ; print 'Q4 : ',coor_Q4

            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = coor_Q1[0]
            vertices[0, 1] = coor_Q1[1]  # Q1
            vertices[1, 0] = coor_Q2[0]
            vertices[1, 1] = coor_Q2[1]  # Q2
            vertices[2, 0] = coor_Q3[0]
            vertices[2, 1] = coor_Q3[1]  # Q3
            vertices[3, 0] = coor_Q4[0]
            vertices[3, 1] = coor_Q4[1]  # Q4

            # matplotlib
            iter_bloc += 1
            pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            # blocs symetriques
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = -coor_Q1[0]
            vertices[0, 1] = coor_Q1[1]
            vertices[1, 0] = -coor_Q4[0]
            vertices[1, 1] = coor_Q4[1]
            vertices[2, 0] = -coor_Q3[0]
            vertices[2, 1] = coor_Q3[1]
            vertices[3, 0] = -coor_Q2[0]
            vertices[3, 1] = coor_Q2[1]

            # matplotlib
            iter_bloc += 1
            pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            theta += theta_bloc + theta_joint

        # cle
        coor_Q1 = (R1 * math.cos(theta), R1 * math.sin(theta) + coor1[1])  # ; print 'Q1 : ',coor_Q1
        coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor3, R3)  # ; print 'Q2 : ',coor_Q2

        vertices = numpy.zeros([4, 2], 'd')
        vertices[0, 0] = coor_Q1[0]
        vertices[0, 1] = coor_Q1[1]  # Q1
        vertices[1, 0] = coor_Q2[0]
        vertices[1, 1] = coor_Q2[1]  # Q2
        vertices[2, 0] = -coor_Q2[0]
        vertices[2, 1] = coor_Q2[1]
        vertices[3, 0] = -coor_Q1[0]
        vertices[3, 1] = coor_Q1[1]

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

    elif bandeau_type == 'TC':
        for i in range(0, int(nb_blocs0 / 2)):
            # blocs REF : cos > 0 / sin > 0
            coor_Q1 = (R1 * math.cos(theta), R1 * math.sin(theta) + coor1[1])  # ; print 'Q1 : ',coor_Q1
            coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q2 : ',coor_Q2
            coor_Q3 = utils.intersection_Cercle(coor1, R1, theta + theta_bloc, coor2, R2)  # ; print 'Q3 : ',coor_Q3
            coor_Q4 = (
            R1 * math.cos(theta + theta_bloc), R1 * math.sin(theta + theta_bloc) + coor1[1])  # ; print 'Q4 : ',coor_Q4
            coor_Q5 = (coor_Q2[0], coor_Q3[1])

            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = coor_Q1[0]
            vertices[0, 1] = coor_Q1[1]  # Q1
            vertices[1, 0] = coor_Q2[0]
            vertices[1, 1] = coor_Q2[1]  # Q2
            vertices[2, 0] = coor_Q5[0]
            vertices[2, 1] = coor_Q5[1]  # Q3
            vertices[3, 0] = coor_Q3[0]
            vertices[3, 1] = coor_Q3[1]  # Q3
            vertices[4, 0] = coor_Q4[0]
            vertices[4, 1] = coor_Q4[1]  # Q4

            # matplotlib
            iter_bloc += 1
            pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            # blocs symetriques
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = -coor_Q1[0]
            vertices[0, 1] = coor_Q1[1]
            vertices[1, 0] = -coor_Q4[0]
            vertices[1, 1] = coor_Q4[1]
            vertices[2, 0] = -coor_Q3[0]
            vertices[2, 1] = coor_Q3[1]
            vertices[3, 0] = -coor_Q5[0]
            vertices[3, 1] = coor_Q5[1]
            vertices[4, 0] = -coor_Q2[0]
            vertices[4, 1] = coor_Q2[1]

            # matplotlib
            iter_bloc += 1
            pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
            points_X[iter_bloc] = numpy.array(pts_X)
            pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
            points_Y[iter_bloc] = numpy.array(pts_Y)
            VERTICES[iter_bloc] = vertices

            theta += theta_bloc + theta_joint

        # cle
        coor_Q1 = (R1 * math.cos(theta), R1 * math.sin(theta) + coor1[1])  # ; print 'Q1 : ',coor_Q1
        coor_Q2 = utils.intersection_Cercle(coor1, R1, theta, coor2, R2)  # ; print 'Q2 : ',coor_Q2

        vertices = numpy.zeros([4, 2], 'd')
        vertices[0, 0] = coor_Q1[0]
        vertices[0, 1] = coor_Q1[1]  # Q1
        vertices[1, 0] = coor_Q2[0]
        vertices[1, 1] = coor_Q2[1]  # Q2
        vertices[2, 0] = -coor_Q2[0]
        vertices[2, 1] = coor_Q2[1]
        vertices[3, 0] = -coor_Q1[0]
        vertices[3, 1] = coor_Q1[1]

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

    description_params = {"points_X": points_X,
                          "points_Y": points_Y,
                          "VERTICES": VERTICES,
                          "e_bloc": e_bloc,
                          "e_joint": e_joint}
    return description_params


def anse_panier(fleche, ep_bandeau, bandeau_type, ancrage, lc_bloc, generated_params):

    VERTICES = {}
    points_X = {}
    points_Y = {}

    b2 = ep_bandeau

    L_intrados = generated_params["L_intrados"]
    n = generated_params["n"]
    # N = generated_params["N"]
    R1 = generated_params["R1"]
    R2 = generated_params["R2"]
    M1 = generated_params["M1"]
    M2 = generated_params["M2"]
    theta1 = generated_params["theta1"]
    if n >= 5:
        R3 = generated_params["R3"]
        M3 = generated_params["M3"]
        theta2 = generated_params["theta2"]
    if n >= 7:
        R4 = generated_params["R4"]
        M4 = generated_params["M4"]
        theta3 = generated_params["theta3"]
    if n >= 9:
        R5 = generated_params["R5"]
        M5 = generated_params["M5"]
        theta4 = generated_params["theta4"]


    # n=nb_centre0; print 'nb centres :',n
    print('nb centres :', n)

    print('longueur caracteristique des voussoirs : ', lc_bloc)
    N = int(L_intrados / lc_bloc)
    nb_blocs0 = N
    if N != 0:
        if N % 2 == 0:
            dlc1 = abs(lc_bloc - L_intrados / (N - 1))
            nb1 = N - 1
            dlc2 = abs(lc_bloc - L_intrados / (N + 1))
            nb2 = N + 1
            dlc = min(dlc1, dlc2)
            if dlc == dlc1:
                nb_blocs0 = nb1
            else:
                nb_blocs0 = nb2
        else:
            nb_blocs0 = N

    # nb bloc minimal
    # nb_blocs0 = max(nb_blocs0, n+6)
    nb_blocs0 = max(nb_blocs0, 2 * n + 2)

    lc_bloc = L_intrados / nb_blocs0
    print('lc voussoir retenue : ', lc_bloc)
    print('nombre de voussoirs : ', nb_blocs0)
    e_bloc = lc_bloc

    # if nb_blocs0 % 2 == 0: nb_blocs0 +=1
    ##print 'nb blocs : ',nb_blocs0

    if bandeau_type == 'H':
        a1 = ancrage + ep_bandeau
        print('ancrage : ', ancrage)
        b2 = a1

    # ouverture angulaire
    theta_joint = math.pi / ((nb_blocs0 - 1) * 10.)
    theta_joint = 0.

    # ouverture d'un bloc, epaisseur d'un bloc et epaisseur du join
    # e_bloc     = b2
    e_joint = 0.005

    theta_bloc = lc_bloc / R1
    # print 'lc_bloc = ',lc_bloc
    if n >= 3: print('theta bloc 1 = ', theta_bloc * 180 / math.pi, '\n theta bloc 2 = ',
                     lc_bloc * 180 / (R2 * math.pi))
    if n >= 5: print('theta bloc 3 = ', lc_bloc * 180 / (R3 * math.pi))
    if n >= 7: print('theta bloc 4 = ', lc_bloc * 180 / (R4 * math.pi))
    if n >= 9: print('theta bloc 5 = ', lc_bloc * 180 / (R5 * math.pi))

    theta = 0.

    iter_bloc = 0
    # on positionne les voussoirs
    for i in range(0, int(nb_blocs0 / 2)):  # nb_blocs % 2 = 1
        # selection des arcs : discretisation angulaire a partir des centres
        THETA = [theta, theta + theta_bloc]
        M = []
        R = []
        ARC = []
        THETA_bloc = []
        THETA_lim = []
        for i_theta in range(0, 2, 1):
            if n == 3:
                if 0. <= THETA[i_theta] <= theta1:
                    M.append(M1); R.append(R1); ARC.append(1); THETA_bloc.append(lc_bloc / R1); THETA_lim.append(
                        1 * theta1)  # arc 1 : 0 -> theta1
                elif theta1 < THETA[i_theta]:
                    M.append(M2); R.append(R2); ARC.append(2); THETA_bloc.append(lc_bloc / R2); THETA_lim.append(
                        2 * theta1)  # arc 2 : theta1 -> Pi/2
            elif n == 5:
                if 0. <= THETA[i_theta] <= theta1:
                    M.append(M1); R.append(R1); ARC.append(1); THETA_bloc.append(lc_bloc / R1); THETA_lim.append(
                        1 * theta1)  # arc 1 : 0 -> theta1
                elif theta1 < THETA[i_theta] <= theta2:
                    M.append(M2); R.append(R2); ARC.append(2); THETA_bloc.append(lc_bloc / R2); THETA_lim.append(
                        2 * theta1)  # arc 2 : theta1 -> theta2
                elif theta2 < THETA[i_theta]:
                    M.append(M3); R.append(R3); ARC.append(3); THETA_bloc.append(lc_bloc / R3); THETA_lim.append(
                        3 * theta1)  # arc 3 : theta2 -> Pi/2
            elif n == 7:
                if 0. <= THETA[i_theta] <= theta1:
                    M.append(M1); R.append(R1); ARC.append(1); THETA_bloc.append(lc_bloc / R1); THETA_lim.append(
                        1 * theta1)  # arc 1 : 0 -> theta1
                elif theta1 < THETA[i_theta] <= theta2:
                    M.append(M2); R.append(R2); ARC.append(2); THETA_bloc.append(lc_bloc / R2); THETA_lim.append(
                        2 * theta1)  # arc 2 : theta1 -> theta2
                elif theta2 < THETA[i_theta] <= theta3:
                    M.append(M3); R.append(R3); ARC.append(3); THETA_bloc.append(lc_bloc / R3); THETA_lim.append(
                        3 * theta1)  # arc 3 : theta2 -> theta3
                elif theta3 < THETA[i_theta]:
                    M.append(M4); R.append(R4); ARC.append(4); THETA_bloc.append(lc_bloc / R4); THETA_lim.append(
                        4 * theta1)  # arc 4 : theta3 -> Pi/2
            elif n == 9:
                if 0. <= THETA[i_theta] <= theta1:
                    M.append(M1); R.append(R1); ARC.append(1); THETA_bloc.append(lc_bloc / R1); THETA_lim.append(
                        1 * theta1)  # arc 1 : 0 -> theta1
                elif theta1 < THETA[i_theta] <= theta2:
                    M.append(M2); R.append(R2); ARC.append(2); THETA_bloc.append(lc_bloc / R2); THETA_lim.append(
                        2 * theta1)  # arc 2 : theta1 -> theta2
                elif theta2 < THETA[i_theta] <= theta3:
                    M.append(M3); R.append(R3); ARC.append(3); THETA_bloc.append(lc_bloc / R3); THETA_lim.append(
                        3 * theta1)  # arc 3 : theta2 -> theta3
                elif theta3 < THETA[i_theta] <= theta4:
                    M.append(M4); R.append(R4); ARC.append(4); THETA_bloc.append(lc_bloc / R4); THETA_lim.append(
                        4 * theta1)  # arc 4 : theta3 -> theta4
                elif theta4 < THETA[i_theta]:
                    M.append(M5); R.append(R5); ARC.append(5); THETA_bloc.append(lc_bloc / R5); THETA_lim.append(
                        5 * theta1)  # arc 5 : theta4 -> Pi/2

        # blocs REF : cos > 0 / sin > 0
        if ARC[0] == ARC[1]:
            Mq1 = utils.droite_1Pt_alpha(M[0], THETA[0])
            uMq1 = Mq1[0]
            vMq1 = Mq1[1]
            Mq4 = utils.droite_1Pt_alpha(M[1], THETA[1])
            uMq4 = Mq4[0]
            vMq4 = Mq4[1]
            q1 = utils.intersection_Cercle_Droite_ux_v(uMq1, vMq1, M[0],
                                                       R[0])  # print ' q1 : ',q1,' - theta : ',THETA[0]*180/math.pi
            q4 = utils.intersection_Cercle_Droite_ux_v(uMq4, vMq4, M[1],
                                                       R[1])  # print ' q4 : ',q4,' - theta : ',THETA[1]*180/math.pi

            # maj increment
            theta += THETA_bloc[1] + theta_joint
            theta_bloc = THETA_bloc[1]

        else:
            # print '-----------------'
            r_theta = THETA_lim[
                          0] - theta  # print 'r_theta         = ',r_theta*180/math.pi,'\nlc_bloc         = ',lc_bloc
            lc_bloc_r_theta = r_theta * R[0]  # print 'lc_bloc_r_theta = ',lc_bloc_r_theta
            lc_bloc_b = lc_bloc - lc_bloc_r_theta  # print 'lc_bloc_b       = ',lc_bloc_b
            BETA = lc_bloc_b / R[1]  # print 'BETA            = ',BETA*180/math.pi
            theta0 = THETA_lim[0] + BETA  # print 'theta           = ',theta*180/math.pi
            theta_bloc = THETA_bloc[1]
            # print '-----------------'

            Mq1 = utils.droite_1Pt_alpha(M[0], THETA[0])
            uMq1 = Mq1[0]
            vMq1 = Mq1[1]
            Mq4 = utils.droite_1Pt_alpha(M[1], theta0)
            uMq4 = Mq4[0]
            vMq4 = Mq4[1]
            q1 = utils.intersection_Cercle_Droite_ux_v(uMq1, vMq1, M[0],
                                                       R[0])  # print ' q1 : ',q1,' - theta : ',THETA[0]*180/math.pi
            q4 = utils.intersection_Cercle_Droite_ux_v(uMq4, vMq4, M[1],
                                                       R[1])  # print ' q4 : ',q4,' - theta : ',theta*180/math.pi

            # maj increment
            theta = theta0 + theta_joint
            theta_bloc = THETA_bloc[1]

        if nb_blocs0 / 2 % 2 != 0:
            if i % 2 == 0:  # blocs courts
                beta = math.atan((q1[1] - M[0][1]) / (q1[0] - M[0][0]))
                dx = ep_bandeau * math.cos(beta)
                dy = ep_bandeau * math.sin(beta)
                q2 = (q1[0] + dx, q1[1] + dy)  # print ' q2 : ',q2
                beta = math.atan((q4[1] - M[1][1]) / (q4[0] - M[1][0]))
                dx = ep_bandeau * math.cos(beta)
                dy = ep_bandeau * math.sin(beta)
                q3 = (q4[0] + dx, q4[1] + dy)  # print ' q3 : ',q3
            else:  # blocs longs
                beta = math.atan((q1[1] - M[0][1]) / (q1[0] - M[0][0]))
                dx = b2 * math.cos(beta)
                dy = b2 * math.sin(beta)
                q2 = (q1[0] + dx, q1[1] + dy)  # print ' q2 : ',q2
                beta = math.atan((q4[1] - M[1][1]) / (q4[0] - M[1][0]))
                dx = b2 * math.cos(beta)
                dy = b2 * math.sin(beta)
                q3 = (q4[0] + dx, q4[1] + dy)  # print ' q3 : ',q3
        else:
            if i % 2 != 0:  # blocs courts
                beta = math.atan((q1[1] - M[0][1]) / (q1[0] - M[0][0]))
                dx = ep_bandeau * math.cos(beta)
                dy = ep_bandeau * math.sin(beta)
                q2 = (q1[0] + dx, q1[1] + dy)  # print ' q2 : ',q2
                beta = math.atan((q4[1] - M[1][1]) / (q4[0] - M[1][0]))
                dx = ep_bandeau * math.cos(beta)
                dy = ep_bandeau * math.sin(beta)
                q3 = (q4[0] + dx, q4[1] + dy)  # print ' q3 : ',q3
            else:  # blocs longs
                beta = math.atan((q1[1] - M[0][1]) / (q1[0] - M[0][0]))
                dx = b2 * math.cos(beta)
                dy = b2 * math.sin(beta)
                q2 = (q1[0] + dx, q1[1] + dy)  # print ' q2 : ',q2
                beta = math.atan((q4[1] - M[1][1]) / (q4[0] - M[1][0]))
                dx = b2 * math.cos(beta)
                dy = b2 * math.sin(beta)
                q3 = (q4[0] + dx, q4[1] + dy)  # print ' q3 : ',q3

        # ----------------
        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = q1[0]
            vertices[0, 1] = q1[1]  # q1
            vertices[1, 0] = q2[0]
            vertices[1, 1] = q2[1]  # q2
            vertices[2, 0] = q3[0]
            vertices[2, 1] = q3[1]  # q3
            vertices[3, 0] = q4[0]
            vertices[3, 1] = q4[1]  # q4
        elif bandeau_type == 'TC':
            q5 = (q2[0], q3[1])
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = q1[0]
            vertices[0, 1] = q1[1]  # q1
            vertices[1, 0] = q2[0]
            vertices[1, 1] = q2[1]  # q2
            vertices[2, 0] = q5[0]
            vertices[2, 1] = q5[1]  # q3
            vertices[3, 0] = q3[0]
            vertices[3, 1] = q3[1]  # q3
            vertices[4, 0] = q4[0]
            vertices[4, 1] = q4[1]  # q4

        # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

        # blocs symetriques
        if bandeau_type == 'BS' or bandeau_type == 'H':
            vertices = numpy.zeros([4, 2], 'd')
            vertices[0, 0] = -q1[0]
            vertices[0, 1] = q1[1]
            vertices[1, 0] = -q4[0]
            vertices[1, 1] = q4[1]
            vertices[2, 0] = -q3[0]
            vertices[2, 1] = q3[1]
            vertices[3, 0] = -q2[0]
            vertices[3, 1] = q2[1]
        elif bandeau_type == 'TC':
            vertices = numpy.zeros([5, 2], 'd')
            vertices[0, 0] = -q1[0]
            vertices[0, 1] = q1[1]
            vertices[1, 0] = -q4[0]
            vertices[1, 1] = q4[1]
            vertices[2, 0] = -q3[0]
            vertices[2, 1] = q3[1]
            vertices[3, 0] = -q5[0]
            vertices[3, 1] = q5[1]
            vertices[4, 0] = -q2[0]
            vertices[4, 1] = q2[1]

            # matplotlib
        iter_bloc += 1
        pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
        points_X[iter_bloc] = numpy.array(pts_X)
        pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
        points_Y[iter_bloc] = numpy.array(pts_Y)
        VERTICES[iter_bloc] = vertices

    # cle
    # print 'theta : ',theta
    q1 = q4
    print(' q1 : ', q1)
    beta = math.atan((q1[1] - M[0][1]) / (q1[0] - M[0][0]))
    dx = b2 * math.cos(beta)
    dy = b2 * math.sin(beta)
    q2 = (q1[0] + dx, q1[1] + dy)  # print ' q2 : ',q2

    vertices = numpy.zeros([4, 2], 'd')
    vertices[0, 0] = q1[0]
    vertices[0, 1] = q1[1]  # Q1
    vertices[1, 0] = q2[0]
    vertices[1, 1] = q2[1]  # Q2
    vertices[2, 0] = -q2[0]
    vertices[2, 1] = q2[1]
    vertices[3, 0] = -q1[0]
    vertices[3, 1] = q1[1]

    # matplotlib
    iter_bloc += 1
    pts_X = list(vertices[:, 0]) + [vertices[0, 0]]
    points_X[iter_bloc] = numpy.array(pts_X)
    pts_Y = list(vertices[:, 1]) + [vertices[0, 1]]
    points_Y[iter_bloc] = numpy.array(pts_Y)
    VERTICES[iter_bloc] = vertices

    print('\n--------------------------------------')
    print('check fleche : ')
    print('  construction : fleche = ', q1[1])
    print('  donnee       : fleche = ', fleche)
    print('  erreur = ', abs(q1[1] - fleche) * 100. / fleche, ' % ')
    print('--------------------------------------\n')

    description_params = {"points_X": points_X,
                          "points_Y": points_Y,
                          "VERTICES": VERTICES,
                          "e_bloc": e_bloc,
                          "e_joint": e_joint}
    return description_params