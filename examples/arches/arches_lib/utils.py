import math


# eq droite passant par 2 points A et B : y=ux+v
def droite_2Pts(Pt1, Pt2):
    xPt1 = Pt1[0]
    yPt1 = Pt1[1]
    xPt2 = Pt2[0]
    yPt2 = Pt2[1]
    u = (yPt1 - yPt2) / (xPt1 - xPt2)
    v = yPt2 - xPt2 * u
    return (u, v)


# eq droite passant par 1 point A et angle : y=ux+v
def droite_1Pt_alpha(Pt1, alpha):
    xPt1 = Pt1[0]
    yPt1 = Pt1[1]
    u = math.tan(alpha)
    v = yPt1 - xPt1 * u
    return (u, v)


# intersection 2 droites : y=u1x+v1 et y=u2x+v2
def intersection_2droites(D1, D2):
    u1 = D1[0]
    v1 = D1[1]
    u2 = D2[0]
    v2 = D2[1]
    x_inter = (v2 - v1) / (u1 - u2)
    y_inter = u1 * x_inter + v1
    return (x_inter, y_inter)


# intersection cercle C2 droite (O1, P1 appartenant a C1)
def intersection_cercle(O1, R1, theta, O2, R2):
    xO1 = O1[0]
    yO1 = O1[1]
    xO2 = O2[0]
    yO2 = O2[1]
    xP1 = R1 * math.cos(theta)
    yP1 = R1 * math.sin(theta)
    v = yO1
    u = (yO1 - yP1) / xP1  # (O1P1): y = ux + v

    A = u ** 2 + 1
    B = -2 * xO2 + 2 * u * v - 2 * u * yO2
    C = (xO2 ** 2 + v ** 2 + yO2 ** 2 - 2 * v * yO2) - R2 ** 2

    delta = B ** 2 - 4 * A * C
    x_inter = 0
    y_inter = 0
    if delta > 0:
        x_inter1 = (-B - math.sqrt(delta)) / (2. * A)
        x_inter2 = (-B + math.sqrt(delta)) / (2. * A)
        x_inter = max(x_inter1, x_inter2)
        y_inter = u * x_inter + v
    elif delta == 0:
        x_inter = -B / (2. * A)
        y_inter = u * x_inter + v
    else:
        print('no solutions')

    return x_inter, y_inter


# intersection cercle Ci (Mi, Ri) par une droite (Pt1Pt2): y = ux + v)
def intersection_Cercle_Droite_ux_v(u, v, Ci, Ri):
    xCi = Ci[0]
    yCi = Ci[1]
    A = u ** 2 + 1
    B = -2 * (xCi + u * yCi - u * v)
    C = xCi ** 2 + yCi ** 2 - Ri ** 2 + v ** 2 - 2 * yCi * v

    delta = B ** 2 - 4 * A * C
    if delta > 0:
        x_inter1 = (-B - math.sqrt(delta)) / (2. * A)
        y_inter1 = u * x_inter1 + v
        x_inter2 = (-B + math.sqrt(delta)) / (2. * A)
        y_inter2 = u * x_inter2 + v
        x_inter = max(x_inter1, x_inter2)
        y_inter = u * x_inter + v
    elif delta == 0:
        x_inter = -B / (2. * A)
        y_inter = u * x_inter + v
    else:
        print('no solutions')
        x_inter = 0.
        y_inter = 0.

    return (x_inter, y_inter)


# caracteristiques d'un cercle passant par 3 points : C, R
def cercle(P1, P2, P3):
    # cercle de centre C de raron R passant par P1, P2 et P3
    x1 = P1[0]
    y1 = P1[1]
    x2 = P2[0]
    y2 = P2[1]
    x3 = P3[0]
    y3 = P3[1]

    xM1 = (x1 + x2) / 2.
    yM1 = (y2 + y3) / 2.
    xM2 = (x1 + x2) / 2.
    yM2 = (y2 + y3) / 2.

    a1 = -(x2 - x1) / (y2 - y1)
    b1 = (x2 ** 2 - x1 ** 2 + y2 ** 2 - y1 ** 2) / (2. * (y2 - y1))
    a2 = -(x3 - x2) / (y3 - y2)
    b2 = (x3 ** 2 - x2 ** 2 + y3 ** 2 - y2 ** 2) / (2. * (y3 - y2))

    xC = (b1 - b2) / (a2 - a1)
    yC = a1 * xC + b1

    rC = math.sqrt((x1 - xC) ** 2 + (y1 - yC) ** 2)

    return rC, (xC, yC)


# calcul arc de parabole y(AB) par discretisation
def P(x, a, b, c):
    return a * x ** 2 + b * x + c


def L_arc_P(Pt1, Pt2, a, b, c, p):
    n_discr = 5000
    dx = p / n_discr
    xPt_min = min(Pt1[0], Pt2[0])
    xPt_max = max(Pt1[0], Pt2[0])
    x0 = xPt_min
    ARC = 0.
    for i in range(0, n_discr):
        while x0 < xPt_max:
            x1 = x0 + dx
            y0 = P(x0, a, b, c)
            y1 = P(x1, a, b, c)

            arc_01 = math.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2)
            ARC += arc_01

            x0 = x1
    return ARC


# eq de la tangente y = ux + v a la Parabole y = a.x^2 + b.x + c au point P1
def tangente_parabole(Pt1, a, b, c):
    xPt1=Pt1[0]; yPt1=Pt1[1]
    x0=xPt1+c/10.; y0 = a*x0**2 + b*x0 + c; X0=(x0,y0)
    x1=xPt1-c/10.; y1 = a*x1**2 + b*x1 + c; X1=(x1,y1)

    return droite_2Pts(X0,X1)


# intersection cercle C2 (O2, R2) par une droite (O1 P1), P1 appartenant a un cercle C1 (01, R1))
def intersection_Cercle(O1, R1, alpha, O2, R2):
    xO1=O1[0]; yO1=O1[1]
    xO2=O2[0]; yO2=O2[1]
    xP1=R1*math.cos(alpha); yP1=R1*math.sin(alpha)+yO1 ; #print 'Q : (',xP1,' ; ',yP1,')'
    v=yO1; u=(yP1-yO1)/xP1; #print 'y = u x + v  : ',u,' . x + ',v  # (O1P1): y = ux + v

    A=u**2+1
    B=-2*xO2+2*u*v-2*u*yO2
    C=(xO2**2+v**2+yO2**2-2*v*yO2)-R2**2

    delta=B**2-4*A*C
    if delta>0:
       x_inter1 = (-B-math.sqrt(delta))/(2.*A); y_inter1 = u*x_inter1 + v
       x_inter2 = (-B+math.sqrt(delta))/(2.*A); y_inter2 = u*x_inter2 + v
       x_inter=max(x_inter1, x_inter2); y_inter = u*x_inter + v
    elif delta==0:
       x_inter = -B/(2.*A); y_inter = u*x_inter + v
    else:
       print('no solutions')
       x_inter = 0.; y_inter = 0.

    return (x_inter, y_inter)