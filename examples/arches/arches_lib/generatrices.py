import math

from arches_lib import utils


def ogive(portee, fleche, ep_bandeau):

    L_floor = portee + ep_bandeau
    #points caracteristiques
    xO= 0.; yO= 0.; O=(xO,yO)
    xA= portee/2.; yA= 0.; A=(xA,yA)
    xB=-portee/2.; yB= 0.; B=(xB,yB)
    xC= 0.; yC= fleche ; C=(xC,yC)
    # courbe d'intrados
    BC= utils.droite_2Pts(B, C)                           # (BC)
    xM1=(xB+xC)*0.5; yM1=(yB+yC)*0.5; M1=(xM1,yM1) # M1 milieu de [BC]
    n_BC=(BC[0],-1)                                # vecteur normal a (BC)
    n_M1O1 = (1/BC[0],1)                           # vecteur normal a (M1O1) ortho a (BC)
    M1O1=(-n_M1O1[0],yM1+n_M1O1[0]*xM1)            # (O1M1)
    xO1=-M1O1[1]/-M1O1[0]; yO1=0.; O1=(xO1,yO1)    # centre O1
    R1=math.sqrt((xC-xO1)**2+(yC-yO1)**2)          # rayon R1
    theta1=math.atan2(fleche, abs(xO1))
    L_intrados = 2*R1*theta1

    # informations intrados
    print('-------------------------------------------')
    print("Arc d'intrados:")
    print('  - centre O1  = ',O1)
    print('  - rayon R1   = ',R1)
    print('  - theta 1    = ',theta1*180./math.pi)
    print('  - L_intrados = ',L_intrados)
    print('-------------------------------------------')

    ## Centre O2
    #AC=droite_2Pts(A,C)                           # (AC)
    #xM2=(xA+xC)*0.5; yM2=(yA+yC)*0.5; M2=(xM2,yM2) # M2 milieu de [AC]
    #n_AC=(AC[0],-1)                                # vecteur normal a (AC)
    #n_M2O2 = (1/AC[0],1)                           # vecteur normal a (M2O2) ortho a (AC)
    #M2O2=(-n_M2O2[0],yM2+n_M2O2[0]*xM2)            # (O2M2)
    #xO2=-M2O2[1]/-M2O2[0]; yO2=0.; O2=(xO2,yO2)    # centre O2
    #R2=math.sqrt((xC-xO2)**2+(yC-yO2)**2)          # rayon R2
    #
    ## informations
    #print '-------------------------------------------'
    #print 'Arc type 2:'
    #print '  - centre O2 = ',O2
    #print '  - rayon R2  = ',R2


    # courbe d'extrados
    R2=R1+ep_bandeau
    # intersection C2 (O2, R2) avec la droite x=0
    A=1; B=-2*yO1; C=xO1**2+yO1**2-R2**2
    delta=B**2-4*A*C
    yS=0.
    if delta>0:
       yS1 = (-B-math.sqrt(delta))/(2.*A)
       yS2 = (-B+math.sqrt(delta))/(2.*A)
       yS=max(yS1, yS2)
    elif delta==0:
       yS = -B/(2.*A)
    else:
       print('no solutions')
    theta2=math.atan2(yS, abs(xO1))
    L_extrados = 2*R2*theta2

    # informations extrados
    print('-------------------------------------------')
    print("Arc d'extrados:")
    print('  - centre O1 = ',O1)
    print('  - rayon R2  = ',R2)
    print('  - theta 2   = ',theta2*180./math.pi)
    print('  - L_extrados = ',L_extrados)
    print('-------------------------------------------')

    generated_params = {"L_floor": L_floor,
                        "L_intrados": L_intrados,
                        "L_extrados": L_extrados,
                        "R1": R1,
                        "R2": R2,
                        "theta1": theta1,
                        "theta2": theta2,
                        "O1": O1,
                        "xO1": xO1}

    return generated_params


def parabole(portee, fleche, ep_bandeau):

    L_floor = portee + ep_bandeau

    # points caracteristiques
    xO = 0.
    yO = 0.
    O = (xO, yO)
    xA = portee / 2.
    yA = 0.
    A = (xA, yA)
    xB = -portee / 2.
    yB = 0.
    B = (xB, yB)
    xC = 0.
    yC = fleche
    C = (xC, yC)

    # Equation de la parabole y: a.x^2+b.x + c = 0 passant par A, B, C
    a = -4 * fleche / portee ** 2
    b = 0.
    c = fleche

    L_intrados = utils.L_arc_P(A, B, a, b, c, portee)
    print('L_intrados = ', L_intrados)

    generated_params = {"L_floor": L_floor,
                        "L_intrados": L_intrados,
                        "a": a,
                        "b": b,
                        "c": c,
                        "A": A,
                        "xA": xA,
                        "yA": yA}

    return generated_params


def plein_cintre(portee, fleche, ep_bandeau, ep_rein, bandeau_type, ancrage):

    b2 = ep_bandeau
    r2 = ep_rein

    xA1 = -portee / 2.
    yA1 = 0.
    xB = 0.
    yB = fleche
    xA2 = portee / 2.
    yA2 = 0.
    R1, coor1 = utils.cercle((xA1, yA1), (xB, yB), (xA2, yA2))
    theta0 = math.atan2((R1 - fleche), (portee * 0.5))
    L_intrados = R1 * (math.pi - 2 * theta0)

    print('intrados:')
    print('---------')
    print('  - rayon = ', R1)
    print('  - coordonnees du centre : x =', coor1[0], ', y = ', coor1[1])
    print('  - theta0 = ', theta0 * 180 / math.pi)
    print('  - L_intrados = ', L_intrados)
    print('')

    # extrados : cercle 2
    # cercle de cetre O2 de rayon R2 passant par C1, D et C2
    xC1 = -(R1 + ep_rein) * math.cos(60 * math.pi / 180.)
    yC1 = (R1 + ep_rein) * math.sin(60 * math.pi / 180.) - R1 + fleche
    xD = 0.
    yD = fleche + ep_bandeau
    xC2 = (R1 + ep_rein) * math.cos(60 * math.pi / 180.)
    yC2 = (R1 + ep_rein) * math.sin(60 * math.pi / 180.) - R1 + fleche
    R2, coor2 = utils.cercle((xC1, yC1), (xD, yD), (xC2, yC2))
    P1 = math.sqrt(R2 ** 2 - (R2 - fleche - ep_bandeau) ** 2)  # demi-travee
    # P=P1
    L_floor = P1

    print('extrados:')
    print('---------')
    print('  - rayon = ', R2)
    print('  - coordonnees du centre : x =', coor2[0], ', y = ', coor2[1])
    print('')

    R3 = None
    coor3 = None
    # double_arches=['double_bandeau','harpage']
    double_arches = ['DB', 'H']
    if bandeau_type == 'DB':
        # extrados 2 : cercle 3
        # cercle de cetre O3 de rayon R3 passant par E1, F et E2
        xE1 = -(R1 + ep_rein + r2) * math.cos(60 * math.pi / 180.)
        yE1 = (R1 + ep_rein + r2) * math.sin(60 * math.pi / 180.) - R1 + fleche
        xF = 0.
        yF = fleche + ep_bandeau + b2
        xE2 = (R1 + ep_rein + r2) * math.cos(60 * math.pi / 180.)
        yE2 = (R1 + ep_rein + r2) * math.sin(60 * math.pi / 180.) - R1 + fleche
        R3, coor3 = utils.cercle((xE1, yE1), (xF, yF), (xE2, yE2))
        P2 = math.sqrt(R3 ** 2 - (R3 - fleche - ep_bandeau - b2) ** 2)  # demi-travee
        # P=P2
        L_floor = P2

        print('extrados:')
        print('---------')
        print('  - rayon = ', R3)
        print('  - coordonnees du centre : x =', coor3[0], ', y = ', coor3[1])
        print('')

    if bandeau_type == 'H':
        # extrados 2 : cercle 3
        # cercle de cetre O3 de rayon R3 passant par E1, F et E2
        xE1 = -(R1 + ep_rein + ancrage - ep_bandeau) * math.cos(60 * math.pi / 180.)
        yE1 = (R1 + ep_rein + ancrage - ep_bandeau) * math.sin(60 * math.pi / 180.) - R1 + fleche
        xF = 0.
        yF = fleche + ancrage
        xE2 = (R1 + ep_rein + ancrage - ep_bandeau) * math.cos(60 * math.pi / 180.)
        yE2 = (R1 + ep_rein + ancrage - ep_bandeau) * math.sin(60 * math.pi / 180.) - R1 + fleche
        R3, coor3 = utils.cercle((xE1, yE1), (xF, yF), (xE2, yE2))
        P2 = math.sqrt(R3 ** 2 - (R3 - fleche - ancrage) ** 2)  # demi-travee
        # P=P2
        L_floor = P2

        print('extrados:')
        print('---------')
        print('  - rayon = ', R3)
        print('  - coordonnees du centre : x =', coor3[0], ', y = ', coor3[1])
        print('')

    generated_params = {"L_floor": L_floor,
                        "L_intrados": L_intrados,
                        "R1": R1,
                        "R2": R2,
                        "R3": R3,
                        "theta0": theta0,
                        "P1": P1,
                        "coor1": coor1,
                        "coor2": coor2,
                        "coor3": coor3}

    return generated_params


def anse_panier(abaque, portee, fleche, ep_bandeau):
    type_anse = abaque["type_anse"]
    type_anse = {float(k): v for k, v in type_anse.items()}

    anse = abaque["anse"]
    anse = {int(k): {float(k2): v2 for k2, v2 in v.items()} for k, v in  anse.items()}

    L_floor = portee + ep_bandeau

    s = fleche / portee
    print('  surbaissement : ', s)
    
    cles = list(map(float, type_anse.keys()))
    cles.sort()
    if len(cles) > 0:
        s_min = cles[0]; s_max = cles[-1]
    else:
        s_min = 0.; s_max = 0.

    print('- lecture abaque : ')
    s0 = round(s, 2)
    if s_min <= s0 <= s_max:
        N = type_anse[s0]
        n = N[0]  # default case
        if len(N) == 1:
            print('  Anse de panier a ', n, ' centres')
        else:
            print('  Anse de panier a ', n, '(defaut) ou ', N[1], ' centres')

        Rayons = anse[n][s0]
        print("  Rayons issus de l'abaque : ", Rayons)
    else:
        print('')
        print('  Valeur de surbaissement hors abaque : ')
        print('  Modifier le nombre de centres et/ou les valeurs de la portee et/ou de la fleche ')
        print('  pour construire une anse de panier a 5, 7 ou 9 centres ')
        print('')
        print("  Construction par defaut d'une anse de panier a 3 centres")
        print('')
        # sys.exit()
        n = 3

    # correction fleche
    if s != s0:
        fleche = s0 * portee
        print('')
        print('  WARNING : correction de la valeur de la fleche ')
        print('    fleche initiale                  : ', s * portee)
        print('    fleche consideree dans le modele : ', fleche)
        print('')

    # points caracteristiques
    xO = 0.
    yO = 0.
    O = (xO, yO)
    xA = portee / 2.
    yA = 0.
    A = (xA, yA)
    xB = -portee / 2.
    yB = 0.
    B = (xB, yB)
    xC = portee / 2.
    yC = fleche
    C = (xC, yC)
    xD = -portee / 2.
    yD = fleche
    D = (xD, yD)
    xE = 0.
    yE = fleche
    E = (xE, yE)
    xF = 0.
    yF = portee / 2.
    F = (xF, yF)

    # definition des arcs Pi
    P = {}
    for i in range(0, n - 1, 1):
        P[i + 1] = (portee * 0.5 * math.cos((i + 1) * math.pi / n), portee * 0.5 * math.sin((i + 1) * math.pi / n))

    #################  A N S E    A    3    C E N T R E S     #####################"
    if n == 3:
        print('')
        print("- Construction des generatrices de l'anse a 3 centres par la methode de HUYGENS...")

        # cercle 1: centre M1, rayon R1, lim angulaire theta1
        theta1 = math.pi / n  # print 'theta 1 = ',theta1

        # N1: intersection (AP1) et (N1E) // (P1F)
        AP1 = utils.droite_2Pts(A, P[1])  # (AP1)
        P1F = utils.droite_2Pts(P[1], F)  # (P1F)
        N1E = (P1F[0], yE - P1F[0] * xE)  # (N1E ) // (P1F)
        N1 = utils.intersection_2droites(AP1, N1E)  # print 'N1 : ',N1
        xN1 = N1[0]
        yN1 = N1[1]

        # M1: intersection (N1M1)=(N1theta1) et (AO)
        N1theta1 = utils. droite_1Pt_alpha(N1, theta1)  # (N1theta1)
        xM1 = -N1theta1[1] / N1theta1[0]
        yM1 = 0.
        M1 = (xM1, yM1)  # print 'M1 : ',M1
        R1 = math.sqrt((xN1 - xM1) ** 2 + (yN1 - yM1) ** 2)  # print 'R1 = ',R1

        # domaine angulaire alpha2
        xQ1 = xM1 + R1 * math.cos(theta1)
        yQ1 = yM1 + R1 * math.sin(theta1)
        Q1 = (xQ1, yQ1)  # print 'Q1 : ',Q1
        OQ1 = math.sqrt(xQ1 ** 2 + yQ1 ** 2)
        alpha1 = math.acos(xQ1 / OQ1)  # print 'alpha 1 = ',alpha1*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 1:')
        print('  - centre M1 = ', M1)
        print('  - rayon R1  = ', R1)
        print('  - theta 1   = ', theta1 * 180. / math.pi)
        print('  - alpha 1   = ', alpha1 * 180. / math.pi)

        #################

        # cercle 2: centre M2, rayon R2
        xM2 = 0.
        yM2 = N1theta1[1]
        M2 = (xM2, yM2)  # print 'M2 : ',M2
        R2 = math.sqrt((xQ1 - xM2) ** 2 + (yQ1 - yM2) ** 2)  # print 'R2 = ',R2

        # informations
        print('-------------------------------------------')
        print('Arc type 2:')
        print('  - centre M2 = ', M2)
        print('  - rayon R2  = ', R2)

        # approche analytique
        print('')
        print('APPROCHE ANALYTIQUE 3C')
        r2 = (fleche - portee * math.cos(theta1 / 2)) / (1 - 2 * math.cos(theta1 / 2))
        r1 = portee - r2

        print('r1 = ', r1)
        print('r2 = ', r2)



    #################  A N S E    A    5    C E N T R E S     #####################"
    elif n == 5:
        print('')
        print("- Construction des generatrices de l'anse a 5 centres par la methode de MICHAL")

        # cercle 1: centre M1, rayon R1, lim angulaire theta1
        wR1 = Rayons[0]
        R1 = wR1 * portee * 0.5  # print 'R1 = ',R1
        xM1 = xA - R1
        yM1 = 0.
        M1 = (xM1, yM1)  # print 'M1 : ',M1
        theta1 = math.pi / n  # print 'theta 1 = ',theta1

        # N1: intersection (AP1) et (M1theta1)
        AP1 = utils.droite_2Pts(A, P[1])  # (AP1)
        M1theta1 = utils.droite_1Pt_alpha(M1, theta1)  # (M1theta1)
        N1 = utils.intersection_2droites(AP1, M1theta1)  # print 'N1 : ',N1
        xN1 = N1[0]
        yN1 = N1[1]

        # domaine angulaire alpha1
        xQ1 = xM1 + R1 * math.cos(theta1)
        yQ1 = yM1 + R1 * math.sin(theta1)
        Q1 = (xQ1, yQ1)  # print 'Q1 : ',Q1
        OQ1 = math.sqrt(xQ1 ** 2 + yQ1 ** 2)
        alpha1 = math.acos(xQ1 / OQ1)  # print 'alpha 1 = ',alpha1*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 1:')
        print('  - centre M1 = ', M1)
        print('  - rayon R1  = ', R1)
        print('  - theta 1   = ', theta1 * 180. / math.pi)
        print('  - alpha 1   = ', alpha1 * 180. / math.pi)

        #################

        # cercle 2: centre M2, rayon R2, lim angulaire theta2
        P1P2 = utils.droite_2Pts(P[1], P[2])  # (P1P2)
        N1N2 = (P1P2[0], yN1 - P1P2[0] * xN1)  # (N1N2) // (P1P2)
        # NON !! il faut prendre la parallele a la corde P2F et non a P2P3
        # P2P3=droite_2Pts(P[2],P[3])    #(P2P3)
        # N2E=(P2P3[0], yE-P2P3[0]*xE)   #(N2E ) // (P2P3)
        P2F = utils.droite_2Pts(P[2], F)  # (P2F)
        N2E = (P2F[0], yE - P2F[0] * xE)  # (N2E ) // (P2F)

        # N2: intersection (N1N2) et (N2E)
        N2 = utils.intersection_2droites(N1N2, N2E)  # print 'N2 : ',N2
        theta2 = 2. * math.pi / n

        # M2: intersection (N2M2)=(N2theta2) et (N1M1)
        N1M1 = utils.droite_2Pts(N1, M1)  # (N1M1)
        N2theta2 = utils.droite_1Pt_alpha(N2, theta2)  # (N2theta2)
        M2 = utils.intersection_2droites(N1M1, N2theta2)  # print 'M2 : ',M2
        xM2 = M2[0]
        yM2 = M2[1]
        # R2=math.sqrt((xQ1-xM2)**2+(yQ1-yM2)**2)  #print 'R2 = ',R2
        R2 = math.sqrt((xN1 - xM2) ** 2 + (yN1 - yM2) ** 2)  # print 'R2 = ',R2

        # domaine angulaire alpha2
        xQ2 = xM2 + R2 * math.cos(theta2)
        yQ2 = yM2 + R2 * math.sin(theta2)
        Q2 = (xQ2, yQ2)  # print 'Q2 : ',Q2
        OQ2 = math.sqrt(xQ2 ** 2 + yQ2 ** 2)
        alpha2 = math.acos(xQ2 / OQ2)  # print 'alpha 2 = ',alpha2*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 2:')
        print('  - centre M2 = ', M2)
        print('  - rayon R2  = ', R2)
        print('  - theta 2   = ', theta2 * 180. / math.pi)
        print('  - alpha 2   = ', alpha2 * 180. / math.pi)

        #################

        # cercle 3: centre M3, rayon R3
        N2M2 = utils.droite_2Pts(N2, M2)  # (N1M1)
        xM3 = 0.
        yM3 = N2M2[1]
        M3 = (xM3, yM3)
        print('M3 : ', M3)
        R3 = math.sqrt((xQ2 - xM3) ** 2 + (yQ2 - yM3) ** 2)
        print('R3 = ', R3)

        # informations
        print('-------------------------------------------')
        print('Arc type 3:')
        print('  - centre M3 = ', M3)
        print('  - rayon R3  = ', R3)

        #################

        ## approche analytique
        # print ''
        # print 'APPROCHE ANALYTIQUE 5C'
        # beta=(math.pi-theta1)/2
        # gamma1=math.pi - beta - 2*theta1

        # r1=r2=r3=1

        # base_1 = 2*r1*math.sin(theta1/2)
        # base_2 = 2*r2*math.sin(theta1/2)
        # base_3 = 2*r3*math.sin(theta1/2)

        ## eq 1 : H1 + H2 + H3 = fleche
        # H1=base_1*math.sin(beta)
        # H2=base_2*math.sin(gamma1)
        # H4=r3*math.cos(theta1/2)
        # H3=r3-H4
        #
        ## eq 2 : L1 + L2 + L3 = portee/2
        # L1=base_1*math.cos(beta)
        # L2=base_2*math.cos(gamma1)
        # L3=r3*math.sin(theta1/2)
        #
        ## eq 3 : fermeture R2 = fleche(R1): aR1 + aR2 + aR3 = 0.
        # aR1=H1-math.sin(theta1); print H1,' ',-math.sin(theta1),' ',aR1
        # aR2=H2+math.sin(theta1)-math.cos(theta1/2); print H2,' ',math.sin(theta1)-math.cos(theta1/2),' ',aR2
        # aR3=0.

        ## systeme
        # S=numpy.array([[H1,H2,H3],[L1,L2,L3],[aR1,aR2,aR3]])
        # V=numpy.array([fleche,portee*0.5,0.])
        # R=numpy.linalg.solve(S,V)

        # print 'r1 = ',R[0]
        # print 'r2 = ',R[1]
        # print 'r3 = ',R[2]

    #################  A N S E    A    7    C E N T R E S     #####################"
    elif n == 7:
        print('')
        print("- Construction des generatrices de l'anse a 7 centres par la methode de MICHAL")

        # cercle 1: centre M1, rayon R1, lim angulaire theta1
        wR1 = Rayons[0]
        R1 = wR1 * portee * 0.5  # print 'R1 = ',R1
        xM1 = xA - R1
        yM1 = 0.
        M1 = (xM1, yM1)  # print 'M1 : ',M1
        theta1 = math.pi / n  # print 'theta 1 = ',theta1

        # N1: intersection (AP1) et (M1theta1)
        AP1 = utils.droite_2Pts(A, P[1])  # (AP1)
        M1theta1 = utils.droite_1Pt_alpha(M1, theta1)  # (M1theta1)
        N1 = utils.intersection_2droites(AP1, M1theta1)  # print 'N1 : ',N1
        xN1 = N1[0]
        yN1 = N1[1]

        # domaine angulaire alpha1
        xQ1 = xM1 + R1 * math.cos(theta1)
        yQ1 = yM1 + R1 * math.sin(theta1)
        Q1 = (xQ1, yQ1)  # print 'Q1 : ',Q1
        OQ1 = math.sqrt(xQ1 ** 2 + yQ1 ** 2)
        alpha1 = math.acos(xQ1 / OQ1)  # print 'alpha 1 = ',alpha1*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 1:')
        print('  - centre M1 = ', M1)
        print('  - rayon R1  = ', R1)
        print('  - theta 1   = ', theta1 * 180. / math.pi)
        print('  - alpha 1   = ', alpha1 * 180. / math.pi)

        #################

        # cercle 2: centre M2, rayon R2, lim angulaire theta2
        wR2 = Rayons[1]
        R2 = wR2 * portee * 0.5  # print 'R2 = ',R2
        xM2 = xN1 - R2 * math.cos(theta1)
        yM2 = yN1 - R2 * math.sin(theta1)
        M2 = (xM2, yM2)  # print 'M2 : ',M2
        theta2 = 2. * math.pi / n

        # N2: intersection (N1N2) // (P1P2) et (M2theta2)
        P1P2 = utils.droite_2Pts(P[1], P[2])  # (P1P2)
        N1N2 = (P1P2[0], yN1 - P1P2[0] * xN1)  # (N1N2) // (P1P2)
        M2theta2 = utils.droite_1Pt_alpha(M2, theta2)  # (M2theta2)
        N2 = utils.intersection_2droites(N1N2, M2theta2)  # print 'N2 : ',N2
        xN2 = N2[0]
        yN2 = N2[1]

        # domaine angulaire alpha2
        xQ2 = xM2 + R2 * math.cos(theta2)
        yQ2 = yM2 + R2 * math.sin(theta2)
        Q2 = (xQ2, yQ2)  # print 'Q2 : ',Q2
        OQ2 = math.sqrt(xQ2 ** 2 + yQ2 ** 2)
        alpha2 = math.acos(xQ2 / OQ2)  # print 'alpha 2 = ',alpha2*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 2:')
        print('  - centre M2 = ', M2)
        print('  - rayon R2  = ', R2)
        print('  - theta 2   = ', theta2 * 180. / math.pi)
        print('  - alpha 2   = ', alpha2 * 180. / math.pi)

        #################

        # cercle 3: centre M3, rayon R3, lim angulaire theta3
        P2P3 = utils.droite_2Pts(P[2], P[3])  # (P2P3)
        N2N3 = (P2P3[0], yN2 - P2P3[0] * xN2)  # (N2N3) // (P2P3)
        # NON !! il faut prendre la parallele a la corde P3F et non a P3P4
        # P3P4=droite_2Pts(P[3],P[4])     #(P3P4)
        # N3E=(P3P4[0], yE-P3P4[0]*xE)    #(N3E) // (P3P4)
        P3F = utils.droite_2Pts(P[3], F)  # (P3F)
        N3E = (P3F[0], yE - P3F[0] * xE)  # (N3E ) // (P3F)

        # N3: intersection (N2N3) et (N3E)
        N3 = utils.intersection_2droites(N2N3, N3E)  # print 'N3 : ',N3
        theta3 = 3. * math.pi / n

        # M3: intersection (N3M3)=(N3theta3) et (N2M2)
        N2M2 = utils.droite_2Pts(N2, M2)  # (N2M2)
        N3theta3 = utils.droite_1Pt_alpha(N3, theta3)  # (N3theta3)
        M3 = utils.intersection_2droites(N2M2, N3theta3)  # print 'M3 : ',M3
        xM3 = M3[0]
        yM3 = M3[1]
        R3 = math.sqrt((xQ2 - xM3) ** 2 + (yQ2 - yM3) ** 2)  # print 'R3 = ',R3

        # domaine angulaire alpha3
        xQ3 = xM3 + R3 * math.cos(theta3)
        yQ3 = yM3 + R3 * math.sin(theta3)
        Q3 = (xQ3, yQ3)  # print 'Q3 : ',Q3
        OQ3 = math.sqrt(xQ3 ** 2 + yQ3 ** 2)
        alpha3 = math.acos(xQ3 / OQ3)  # print 'alpha 3 = ',alpha3*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 3:')
        print('  - centre M3 = ', M3)
        print('  - rayon R3  = ', R3)
        print('  - theta 3   = ', theta3 * 180. / math.pi)
        print('  - alpha 3   = ', alpha3 * 180. / math.pi)

        #################

        # cercle 4: centre M4, rayon R4
        xM4 = 0.
        yM4 = N3theta3[1]
        M4 = (xM4, yM4)  # print 'M4 : ',M4
        R4 = math.sqrt((xQ3 - xM4) ** 2 + (yQ3 - yM4) ** 2)  # print 'R4 = ',R4

        # informations
        print('-------------------------------------------')
        print('Arc type 4:')
        print('  - centre M4 = ', M4)
        print('  - rayon R4  = ', R4)

        #################


    #################  A N S E    A    9    C E N T R E S     #####################"
    elif n == 9:
        print('')
        print("- Construction des generatrices de l'anse a 9 centres par la methode de MICHAL")

        # cercle 1: centre M1, rayon R1, lim angulaire theta1
        wR1 = Rayons[0]
        R1 = wR1 * portee * 0.5  # print 'R1 = ',R1
        xM1 = xA - R1
        yM1 = 0.
        M1 = (xM1, yM1)  # print 'M1 : ',M1
        theta1 = math.pi / n  # print 'theta 1 = ',theta1

        # N1: intersection (AP1) et (M1theta1)
        AP1 = utils.droite_2Pts(A, P[1])  # (AP1)
        M1theta1 = utils.droite_1Pt_alpha(M1, theta1)  # (M1theta1)
        N1 = utils.intersection_2droites(AP1, M1theta1)  # print 'N1 : ',N1
        xN1 = N1[0]
        yN1 = N1[1]

        # domaine angulaire alpha1
        xQ1 = xM1 + R1 * math.cos(theta1)
        yQ1 = yM1 + R1 * math.sin(theta1)
        Q1 = (xQ1, yQ1)  # print 'Q1 : ',Q1
        OQ1 = math.sqrt(xQ1 ** 2 + yQ1 ** 2)
        alpha1 = math.acos(xQ1 / OQ1)  # print 'alpha 1 = ',alpha1*180./math.pi

        # centre sym M1 = M7
        xM7 = -xM1
        yM7 = 0.
        M7 = (xM7, yM7)  # print 'M7 : ',M7

        # informations
        print('-------------------------------------------')
        print('Arc type 1:')
        print('  - centre M1 = ', M1)
        print('  - rayon R1  = ', R1)
        print('  - theta 1   = ', theta1 * 180. / math.pi)
        print('  - alpha 1   = ', alpha1 * 180. / math.pi)

        #################

        # cercle 2: centre M2, rayon R2, lim angulaire theta2
        wR2 = Rayons[1]
        R2 = wR2 * portee * 0.5  # print 'R2 = ',R2
        xM2 = xN1 - R2 * math.cos(theta1)
        yM2 = yN1 - R2 * math.sin(theta1)
        M2 = (xM2, yM2)  # print 'M2 : ',M2
        theta2 = 2. * math.pi / n

        # N2: intersection (N1N2) // (P1P2) et (M2theta2)
        P1P2 = utils.droite_2Pts(P[1], P[2])  # (P1P2)
        N1N2 = (P1P2[0], yN1 - P1P2[0] * xN1)  # (N1N2) // (P1P2)
        M2theta2 = utils.droite_1Pt_alpha(M2, theta2)  # (M2theta2)
        N2 = utils.intersection_2droites(N1N2, M2theta2)  # print 'N2 : ',N2
        xN2 = N2[0]
        yN2 = N2[1]

        # domaine angulaire alpha2
        xQ2 = xM2 + R2 * math.cos(theta2)
        yQ2 = yM2 + R2 * math.sin(theta2)
        Q2 = (xQ2, yQ2)  # print 'Q2 : ',Q2
        OQ2 = math.sqrt(xQ2 ** 2 + yQ2 ** 2)
        alpha2 = math.acos(xQ2 / OQ2)  # print 'alpha 2 = ',alpha2*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 2:')
        print('  - centre M2 = ', M2)
        print('  - rayon R2  = ', R2)
        print('  - theta 2   = ', theta2 * 180. / math.pi)
        print('  - alpha 2   = ', alpha2 * 180. / math.pi)

        #################

        # cercle 3: centre M3, rayon R3, lim angulaire theta3
        wR3 = Rayons[2]
        R3 = wR3 * portee * 0.5  # print 'R3 = ',R3
        xM3 = xN2 - R3 * math.cos(theta2)
        yM3 = yN2 - R3 * math.sin(theta2)
        M3 = (xM3, yM3)  # print 'M3 : ',M3
        theta3 = 3. * math.pi / n

        # N3: intersection (N2N3) // (P2P3) et (M3theta3)
        P2P3 = utils.droite_2Pts(P[2], P[3])  # (P2P3)
        N2N3 = (P2P3[0], yN2 - P2P3[0] * xN2)  # (N2N3) // (P2P3)
        M3theta3 = utils.droite_1Pt_alpha(M3, theta3)  # (M3theta3)
        N3 = utils.intersection_2droites(N2N3, M3theta3)  # print 'N3 : ',N3
        xN3 = N3[0]
        yN3 = N3[1]

        # domaine angulaire alpha3
        xQ3 = xM3 + R3 * math.cos(theta3)
        yQ3 = yM3 + R3 * math.sin(theta3)
        Q3 = (xQ3, yQ3)  # print 'Q3 : ',Q3
        OQ3 = math.sqrt(xQ3 ** 2 + yQ3 ** 2)
        alpha3 = math.acos(xQ3 / OQ3)  # print 'alpha 3 = ',alpha3*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 3:')
        print('  - centre M3 = ', M3)
        print('  - rayon R3  = ', R3)
        print('  - theta 3   = ', theta3 * 180. / math.pi)
        print('  - alpha 3   = ', alpha3 * 180. / math.pi)

        #################

        # cercle 4: centre M4, rayon R4, lim angulaire theta4
        P3P4 = utils.droite_2Pts(P[3], P[4])  # (P3P4)
        N3N4 = (P3P4[0], yN3 - P3P4[0] * xN3)  # (N3N4) // (P3P4)
        # NON !! il faut prendre la parallele a la corde P4F et non a P4P5
        # P4P5=droite_2Pts(P[4],P[5])     #(P4P5)
        # N4E=(P4P5[0], yE-P4P5[0]*xE)    #(N4E) // (P4P5)
        P4F = utils.droite_2Pts(P[4], F)  # (P4F)
        N4E = (P4F[0], yE - P4F[0] * xE)  # (N4E ) // (P4F)

        # N4: intersection (N3N4) et (N4E)
        N4 = utils.intersection_2droites(N3N4, N4E)  # print 'N4 : ',N4
        theta4 = 4. * math.pi / n

        # M4: intersection (N4M4)=(N4theta4) et (N3M3)
        N3M3 = utils.droite_2Pts(N3, M3)  # (N3M3)
        N4theta4 = utils.droite_1Pt_alpha(N4, theta4)  # (N4theta4)
        M4 = utils.intersection_2droites(N3M3, N4theta4)  # print 'M4 : ',M4
        xM4 = M4[0]
        yM4 = M4[1]
        R4 = math.sqrt((xQ3 - xM4) ** 2 + (yQ3 - yM4) ** 2)  # print 'R4 = ',R4

        # domaine angulaire alpha4
        xQ4 = xM4 + R4 * math.cos(theta4)
        yQ4 = yM4 + R4 * math.sin(theta4)
        Q4 = (xQ4, yQ4)  # print 'Q4 : ',Q4
        OQ4 = math.sqrt(xQ4 ** 2 + yQ4 ** 2)
        alpha4 = math.acos(xQ4 / OQ4)  # print 'alpha 4 = ',alpha4*180./math.pi

        # informations
        print('-------------------------------------------')
        print('Arc type 4:')
        print('  - centre M4 =  ', M4)
        print('  - rayon R4  = ', R4)
        print('  - theta 4   = ', theta4 * 180. / math.pi)
        print('  - alpha 4   = ', alpha4 * 180. / math.pi)

        #################

        # cercle 5: centre M5, rayon R5
        xM5 = 0.
        yM5 = N4theta4[1]
        M5 = (xM5, yM5)  # print 'M5 : ',M5
        R5 = math.sqrt((xQ4 - xM5) ** 2 + (yQ4 - yM5) ** 2)  # print 'R5 = ',R5

        # informations
        print('-------------------------------------------')
        print('Arc type 5:')
        print('  - centre M5 = ', M5)
        print('  - rayon R5  = ', R5)

    #################
    ## check
    # Points=[]
    # Points.append(O); Points.append(A); Points.append(B); Points.append(C); Points.append(D); Points.append(E)
    # for i in range(0, n-1, 1): Points.append(P[i+1])
    # Points.append(M1); Points.append(M2); Points.append(M3); Points.append(M4)
    # Points.append(Q1); Points.append(Q2); Points.append(Q3)
    # Points.append(N1); Points.append(N2); Points.append(N3)
    #
    # data=open('points.dat','w')
    # for pts in Points:
    #    data.write(str(pts[0])+' '+str(pts[1])+'\n')
    # data.close()

    print('R1/R2 = ', R1 / R2)
    if n >= 5: print('R2/R3 = ', R2 / R3)
    if n >= 7: print('R3/R4 = ', R3 / R4)
    if n >= 9: print('R4/R5 = ', R3 / R4)

    alpha = math.pi / n
    if n == 3: L_intrados = (2 * R1 + R2) * alpha; print('L_intrados = ', L_intrados)
    if n == 5: L_intrados = (2 * (R1 + R2) + R3) * alpha; print('L_intrados = ', L_intrados)
    if n == 7: L_intrados = (2 * (R1 + R2 + R3) + R4) * alpha; print('L_intrados = ', L_intrados)
    if n == 9: L_intrados = (2 * (R1 + R2 + R3 + R4) + R5) * alpha; print('L_intrados = ', L_intrados)

    generated_params = {"L_floor": L_floor,
                        "L_intrados": L_intrados,
                        "n": n,
                        "R1": R1,
                        "R2": R2,
                        "M1": M1,
                        "M2": M2,
                        "theta1": theta1
                        }

    if n >= 5:
        generated_params["R3"] = R3
        generated_params["M3"] = M3
        generated_params["theta2"] = theta2
    if n >= 7:
        generated_params["R4"] = R4
        generated_params["M4"] = M4
        generated_params["theta3"] = theta3
    if n >= 9:
        generated_params["R5"] = R5
        generated_params["M5"] = M5
        generated_params["theta4"] = theta4

    return generated_params