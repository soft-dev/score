import os
import json
import numpy

import pylmgc90.pre as pre

from arches_lib import generatrices
from arches_lib import description_discrete

root_directory = os.path.dirname(os.path.realpath(__file__))
data_directory = os.path.join(root_directory, "data")
datbox_directory = os.path.join(root_directory, "DATBOX")


def generate_sample(ark_type, ark_parameters):

    if ark_type == "Og":
        generated_params = generatrices.ogive(ark_parameters["portee0"],
                                              ark_parameters["fleche0"],
                                              ark_parameters["ep_bandeau0"])

        description_params = description_discrete.ogive(ark_parameters["fleche0"],
                                                        ark_parameters["ep_bandeau0"],
                                                        ark_parameters["bandeau_type0"],
                                                        ark_parameters["ancrage0"],
                                                        ark_parameters["Lc_blocs0"],
                                                        generated_params)
    elif ark_type == "Par":
        generated_params = generatrices.parabole(ark_parameters["portee0"],
                                                 ark_parameters["fleche0"],
                                                 ark_parameters["ep_bandeau0"])

        description_params = description_discrete.parabole(ark_parameters["portee0"],
                                                           ark_parameters["fleche0"],
                                                           ark_parameters["ep_bandeau0"],
                                                           ark_parameters["bandeau_type0"],
                                                           ark_parameters["ancrage0"],
                                                           ark_parameters["Lc_blocs0"],
                                                           generated_params)

    elif ark_type == "PC":
        generated_params = generatrices.plein_cintre(ark_parameters["portee0"],
                                                     ark_parameters["fleche0"],
                                                     ark_parameters["ep_bandeau0"],
                                                     ark_parameters["ep_rein0"],
                                                     ark_parameters["bandeau_type0"],
                                                     ark_parameters["ancrage0"])

        description_params = description_discrete.plein_cintre(ark_parameters["fleche0"],
                                                               ark_parameters["ep_bandeau0"],
                                                               ark_parameters["bandeau_type0"],
                                                               ark_parameters["ancrage0"],
                                                               ark_parameters["Lc_blocs0"],
                                                               generated_params)

    elif ark_type == "AP":

        with open(os.path.join(data_directory, "abaque_anse_panier_n_Centre.json"), 'r') as f:
            abaque = json.load(f)

        generated_params = generatrices.anse_panier(abaque,
                                                    ark_parameters["portee0"],
                                                    ark_parameters["fleche0"],
                                                    ark_parameters["ep_bandeau0"])

        description_params = description_discrete.anse_panier(ark_parameters["fleche0"],
                                                              ark_parameters["ep_bandeau0"],
                                                              ark_parameters["bandeau_type0"],
                                                              ark_parameters["ancrage0"],
                                                              ark_parameters["Lc_blocs0"],
                                                              generated_params)

    else:
        raise Exception("ark type not known : {}".format(ark_type))

    if not os.path.isdir(datbox_directory):
      os.mkdir(datbox_directory)

    # on se place en 2D
    dim = 2

    # creation des conteneurs
    bodies = pre.avatars()
    mat    = pre.materials()
    svs    = pre.see_tables()
    tacts  = pre.tact_behavs()

    # creations de deux materiaux
    tdur = pre.material(name='TDURx',materialType='RIGID',density=ark_parameters["Umas_bloc0"])
    stone = pre.material(name='STONE',materialType='RIGID',density=ark_parameters["Umas_bloc0"])
    mat.addMaterial(tdur,stone)

    # on cree un modele de rigide
    mod = pre.model(name='rigid', physics='MECAx', element='Rxx2D', dimension=dim)

    H = ark_parameters["h_remblais0"] + ark_parameters["fleche0"] + ark_parameters["ep_bandeau0"]
    i_bloc=0
    # on construit les voussoirs rigides
    for vertices in list(description_params["VERTICES"].values()):
        voussoir = pre.rigidPolygon(model=mod, material=stone, center=numpy.array([0., 0.]), color='REDxx',
                             generation_type='full',vertices=vertices)
        bodies.addAvatar(voussoir)

        ###############################################################################
        # LOADING ARCH :: PERMANENT LOADS ONLY
        X=vertices[:,0]; Y=vertices[:,1]; xmean=X.mean()
        if X.shape[0] == 4:                                  # voussoir classique
           if xmean >=0. : x0=X[2];x1=X[1]; y0=Y[1];y1=Y[2]  #   ** blocs REF
           else:           x0=X[3];x1=X[2]; y0=Y[3];y1=Y[2]  #   ** blocs SYM
        if X.shape[0] == 5:                                  # voussoir Tas de Charge
           if xmean >=0. : x0=X[3];x1=X[2]; y0=Y[2];y1=Y[2]  #   ** blocs REF
           else:           x0=X[3];x1=X[2]; y0=Y[2];y1=Y[2]  #   ** blocs SYM
        if X.shape[0] == 3:                                  # sommier
           if xmean >=0. : x0=X[2];x1=X[1]; y0=Y[1];y1=Y[2]  #   ** blocs REF
           else:           x0=X[2];x1=X[1]; y0=Y[2];y1=Y[1]  #   ** blocs SYM

        xi=(x0+x1)*0.5;yi=(y0+y1)*0.5; Li=abs(x0-x1)

        # Efforts
        Hi=H-yi
        P_remblais = Li*Hi*ark_parameters["h_remblais0"] ; Pu=Li*ark_parameters["lambda_u0"]
        Pi=Pu+P_remblais

        # Couples
        if X.shape[0] == 4 or  X.shape[0] == 3: # voussoir classique ou sommier
           # Barycentre du remblais: Rectangle (R) + Triangle (T)
           #   ** Rectangle: H=H-y1 x Li  **
           mR=(H-y1)*Li; yR=(H+y1)/2.; xR=xi
           #   ** Triangle: H=y1-y0,  Li  **
           mT=(y1-y0)*Li*0.5; yT=(y0+2*y1)/3.
           if xmean >= 0: xT=(x0+2*x1)/3.
           else: xT=(x1+2*x0)/3.
           xTR=xT+(mR/(mR+mT))*(xR-xT); yTR=yT+(mR/(mR+mT))*(yR-yT)
        elif X.shape[0] == 5: # voussoir Tas de Charge
           xTR = xi

        xG=voussoir.nodes[1].coor[0]
        Ci = Pu*(xG-xi) + P_remblais*(xG-xTR)
        #voussoir.imposeDrivenDof(component=[1, 2], dofty='vlocy')
        voussoir.imposeDrivenDof(component=2,ct=-Pi,dofty='force')
        voussoir.imposeDrivenDof(component=3,ct=-Ci,dofty='force')

        #print '------------------------------------------------'
        #print 'check'
        #print ''
        #print ' bloc ',i_bloc
        #print 'x0 = ',x0,' - x1 = ',x1,' - xi = ',xi,' - Li = ',Li
        #print 'y0 = ',y0,' - y1 = ',y1,' - yi = ',yi
        #print ''
        #print 'xTR = ',xTR
        #print 'xG = ',xG
        #print ''
        #print 'Pi = ',Ci
        #print 'Ci = ',Pi
        #print ''
        #i_bloc+=1

        ###############################################################################


    # ajout d'une fondation, i.e. faite d'un jonc :
    down=pre.rigidJonc(model=mod, material=tdur, center=numpy.array([0., -0.25*description_params["e_bloc"]]),
                       color='GREEN', axe1=generated_params["L_floor"] + 0.5*description_params["e_bloc"],
                       axe2=0.25*description_params["e_bloc"])
    bodies.addAvatar(down)
    down.imposeDrivenDof(component=[1, 2, 3], dofty='vlocy')



    # gestion des interactions :
    #   * declaration des lois
    if ark_parameters["mortar_type0"] == 'M4':
       law_name_blocs='M4___'
       mu_joint=0.75
       kn_joint=8.2e+10                     # en N/m^3 (i.e. Pa/m)
       kt_joint=3.6e+10                     # en N/m^3 (i.e. Pa/m)
       sigmaMaxI_joint=2.5e+05              # en Pa = N/m^2
       GI_joint=1.8e+01                     # en N/m (i.e. en J/m^2)
       sigmaMaxII_joint=1.4*sigmaMaxI_joint # en Pa = N/m^2
       GII_joint=1.25e+02                   # en N/m (i.e. en J/m^2)
    elif ark_parameters["mortar_type0"] == 'M3':
       law_name_blocs='M3___'
       mu_joint=0.75
       kn_joint=8.2e+09                     # en N/m^3 (i.e. Pa/m)
       kt_joint=2.9e+10                     # en N/m^3 (i.e. Pa/m)
       sigmaMaxI_joint=2.0e+05              # en Pa = N/m^2
       GI_joint=1.2e+01                     # en N/m (i.e. en J/m^2)
       sigmaMaxII_joint=1.2*sigmaMaxI_joint # en Pa = N/m^2
       GII_joint=1.00e+02                   # en N/m (i.e. en J/m^2)
    elif ark_parameters["mortar_type0"] == 'M2':
       law_name_blocs='M2___'
       mu_joint=0.75
       kn_joint=8.2e+08                     # en N/m^3 (i.e. Pa/m)
       kt_joint=2.2e+10                     # en N/m^3 (i.e. Pa/m)
       sigmaMaxI_joint=1.75e+05             # en Pa = N/m^2
       GI_joint=1.0e+01                     # en N/m (i.e. en J/m^2)
       sigmaMaxII_joint=1.0*sigmaMaxI_joint # en Pa = N/m^2
       GII_joint=0.9e+02                    # en N/m (i.e. en J/m^2)
    elif ark_parameters["mortar_type0"] == 'M1':
       law_name_blocs='M1___'
       mu_joint=0.75
       kn_joint=8.2e+07                     # en N/m^3 (i.e. Pa/m)
       kt_joint=1.8e+10                     # en N/m^3 (i.e. Pa/m)
       sigmaMaxI_joint=1.25e+05             # en Pa = N/m^2
       GI_joint=0.9e+01                     # en N/m (i.e. en J/m^2)
       sigmaMaxII_joint=0.7*sigmaMaxI_joint # en Pa = N/m^2
       GII_joint=6.12e+01                   # en N/m (i.e. en J/m^2)
    elif ark_parameters["mortar_type0"] == 'M0':
       law_name_blocs='M0___'
       mu_joint=0.75

    if law_name_blocs == 'M0___':
       lplpl=pre.tact_behav(name=law_name_blocs,law='IQS_CLB_g0',fric=mu_joint)
       tacts+=lplpl
    else:
       lplpl=pre.tact_behav(name=law_name_blocs,law='IQS_MAL_CZM',dyfr=mu_joint,stfr=mu_joint,
                            cn=kn_joint,ct=kt_joint,smax=sigmaMaxI_joint,w=GI_joint)
       tacts+=lplpl

    lpljc=pre.tact_behav(name='iqsc1',law='IQS_CLB',fric=0.5)
    tacts+=lpljc
    #   * declaration des tables de visibilite
    svplpl = pre.see_table(CorpsCandidat='RBDY2', candidat='POLYG', colorCandidat='REDxx', behav=law_name_blocs,
                       CorpsAntagoniste='RBDY2', antagoniste='POLYG', colorAntagoniste='REDxx', alert=description_params["e_joint"])
    svs+=svplpl
    svpljc = pre.see_table(CorpsCandidat='RBDY2', candidat='POLYG', colorCandidat='REDxx', behav='iqsc1',
                       CorpsAntagoniste='RBDY2', antagoniste='JONCx', colorAntagoniste='GREEN', alert=description_params["e_joint"])
    svs+=svpljc

    # ecriture des fichiers
    pre.writeBodies(bodies, chemin=datbox_directory)
    pre.writeBulkBehav(mat, chemin=datbox_directory, dim=2)
    pre.writeTactBehav(tacts, svs, chemin=datbox_directory)
    pre.writeDrvDof(bodies, chemin=datbox_directory)
    pre.writeDofIni(bodies, chemin=datbox_directory)
    pre.writeVlocRlocIni(chemin=datbox_directory)

    post = pre.postpro_commands()
    nlgs = pre.postpro_command(name='SOLVER INFORMATIONS', step=1)
    post.addCommand(nlgs)
    pre.writePostpro(commands=post, parts=bodies, path=datbox_directory)

    return description_params["points_X"], description_params["points_Y"]