# -*- coding: utf-8 -*-

import sys
import os
from swaf.tools.network import get_ip


def compute(data_to_return):
    print(">>Le script est exécuté sur le serveur {}".format(get_ip()))
    print(">>Le chemin de l'exécutable python est {}".format(sys.executable))
    print(">>Le dossier de calcul est {}".format(os.path.dirname(os.path.realpath(__file__))))
    return data_to_return
