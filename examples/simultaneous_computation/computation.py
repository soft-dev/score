# -*- coding: utf-8 -*-

import numpy
import matplotlib.pyplot as plt
import socket
import time
from swaf.tools import messaging

plt.ioff()

def compute(values):
    
    a = plt.figure('test')
    axis = a.add_subplot(1, 1, 1)
    axis.plot([0,1,2,3],[1,2,3,4])
    messaging.send_data_ssh(a)
#     messaging.send_data_ssh({'tagada': 'tsointsoin', 2: [1,2,3,4]})
#     print("tagada tsointsoin")
    for i in range(20):
        messaging.send_status(i*5, f"computation in progress : {i*5}%")
        messaging.send_message(f"User message at {i}")
        time.sleep(2)
    return socket.gethostname(), numpy.array(values) * 2
