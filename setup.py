#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

from setuptools import setup, find_packages

setup(
    name='score',
    packages=find_packages(),
    include_package_data=True,

    install_requires=[
        'paramiko',
        'docker-py',
        'spython',
        'ipywidgets',
        'nbformat',
        'pycryptodomex',
        'requests',
        'IPython',
        'importlib_resources ; python_version < "3.7.0"',
    ],


    extras_require={
        'slurm': ['radical.saga'],
        'dev': ['pytest', 'sphinx', 'sphinx_rtd_theme'],
    }

)
