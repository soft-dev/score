# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import json
from shutil import rmtree
import traceback


class ScoreFileSystem:

    """ Class managing internal file system, created at the root of the computation directory.\n
    There is one file system on user side and on computation side, which does not contain the same files.\n
    The core configuration file and summaries exist only on user side, and the compute configuration only on computation
    side.\n
    Both of them contain inputs and outputs as pickle data.

    """

    MAIN_CONFIG_DIR = "$HOME/.score"
    MAIN_COMPUTE_DIR = "$HOME/score_computations"

    def __init__(self, root='', create_tree=False):

        self.is_relative = False
        if not root:
            self.is_relative = True
            self.root = '.'
        else:
            self.root = root

        # Main config directory
        self.score_directory = 'SCORE_MANAGE'

        self.main = os.path.join(root, self.score_directory)
        self.inputs = os.path.join(self.main, 'INPUTS')
        self.outputs = os.path.join(self.main, 'OUTPUTS')
        self.computation_summaries = os.path.join(self.main, 'COMPUTATIONS')

        self.developer_core_configuration_file = os.path.join(self.main, 'dev_core_config.json')
        self.core_configuration_file = os.path.join(self.main, 'core_config.json')
        self.compute_configuration_file = os.path.join(self.main, 'compute_config.json')

        self.log_file = os.path.join(self.main, 'score.log')
        self.error_file = os.path.join(self.main, 'score.error')
        self.com_file = os.path.join(self.main, 'score.com')

        self.socket_port_file = os.path.join(self.main, 'socket_port')

        # Module containing callbacks functions
        self.callbacks_modules = 'score_callbacks'
        self.callbacks_modules_file = os.path.join(self.main, f'{self.callbacks_modules}.py')

        if create_tree:
            self.create_tree()

    def make_main_dir(self):

        """ Create main folder used for SCORE management, and add inside a .gitignore file """

        if not os.path.isdir(self.main):
            os.mkdir(self.main)

        with open(os.path.join(self.main, '.gitignore'), 'w') as fd:
            fd.write('*')

    def create_tree(self):

        """ Create all folders needed for SCORE manage """

        self.make_main_dir()

        if not os.path.isdir(self.inputs):
            os.mkdir(self.inputs)

        if not os.path.isdir(self.outputs):
            os.mkdir(self.outputs)

    def get_core_configuration(self):

        """ Get SCORE core configuration file contents if exists """

        if os.path.isfile(self.core_configuration_file):
            try:
                with open(self.core_configuration_file, 'r') as fd:
                    return json.load(fd)
            except json.JSONDecodeError:
                traceback.print_exc()
                return {}
        else:
            return {}

    def set_core_configuration(self, configuration):

        """ Save configuration to SCORE core configuration file

        .. todo::
            Check configuration contents.
        """

        with open(self.core_configuration_file, 'w') as fd:
            json.dump(configuration, fd)

    def clean_core_configuration(self):

        """ Remove core configuration file and OUTPUTS data which come from previous computations """

        if os.path.isfile(self.developer_core_configuration_file):
            os.remove(self.developer_core_configuration_file)
        if os.path.isfile(self.core_configuration_file):
            os.remove(self.core_configuration_file)
        if os.path.isdir(self.outputs):
            for file in os.listdir(self.outputs):
                os.remove(os.path.join(self.outputs, file))
        if os.path.isdir(self.computation_summaries):
            rmtree(self.computation_summaries)

    def get_developer_core_configuration(self):

        """ Get SCORE developer core configuration file contents if exists """

        if os.path.isfile(self.developer_core_configuration_file):
            try:
                with open(self.developer_core_configuration_file, 'r') as fd:
                    return json.load(fd)
            except json.JSONDecodeError:
                traceback.print_exc()
                return {}
        else:
            return {}

    def set_developer_core_configuration(self, configuration):

        """ Save configuration to SCORE developer configuration file

        .. todo::
            Check configuration contents.
        """

        with open(self.developer_core_configuration_file, 'w') as fd:
            json.dump(configuration, fd)

    def get_compute_configuration(self):

        """ Get SCORE compute configuration file contents if exists """

        if os.path.isfile(self.compute_configuration_file):
            try:
                with open(self.compute_configuration_file, 'r') as fd:
                    return json.load(fd)
            except json.JSONDecodeError:
                traceback.print_exc()
                return {}
        else:
            return {}

    def set_compute_configuration(self, configuration):

        """ Save configuration to SCORE compute configuration file

        .. todo::
            Check configuration contents.
        """

        with open(self.compute_configuration_file, 'w') as fd:
            json.dump(configuration, fd)

    def summaries_exist(self):

        """ Check if computation summary files exists """

        if not os.path.isdir(self.computation_summaries):
            return False
        files = [os.path.join(self.computation_summaries, f) for f in os.listdir(self.computation_summaries) if
                 os.path.isfile(os.path.join(self.computation_summaries, f))]
        if len(files):
            return True
        else:
            return False

    def get_all_summaries(self):

        """ Return a list containing all the computations (running or over) summaries """

        all_summaries = list()

        if not os.path.isdir(self.computation_summaries):
            return all_summaries

        files = [os.path.join(self.computation_summaries, f) for f in os.listdir(self.computation_summaries) if
                 os.path.isfile(os.path.join(self.computation_summaries, f))]
        files.sort()
        for summary_file in files:
            try:
                with open(summary_file, 'r') as fd:
                    all_summaries.append(json.load(fd))
            except:
                pass
        return all_summaries
