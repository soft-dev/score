# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import json
import ipywidgets as widgets
from IPython.display import display, HTML, Javascript


class ComputeWidget:

    """ A computation widget composed by a compute and a log button, and as much ComputationStatusWidget as running
    computation.

    :param reconnect_fc: (callable) function to call if trying to reconnect to previous computations.\n
                         That function is usually the one included in the score.Core object which will launch the
                         computation (Core.reconnect())

    .. todo::
        Find an other way to show logs.\n
        Currently it uses button tooltips to not be too intrusive in page, and as javascript is not working in Voilà.
    """

    def __init__(self, reconnect_fc=None):

        self.reconnect_fc = reconnect_fc

        self.button_desc = 'Compute'
        self.logs = ['SCORE logs :']
        self.displayed = False
        self.computations = list()

        self.reconnect_mode = False

        self.compute_function = None

        self.score_core_widget = None
        self.full_widget = None

        self.select_server = None

        self.compute_button = widgets.Button(
            description=self.button_desc,
            disabled=False,
            button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click to compute',
        )
        self.compute_button.add_class('score_button')

        self.logs_button = widgets.Button(
            description='Logs',
            disabled=True,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='\n'.join(self.logs),
            layout=widgets.Layout(width='50px')
        )
        self.logs_button.add_class('score_button')

        self.error_button = widgets.Button(
            description='Error',
            disabled=True,
            button_style='danger',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='',
            layout=widgets.Layout(width='50px', visibility='hidden')
        )

        self.status = widgets.Label(
            value="",
            layout=widgets.Layout(margin='0px')
        )

        self.progress_status = widgets.FloatProgress(
            value=0.,
            min=0.,
            max=100.,
            layout=widgets.Layout(width='150px', height='10px', margin='0px', visibility="hidden")
        )

    def get_compute_button(self):

        """ Return the compute button. """

        return self.compute_button

    def button_clicked(self, button):

        """ Callback function linked to click event on compute button. """

        if callable(self.compute_function):
            self.compute_function()
        else:
            self.append_log('There is no function linked to computation')

    def display_widget(self, compute_function, vertical=False, disabled=False, compute_text=None):

        """ Function associating compute widget to callback function, and displaying the full widget in Jupyter.

        :param compute_function: (callable) to call to launch computation.
        :param vertical: (boolean, optional) whether the full widget must be vertical or not.
        :param disabled: (boolean, optional) whether to disable the button or not.
        :param compute_text: (str, optional) if developer want to custom text on compute button instead of 'Compute'.
        """

        self.compute_function = compute_function

        if compute_text is not None:
            self.button_desc = compute_text
            if not self.reconnect_mode:
                self.compute_button.description = compute_text

        self.compute_button.disabled = disabled
        self.compute_button.on_click(self.button_clicked)

        if vertical:
            self.score_core_widget = widgets.HBox([self.compute_button,
                                                   widgets.VBox([self.status, self.progress_status]),
                                                   widgets.VBox([self.logs_button, self.error_button])])
            self.full_widget = widgets.VBox([self.score_core_widget, ])
        else:
            self.score_core_widget = widgets.VBox([self.compute_button,
                                                   self.status,
                                                   self.progress_status,
                                                   widgets.HBox([self.logs_button, self.error_button])])
            self.full_widget = widgets.HBox([self.score_core_widget, ])

        if self.select_server is not None:
            self.score_core_widget.children = (*self.score_core_widget.children, self.select_server)

        display(HTML("""
            <style>
                .score_button {
                    border-radius: 5px;
                }
                .score_small_button {
                    width: auto;
                    font-size: 10px;
                    padding-left: 5px;
                    padding-right: 5px;
                }
                .score_compute_title > * .p-Collapse-header {
                    color: blue;
                    font-size: 16px;
                    padding-top: 3px;
                    padding-bottom: 3px;
                }
                .score_compute_title > * .p-Collapse-contents {
                    padding: 1px;
                }
            </style>
        """))

        display(self.full_widget)

        self.displayed = True

        if self.reconnect_mode:
            if callable(self.reconnect_fc):
                self.reconnect_fc()
            else:
                self.append_log('There is no function linked to connect back to computation')
            self.reconnect_mode = False

    def add_progress(self, compute_id=None):

        """ Add a ComputationStatusWidget to the full widget.

        :param compute_id: (int) ID of the associated computation.
        """

        new_progress = ComputationStatusWidget(compute_id)
        self.computations.append(new_progress)

        if self.full_widget:
            self.full_widget.children = (*self.full_widget.children, new_progress.widget)

    def get_callbacks(self, compute_id=None):

        """ Get the 2 callbacks functions (logs and status) of a specific ComputationStatusWidget.

        :param compute_id: (int) ID related to a specific ComputationStatusWidget
        :return: (tuple x3 of callable) the 3 callbacks function of the widgets ComputationStatusWidget to call when
                 the status changes, a log is received or an error occurred.
        """

        if compute_id is None:
            return self.computations[0].change_status, self.computations[0].append_log, \
                   self.computations[0].display_error_widget

        else:
            for computation in self.computations:
                if computation.compute_id == compute_id:
                    return computation.change_status, computation.append_log, computation.display_error_widget
            return None, None, None

    def link_kill_function(self, compute_id, kill_fc):

        """ Link the computation widget with a function to kill computation.

        :param compute_id: (int) ID of the related computation.
        :param kill_fc: (callable) function to call when the kill button is pressed.
        """

        for computation in self.computations:
            if computation.compute_id == compute_id:
                computation.link_kill_function(kill_fc)
                break

    def change_status(self, status, progress=None):

        """ Change the current status value, and show a progress if necessary

        :param status: (str) current status
        :param progress: (float, optional) progress in case of copy log.
        """

        if status:
            self.status.value = status

        if progress is not None and isinstance(progress, float):
            self.progress_status.layout.visibility = "visible"
            self.progress_status.value = progress
        else:
            self.progress_status.layout.visibility = "hidden"

    def append_log(self, logs):

        """ Called as callback from score.Core when new logs are wrote.

        :param logs: (str) new log.
        """

        self.logs.append(logs)
        self.logs_button.tooltip = '\n- '.join(self.logs)

    def display_error_widget(self, error):

        """ Make error button visible.

        :param error: (str) error from computation script.
        """

        self.error_button.tooltip = error
        self.error_button.layout.visibility = 'visible'

    def add_computation_server_select(self, computations_server):

        """ Add a widget to automatically select a computation server.\n
        Must be used only in development mode.

        :param computations_server: (dict) containing information about each computation server :\n
        {\n
            alias :\n
            {\n
                'ip_address': (str) ip_address (or dns name) of the server,\n
                'username': (str) username used to connect to the server using SSH,\n
                'ssh_key': (str) path to the SSH private key file allowed to connect to the server\n
            },\n
            ...\n
        }\n
        """

        self.select_server = widgets.Dropdown(
            options=['No computation server'] + list(computations_server.keys()),
            value='No computation server',
            description='',
            disabled=False,
            layout=widgets.Layout(width='200px')
        )

    def show_logs(self, e):

        """ This function should write logs in a modal, but Javascript is not working in Voilà for now.\n

        .. warning::
            NOT WORKING IN VOILA-DASHBOARD.

        :param e:
        """

        display(Javascript(f"""
            require(
                ["base/js/dialog"],
                function(dialog)
                {{
                    dialog.modal({{
                        title: 'System logs',
                        body: ['<p>{"</p><p>".join(self.logs)}</p>'],
                        buttons: {{
                            'OK': {{}}
                        }}
                    }});
                }}
            );
        """))


class ComputationStatusWidget:

    """ A status widget composed by a status label and a progress bar.

    :param compute_id: (int) ID of the computation related to that widget.
    """

    def __init__(self, compute_id=None):

        """ Initialize the widget composed by a Label, a progress bar and a log button. """

        self.logs = ['Computation logs :']

        self.compute_id = compute_id

        self.status_label = widgets.Label(value="Waiting for computation...")
        self.progress_bar = widgets.IntProgress(
            value=0,
            min=0,
            max=100,
            layout=widgets.Layout(width='150px')
        )

        self.logs_button = widgets.Button(
            description='Logs',
            disabled=True,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='\n'.join(self.logs),
        )
        self.logs_button.add_class('score_button')
        self.logs_button.add_class('score_small_button')
        # self.logs_button.on_click(self.show_logs)

        self.kill_button = widgets.Button(
            description='STOP',
            button_style='danger',
            tooltip='KILL instantaneously the computation, not reversible',
            disabled=True
        )
        self.kill_button.add_class('score_button')
        self.kill_button.add_class('score_small_button')

        self.delete_dir = widgets.Checkbox(
            value=True,
            description='delete',
            disabled=False,
            indent=False,
            layout=widgets.Layout(width='auto')
        )

        self.title = widgets.Accordion([widgets.HBox([self.logs_button, self.kill_button, self.delete_dir])],
                                       selected_index=None)
        if compute_id is not None:
            self.title.set_title(0, f"Computation {compute_id} status")
        else:
            self.title.set_title(0, f"Computation status")
        self.title.add_class('score_compute_title')

        self.error_button = widgets.Button(
            description='Error',
            disabled=True,
            button_style='danger',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='',
            layout=widgets.Layout(visibility='hidden')
        )
        self.error_button.add_class('score_button')
        self.error_button.add_class('score_small_button')
        # self.error_button.on_click(self.show_error)

        self.widget = widgets.VBox([self.title, self.status_label,
                                    widgets.HBox([self.progress_bar, self.error_button])],
                                   layout=widgets.Layout(border='solid 1px', width='225px'))

    def append_log(self, logs):

        """ Called as callback from score.monitoring.ClientBase when new logs are wrote.

        :param logs: (str) new log.
        """

        self.logs.append(logs)
        self.logs_button.tooltip = '\n- '.join(self.logs)

    def display_error_widget(self, error):

        """ Make error button visible.

        :param error: (str) error from computation script.
        """

        self.error_button.tooltip = error
        self.error_button.layout.visibility = 'visible'

    def change_status(self, progress, message):

        """ Called as callback from score.monitoring.ClientBase when computation status changes.

        :param progress: (int) current computation progression (0-100).
        :param message: (str) current computation status.
        """

        self.status_label.value = message
        self.progress_bar.value = progress

    def link_kill_function(self, kill_fc):

        """ Link the current widget with a function to kill computation.

        :param kill_fc: (callable) function to call when user press KILL button.
        """

        def kill(button):
            # Do we have to disabled the button after click, as it is supposed to be usable only once ?
            # button.disabled = True
            if callable(kill_fc):
                kill_fc(self.delete_dir.value)

        self.kill_button.on_click(kill)
        self.kill_button.disabled = not callable(kill_fc)

    def show_logs(self, e):

        """ This function should write logs in a modal, but Javascript is not working in Voilà for now.\n

        .. warning::
            NOT WORKING IN VOILA-DASHBOARD.

        :param e:
        """

        display(Javascript(f"""
            require(
                ["base/js/dialog"],
                function(dialog)
                {{
                    dialog.modal({{
                        title: 'System logs',
                        body: ['<p>{"</p><p>".join(self.logs)}</p>'],
                        buttons: {{
                            'OK': {{}}
                        }}
                    }});
                }}
            );
        """))


def jupyter_disable_inputs(local_var, disabled=False):

    """ Disables all input and button ipywidget in environment in a dictionary of variables.

    :param local_var: (dict) of local variables (usually coming from 'dict(locals())' command).
    :param disabled: (boolean) desired status of disabled property of widgets.
    """

    for name, var in local_var.items():
        if isinstance(var, (widgets.ValueWidget, widgets.Button)):
            var.disabled = disabled


def jupyter_backup_inputs(local_var, backup_filename=''):

    """ Adds an observer on each input widget from ipywidget in a dictionary of variables, to save inputs current
    value to a file.\n
    This function can be called at the end of a notebook file to be able to retrieve input value even after shutting
    down notebook server.

    :param local_var: (dict) of local variables (usually coming from 'dict(locals())' command).
    :param backup_filename: (str, optional) name of backup file name.
    """

    def backup_input(e):

        """ Changes backup values dictionary and save it to a file.

        :param e: (dict) holding the information about the change (see traitlets events).
        """

        for name, var in input_to_backup.items():
            if e['owner'] == var:
                current_backuped_values[name] = e['new']
                break
        with open(backup_filename, 'w') as fp:
            json.dump(current_backuped_values, fp)

    if not backup_filename:
        backup_filename = '.{}.json'.format('__SCORE_input_backup__')
    if os.path.isfile(backup_filename):
        with open(backup_filename, 'r') as fp:
            current_backuped_values = json.load(fp)
    else:
        current_backuped_values = {}

    input_to_backup = {}
    input_id_backuped = []

    for name, var in local_var.items():
        if isinstance(var, widgets.ValueWidget) and var.model_id not in input_id_backuped:
            var.observe(backup_input, names='value')
            input_to_backup[name] = var
            input_id_backuped.append(var.model_id)
            if name in current_backuped_values.keys():
                try:
                    var.value = current_backuped_values[name]
                except:
                    print("Error while trying to load {} in variable '{}'".format(current_backuped_values[name], name))


def jupyter_request_token(core, label=None, test_token_url=None, callback=None):

    """ Display a Jupyter Notebook box which allow a user to paste a token and to store it in a score.Core object.

    :param core: (score.Core object).
    :param label: (str) message shown above the input.
    :param test_token_url: (str) URL to test the validity of the token.
    :param callback: (callable) callback function if token is working fine.
    """

    def set_token(widget):

        """ Set the token pasted by user in score.Core object.

        :param widget: (ipywidgets.Widgets object, not used) which called the function as callback.
        """

        if core.set_token(score_request_token.value, test_token_url):
            if callback is not None:
                callback()

    if not isinstance(label, str):
        label = "That application need your SCORE server token"
    request_token_label = widgets.Label(value=label)
    score_request_token = widgets.Text(
        disabled=False,
        placeholder='Paste your SCORE server token here',
    )
    validate_score_request_token = widgets.Button(
        description='OK',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
    )
    validate_score_request_token.on_click(set_token)
    display(widgets.VBox([request_token_label,
                          widgets.HBox([score_request_token, validate_score_request_token])
                          ])
            )
