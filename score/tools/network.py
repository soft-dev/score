# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import socket


def get_ip():

    """ Function returning network ip address of computer/server (one of them).\n
    The idea is to reach the port 1 (tcpmux) of fictif private network.\n
    This is done by one of the network ip address, don't know what append if several IP address exists.

    .. todo::
        make some tests with several network IP address for reliability

    :return: (str) IP address of server if it is on network, localhost IP address otherwise.
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


def get_free_port():

    """ Get a free port on local host.

    .. todo::
        Avoid risk of concurrent use of one port if this function is called 2 times and return same value 2 times.

    :return: (int) of an unused port.
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    port = s.getsockname()[1]
    s.close()
    return port
