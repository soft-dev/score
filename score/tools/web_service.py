import os
import json
import requests
import datetime
import zipfile


def request_server(url, token=None, body=None, timeout=5.):
    """ Request a GET web service

    :param url: (str) URL of the web service
    :param token: (str, optional) token value of the user on the web service if an authentication is necessary.
                  If the token is not provided, the current object's one is used.
    :param body: (dict, optional) data to send to web service if necessary
    :param timeout: (float) timeout of request
    :return: (requests.Response object) from the web service
    """

    headers = {'Content-Type': 'application/json'}
    if token is not None:
        headers['Authorization'] = f'{token}'

    request_body = ''
    if body is not None:
        request_body = json.dumps(body)

    return requests.get(url, headers=headers, data=request_body, timeout=timeout)


def download_json_data(url, token=None, body=None):
    """ Download some JSON data from a web service.

    :param url: (str) URL of the web service
    :param token: (str, optional) token value of the user on the web service if an authentication is necessary.
                  If the token is not provided, the current object's one is used.
    :param body: (dict, optional) data to send to web service if necessary
    :return: (dict) representing the JSON data
    """

    response = request_server(url, token, body)

    if response.status_code in [200, 201]:
        return json.loads(response.content)
    else:
        raise Exception(f'Error downloading\n'
                        f'\tHTTP status code : {response.status_code}\n'
                        f'\tMessage : {json.loads(response.content)}')


def download_zip_data(url, directory, token=None, body=None):
    """ Download data from web service (zip archive) and unzip it in the specified directory.

    :param url: (str) URL of the web service
    :param directory: (str) local path where to unzip the downloaded data
    :param token: (str, optional) token value of the user on the web service if an authentication is necessary.
                  If the token is not provided, the current object's one is used.
    :param body: (dict, optional) data to send to web service if necessary
    """

    response = request_server(url, token, body)

    if response.status_code not in [200, 201]:
        raise Exception(f'Error downloading\n'
                        f'\tHTTP status code : {response.status_code}\n'
                        f'\tMessage : {json.loads(response.content)}')

    zip_file_name = f'SCORE_temp_{datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")}.zip'
    with open(zip_file_name, 'wb') as fd:
        for chunk in response.iter_content(chunk_size=256):
            fd.write(chunk)
    with zipfile.ZipFile(zip_file_name, 'r') as zip_ref:
        zip_ref.extractall(directory)
    os.remove(zip_file_name)
