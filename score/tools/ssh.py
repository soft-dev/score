# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import posixpath
import stat
import time
import random
import string
from threading import Thread
from stat import S_ISDIR, S_ISREG
from Cryptodome.PublicKey import RSA
import socket
import paramiko

from .RegexCallbacks import RegexCallbacks
from ..statics.file_system import ScoreFileSystem


class SSHHandler(paramiko.SSHClient, Thread):

    """ Class used to simplify the usage of paramiko (ssh), and to add 2 features :\n
    - execute a detached command, meaning that the ssh session can be closed without breaking the computation\n
    - execute a command in a channel, allowing to stream the output without waiting for the end of the process (for
      example to follow the modification of a file : 'tail -f').

    :param server: (str) IP or DNS name of the server to connect to.
    :param username: (str) username of the session on the remote server.
    :param key_file: (str) path of the private key file to use for connection.
    """

    def __init__(self, server, username, key_file):

        """ Initializes all attributes of SSH object """

        paramiko.SSHClient.__init__(self)
        Thread.__init__(self)

        self.server = server
        self.username = username
        self.pkey = paramiko.RSAKey(filename=key_file)
        # self.key_file = key_file
        self.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # Used to execute a command in an interactive channel, following the output of the command
        self.chan_stdin = None
        self.chan_stdout = None
        self.chan_stderr = None
        self.channel_pid = None

        self.analyse_received = RegexCallbacks()
        self.period = 0.2
        self.carry_on = True

        self.commands = list()
        self.detached_commands = list()
        self.cmd_channel = ''
        self.remote_prompt = ''

        self.reconnect()

    def reconnect(self):
        # super().connect(self.server, username=self.username, key_filename=self.key_file,
        #                 look_for_keys=False, allow_agent=False)
        super().connect(self.server, username=self.username, pkey=self.pkey,
                        look_for_keys=False, allow_agent=False)

    def simple_command(self, command, bufsize=-1, timeout=None, environment=None, binary_output=False,
                       close_session=False):

        """ Execute a command on an opened ssh connection.

        :param command: (str) command to execute on remote server.
        :param bufsize: (int, optional) size of the buffer.
        :param timeout: (int, optional) set command's channel timeout.\n
                        See `paramiko.Channel.settimeout` for command execution.
        :param environment: (dict, optional) a dict of shell environment variables, to be merged into the default
                            environment that the remote command executes within.

            .. warning::
                Servers may silently reject some environment variables; see the warning in
                `paramiko.Channel.set_environment_variable` for details.
        :param binary_output: (boolean, optional) whether we want the output in binary format and not decoded in
                              UTF-8 ASCII.
        :param close_session: (boolean, optional) whether to close session after execution.\n
                              Allow to use this object in one shot command.
        :return:
            (str) standard output of the remote command.
            (str) standard error of the remote command.
        """

        _, stdout, stderr = self.exec_command(command, bufsize=bufsize, timeout=timeout,
                                              get_pty=False, environment=environment)
        self.commands.append(command)
        stdout = stdout.read().strip()
        if not binary_output:
            stdout = stdout.decode('utf-8')
        stderr = stderr.read().decode('utf-8').strip()

        if close_session:
            self.kill()
        return stdout, stderr

    def exec_detach(self, command, error_file='', bufsize=-1, timeout=None, environment=None):

        """ Execute a detached command on remote server, without waiting for the end of process.

        :param command: (str) command to execute on remote server in detached mode.
        :param error_file: (str, optional) file for command's errors redirection, because there is not stdout in
                           detached mode. Creates a random name in remote temporary folder if not populated.
        :param bufsize: (int, optional) size of the buffer.
        :param timeout: (int, optional) set command's channel timeout.\n
                        See `paramiko.Channel.settimeout` for command execution.
        :param environment: (dict, optional)  a dict of shell environment variables, to be merged into the default
                            environment that the remote command executes within.

            .. warning::
                Servers may silently reject some environment variables; see the warning in
                `paramiko.Channel.set_environment_variable` for details.
        :return:
            (int) remote PID.
            (str) remote error file path.
        """

        if not error_file:
            error_file = '/tmp/{}'.format(random_string_digits())

        # detach_command = 'screen -d -m `{} > /dev/null 2>> {}`'.format(command, error_file)
        detach_command = '{} > /dev/null 2>> {} &'.format(command, error_file)
        self.exec_command(detach_command, bufsize=bufsize, timeout=timeout, get_pty=False, environment=environment)
        return error_file

    def exec_channel(self, command):

        """ Executes a command in a specific channel, and run a thread for periodically read outputs.

        :param command: (str) command to execute on remote server.
        """

        self.cmd_channel = command
        self.chan_stdin, self.chan_stdout, self.chan_stderr = self.exec_command(command)
        self.channel_pid = self.get_pid(command)
        self.start()

    def run(self):

        """ Run an infinite loop in a thread to read new strings from remote process.\n
        Each received string is send to RegexCallbacks object to compare it to regex.

        .. todo::
             Fix: no exception caught if the file or the command doesn't exists.
        """

        while self.carry_on:
            if self.chan_stdout.channel.recv_ready():
                try:
                    recv_data = self.chan_stdout.channel.recv(9999).decode('utf-8').strip()
                    for line in recv_data.split('\n'):
                        line = line.strip()
                        if line and not line == self.remote_prompt and not line == self.cmd_channel:
                            if self.analyse_received.is_alive():
                                self.analyse_received.data_queue.put(line)
                            else:
                                print(line)
                except socket.timeout:
                    time.sleep(self.period)
            # TODO error is not catch if file or command doesn't exists ???
            # elif self.chan_stderr.channel.recv_ready():
            #     recv_error = self.chan_stderr.channel.recv(9999).decode('utf-8').strip()
            #     raise recv_error
            else:
                time.sleep(self.period)
        self.chan_stdin.close()
        self.chan_stdout.close()
        self.chan_stderr.close()
        self.exec_command(f'kill {self.channel_pid}')

    def get_pid(self, command):

        """ Get remote PID of a process with an exact command line.

        .. todo::
            - Fix empty return if 2 process have the exact same command.
            - Fix empty result if the command contains '&&' : there is 1 PID by sub-command.

        :param command: (str) full command line to use to retrieve process PID.\n
                              This can be different than sent command in case of singularity command for example, the
                              command could be 'singularity exec image_name command arg1 arg2' when the resulting
                              process would keep only 'command arg1 arg2'.
        :return: (int) PID of the process.

        """

        pid_command = "pgrep -fx '{}' -u {}".format(command, self.username)
        _, stdout, stderr = self.exec_command(pid_command)
        output = stdout.read().decode('utf-8').strip()
        try:
            return int(output)
        except:
            return 0

    def set_regex_callbacks(self, *args):

        """ Set the RegexCallback object associated to this object.\n
        See score.tools.RegexCallbacks for more information

        :param args: (((str), (function)), list of 2 values tuples), linking regex to function.
        """

        self.analyse_received.set_regex_callbacks(*args)

    def stop_channel(self):

        """ Stop the thread that read output from a remote process.\n
        As soon as the thread is stopped, the remote process is killed.
        """

        self.carry_on = False
        # Waiting to be sure the thread is over
        time.sleep(self.period + 0.1)

    def kill(self):

        """ Close the connection, the channel thread if necessary and the RegexCallbacks thread. """

        if self.is_alive():
            self.stop_channel()
        self.close()
        self.analyse_received.stop()


class SCPHandler(paramiko.SFTPClient):

    """ Class used to simplify the usage of paramiko (ssh) for SCP, allowing putting and getting directories.

    :param server: (str) IP or DNS name of the server to connect to.
    :param username: (str) username of the session on the remote server.
    :param key_file: (str) path of the private key file to use for connection.
    """

    def __init__(self, server, username, key_file):

        """ Initializes all attributes of SCP object. """

        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.pkey = paramiko.RSAKey(filename=key_file)
        # self.client.connect(server, username=username, key_filename=key_file, look_for_keys=False, allow_agent=False)
        self.client.connect(server, username=username, pkey=self.pkey, look_for_keys=False, allow_agent=False)
        self.transport = self.client.get_transport()
        self.chan = self.transport.open_session()
        if self.chan is not None:
            self.chan.invoke_subsystem("sftp")
            super().__init__(self.chan)

    def get(self, remote_path, local_path, callback=None, threading=False, one_shot=False):

        """ As this function is the one of paramiko (except for threading and one_shot), the docstring too.\n
        Copy a remote file (remote_path) from the SFTP server to the local host as local_path.\n
        Any exception raised by operations will be passed through.\n
        This method is primarily provided as a convenience.



        :param remote_path: (str) the remote file to copy.
        :param local_path: (str) the destination path on the local host.
        :param callback: (callable) optional callback function (form: func(int, int)) that accepts the bytes transferred
                         so far and the total bytes to be transferred.
        :param threading: (boolean) whether to execute the function in a specific thread.\n
                          This option is not compatible with one_shot as the function will not know when the copy is
                          over, so will not be able to close session.\n
                          To close properly the session you'll have to use the callback function.
        :param one_shot: (boolean) whether to close session after execution.\n
                         Allow to use this object in one shot command.
        """

        if threading:
            Thread(target=super().get, name='score-get', args=(remote_path, local_path, callback)).start()
        else:
            super().get(remote_path, local_path, callback)
            if one_shot:
                self.close()

    def put(self, local_path, remote_path, callback=None, confirm=True, threading=False, one_shot=False):

        """ As this function is the one of paramiko (except for threading and one_shot), the docstring too.\n
        Copy a local file (local_path) to the SFTP server as remote_path.\n
        Any exception raised by operations will be passed through.\n
        This method is primarily provided as a convenience.

        :param local_path: (str) the local file to copy.
        :param remote_path: (str) the destination path on the SFTP server. Note that the filename should be included.
                            Only specifying a directory may result in an error.
        :param callback: (callable) optional callback function (form: func(int, int)) that accepts the bytes transferred
                         so far and the total bytes to be transferred.
        :param confirm: (boolean) whether to do a stat() on the file afterwards to confirm the file size.
        :param threading: (boolean) whether to execute the function in a specific thread.\n
                          This option is not compatible with one_shot as the function will not know when the copy is
                          over, so will not be able to close session.\n
                          To close properly the session you'll have to use the callback function.
        :param one_shot: (boolean) whether to close session after execution.\n
                         Allow to use this object in one shot command.
        """

        if threading:
            Thread(target=super().put, name='score-put', args=(local_path, remote_path, callback, confirm)).start()
        else:
            super().put(local_path, remote_path, callback, confirm)
            if one_shot:
                self.close()

    def get_dir(self, source, target, one_shot=False):

        """ Copy remote directory contents to a local path.

        .. todo::
            Do we have to make it works in a thread ?

        :param source: (str) path to remote directory.
        :param target: (str) local path to copy remote directory.
        :param one_shot: (boolean) whether to close session after execution.\n
                         Allow to use this object in one shot command.
        """

        if not os.path.isdir(target):
            os.mkdir(target)
        for entry in self.listdir_attr(source):
            remote_path = os.path.join(source, entry.filename)
            local_path = os.path.join(target, entry.filename)
            mode = entry.st_mode
            if S_ISDIR(mode):
                if not os.path.isdir(local_path):
                    os.mkdir(local_path)
                self.get_dir(remote_path, local_path)
            elif S_ISREG(mode):
                self.get(remote_path, local_path)
        if one_shot:
            self.close()

    def put_dir(self, source, target, one_shot=False):

        """ Copy a local directory to a remote path.

        .. todo::
            Do we have to make it works in a thread ?

        :param source: (str) path to local directory to copy.
        :param target: (str) remote path.
        :param one_shot: (boolean) whether to close session after execution.\n
                         Allow to use this object in one shot command.
        """

        # Using lstat seems to be the best way to know if remote folder already exist, otherwise it raises an error
        try:
            self.lstat(target)
        except FileNotFoundError:
            self.mkdir(target, recursive=True)

        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                self.put(os.path.join(source, item), os.path.join(target, item))
            else:
                self.mkdir(os.path.join(target, item), ignore_existing=False)
                self.put_dir(os.path.join(source, item), os.path.join(target, item))
        if one_shot:
            self.close()

    def mkdir(self, path, mode=0o755, ignore_existing=False, one_shot=False, recursive=False):

        """ Create a directory on remote server.

        :param path: (str) name of the folder to create.
        :param mode: (int) permissions (posix-style) for the newly-created folder.
        :param ignore_existing: (boolean) to not raise error if the folder already exists.
        :param one_shot: (boolean) whether to close session after execution.\n
                         Allow to use this object in one shot command.
        :param recursive: (boolean) whether create nested folder or not.
        """

        try:
            super().mkdir(path, mode)
        except FileNotFoundError as e:
            if recursive:
                self.mkdir(os.path.dirname(path))
                super().mkdir(path, mode)
        except IOError:
            if ignore_existing:
                super().chmod(path, mode)
            else:
                if one_shot:
                    self.close()
                raise

        if one_shot:
            self.close()

    def rmtree(self, remote_path, one_shot=False):

        """ Remove remote directory and its content.\n
        Copied (and modified) from stackoverflow
        (https://stackoverflow.com/questions/3406734/how-to-delete-all-files-in-directory-on-remote-sftp-server-in-python)

        :param remote_path: (str) path of the directory to remove.
        :param one_shot: (boolean) whether to close session after execution.\n
                         Allow to use this object in one shot command.
        """

        for f in self.listdir_attr(remote_path):
            rpath = posixpath.join(remote_path, f.filename)
            if stat.S_ISDIR(f.st_mode):
                self.rmtree(rpath)
            else:
                rpath = posixpath.join(remote_path, f.filename)
                self.remove(rpath)
        self.rmdir(remote_path)

        if one_shot:
            self.close()

    def close(self):

        """ Close the connection """

        self.client.close()
        super().close()


def create_ssh_key_pair(ssh_keys_directory, ssh_key_name, username='unknown_user'):

    """ Create a SSH keys pair (public / private).

    :param ssh_keys_directory: (str) directory where to store keys.
    :param ssh_key_name: (str) name of the private key.
    :param username: (str) name of the keys user, to add information at end of the public key.
    :return: (str) path of the private key.
    """

    if not os.path.isdir(ssh_keys_directory):
        os.mkdir(ssh_keys_directory)
    key_path = os.path.join(ssh_keys_directory, ssh_key_name)
    if os.path.isfile(key_path):
        os.remove(key_path)

    key = RSA.generate(2048)
    key_info = bytes('  {}@SCORE\n'.format(username), 'utf-8')

    with open('{}.pub'.format(key_path), 'wb') as fp:
        fp.write(key.publickey().exportKey('OpenSSH') + key_info)
    with open(key_path, 'wb') as fp:
        fp.write(key.exportKey('PEM'))
        fp.flush()

    os.chmod(key_path, 0o400)

    return key_path


def deploy_ssh_public_key(client, key):

    """ Deploy the public key on an SSH client already connected.

    :param client: (paramiko.SSHClient) SSH client already connected.
    :param key: (str) value of public key decoded from hexadecimal format.
    :return: (boolean) whether public key has been deployed or not.
    """

    commands = ['mkdir -p ~/.ssh/',
                'if (! grep "{}" .ssh/authorized_keys ) > /dev/null 2>&1 ; ' \
                'then echo -e "{}" >> .ssh/authorized_keys; fi'.format(key, key),
                'chmod 644 ~/.ssh/authorized_keys',
                'chmod 700 ~/.ssh/']
    for command in commands:
        stdin, stdout, stderr = client.exec_command(command)
        err = stderr.read().decode('utf-8')
        if err:
            print("Error while executing {}".format(command))
            print(err)
            return False
    return True


def remove_remote_authorized_key(server, username, key_file, key_to_remove):

    """ Remove an authorized key from a remote server .ssh/authorized_keys file.

    :param server: (str) IP or DNS name of the server to connect to.
    :param username: (str) username of the session on the remote server.
    :param key_file: (str) path of the private key file to use for connection.
    :param key_to_remove: (str) value of public key (decoded from hexadecimal format) to remove.
    :return: (boolean) whether public key has been removed or not.
    """

    command = f'if test -f $HOME/.ssh/authorized_keys; then \
                    if grep -v "{key_to_remove}" $HOME/.ssh/authorized_keys > $HOME/.ssh/tmp; then \
                        cat $HOME/.ssh/tmp > $HOME/.ssh/authorized_keys && rm $HOME/.ssh/tmp; \
                    else \
                        rm $HOME/.ssh/authorized_keys && rm $HOME/.ssh/tmp; \
                    fi; \
                fi'
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    pkey = paramiko.RSAKey(filename=key_file)
    # client.connect(server, username=username, key_filename=key_file, look_for_keys=False, allow_agent=False)
    client.connect(server, username=username, pkey=pkey, look_for_keys=False, allow_agent=False)
    stdin, stdout, stderr = client.exec_command(command)
    err = stderr.read().decode('utf-8')
    if err:
        print("Error while executing {}".format(command))
        print(err)
        return False
    return True


def test_ssh_connection(server, username, key_file):

    """ Test SSH connection using key file.\n
    This test use 'whoami' command on remote server, and compare output to provided username.

    :param server: (str) IP address or hostname of remote server we'd like to connect to.
    :param username: (str) username used for the connection.
    :param key_file: (str) path of the private key file to use for connection.
    :return: (boolean) whether test passed or not.
    """

    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        pkey = paramiko.RSAKey(filename=key_file)
        # client.connect(server, username=username, key_filename=key_file, look_for_keys=False, allow_agent=False)
        client.connect(server, username=username, pkey=pkey, look_for_keys=False, allow_agent=False)
        stdin, stdout, stderr = client.exec_command('whoami')
        output = stdout.read().decode('utf-8').strip()
        client.close()
        if output != username:
            return False
        return True
    except:
        # print("Connection test failed")
        return False


def deploy_main_key(server, username, password, key_file):

    """ Deploy SSH public key to remote server using password connexion.\n
    This function allow using a new remote servers in SCORE without having previously copied the key.

    :param server: (str) IP address or hostname of remote server we'd like to copy the public key to.
    :param username: (str) username used for the connection.
    :param password: (str) password used for the connection.
    :param key_file: (str) path of the private key we want to use for next connection, used only to get public key file.
    :return: (boolean) whether the test_connection passed or not.
    """

    with open('{}.pub'.format(key_file), 'r') as fp:
        key = fp.read().strip()

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, username=username, password=password, look_for_keys=False, allow_agent=False)
    deployed = deploy_ssh_public_key(client, key)
    client.close()
    if deployed:
        return test_ssh_connection(server, username, key_file)
    else:
        return deployed


def deploy_additional_key(server, username, key_file, new_key):

    """ Deploy SSH public key to remote server using key pair connexion.\n
    This function is used to distribute a nex public key to a server already set up.\n
    Like that the same user can connect with several key pair, used on several servers.

    :param server: (str) IP address or hostname of remote server we'd like to copy the public key to.
    :param username: (str) username used for the connection.
    :param key_file: (str) path of the private key file to use for connection.
    :param new_key: (str) value of public key to deploy.
    :return: (boolean) whether the key has been deployed or not.
    """

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    pkey = paramiko.RSAKey(filename=key_file)
    # client.connect(server, username=username, key_filename=key_file, look_for_keys=False, allow_agent=False)
    client.connect(server, username=username, pkey=pkey, look_for_keys=False, allow_agent=False)
    deployed = deploy_ssh_public_key(client, new_key)
    client.close()
    return deployed


def create_remote_ssh_key(server, username, key_file):

    """ Create SSH key pair on remote server, and return public key hexadecimal value.\n
    The server must have been already set up and be able to be connected using key file.

    :param server: (str) IP address or hostname of remote server we'd like to create a key pair.
    :param username: (str) username used for the connection.
    :param key_file: (str) path of the private key file to use for connection.
    :return: (bytes) value of generated remote public key in hexadecimal.
    """

    if not create_remote_directory(server, username, key_file, ScoreFileSystem.MAIN_CONFIG_DIR):
        print('Error while creating SCORE directory')
        return b''
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    pkey = paramiko.RSAKey(filename=key_file)
    # client.connect(server, username=username, key_filename=key_file, look_for_keys=False, allow_agent=False)
    client.connect(server, username=username, pkey=pkey, look_for_keys=False, allow_agent=False)

    # Check if a SCORE key already exists
    _, stdout, stderr = client.exec_command(f'cat {ScoreFileSystem.MAIN_CONFIG_DIR}/score_id_rsa.pub')
    error = stderr.read().decode('utf-8')
    if not error:
        key = stdout.read()
        client.close()
        return key

    _, _, stderr = client.exec_command(f'yes y | ssh-keygen -b 2048 -t rsa -f '
                                       f'{ScoreFileSystem.MAIN_CONFIG_DIR}/score_id_rsa -q -N ""')
    error = stderr.read().decode('utf-8')
    if not error:
        stdin, stdout, stderr = client.exec_command(f'cat {ScoreFileSystem.MAIN_CONFIG_DIR}/score_id_rsa.pub')
        error = stderr.read().decode('utf-8')
        key = stdout.read()
        client.close()
        if not error:
            return key
        else:
            print('Error during key import : "{}"'.format(error))
            return b''
    else:

        print('Error during keygen : "{}"'.format(error))
        return b''


def create_remote_directory(server, username, key_file, directory, chmod=''):

    """ Create a directory on remote server, and return its path.\n
    The server must have been already set up and be able to be connected using key file.

    :param server: (str) IP address or hostname of remote server we'd like to create directory.
    :param username: (str) username used for the connection.
    :param key_file: (str) path of the private key file to use for connection.
    :param directory: (str) path of the directory we'd like to create on remote server.
    :param chmod: (str) permission mode according to Linux permission, composed by 3 numbers of 3 bits (0 to 7).
    :return: (str) path of the created directory, empty string if error occurred.
    """

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    pkey = paramiko.RSAKey(filename=key_file)
    # client.connect(server, username=username, key_filename=key_file, look_for_keys=False, allow_agent=False)
    client.connect(server, username=username, pkey=pkey, look_for_keys=False, allow_agent=False)
    command = 'mkdir -p {}'.format(directory)
    if chmod:
        command += ' -m {}'.format(chmod)
    stdin, stdout, stderr = client.exec_command(command)
    error = stderr.read().decode('utf-8')
    if error:
        print(error)
        return ''
    client.close()

    return directory


def random_string_digits(length=8):

    """ Generate a random string of letters and digits.

    :param length: (int) length of generated string.
    :return: (str) generated string.
    """

    length = max(length, 0)
    available_chars = string.ascii_letters + string.digits
    return ''.join(random.choice(available_chars) for i in range(length))
