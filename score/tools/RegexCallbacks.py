import re
import time
from threading import Thread
from queue import Queue
import inspect


class RegexCallbacks(Thread):

    """
    This class is used to compare string to regular expressions, and call a callback function when a regex matchs.\n
    It run in a specific thread to not disturb main process, and use queue for the list of strings to compare to regex.\n
    Main process only feed the queue each time a new string has to be compare.

    :param handler: (score.tools.ssh.SSHHandler, optional) linked to this object.\n
                    This handler is an object that can be returned in the callback function, if that callback function
                    allows 2 arguments.
    """

    def __init__(self, handler=None):

        super().__init__()
        self.carry_on = True
        self.handler = handler
        self.data_queue = Queue()
        self.regex_callbacks = list()

    def run(self):

        """ Run an infinite loop in a thread to compare new strings to regex. """

        while True:
            if not self.data_queue.empty():
                try:
                    data = self.data_queue.get_nowait()
                    self._match_regex(data)
                except:
                    pass
            else:
                if not self.carry_on:
                    break
                time.sleep(1)

    def stop(self):

        """ Change a flag to stop the infinite loop. """

        self.carry_on = False

    def set_regex_callbacks(self, *args):

        """ Link regular expressions to callback functions.

        :param args: (((str), (function)), list of 2 values tuples), linking regex to function.
        """

        callbacks_init = args
        callbacks = list()

        for callback in callbacks_init:
            if not isinstance(callback, tuple) or not len(callback) == 2:
                raise Exception(f'Error with arg \'{callback}\', this should be a tuple of 2 values')
            if not isinstance(callback[0], str):
                raise Exception(f'Error with arg \'{callback}\', first value must be a string')
            if not callable(callback[1]):
                raise Exception(f'Error with arg \'{callback}\', second value must be a function')

            # Check arguments list of the callback function, there should be 1 or 2 (match_object and optionally the
            # handler the current object)
            args_list = inspect.getfullargspec(callback[1]).args
            if args_list[0] == 'self':
                args_list = args_list[1:]
            if len(args_list) == 0 or len(args_list) > 2:
                raise Exception(f'Error with arg \'{callback}\', the function must allow 1 or 2 arguments')

            callbacks.append((callback[0], callback[1], (len(args_list) == 2)))

        for callback in callbacks:
            self.regex_callbacks.append(callback)

        if not self.is_alive():
            self.start()

    def _match_regex(self, data):

        """ Performs comparaison between a string and all regex.\n
        If a regex matchs, calls the callback function and stop comparaison.

        :param data: (str) to compare to all regex.
        """

        for regex_callback in self.regex_callbacks:
            mo = re.match(regex_callback[0], data)
            if mo:
                if regex_callback[2]:
                    self._call_function(regex_callback[1], mo, self.handler)
                else:
                    self._call_function(regex_callback[1], mo)
                break

    @staticmethod
    def _call_function(function, *args):

        """ Call the function with all parameters.\n
        Currently parameters contains the and match objects and optionally the handler connected to that object.

        :param function: (function) to call.
        :param args: (arguments) to send to function.
        """

        function(*args)
