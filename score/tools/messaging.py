# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import pickle
import json
from string import Template

from ..statics.file_system import ScoreFileSystem

"""
.. todo::
    Look for standards regex for message type.
"""

STATE_PATTERN = r"^##(?P<status>[0-9]*)##(?P<message>.*)"
STATE_TEMPLATE = Template("##${status}##${message}")

ERROR_PATTERN = r"^!!(?P<no_kill>[%]{2})?(?P<delete_dir>[&]{2})?(?P<message>.*)$"
ERROR_TEMPLATE = Template("!!${no_kill}${delete_dir}${message}")

MESSAGE_PATTERN = r"^>>(?P<message>.*)$"
MESSAGE_TEMPLATE = Template(">>${message}")

SYSTEM_MESSAGE_PATTERN = r"^<>(?P<message>.*)$"
SYSTEM_MESSAGE_TEMPLATE = Template("<>${message}")

PICKLE_DATA_PATTERN = r"^\?\?(?P<intermediate>[%]{2})?(?P<message>.*)"
PICKLE_DATA_TEMPLATE = Template("??${intermediate}${message}")

KILL_COMPUTATION_PATTERN = r"^KILL_COMPUTATION(?P<delete>[!]{2})?"
KILL_COMPUTATION_TEMPLATE = Template("KILL_COMPUTATION${delete}")


def send_status(status, msg):

    """ Allow user to send status message (the print is supposed captured by Capturing class). """

    print(_state(int(status), msg))


def send_message(msg):

    """ Allow user to send string data from his computation function. """

    print(_message(msg))


def send_data(data):

    """ Allow user to send complex intermediate data from his computation function. """

    fs = ScoreFileSystem(os.getcwd())
    data_path = os.path.join(fs.outputs, f'INTERMEDIATE_DATA.pkl')
    pickle.dump(data, open(data_path, 'wb'))
    data_dict = {'length': os.stat(data_path).st_size, 'path': data_path}
    print(_pickle_data(json.dumps(data_dict), intermediate=True))


# Retro compatibility
def send_data_ssh(data):
    print(_system_message("DEPRECATED : use messaging.send_data() instead of messaging.send_data_ssh() function."))
    send_data(data)


def _state(status, msg):

    """ Format status message. """

    return STATE_TEMPLATE.substitute({'status': status, 'message': msg})


def _error(msg, no_kill=False, delete_dir=False):

    """ Format error message. """

    return ERROR_TEMPLATE.substitute({'message': msg,
                                      'no_kill': '%%' if no_kill else '',
                                      'delete_dir': '&&' if delete_dir else ''})


def _message(msg):

    """ Format user message. """

    return MESSAGE_TEMPLATE.substitute({'message': msg})


def _system_message(msg):

    """ Format system message. """

    return SYSTEM_MESSAGE_TEMPLATE.substitute({'message': msg})


def _pickle_data(msg, intermediate=False):

    """ Format data message. """

    return PICKLE_DATA_TEMPLATE.substitute({'message': msg, 'intermediate': '%%' if intermediate else ''})


def _kill_computation(delete=False):

    """ Format kill computation message. """

    return KILL_COMPUTATION_TEMPLATE.substitute({'delete': '!!' if delete else ''})
