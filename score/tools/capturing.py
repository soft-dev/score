# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import sys


class Capturing:

    """ This class overwrite print standard class, to redirect the outputs of a function to our own stdout.\n
    What we called 'monitor' is one of (WorkerSocket, WorkerSsh) instance, but any class can be used as soon as it
    implements 'send_string' function.\n

    .. note::
        This object must be used as a context manager ('with' statement).

    :Example:
        >>> from score.monitoring import WorkerFile
        >>> worker_ssh = WorkerFile
        >>> with Capturing(worker_ssh):
        ...     function()

    :param monitor: one of (score.monitoring.WorkerLocal, score.monitoring.WorkerSocket, score.monitoring.WorkerSsh)
                    object.
    :param prefix: (str) optional, to prefix all output if necessary.
    """

    def __init__(self, monitor, prefix=""):

        self.monitor = monitor
        self.prefix = prefix

    def __enter__(self):

        """ Initialize context manager by changing stdout. """

        self._stdout = sys.stdout
        sys.stdout = self

    def write(self, message):

        """ Write message to our own stdout. """

        self.monitor.send_string(self.prefix + message)

    def __exit__(self, *args):

        """ Finalize context manager by resetting stdout. """

        sys.stdout = self._stdout
