# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

from ..statics.file_system import ScoreFileSystem


def config_computation_server(directory, server_ip, username, key_file):

    """ Add computation information in SCORE configuration file.

    :param directory: (str) path to the directory of the application.
    :param server_ip: (str) IP address or hostname of the new computation server.
    :param username: (str) username used for the connection.
    :param key_file: (str) path of the private key file to use for connection.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration['computation'] = {'ip_address': server_ip,
                                    'username': username,
                                    }
    configuration['key_file'] = key_file
    fs.set_core_configuration(configuration)


def config_storage_server(directory, local_path, key_file, remote=None):

    """ Add storage server information in SCORE configuration file.

    :param directory: (str) path to the directory of the application.
    :param local_path: (str) path on storage server where to save data.
    :param key_file: (str) path of the private key file to use for connection.
    :param remote: (dict, optional) Dictionary containing remote information :\n
    {\n
        "ip_address": (str) IP address or hostname of remote server we'd like to save data,\n
        "username": (str) username used for the connection,\n
        "computation_key": (str) value of computation public key to deploy\n
    }

    .. todo::
        check that remote dictionary contains all data.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration['storage'] = {'data_path': local_path}
    if remote:
        configuration['storage']['remote'] = remote
    configuration['key_file'] = key_file
    fs.set_core_configuration(configuration)


def remove_storage_server(directory):

    """ Remove storage server already set up.\n
    As storage server is optional, this function is used to restore default value (no storage server).

    :param directory: (str) path to the directory of the application.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    if 'storage' in configuration.keys():
        configuration.pop('storage')
    fs.set_core_configuration(configuration)
    

def config_simage(directory, simage_file):

    """ Add singularity image information in SCORE configuration file.

    :param directory: (str) path to the directory of the application.
    :param simage_file: (str) full path to the singularity image to use for computation.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration['simage_file'] = simage_file
    fs.set_core_configuration(configuration)


def config_callbacks_function(directory, callback_init=None, callback_over=None):

    """ Add callbacks function which will be called when computation begins and ends.

    :param directory: (str) path to the directory of the application.
    :param callback_init: (str) name of the function in 'fs.callbacks_module' to call when computation start.
    :param callback_over: (str) name of the function in 'fs.callbacks_module' to call when computation is over.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration['callback_functions'] = {}
    if callback_init:
        configuration['callback_functions']['init'] = callback_init
    if callback_over:
        configuration['callback_functions']['over'] = callback_over
    fs.set_core_configuration(configuration)


def config_token(directory, token):
    """ Add user token for Rest API.

    :param directory: (str) path to the directory of the application.
    :param token: (str) user token.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration["token"] = token
    fs.set_core_configuration(configuration)


def config_lock(directory, lock=True):

    """ Add "locked" parameter to avoid user to change configuration.

    :param directory: (str) path to the directory of the application.
    :param lock: (boolean) whether to lock or unlock configuration.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration["locked"] = lock
    fs.set_core_configuration(configuration)


def config_local_computation_directory(directory, computation_directory):

    """ Add a local computation directory path.

    :param directory: (str) path to the directory of the application.
    :param computation_directory: (str) path to directory of the computation.
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    configuration = fs.get_core_configuration()
    configuration["local_computation_directory"] = computation_directory
    fs.set_core_configuration(configuration)


def dev_config_computation_server(directory, alias, server):

    """ Append a computation server to the dedicated dictionary.

    :param directory: (str) path to the directory of the application.
    :param alias: (str) alias of the computation server, may be unique.
    :param server: (dict) describing the server, containing the following items :\n
    {\n
        'ip_address': (str) ip_address (or dns name) of the server,\n
        'username': (str) username used to connect to the server using SSH,\n
        'ssh_key': (str) path to the SSH private key file allowed to connect to the server\n
    }
    """

    fs = ScoreFileSystem(root=directory, create_tree=True)
    dev_configuration = fs.get_developer_core_configuration()
    if 'computation_servers' not in dev_configuration.keys():
        dev_configuration['computation_servers'] = dict()

    dev_configuration['computation_servers'][alias] = server

    fs.set_developer_core_configuration(dev_configuration)
