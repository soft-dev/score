# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import time
import traceback
from importlib import import_module

PID = os.getpid()

WorkerCom = getattr(import_module('score.monitoring'), '${worker}')

root_directory = os.path.dirname(os.path.realpath(__file__))


def main(port=None):
    score_worker = WorkerCom(root_directory, main_pid=PID, tcp_port=port)

    # TODO: remove that sleeping time in production
    while not score_worker.is_connection_established():
        time.sleep(0.5)

    try:
        # Get inputs (if necessary)
        score_worker.get_inputs()

        try:
            # Call initialization callback function (if necessary)
            score_worker.call_callback_init()

            # Download data from server (if necessary)
            score_worker.download_data()

            # Call compute function
            score_worker.compute()

            # Send returned values to script python environment
            score_worker.send_data(score_worker.results)

            # Save computation folder in a specific path on (remote) server
            score_worker.save_data()

            # Call finalization callback function (if necessary)
            score_worker.call_callback_over()
            score_worker.send_system_message("END")
        except:
            message = f"Error in calculation script : {os.linesep}"
            message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
            score_worker.send_error(message)
            raise

    except:
        message = f"Error in SCORE script : {os.linesep}"
        message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
        score_worker.send_error(message)
        raise
    finally:
        score_worker.stop()


if __name__ == "__main__":
    main()
