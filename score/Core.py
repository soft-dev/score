# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import sys
import string
import os
import json
import datetime
import pickle
import traceback
from tempfile import mkdtemp
from shutil import copytree, ignore_patterns, make_archive, rmtree
from importlib import import_module

try:
    from importlib import resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from threading import Semaphore

from .runner import RunnerLocal, RunnerSsh, RunnerSlurm
from .tools import ssh, notebook_lib, get_ip
from .monitoring import ClientLocal, ClientSsh, ClientSlurm, ClientSocket
from .statics.file_system import ScoreFileSystem
from . import templates
from .tools.web_service import download_json_data, download_zip_data


class Core:
    """ CoreBase is the main class of SCORE to export computation on remote server.

    :param monitoring_tool: (str) one of ['socket', 'ssh'], tool for client / worker communication.
    :param testing: (boolean) whether it is a test or a real computation.
    """

    monitoring_options = ['local', 'socket', 'ssh', 'slurm']

    def __init__(self, monitoring_tool='local', testing=False):

        self.testing = testing

        self.runner = RunnerLocal(testing=self.testing, log_callback=self._append_log, error_callback=self._show_error)
        self.local_computation_directory = ""

        self.monitoring_tool = monitoring_tool if monitoring_tool in self.monitoring_options else 'local'
        self.python_path = None

        self.callback_init_fn = None
        self.callback_over_fn = None
        self.make_temp = True

        self.containerisation = "singularity"

        self.monitors = list()

        self.token = None

        self.save_data = False
        self.status = "Initialisation"

        self.simultaneous_computation = 1
        self.launching_computation = Semaphore()

        # SCORE files and directories
        self.relative_fs = ScoreFileSystem(create_tree=True)

        self.developer_core_configuration = dict()
        self.core_configuration = dict()
        self.compute_configuration = dict()

        self.logs = []
        self.error = ''
        self.compute_widget = notebook_lib.ComputeWidget(reconnect_fc=self.reconnect)

        self._initialize()

    def _initialize(self):

        """ Initializes the score.Core object, retrieving configuration from files. """

        self._change_status("Initialization")

        self.developer_core_configuration = self.relative_fs.get_developer_core_configuration()
        if 'computation_servers' in self.developer_core_configuration:
            self.compute_widget.add_computation_server_select(self.developer_core_configuration['computation_servers'])

        self.core_configuration = self.relative_fs.get_core_configuration()

        if 'computation' in self.core_configuration.keys():
            self._append_log("Getting SSH computation information from file")
            calc = self.core_configuration['computation']
            self.set_computation_server(calc['ip_address'],
                                        calc['username'],
                                        key_file=self.core_configuration.get('key_file'),
                                        simage_file=self.core_configuration.get('simage_file'),
                                        remote_computation_directory=calc.get('computation_directory',
                                                                              ScoreFileSystem.MAIN_COMPUTE_DIR)
                                        )

        if 'slurm' in self.core_configuration.keys():
            self._append_log("Getting SLURM information from file")
            slurm = self.core_configuration['slurm']
            self.set_slurm_server(slurm['ip_address'],
                                  slurm['username'],
                                  queue=slurm.get('queue'),
                                  modules=slurm.get('modules'),
                                  key_file=self.core_configuration.get('key_file'),
                                  simage_file=self.core_configuration.get('simage_file'),
                                  remote_computation_directory=slurm.get('computation_directory',
                                                                         ScoreFileSystem.MAIN_COMPUTE_DIR))

        if 'storage' in self.core_configuration.keys():
            self._append_log("Getting storage from file")
            storage = self.core_configuration['storage']
            self.set_storage(storage['data_path'],
                             key_file=self.core_configuration.get('key_file'),
                             remote=storage.get('remote')
                             )

        if 'token' in self.core_configuration.keys():
            self.token = self.core_configuration['token']

        if 'callback_functions' in self.core_configuration.keys():
            self._append_log('Getting callbacks from file')
            self.set_callback_computation_init(self.core_configuration['callback_functions'].get('init'))
            self.set_callback_computation_over(self.core_configuration['callback_functions'].get('over'))

        if 'local_computation_directory' in self.core_configuration.keys():
            self.local_computation_directory = self.core_configuration['local_computation_directory']

        if self.core_configuration.get('locked'):
            self.set_local_server = self._set_server_locked
            self.set_computation_server = self._set_server_locked
            self.set_slurm_server = self._set_server_locked
            self.set_storage = self._set_storage_locked
            self.set_callback_computation_init = self._set_callbacks_locked
            self.set_callback_computation_over = self._set_callbacks_locked
            self.test_downloading_data = self._test_downloading_data_locked

        if self.relative_fs.summaries_exist():
            self._change_status("Reconnection to previous computations")
            self.compute_widget.reconnect_mode = True

        if len(self.monitors) >= self.simultaneous_computation:
            self._change_status("Computation(s) reconnected")
        else:
            self._change_status("Ready for computation")

    def compute(self, function, callback_intermediate_data=None, callback_final_data=None, **kwargs):

        """ Set up the application and run it using the runner.\n
        (1) setups application (create script, pickle data...)\n
        (2) adds server information in directory\n
        (3) launch the computation using the runner\n
        (4) save computation summary in JSON file (to connect monitoring client)\n
        (4) connect a monitoring client to follow computation

        :param function: (callable) function to execute (locally or remotely).
        :param callback_intermediate_data: (callable) function to call as callback when intermediate data are received.
        :param callback_final_data: (callable) function to call as callback when final data are received.
        :param kwargs: keywords arguments specifics to the 'function'.
        :return: (score.monitoring.Client) object connected to this computation.
        """

        if not self.runner.ready_to_compute:
            print("The remote computation has not been properly set up, computation is not available")
            return

        self.launching_computation.acquire()

        try:
            if self.compute_widget.select_server is not None:
                self._get_widget_selected_server()

            if len(self.monitors) >= self.simultaneous_computation:
                self._append_log(f"Your have already {len(self.monitors)} computation(s) running, "
                                 f"you're not allowed to run more computations")
                return None

            self.compute_configuration['computation_id'] = len(self.monitors) + 1

            if self.compute_widget is not None:
                self.compute_widget.add_progress(compute_id=self.compute_configuration['computation_id'])

            self._setup_application(function, **kwargs)

            storage_path = None
            if 'storage' in self.compute_configuration.keys() and \
                    'remote' not in self.compute_configuration['storage'].keys():
                storage_path = self.compute_configuration['storage']['data_path']

            self._change_status("Running computation")
            application_full_path = self.runner.compute(self.compute_configuration['application_src'], storage_path)

            if self.make_temp and self.runner.type != 'local':
                rmtree(os.path.dirname(self.compute_configuration['application_src']))

            computation_summary = self._set_computation_summary(application_full_path,
                                                                callback_intermediate_data,
                                                                callback_final_data)
            self._change_status("Connecting to computation")
            return self._connect_to_computation(computation_summary)

        except:
            traceback.print_exc()
            return None

        finally:
            self.launching_computation.release()
            if len(self.monitors) >= self.simultaneous_computation:
                self._change_status("Computation(s) started")
            else:
                self._change_status("Ready for computation")

    def set_local_server(self, computation_directory=None):

        """ Set runner as 'local' to perform computation on local server.

        :param computation_directory: (str, optional) path to the local computation directory.
        """

        self.runner = RunnerLocal(testing=self.testing, log_callback=self._append_log, error_callback=self._show_error)
        if computation_directory is not None:
            self.local_computation_directory = computation_directory
        self.monitoring_tool = 'local'

    def set_computation_server(self, server_ip, username, key_file=None, simage_file=None,
                               remote_computation_directory=ScoreFileSystem.MAIN_COMPUTE_DIR):

        """ Set runner as 'SSH' to perform computation on a remote server.

        :param server_ip: (str) IP address or hostname of remote server we'd like to use for computation.
        :param username: (str) username used for the connection.
        :param key_file: (str, optional) path of the private key file to use for connection.\n
                         This parameters is optional to stay generic, but should be filled. If not, ssh will try to
                         connect using user ssh key pair (see ssh documentation for more information).
        :param simage_file: (str, optional) local path of singularity image to use for computation.
        :param remote_computation_directory: (str, optional) remote path on computation server to process the
                                             computation, allows to process on SSD for faster access for example.
        """

        self.runner = RunnerSsh(testing=self.testing,
                                status_callback=self._change_status,
                                log_callback=self._append_log,
                                error_callback=self._show_error)
        self.monitoring_tool = 'ssh'
        server_ok = self.runner.set_server(server_ip,
                                           username,
                                           key_file=key_file,
                                           simage_file=simage_file,
                                           remote_computation_directory=remote_computation_directory)
        if server_ok:
            self.compute_configuration['computation'] = {'type': self.runner.type,
                                                         'ip_address': self.runner.ip_address,
                                                         'username': self.runner.username,
                                                         'key_file': self.runner.key_file,
                                                         'remote_path': self.runner.computation_dir}
        else:
            self.runner = RunnerLocal(testing=self.testing,
                                      log_callback=self._append_log,
                                      error_callback=self._show_error)
            self.monitoring_tool = 'local'

    def set_slurm_server(self, server_ip, username, queue=None, modules=None, key_file=None, simage_file=None,
                         remote_computation_directory=ScoreFileSystem.MAIN_COMPUTE_DIR):

        """ Set runner as 'SLURM' to perform computation on a remote server using SLURM jobs scheduler.

        :param server_ip: (str) IP address or hostname of remote server we'd like to use for computation.
        :param username: (str) username used for the connection.
        :param key_file: (str, optional) path of the private key file to use for connection.\n
                         This parameters is optional to stay generic, but should be filled. If not, ssh will try to
                         connect using user ssh key pair (see ssh documentation for more information).
        :param queue: (str, optional) name of the queue to use on slurm server (use default if None).
        :param modules: (str or list of str, optional) name of the modules to load on the cluster.
        :param simage_file: (str, optional) local path of singularity image to use for computation.
        :param remote_computation_directory: (str, optional) remote path on computation server to process the
                                             computation, allows to process on SSD for faster access for example.
        """

        self.runner = RunnerSlurm(testing=self.testing, log_callback=self._append_log, error_callback=self._show_error)
        self.monitoring_tool = 'slurm'
        server_ok = self.runner.set_server(server_ip,
                                           username,
                                           key_file=key_file,
                                           queue=queue,
                                           modules=modules,
                                           simage_file=simage_file,
                                           remote_computation_directory=remote_computation_directory)
        if server_ok:
            self.compute_configuration['computation'] = {'type': self.runner.type,
                                                         'ip_address': self.runner.ip_address,
                                                         'username': self.runner.username,
                                                         'key_file': self.runner.key_file,
                                                         'remote_path': self.runner.computation_dir}

        else:
            self.runner = RunnerLocal(testing=self.testing,
                                      log_callback=self._append_log,
                                      error_callback=self._show_error)
            self.monitoring_tool = 'local'

    def set_storage(self, data_path, key_file=None, remote=None):

        """ Set up a storage server for retrieving futures computation's data.\n
        The computation public key is copied to the storage server, so the computation is no dependant on
        SCORE main server anymore.

        .. todo::
            Test creating folder on storage server from computation server, instead of waiting the end of computation to
            find an issue.

        :param data_path: (str) path on server where data will be saved.
        :param key_file: (str, optional) path of the private key file to use to copy computation key.\n
                         This parameters is optional to stay generic, but should be filled. If not, ssh will try to
                         connect using user ssh key pair (see ssh documentation for more information).
        :param remote: (dict, optional) Dictionary containing remote information with the following keys :\n
                       {\n
                           "ip_address": (str) IP address or hostname of remote server we'd like to save data,\n
                           "username": (str) username used for the connection,\n
                           "computation_key": (str) value of computation public key to deploy\n
                       }\n
        :return: (boolean) whether the storage server can and will be used for future computation.
        """

        self.save_data = True

        if remote and remote['computation_key']:
            try:
                self.save_data = ssh.deploy_additional_key(remote['ip_address'],
                                                           remote['username'],
                                                           key_file,
                                                           remote['computation_key'])
                if not self.save_data:
                    self._append_log("Error during setting up storage server, the data will not be saved")
                    self._show_error(traceback.format_exc())
            except:
                self._append_log("Error during setting up storage server, the data will not be saved")
                self._show_error(traceback.format_exc())

        if self.save_data:
            self.compute_configuration['storage'] = {'data_path': data_path}
            if remote:
                self.compute_configuration['storage']['remote'] = {'ip_address': remote['ip_address'],
                                                                   'username': remote['username'],
                                                                   }
                if not remote['computation_key']:
                    self.compute_configuration['storage']['remote']['ssh_key_file'] = key_file
            else:
                self._append_log("No remote storage server provided, data will be saved on computation server")
        return self.save_data

    def set_simage(self, simage_file, simage_md5sum='', simage_definition_file=''):

        """ Set the singularity image to use for computation.\n
        It may not work if the current environment is a singularity environment and the computation run locally
        (it is not possible to run a singularity container from a singularity container).

        :param simage_file: (str) local path of singularity image to use for computation (or to build if necessary).
        :param simage_md5sum: (bytes) expected md5sum of the image.
        :param simage_definition_file: (str) path to singularity definition file, used to build image if it doesn't
                                       exist.\n
                                       The definition file is not kept if the code run in a singularity container, as it
                                       is not possible to build an image from a container.
        """

        self.runner.set_simage(simage_file, simage_md5sum=simage_md5sum, simage_definition_file=simage_definition_file)

    def download_json_data(self, url, body=None):

        """ Download some JSON data from a web service.

        :param url: (str) URL of the web service.
        :param body: (dict, optional) data to send to web service if necessary.
        :return: (dict) representing the JSON data.
        """

        return download_json_data(url, token=self.token, body=body)

    def set_data_to_download(self, url, directory, body):

        """ Add some data to download from the computation server before the beginning of the computation, in the
        compute configuration.\n
        The data must be downloaded using web URL (web service), and in a zipped archive.

        :param url: (str) URL of the web service.
        :param directory: (str) relative path where to store downloaded data.
        :param body: (dict, optional) data to send to web service if necessary.
        """

        if 'data_to_download' not in self.compute_configuration:
            self.compute_configuration['data_to_download'] = list()

        data_to_download = {'url': url,
                            'directory': directory,
                            'body': body}

        for d in self.compute_configuration['data_to_download']:
            if d == data_to_download:
                return

        self.compute_configuration['data_to_download'].append(data_to_download)

    def remove_data_to_download(self, url, directory, body):

        """ Remove some data previously set as data to download from the compute configuration.

        :param url: (str) URL of the web service.
        :param directory: (str) relative path where to store downloaded data.
        :param body: (dict, optional) data to send to web service if necessary.
        """

        if 'data_to_download' not in self.compute_configuration:
            return
        data_to_remove = {'url': url,
                          'directory': directory,
                          'body': body}
        for d in self.compute_configuration['data_to_download']:
            if d == data_to_remove:
                self.compute_configuration['data_to_download'].remove(d)
                break

    def test_downloading_data(self):

        """ DEVELOPMENT MODE ONLY : Download file data as zip from a web service.\n
        This function is useful for the development of the application, to ensure which files will be downloaded by
        the end user from the computation server.\n

        """

        if 'data_to_download' not in self.compute_configuration or not self.compute_configuration['data_to_download']:
            self._append_log('You have not set any data to download')
            return

        for d in self.compute_configuration['data_to_download']:
            download_zip_data(d['url'], d['directory'], token=self.token, body=d['body'])

    def allow_simultaneous_computation(self, simultaneous_computation):

        """ Allow user to run a several computations with the same score.Core object.

        :param simultaneous_computation: (int) maximum number of simultaneous computation allowed for user.
        """

        self.simultaneous_computation = simultaneous_computation

    def set_callback_computation_init(self, fn):

        """ Define the function to call when process is launched.

        :param fn: (function) function to call, which requires a dictionary as input describing servers.
        :return:
        """

        self.callback_init_fn = fn

    def set_callback_computation_over(self, fn):

        """ Define the function to call when process is over.

        :param fn: (function) function to call, which requires a dictionary as input describing servers.
        """

        self.callback_over_fn = fn

    def get_compute_button(self):

        """ Return associated compute button.

        .. warning::
            Works only in Jupyter Notebook environment.
        """

        return self.compute_widget.get_compute_button()

    def display_compute_widget(self, compute_function, vertical=False, disabled=False):

        """ Add a SCORE computation widget in the Notebook.

        .. warning::
            Works only in Jupyter Notebook environment.

        :param compute_function: (function) which will launch computation.
        :param vertical: (boolean, optional) whether we want a vertical widget or not.
        :param disabled: (boolean, optional) whether to disable the button or not.
        """

        self.compute_widget.display_widget(compute_function, vertical=vertical, disabled=disabled)

    def reconnect(self):

        """ Connect back to running or terminated computation(s) """

        for computation_summary in self.relative_fs.get_all_summaries():
            if self.compute_widget is not None:
                self.compute_widget.add_progress(compute_id=computation_summary['computation_id'])
                self._connect_to_computation(computation_summary)

    def _get_widget_selected_server(self):

        """ Setup server according to user selection, if the select server dropbox exists in widget. """

        alias = self.compute_widget.select_server.value
        if 'computation_servers' in self.developer_core_configuration.keys():
            if alias in self.developer_core_configuration['computation_servers'].keys():
                calc = self.developer_core_configuration['computation_servers'][alias]
                self.set_computation_server(server_ip=calc['ip_address'],
                                            username=calc['username'],
                                            key_file=calc['ssh_key'],
                                            simage_file=self.core_configuration.get('simage_file'),
                                            remote_computation_directory=calc.get('remote_computation_directory',
                                                                                  ScoreFileSystem.MAIN_COMPUTE_DIR)
                                            )
                self._append_log(f"Selected computation server : "
                                 f"{self.developer_core_configuration['computation_servers'][alias]}")

        if 'slurm_servers' in self.developer_core_configuration.keys():
            if alias in self.developer_core_configuration['slurm_servers'].keys():
                slurm = self.developer_core_configuration['slurm_servers'][alias]
                self.set_slurm_server(server_ip=slurm['ip_address'],
                                      username=slurm['username'],
                                      queue=slurm.get('queue'),
                                      modules=slurm.get('modules'),
                                      key_file=slurm['ssh_key'],
                                      simage_file=self.core_configuration.get('simage_file'),
                                      remote_computation_directory=slurm.get('remote_computation_directory',
                                                                             ScoreFileSystem.MAIN_COMPUTE_DIR)
                                      )
                self._append_log(f"Selected SLURM server : {self.developer_core_configuration['slurm_servers'][alias]}")

    def _setup_application(self, function, **kwargs):

        """ Setup user application by adding pickles for each parameters of the called function, and creating
        score_compute.py script which will execute function.

        :param function: (function) function that user want to compute.
        :param kwargs: keywords arguments specifics to the 'function'.
        """

        self._change_status("Setting up application")

        application_src = self._get_module_path(function)

        # Create a local computation directory
        if self.make_temp or self.local_computation_directory:
            application_src = self._copy_files(application_src, self.local_computation_directory)

        # Guess if computation is local or remote (useful for socket communication only)
        # TODO : find a generic way to get that information (127.0.0.1 is not generic at all)
        local = self.containerisation == 'docker' or self.runner.type == 'local' or \
                (self.runner.type == 'ssh' and self.compute_configuration['computation']['ip_address'] in ['127.0.0.1',
                                                                                                           'localhost'])

        # create compute configuration dictionary
        self.compute_configuration['application_folder'] = os.path.basename(application_src)
        self.compute_configuration['application_src'] = application_src
        self.compute_configuration['input_names'] = list(kwargs.keys())
        self.compute_configuration['module_name'] = sys.modules.get(function.__module__).__name__
        self.compute_configuration['function_name'] = function.__name__
        self.compute_configuration['callback_init_fn'] = self.callback_init_fn
        self.compute_configuration['callback_over_fn'] = self.callback_over_fn
        self.compute_configuration['local'] = local

        # Create computation script
        script = self._create_script()

        # Create Score file system, including compute configuration file
        fs_application = ScoreFileSystem(application_src, create_tree=True)
        fs_application.clean_core_configuration()
        fs_application.set_compute_configuration(self.compute_configuration)

        # Add inputs in computation directory
        pickle_dir = fs_application.inputs
        for name, value in kwargs.items():
            with open(os.path.join(pickle_dir, "{}.pkl".format(name)), 'wb') as fd:
                pickle.dump(value, fd)

        # Add computation script in computation directory
        with open(os.path.join(application_src, "score_compute.py"), 'w') as fd:
            fd.write(script)

    def _create_script(self):

        """ Creates script's text from template which will be able :\n
            - to execute user function with parameters encapsulated\n
            - to manage communication between the applicant and the computation\n
            - to save all data in an external storage server if necessary

        :return: (str) text to be saved in a file to constitute a python script.
        """

        sock = (self.monitoring_tool == 'socket')

        data = {'worker': "WorkerSocket" if sock else "WorkerFile",
                }

        template_text = pkg_resources.read_text(templates, 'score_compute.py')
        template = string.Template(template_text)
        script = template.substitute(data)

        return script

    def _create_archive(self, application_src):

        """ Creates a zip archive from a python function.

        :param application_src: (str) path to application directory we'd like to archive.
        :return: (str) path to the zip archive.
        """

        make_archive(application_src, "zip", application_src)
        self._append_log("Zip path = {}\ncopied_src = {}\ntemp_dir = {}".format("{}.zip".format(application_src),
                                                                                application_src,
                                                                                os.path.dirname(application_src)))
        return "{}.zip".format(application_src)

    def _set_computation_summary(self, application_full_path, bind_intermediate, bind_final):

        """ Save current computation settings in a file for join back the computation. """

        if not os.path.isdir(self.relative_fs.computation_summaries):
            os.mkdir(self.relative_fs.computation_summaries)

        computation_summary = {'monitoring_tool': self.monitoring_tool,
                               'application_full_path': application_full_path,
                               # 'application_folder': self.compute_configuration['application_folder'],
                               'computation_id': self.compute_configuration['computation_id'],
                               }

        if bind_intermediate is not None:
            computation_summary['bind_intermediate'] = {
                'module': sys.modules.get(bind_intermediate.__module__).__name__,
                'function': bind_intermediate.__name__
            }

        if bind_final is not None:
            computation_summary['bind_final'] = {'module': sys.modules.get(bind_final.__module__).__name__,
                                                 'function': bind_final.__name__
                                                 }

        if 'computation' in self.compute_configuration.keys():
            computation_summary['computation'] = self.compute_configuration['computation']
            if hasattr(self.runner, 'job_id'):
                computation_summary['computation']['job_id'] = self.runner.job_id

        summary_file_path = os.path.join(self.relative_fs.computation_summaries,
                                         f'computation_{self.compute_configuration["computation_id"]:02}')

        with open(summary_file_path, 'w') as fd:
            json.dump(computation_summary, fd)

        return computation_summary

    def _connect_to_computation(self, computation_summary):

        """ Connect score.Core object to a remote running computation using SSH or socket.\n
        A score.monitoring.ClientBase object is appended to class attribute list self.monitors.\n
        It is called systematically when a new computation is launched, but can be called manually to follow connect
        to a process already running.

        :param computation_summary: (dict) containing information how to connect to computation Worker.
        :return: (score.monitoring.ClientBase object), one of [ClientLocal, ClientSocket] depending on monitoring_tool.
        """

        self._append_log("Connecting to computation")
        monitor = None

        if computation_summary['monitoring_tool'] == 'local':
            monitor = ClientLocal(computation_summary['application_full_path'],
                                  computation_id=computation_summary['computation_id'])

        elif computation_summary['monitoring_tool'] == 'ssh':
            if 'computation' in computation_summary.keys():
                calc = computation_summary['computation']
            else:
                self._show_error('Information about remote server is missing to use SSH monitoring')
                return None

            monitor = ClientSsh(calc['ip_address'],
                                calc['username'],
                                computation_summary['application_full_path'],
                                key_file=calc['key_file'],
                                computation_id=computation_summary['computation_id'])

        elif computation_summary['monitoring_tool'] == 'slurm':
            if 'computation' in computation_summary.keys():
                slurm = computation_summary['computation']
            else:
                self._show_error('Information about remote server is missing to use SLURM monitoring')
                return None

            monitor = ClientSlurm(slurm['ip_address'],
                                  slurm['username'],
                                  computation_summary['application_full_path'],
                                  slurm['job_id'],
                                  key_file=slurm['key_file'],
                                  computation_id=computation_summary['computation_id'])

        elif computation_summary['monitoring_tool'] == 'socket':

            port = self.runner.get_socket_port(computation_summary['application_full_path'])

            local_ip = get_ip()
            if port:
                monitor = ClientSocket(tuple((local_ip, port,)),
                                       computation_id=computation_summary['computation_id'])
            else:
                self._append_log("Error getting socket port on server")
                return None

        if monitor is not None:
            self.monitors.append(monitor)

        if self.compute_widget is not None and monitor:
            if hasattr(monitor, 'kill_computation') and callable(getattr(monitor, 'kill_computation')):
                self.compute_widget.link_kill_function(computation_summary['computation_id'],
                                                       monitor.kill_computation)
            callbacks = self.compute_widget.get_callbacks(computation_summary['computation_id'])
            monitor.bind_to_status(callbacks[0])
            monitor.bind_to_logs(callbacks[1])
            monitor.bind_to_error(callbacks[2])

            if 'bind_intermediate' in computation_summary.keys():
                bind_intermediate = getattr(import_module(computation_summary['bind_intermediate']['module']),
                                            computation_summary['bind_intermediate']['function'])
                monitor.bind_to_intermediate_data(bind_intermediate)
            if 'bind_final' in computation_summary.keys():
                bind_finale = getattr(import_module(computation_summary['bind_final']['module']),
                                      computation_summary['bind_final']['function'])
                monitor.bind_to_final_data(bind_finale)

        return monitor

    def _change_status(self, status, progress=None):

        """ Function called when the current status changes.

        :param status: (str) current status.
        :param progress: (float, optional) progress value.
        """

        if self.compute_widget is not None:
            self.compute_widget.change_status(status, progress)

    def _append_log(self, log):

        """ Function called when some logs are to be wrote.

        :param log: (str) new log.
        """

        self.logs.append(log)
        if self.compute_widget is not None:
            self.compute_widget.append_log(log)

        if not self.compute_widget.displayed:
            print(log)

    def _show_error(self, error):

        """ Function called when an error occurred before computation has been launched.

        :param error: (str) error text.
        """

        self.error += error + '\n'
        if self.compute_widget is not None:
            self.compute_widget.display_error_widget(error)

        if not self.compute_widget.displayed:
            print(f'\033[31m{error}\033[0m')

    def _get_socket_port(self, application_src, count=3):

        """ If monitoring tools is socket, this function will retrieve the tcp socket port used by local server. """

        pass

    @staticmethod
    def _copy_files(application_src, directory=''):

        """ Copies the source directory tree to a temp directory.

        :param application_src: (str) path to application directory we'd like to copy.
        :param directory: (str, optional) path to a local directory to copy files, useful only for local computation.\n
                          If it is not provided, creates a temporary folder.
        :return: (str) path to the copied folder.
        """

        if not directory:
            directory = mkdtemp(prefix="score_client_")
        else:
            directory = mkdtemp(prefix="score_client_", dir=os.path.expandvars(directory))

        date_text = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S.%f')[:-3]
        output_folder = "{}_{}".format(os.path.basename(application_src), date_text)

        application_src = copytree(application_src, os.path.join(directory, output_folder),
                                   ignore=ignore_patterns('*.ipynb*', '__pycache__'))

        return application_src

    @staticmethod
    def _get_module_path(function):

        """ Get the module file path of a specific function.

        :param function:  (function) python function imported in caller script.
        :return: (src) path to the root directory including the module.
        """

        module = sys.modules.get(function.__module__)
        src = os.path.dirname(os.path.realpath(module.__file__))
        return src

    @staticmethod
    def _set_server_locked(*args, **kwargs):

        """ Disabled set_*_server() functions for computation mode. """

        print("Configuration is locked, you cannot change computation server")

    @staticmethod
    def _set_simage_locked(*args, **kwargs):

        """ Disabled set_simage() functions for computation mode. """

        print("Configuration is locked, you cannot change singularity image")

    @staticmethod
    def _set_storage_locked(*args, **kwargs):

        """ Disabled set_storage() function for computation mode. """

        print("Configuration is locked, you cannot change storage server")

    @staticmethod
    def _set_callbacks_locked(*args, **kwargs):

        """ Disabled set_callback_computation() functions for computation mode. """

        print("Configuration is locked, you cannot change callback functions")

    @staticmethod
    def _test_downloading_data_locked(*args, **kwargs):

        """ Disabled test_downloading_data() function for computation mode. """

        print("You can download data only in configuration mode")
