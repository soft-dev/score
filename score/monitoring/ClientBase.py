# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import traceback
import time
import pickle
from threading import Thread
from threading import Semaphore
from queue import Queue
import os

from ..tools import messaging

__all__ = ['ClientBase']


class ClientBase(Thread):

    """ Client base class for communication between web server and computation server.\n
    The client is to be used on web server server, it is wondering the computation service.

    :param computation_id: (int) used when several computations are send by the same score.Core object, to distinguish
                           data from each computation.
    """

    def __init__(self, computation_id=None):

        super().__init__()
        self.computation_id = computation_id
        self.computation_over = False
        self.status_observers = list()
        self.logs_observers = list()
        self.error_observers = list()
        self.intermediate_data_observer = None
        self.final_data_observer = None

        self.status = [0, "Initialisation"]
        self.logs = list()
        self.error = ''

        self.queue_intermediate_data = Queue()
        self.queue_final_data = Queue()
        self.waiting_code_return = Semaphore()

        self.killing_wondered = False

        self.pattern_state = messaging.STATE_PATTERN
        self.pattern_error = messaging.ERROR_PATTERN
        self.pattern_message = messaging.MESSAGE_PATTERN
        self.pattern_system_message = messaging.SYSTEM_MESSAGE_PATTERN
        self.pattern_data = messaging.PICKLE_DATA_PATTERN

    def wait_for_end(self):

        """ Waiting for the end of computation, as computation_over flag became True when message received from worker.

        .. todo::
            Add an option to stop waiting.
        """

        while not self.computation_over:
            time.sleep(0.5)

    def bind_to_status(self, callback):

        """ Add a callback function when status changes.

        :param callback: (function) to add to callbacks list.
        """

        self.status_observers.append(callback)

    def bind_to_logs(self, callback):

        """ Add a callback function when writing new log.

        :param callback: (function) to add to callbacks list.
        """

        self.logs_observers.append(callback)
        for log in self.logs:
            callback(log)

    def bind_to_error(self, callback):

        """ Add a callback function when error occurred on computation.

        :param callback: (function) to add to callbacks list.
        """

        self.error_observers.append(callback)
        if self.error:
            callback(self.error)

    def bind_to_intermediate_data(self, callback):

        """ Bind a function to intermediate data reception event.

        :param callback: (function) to call when new intermediate data are received.
        """

        self.intermediate_data_observer = callback

    def bind_to_final_data(self, callback):

        """ Bind a function to final data reception event.

        :param callback: (function) to call when final data are received.
        """

        self.final_data_observer = callback

    def change_status(self, progress, message):

        """ Function called when computation status changes.

        :param progress: (int) current status between 0 and 100.
        :param message: (str) Détails of the computation status.
        """

        self.status = [progress, message]
        for callback in self.status_observers:
            callback(progress, message)

    def append_log(self, log):

        """ Function called when some logs are wrote.

        :param log: (str) new log.
        """

        self.logs.append(log)
        if self.logs_observers:
            for callback in self.logs_observers:
                callback(log)
        else:
            print(log)

    def show_error(self, error):
        """ Function called when an error append.

        :param error: (str) error message.
        """

        self.error += error + '\n'
        if self.error_observers:
            for callback in self.error_observers:
                callback(self.error)
        else:
            print(f'\033[31m{error}\033[0m')

    def get_intermediate_data(self):

        """ Retrieve intermediate data during computation.

        :return: (data) no specific type, but each data come from a pickle object.
        """

        if self.queue_intermediate_data.empty():
            return None

        try:
            data = pickle.load(open(self.queue_intermediate_data.get(timeout=1), 'rb'))
        except:
            message = f"Error while trying to read intermediate data : {os.linesep}"
            message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
            self.show_error(message)
            return None

        if self.intermediate_data_observer is not None:
            try:
                self.intermediate_data_observer(self, data)
                self.append_log("Bound function to intermediate data called")
            except:
                message = f"Error in intermediate data callback function : {os.linesep}"
                message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
                self.show_error(message)
            return None

        else:
            return data

    def get_final_data(self):

        """ Retrieves data in queue, independently of the monitoring tools used.

        :return: (data) no specific type, but each data come from a pickle object.
        """

        self.wait_for_end()
        if self.queue_final_data.empty():
            raise Exception("Final data list is empty !!")

        return self._get_final_data()

    def _get_final_data(self):

        """ Get final data assuming that data exists. """

        try:
            data = pickle.load(open(self.queue_final_data.get(timeout=1), 'rb'))
        except:
            message = f"Error while trying to read final data : {os.linesep}"
            message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
            self.show_error(message)
            return None

        if self.final_data_observer is not None:
            try:
                self.final_data_observer(self, data)
                self.append_log("Bound function to final data called")
            except:
                message = f"Error in final data callback function : {os.linesep}"
                message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
                self.show_error(message)
            return None
        else:
            return data
