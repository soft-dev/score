# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import time
import json
import re
import os
import socket
import traceback

from .ClientBase import ClientBase
from ..tools import get_ip, messaging
from ..statics.file_system import ScoreFileSystem

__all__ = ['ClientSocket']


class ClientSocket(ClientBase):

    """ Extends :py:class:`score.monitoring.ClientBase.ClientBase`\n
    Client socket class for communication between web server (client) and computation server (worker).\n
    The main idea is to read an UDP socket and interpret the message from regex.

    :param remote_address: (tuple (str, int)) containing IP address and port of TCP socket to reach.
    """

    def __init__(self, remote_address, computation_id=None):

        """ (1) initializes attributes
            (2) create an UDP socket and listen to it
            (3) connect to the TCP socket of remote server, which init computation
        """

        super().__init__(computation_id=computation_id)
        self.append_log(f"remote is {remote_address[0]}:{remote_address[1]}")
        self.remote_address = remote_address

        self.waiting_data_bytes = None
        self.bytes_received = b''
        self.intermediate_data = False
        self.data_length = 65000

        if remote_address[0] in ['127.0.0.1', 'localhost']:
            self.HOST = remote_address[0]
        else:
            self.HOST = get_ip()
        self.PORT = 0  # to pick automatically a free port
        self.sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            self.sock_udp.bind((self.HOST, self.PORT))
            self.PORT = self.sock_udp.getsockname()[1]
        except socket.error as msg:
            self.append_log(str(msg))

        # print("UDP server launch on {}, connecting to TCP server".format(self.sock_udp.getsockname()))
        self.sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.connection_established = self._connect()
        if self.connection_established:
            self.append_log("TCP server reached")
            self.start()
            self.append_log("Monitoring started")
        else:
            self.show_error(f"ERROR while connecting to computation server at {remote_address[0]}:{remote_address[1]}")

    def run(self):

        """ Begin the thread by listening in an infinite loop on the UDP port.\n
        Each string received is treated depending to regex pattern.\n
        The kinds of regex expected are :\n
            - status :\n
                -> put status to the dedicated queue\n
            - user message :\n
                -> append message to the log list.\n
            system message:\n
                message == 'END' : end process by turning computation_over at True\n
                message == 'INIT' : the remote process wait for the creation of a file, which mean that some program is
                                    listening to it\n
            - error :\n
                -> print error and end computation\n
            - data :\n
                -> wait for following data, in bytes constituting a pickle\n

        .. todo::
            - use RegexCallback instead of analysing string in loop.
            - test with large data, which should not come through network as it is implemented now\n
        """

        try:
            while True:
                data, address = self.sock_udp.recvfrom(self.data_length)
                if self.waiting_data_bytes is None:
                    data = data.decode()
                    # print("Data received on client : {}, from {}".format(data, address))

                    m = re.match(self.pattern_state, data)
                    if m:
                        self.change_status(int(m.group('status')), m.group('message'))
                        # self.queue_status.put([m.group(2), int(m.group(1))])
                        continue

                    m = re.match(self.pattern_error, data, re.DOTALL)
                    if m:
                        self.computation_over = True
                        self.sock_udp.close()
                        self.show_error(m.group('message'))
                        if not m.group('no_kill'):
                            self.append_log("Process over with error !")
                            break

                    m = re.match(self.pattern_message, data, re.DOTALL)
                    if m:
                        self.append_log(f'Received : {m.group("message")}')
                        continue

                    m = re.match(self.pattern_system_message, data, re.DOTALL)
                    if m:
                        if m.group('message') == "END":
                            self.computation_over = True
                            self.append_log('Computation over')
                            if self.waiting_code_return.acquire(blocking=False):
                                self.waiting_code_return.release()
                                break
                            else:
                                continue

                        elif m.group('message') == "end_code":
                            self.waiting_code_return.release()
                            if self.computation_over:
                                break
                            else:
                                continue
                        else:
                            self.append_log(f'SCORE internal message : {m.group("message")}')
                            continue

                    m = re.match(self.pattern_data, data)
                    if m:
                        try:
                            self.waiting_data_bytes = json.loads(m.group('message'))
                        except json.decoder.JSONDecodeError:
                            self.append_log("Error in JSON data information")
                            continue
                        self.data_length = self.waiting_data_bytes['length']
                        if m.group('intermediate'):
                            self.intermediate_data = True
                        # print("Waiting now for {}".format(self.waiting_data_bytes))
                        continue

                    # print(data)
                else:
                    self.bytes_received += data
                    if len(self.bytes_received) == self.waiting_data_bytes['length']:
                        fs = ScoreFileSystem(create_tree=True)
                        received_data_name, ext = os.path.splitext(os.path.basename(self.waiting_data_bytes['path']))
                        new_data_file_name = f'{received_data_name}_{self.computation_id}{ext}'
                        local_data_path = os.path.join(fs.outputs, new_data_file_name)
                        with open(local_data_path, 'wb') as fd:
                            fd.write(self.bytes_received)
                        # print("Data received : {}".format(data_temp))
                        if self.intermediate_data:
                            self.queue_intermediate_data.put(local_data_path)
                            if self.intermediate_data_observer is not None:
                                self.get_intermediate_data()
                        else:
                            self.queue_final_data.put(local_data_path)
                            self.append_log("Final data received")
                            if self.final_data_observer is not None:
                                self._get_final_data()
                        self.data_length += 1
                    else:
                        self.append_log("ERROR in data length")

                    self._init_data()

        except KeyboardInterrupt:
            self.append_log("Server stopped by user")
        except:
            print(traceback.format_exc())
        finally:
            self.append_log("Closing web server UDP and TCP")
            # self.computation_over = True
            self.sock_udp.close()
            self.sock_tcp.close()

    def kill_computation(self, delete_directory=False):

        """ Send message to kill main process.

        .. todo::
            find a way to delete directory using socket.

        :param delete_directory: (boolean) whether to delete computation directory or not.
        """

        if not self.computation_over:
            self.sock_tcp.send(messaging._kill_computation(delete_directory).encode())

    def send_code(self, code):

        """ Send python code on remote server using its TCP port.

        :param code: (str) code to execute on remote server.
        """

        retries = 2
        self.waiting_code_return.acquire()
        try:
            while retries:
                retries -= 1
                if self.sock_tcp.send("code={}".format(code).encode()):
                    return
            self.waiting_code_return.release()
        except:
            self.waiting_code_return.release()

    def _connect(self):

        """ Try to connect to TCP socket of remote server.

        :return: (boolean) whether the connexion succeed or not.
        """

        i = 0
        for i in range(10):
            try:
                self.sock_tcp.connect(self.remote_address)
                # print("Connection accepted")
                break
            except ConnectionRefusedError:
                time.sleep(1)
        if i == 9:
            self.append_log("error connection TCP")
            return False
        self.sock_tcp.send("ip={};port={}".format(self.HOST, self.PORT).encode())
        return True

    def _init_data(self):

        """ Init data buffer. """

        self.waiting_data_bytes = None
        self.bytes_received = b''
        self.intermediate_data = False
        self.data_length = 1024
