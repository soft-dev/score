# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import time
import json
from shutil import rmtree

from .ClientBase import ClientBase
from ..statics.file_system import ScoreFileSystem
from ..tools.RegexCallbacks import RegexCallbacks
from ..tools import messaging

__all__ = ['ClientLocal']


class ClientLocal(ClientBase):

    """ Extends :py:class:`score.monitoring.ClientBase.ClientBase`\n
    Client Local class for communication between the client and the computation on the same server.\n
    The main idea is to read on log file (filled by worker), and handle function on specific regex read.

    .. todo::
        Catch the end of main process if an error occurred instead of waiting endlessly

    :param local_path: (str) path to the root folder containing encapsulate.py script.
    """

    def __init__(self, local_path, computation_id=None):

        """ (1) initializes attributes, and create handlers which will be called when we read specifics regex
            (2) waits for remote computation has began
            (3) handles a process which read a remote log file.
        """

        super().__init__(computation_id=computation_id)

        # SCORE files and directories
        self.fs = ScoreFileSystem(root=local_path)

        self.communication_file = ''
        self.cmd_error = None

        self.analyse_received = RegexCallbacks()
        self.analyse_received.set_regex_callbacks((self.pattern_state, self.state_callback),
                                                  (self.pattern_error, self.error_callback),
                                                  (self.pattern_message, self.user_message_callback),
                                                  (self.pattern_system_message, self.system_message_callback),
                                                  (self.pattern_data, self.data_callback))

        self.append_log(f"Computation start, local folder is {local_path}")

        if not os.path.isdir(self.fs.root):
            self.show_error(f'The local directory {self.fs.root} does not exists anymore')
            return

        while not os.path.isfile(self.fs.log_file):
            time.sleep(0.5)

        self.fd = open(self.fs.log_file, 'r')

        self.start()

    def run(self):

        """ Begin the thread by reading the computation log file.\n
        Each line is sent to a RegexCallbacks object which analyse message and call the corresponding callback function.
        """

        while not self.computation_over:
            line = self.fd.readline()
            if not line:
                time.sleep(0.1)
                continue
            self.analyse_received.data_queue.put(line.strip())

        self.analyse_received.stop()

    def kill_computation(self, delete_directory=False):

        """ Send message to kill main process.

        :param delete_directory: (boolean) whether to delete computation directory or not.
        """

        if not self.computation_over:
            self.killing_wondered = True
            with open(self.communication_file, 'a') as fd:
                fd.write(messaging._kill_computation(delete_directory))

    def user_message_callback(self, mo):

        """ Handler called at user message incoming event : append message to the log list.

        :param mo: (re.MatchObject) from re.match containing group 'message'.
        """

        if not mo:
            return
        msg = mo.group('message').rstrip()
        self.append_log(f'Received : {msg}')

    def state_callback(self, mo):

        """ Handler called at status message incoming event : put status to the dedicated queue.

        :param mo: (re.MatchObject) from re.match, containing groups 'status' and 'message'.
        """

        if not mo:
            return
        self.change_status(int(mo.group('status').rstrip()), mo.group('message').rstrip())

    def data_callback(self, mo):

        """ Handler called when data regex is read : these data from remote server.

        :param mo: (re.MatchObject) from re.match containing group 'message' (local path of data) and optionally
                   'intermediate'.
        """

        if not mo:
            return

        local_data_path = json.loads(mo.group('message'))['path']

        if mo.group('intermediate'):
            self.queue_intermediate_data.put(local_data_path)
            if self.intermediate_data_observer is not None:
                self.get_intermediate_data()
        else:
            self.queue_final_data.put(local_data_path)
            self.computation_over = True
            if self.final_data_observer is not None:
                self.get_final_data()

    def error_callback(self, mo):

        """ Handler called at error message incoming event :\n
        -> read error text\n
        -> kill process after reading if worker ask for (by adding ?? in message)\n

        :param mo: (re.MatchObject) from re.match containing group 'message', and optionally 'no_kill' and 'delete_dir'.
        """

        if not mo:
            return

        with open(mo.group('message').rstrip(), 'r') as fd:
            out = fd.read()
        self.show_error(out)
        if not mo.group('no_kill'):
            self.computation_over = True
            self.append_log("Process over with error !")
        if self.killing_wondered and mo.group('delete_dir'):

            # This sleep time let time for all clients to disconnect, otherwise as clients are reading a file in
            # directory, the directory cannot be removed (BUSY)
            time.sleep(2)

            rmtree(self.fs.root)
            self.append_log("Computation directory deleted")

    def system_message_callback(self, mo):

        """ Handler called at system message incoming event :\n
                message == 'END' : end process by turning computation_over at True\n
                message == 'INIT' : the remote process wait for the creation of a file, which mean that some program
                                    is listening to it

        :param mo: (re.MatchObject) from re.match containing group 'message'.
        """

        if not mo:
            return
        msg = mo.group('message').rstrip()
        if msg == "END":
            self.computation_over = True
            self.append_log('Computation over')
            if self.waiting_code_return.acquire(blocking=False):
                self.waiting_code_return.release()
        elif "INIT" in msg:
            if not self.communication_file:
                self.communication_file = msg[4:]
                open(self.communication_file, 'w').close()

        elif msg == "end_code":
            self.waiting_code_return.release()
            if self.computation_over:
                self.append_log("computation over")
        else:
            self.append_log(f'SCORE internal message : {msg}')
