# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import pickle
import traceback
from shutil import copytree
from threading import Thread
from importlib import import_module

from ..statics.file_system import ScoreFileSystem
from ..tools import Capturing
from ..tools.ssh import SSHHandler, SCPHandler
from ..tools.web_service import download_zip_data

__all__ = ['WorkerBase']


class WorkerBase(Thread):

    """ Worker base class for communication between web server and computation server.\n
    The worker is to be used on computation server, it is providing computation service.

    :param root_directory: (str) root directory of the application to run.
    """

    def __init__(self, root_directory, main_pid=None):

        super().__init__()

        self.root_directory = root_directory

        self.main_pid = main_pid

        self.carry_on = True

        # SCORE files and directories
        self.fs = ScoreFileSystem(root=root_directory)
        self.fs.create_tree()

        self.capturing = Capturing(self)
        self.compute_function = None
        self.results = None

        self.configuration = dict()
        self.computation_id = None

        self.token_key = None

        self.callback_init = None
        self.callback_over = None

        self.computation_cancelled = False

        self.inputs = dict()

        os.chdir(root_directory)

        self._initialize()

    def compute(self):

        """ Performs the computation, using a context manager to catch print and error. """

        with self.capturing:
            self.results = self.compute_function(**self.inputs)

    def get_inputs(self):

        """ Retrieves input data set up by end user. """

        for input_name in self.configuration.get('input_names', list()):
            with open(os.path.join(self.fs.inputs, f'{input_name}.pkl'), 'rb') as fd:
                self.inputs[input_name] = pickle.load(fd)

    def call_callback_init(self):

        """ Call the init callback function """

        if self.callback_init is not None:
            if self.callback_init(self.configuration):
                print('Computation init information populated in database')
            else:
                print("Error while populating computation init information on database")

    def call_callback_over(self):

        """ Call the callback function when computation is over """

        if self.callback_over is not None:
            if self.callback_over(self.configuration, cancelled=self.computation_cancelled):
                print('Computation over information populated in database')
            else:
                print("Error while populating computation over information on database")

    def download_data(self):

        """ Download data from a web service, as zip archive """

        if 'data_to_download' in self.configuration.keys():
            for d in self.configuration['data_to_download']:
                download_zip_data(d['url'], d['directory'], token=self.token_key, body=d['body'])

    def save_data(self):

        """ Save computation folder in a specific path on (a remote) server """

        if 'storage' in self.configuration.keys():

            data_directory = self.configuration['storage']['data_path']

            if 'remote' in self.configuration['storage'].keys():
                if 'ssh_key_file' in self.configuration['storage']['remote'].keys():
                    ssh_key_file = self.configuration['storage']['remote']['ssh_key_file']
                else:
                    ssh_key_file = os.path.join(os.path.expandvars(self.fs.MAIN_CONFIG_DIR), 'score_id_rsa')
                try:
                    # Get full path if path use environmental variables
                    out, err = SSHHandler(self.configuration['storage']['remote']['ip_address'],
                                          self.configuration['storage']['remote']['username'],
                                          key_file=ssh_key_file).simple_command(f'echo {data_directory}',
                                                                                close_session=True)
                    scp = SCPHandler(self.configuration['storage']['remote']['ip_address'],
                                     self.configuration['storage']['remote']['username'],
                                     key_file=ssh_key_file)
                    scp.chmod(out, 0o750)
                    scp.put_dir(self.root_directory, os.path.join(out, os.path.basename(self.root_directory)))
                    scp.chmod(out, 0o550)
                    self.send_system_message(f"Data saved on storage server at {data_directory}")
                except:
                    message = f"Error in calculation script : {os.linesep}"
                    message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
                    self.send_error(message)
                    self.send_system_message("An error occurred while saving data on storage server.")
            else:
                try:
                    data_directory = os.path.expandvars(data_directory)
                    os.chmod(data_directory, 0o770)
                    copytree(self.root_directory, os.path.join(data_directory, os.path.basename(self.root_directory)))
                    os.chmod(data_directory, 0o550)
                    self.send_system_message(f"Data saved on server at {data_directory}")
                except:
                    self.send_system_message("An error occurred while saving data on server at {data_directory}")

    def send_string(self, message):
        raise NotImplementedError

    def send_message(self, message):
        raise NotImplementedError

    def send_state(self, percent, message):
        raise NotImplementedError

    def send_data(self, data):
        raise NotImplementedError

    def send_error(self, message):
        raise NotImplementedError

    def send_system_message(self, message):
        raise NotImplementedError

    def is_connection_established(self):
        # TODO: This function is useful only to debug, in real life computation can be done without any client

        raise NotImplementedError

    def stop(self):
        raise NotImplementedError

    def _initialize(self):

        """ Initializes the Worker object, retrieving configuration from file, and setting up :
        - the compute function
        - the callbacks functions
        - the user token (to access data from SWAF application for example)
        """

        self.configuration = self.fs.get_compute_configuration()
        self.computation_id = self.configuration['computation_id']

        # Get compute function
        self.compute_function = getattr(import_module(self.configuration['module_name']),
                                        self.configuration['function_name'])
        # Get user's token key
        token_file_path = os.path.join(os.path.expandvars(self.fs.MAIN_CONFIG_DIR), 'user_token')
        if os.path.isfile(token_file_path):
            with open(token_file_path, 'r') as fd:
                self.token_key = fd.read().strip()

        # Get callback functions
        try:
            self.callback_init = getattr(import_module(f'{self.fs.score_directory}.{self.fs.callbacks_modules}'),
                                         self.configuration.get('callback_init_fn', None))
            self.callback_over = getattr(import_module(f'{self.fs.score_directory}.{self.fs.callbacks_modules}'),
                                         self.configuration.get('callback_over_fn', None))
        except ModuleNotFoundError:
            # keep callback functions at None
            pass
        except AttributeError:
            self.send_error("Error in callback function's names")
