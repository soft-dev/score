# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import time

try:
    import radical.saga as rs
    SAGA = True
except ModuleNotFoundError:
    SAGA = False

from .ClientSsh import ClientSsh
from ..tools import messaging

__all__ = ['ClientSlurm']


class ClientSlurm(ClientSsh):

    """ Extends :py:class:`score.monitoring.ClientSsh.ClientSsh`\n
    Client Slurm for communication between web server (client) and computation server (worker) with Slurm as job
    scheduler.\n
    The main idea is to wait for the job to run (requesting scheduler), then read on remote log file (filled by worker),
    and handle function on specific regex read.

    :param ip_address: (str) remote server ip address.
    :param username: (str) remote server username.
    :param remote_path: (str) path to the root folder containing the application.
    :param job_id: (str) SLURM job ID which has been scheduled.
    :param key_file: (str, optional) path of the private key file to use for connection.\n
                     This parameters is optional to stay generic, but should be filled. If not, ssh will try to connect
                     using user ssh key pair (see ssh documentation for more information).
    """

    def __new__(cls, *args, **kwargs):

        """ Prevent object creation if all required dependencies are not satisfied """

        if not SAGA:
            print("You need to install radical-saga to use Slurm job scheduler.")
            return None

        instance = super().__new__(cls)
        return instance

    def __init__(self, ip_address, username, remote_path, job_id, key_file=None, computation_id=None):

        """
        (1) initializes attributes, and create handlers which will be called when we read specifics regex
        (2) waits for remote computation has began
        (3) handles a process which read a remote log file.
        """

        self.slurm_url = f"slurm+ssh://{ip_address}"
        self.slurm_context = rs.Context('ssh')
        self.slurm_context.user_id = username
        if key_file:
            self.slurm_context.user_key = key_file
        self.slurm_session = rs.Session()
        self.slurm_session.add_context(self.slurm_context)
        self.slurm_job_service = rs.job.Service(self.slurm_url, self.slurm_session)
        self.job = self.slurm_job_service.get_job(job_id)

        super().__init__(ip_address=ip_address, username=username, remote_path=remote_path, key_file=key_file,
                         computation_id=computation_id)

    def kill_computation(self, delete_directory=False):

        """ Send message to kill main process.

        :param delete_directory: (boolean) whether to delete computation directory or not.
        """

        if not self.computation_over:
            self.killing_wondered = True
            # self.job.
            command = f'echo "{messaging._kill_computation(delete_directory)}" >> {self.communication_file}'
            out, err = self.ssh.simple_command(command)
            if err:
                # log an error if ssh command fails, as if it was send by computation server
                self.ssh.analyse_received.data_queue.put(messaging._error('Error while killing the remote computation',
                                                                          no_kill=True))

    def _connect(self):

        """ Look for the log file on the remote server, which means that computation is running.

        :return: (boolean) whether the connexion succeed or not.
        """

        for i in range(20):
            if self.job.state == 'Pending':
                self.change_status(0, 'SLURM : Pending')
                time.sleep(1)
            elif self.job.state == 'Running':
                self.change_status(0, 'SLURM : Running')
                break
            else:
                self.change_status(0, f'SLURM : {self.job.state}')
                return False

        for i in range(20):
            out, err = self.ssh.simple_command('[ -f {} ] && echo "OK" || echo "NO"'.format(self.fs.log_file))
            if out == "OK":
                return True
            time.sleep(1)
        return False
