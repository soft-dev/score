# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

from .ClientSocket import ClientSocket
from .WorkerSocket import WorkerSocket

from .WorkerFile import WorkerFile
from .ClientLocal import ClientLocal

try:
    from .ClientSsh import ClientSsh
except ModuleNotFoundError as error:
    print(f"'{error.name}' is missing to import ClientSsh")

try:
    from .ClientSlurm import ClientSlurm
except ModuleNotFoundError as error:
    print(f"'{error.name}' is missing to import ClientSlurm")
