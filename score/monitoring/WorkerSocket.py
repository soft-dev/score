# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import time
import re
import json
import socket
import datetime
from threading import Thread, Semaphore
import traceback
import pickle
import signal

from .WorkerBase import WorkerBase
from ..tools import Capturing, get_ip, messaging, get_free_port

__all__ = ['WorkerSocket']


class WorkerSocket(WorkerBase):

    """ Extends :py:class:`score.monitoring.WorkerBase.WorkerBase`\n
    Worker socket class for communication between web server (client) and computation server (worker).\n
    The main idea is to :\n
        -> open TCP port to be reach by client.\n
        -> create a WorkerConnection objet by client, which will manage communication with this specific client.\n
        -> write status, log message, errors, data... to each identified client (UDP port), using the
        WorkerConnection object.

    :param root_directory: (str) path to root directory of the application.
    :param tcp_port: (int, optional) TCP port to listen to, if None the default will be 9999.
    :param udp_port: (int, optional) UNUSED NOW : UDP port to use for message transmission.\n
                     This could be necessary in case of outbound firewall rules, but not sure it could work with
                     several clients.
    """

    type = 'socket'

    def __init__(self, root_directory, main_pid=None, tcp_port=None, udp_port=None):

        """ (1) initializes ip and ports (TCP and UDP)
            (2) write used TCP port in a file, useless if using CoreWebService, to say client which port reach
            (3) listen to the TCP port and wait for remote connection

        TODO: (or not)
            - Why (the f***) in docker I can reach the TCP port on 'localhost' or '127.0.0.1' without
            error but nothing happen (sock_tcp.accept() seems never called).
            - And if we'd like to use docker as user image, there is something to do about port forwarding after a
            lot of tries I was able to redirect port inside docker to localhost but never to network address.
        """

        super().__init__(root_directory, main_pid=main_pid)

        if self.configuration.get('local', False):
            self.HOST = ""
        else:
            self.HOST = get_ip()

        self.TCP_PORT = None
        if tcp_port is None:
            if os.path.isfile(self.fs.socket_port_file):
                with open(self.fs.socket_port_file, 'r') as fp:
                    self.TCP_PORT = int(fp.read().strip())
            else:
                self.TCP_PORT = get_free_port()
                with open(self.fs.socket_port_file, 'w') as fd:
                    fd.write(str(self.TCP_PORT))

        if udp_port is None:
            self.UDP_PORT = 11000
        else:
            self.UDP_PORT = udp_port

        self.connection_list = []
        self.sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock_tcp.settimeout(1)
        try:
            self.sock_tcp.bind((self.HOST, self.TCP_PORT))
        except socket.error as msg:
            print(str(msg))
        self.sock_tcp.listen(10)
        self.send_retries = 2
        self.start()

    def run(self):

        """ Begins the thread by listen in an infinite loop on the TCP port, waiting for client connection. """

        while self.carry_on:
            try:
                # print("Waiting for tcp accept on {}".format(self.sock_tcp.getsockname()))
                c, a = self.sock_tcp.accept()
                # print("Init connexion with {}".format(a))
                self.connection_list.append(WorkerConnection(c, a, self))
            except KeyboardInterrupt:
                print("Server stopped by user")
            except socket.timeout:
                pass

        self.sock_tcp.close()

    def is_connection_established(self):

        """ Look if at least on client is connected.

        :return: (boolean) whether a client is listening or not.
        """

        for conn in self.connection_list:
            if conn.udp_initialised:
                return True
        return False

    def send_string(self, message, data=False):

        """ Sends a string to all identified clients, each connected to a WorkerConnection objet.\n
        This function is usually called in replacement of the stdout write function (cf. score.tools.capturing), so it
        is called by print function, which write in 2 time : (i) the string and (ii) a line feed character.

        .. todo::
            Be able to send data properly when message is data path, for user side sending data using tools.messaging.

        :param message: (str) string to send.
        :param data: (boolean) whether the string will be follow by some data (json description of data), for future
                     string wait until data has been sent.
        """

        # That rstrip is here to not send the line feed character alone.
        message = message.rstrip()
        if not message:
            return

        data_path = None
        data = None
        # If the message concern data, send the data
        m = re.match(messaging.PICKLE_DATA_PATTERN, message)
        if m:
            data_path = json.loads(m.group("message"))['path']
            with open(data_path, 'rb') as fd:
                data = fd.read()

        self._write_log(message)
        for conn in self.connection_list:
            retries = self.send_retries
            while retries:
                retries -= 1
                if conn.send_string(message, data_path is not None):
                    retries = 0

        if data_path is not None:
            self._send_bytes(data)

    def send_data(self, results):

        """ Send data to all identified clients, each connected to a WorkerConnection objet.\n
        (1) creates and saves pickles from user computation results\n
        (2) sends json information about this pickle to clients\n
        (3) sends pickles in binary format\n

        :param results: (list) of outputs of the computed function.
        """

        data_path = os.path.join(self.fs.outputs, f'FINAL_OUTPUT.pkl')
        pickle.dump(results, open(data_path, 'wb'))
        data_dict = {'length': os.stat(data_path).st_size, 'path': data_path}
        self.send_string(messaging._pickle_data(json.dumps(data_dict)))
        time.sleep(0.2)

    def send_error(self, message, no_kill=False):

        """ Sends an error message to all clients.

        :param message: (str) error message to send
        :param no_kill: (boolean) whether client must kill process or not (critical error or not)
        :return:
        """

        self.send_string(messaging._error(message, no_kill))

    def send_system_message(self, message):

        """ Send SCORE internal system message.

        :param message: (str) message to send.
        """

        self.send_string(messaging._system_message(message))

    def stop(self):

        """ Close each active TCP connection, and stop listening on TCP port. """

        self.carry_on = False
        for conn in self.connection_list:
            conn.stop = True

    def _send_bytes(self, data):

        """ Sends bytes to all identified clients, each connected to a WorkerConnection objet.

        :param data: (bytes) binary data to send.
        """

        for conn in self.connection_list:
            retries = self.send_retries
            while retries:
                retries -= 1
                if conn.send_bytes(data):
                    retries = 0

    def _kill_computation(self):

        """ Kill the computation. """

        self.send_error("Computation stopped by user")
        self.computation_cancelled = True
        self.call_callback_over()
        os.kill(self.main_pid, signal.SIGKILL)

    def _write_log(self, message):

        """ Write log into local file, only for debug. """

        with open(self.fs.log_file, 'a+') as fd:
            fd.write(f'{datetime.datetime.now().strftime("%H-%M-%S")} : {message}\n')


class WorkerConnection(Thread):

    """ Manage connection to one specific client.\n
    Basically it is used to :\
        -> send information to client using UDP port\n
        -> receive information from client by TCP connection, including python code to execute\n

    :param conn: active TCP socket connection with client
    :param address: (tuple(str, int)) TCP address information, will be used to construct UDP address

    .. todo::
        Remove this address parameter which seems totally useless as 'conn' might contain the same information, and as
        client send this information again.
    """

    def __init__(self, conn, address, worker=None):

        super().__init__()
        self.sock_tcp = conn
        self.address_tcp = address
        self.sock_tcp.settimeout(10)
        self.worker = worker
        self.stop = False

        self.writing_semaphore = Semaphore()
        self.sending_data_semaphore = Semaphore()

        self.sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self.sock_udp.bind((socket.gethostbyname(socket.gethostname()), 11000))
        # self.sock_udp.bind(("", 11000))
        self.remote_address = None
        self.pattern_address = r"^ip=((?:[0-9]{1,3}\.){3}[0-9]{1,3}|localhost);port=([0-9]{1,5})$"
        self.pattern_code = r"^code=(.*)$"

        self.udp_initialised = False
        print(f"Connexion with {self.address_tcp} initialized")
        self.start()

    def run(self):

        """ Begin the thread by listen in an infinite loop on the TCP connection.\n
        Each string received is treated depending to regex pattern.\n
        The kinds of regex expected are :\n
            - UDP address :\n
                -> set up the UDP port to send message to client\n
                -> update udp_initialised flag to allow to begin computation, as a client is connected\n
            - code :\n
                -> execute code in local\n
                -> send results to client\n

        .. todo::
            Find a way to remove local folder :
            - Folder cannot be removed while script is running, because it is used by singularity as bound folder
            - If the script is killed before, no way to remove the folder as the thread is killed too
            The only way seems to be killing the process (and the communication) and then remove the directory (using
            Core objet instead of Worker or Client).
        """

        while not self.stop:
            try:
                received_data = self.sock_tcp.recv(1024).decode()
                if received_data:
                    print(f"Data received on server : {received_data}")
                    m = re.match(self.pattern_address, received_data)
                    if m:
                        self.remote_address = (m.group(1), int(m.group(2)))
                        # print("remote address is {}".format(self.remote_address))
                        self.send_string("Connection established, computation will begin soon")
                        self.udp_initialised = True
                        continue

                    m = re.match(self.pattern_code, received_data)
                    if m:
                        try:
                            self.send_string(messaging._message("Results of: '{}'".format(m.group(1))))
                            with Capturing(self, prefix=">>"):
                                exec(m.group(1))
                        except:
                            message = "Error in terminal command : {}".format(os.linesep)
                            message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
                            self.send_string(messaging._message(message))
                        finally:
                            self.send_string(messaging._message("end_code"))
                        continue

                    m = re.match(messaging.KILL_COMPUTATION_PATTERN, received_data)
                    if m:
                        if self.worker is not None:
                            if m.group('delete'):
                                self.worker.send_system_message("The directory cannot be removed using socket "
                                                                "monitoring")

                                # TODO: removing folder
                                # try:
                                #     from shutil import rmtree
                                #     self.worker.send_message("Deleting computation directory main process")
                                #     rmtree(self.worker.fs.root)
                                #     self.worker.send_message("Computation directory deleted")
                                # except:
                                #     message = f"Error in calculation script : {os.linesep}"
                                #     message += os.linesep.join(traceback.format_exc().split(os.linesep)[3:])
                                #     self.worker.send_error(message)

                            self.worker._kill_computation()

            except socket.timeout:
                pass
            except:
                print(traceback.format_exc())
                break
        self.sock_tcp.close()

    def send_string(self, message, data=False):

        """ Send string to client on UDP port.

        :param message: (str) message to send.
        :param data: (boolean) whether the string will be follow by some data (json description of data), for future
                     string wait until data has been sent.
        """

        self.sending_data_semaphore.acquire()
        self.writing_semaphore.acquire()
        # if len(message) > 65000:
        #     return self.send_long_string(message)
        try:
            nb_bytes = self.sock_udp.sendto(message.encode(), self.remote_address)
            return True
        except:
            # Do not PRINT here, print are caught to be send by this same function... infinite loop ?
            return False
        finally:
            self.writing_semaphore.release()
            if not data:
                self.sending_data_semaphore.release()

    def send_bytes(self, all_bytes):

        """ Send binary data to client.

        :param all_bytes: (bytes) binary data to send.
        """

        self.writing_semaphore.acquire()
        try:
            nb_bytes = self.sock_udp.sendto(all_bytes, self.remote_address)
            return True
        except:
            print(traceback.format_exc())
            return False
        finally:
            self.writing_semaphore.release()
            self.sending_data_semaphore.release()

    def _send_long_string(self, message):

        """ Experimental simple way to send long message.
        TODO: finalize it if necessary, beginning by the implementation of the client part of this functionality.

        :param message: (str) long message to send.
        """

        prefix = message[:2]
        message = message[2:]
        full_length = len(message)
        current_index = 0
        try:
            while current_index < full_length:
                part_message = "{}{}".format(prefix, message[current_index: current_index + 65000])
                current_index += 65000
                nb_bytes = self.sock_udp.sendto(part_message.encode(), self.remote_address)
            return True
        except:
            print(traceback.format_exc())
            return False
