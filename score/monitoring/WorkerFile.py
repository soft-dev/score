# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import pickle
import time
import signal
import re
import json

from .WorkerBase import WorkerBase
from ..tools import messaging

__all__ = ['WorkerFile']


class WorkerFile(WorkerBase):

    """ Extends :py:class:`score.monitoring.WorkerBase.WorkerBase`\n
    Worker File class for communication between web server (client) and computation server (worker).\n
    The main idea is to write to a log file, which will be read locally or remotely by client.

    :param root_directory: (str) path to root directory of the application
    :param kwargs: keywords arguments "garbage", to have equivalent signature to other worker
    """

    type = 'file'

    def __init__(self, root_directory, main_pid=None, **kwargs):

        super().__init__(root_directory, main_pid=main_pid)

        self.com_fd = None
        self.send_system_message("INIT" + self.fs.com_file)
        self.start()

    def run(self):

        """ Thread reading communication file written by computation client.
        For now the only message is to kill process remotely.
        """

        while self.carry_on:
            if self.com_fd is None:
                if os.path.isfile(self.fs.com_file):
                    self.com_fd = open(self.fs.com_file, 'r')
                else:
                    time.sleep(1)
                continue
            line = self.com_fd.readline().strip()
            if not line:
                time.sleep(0.1)
                continue

            m = re.match(messaging.KILL_COMPUTATION_PATTERN, line)
            if m:
                if self.main_pid is not None:
                    delete_dir = m.group('delete') is not None
                    self.send_error("Computation stopped by user", delete_dir=delete_dir)
                    self.computation_cancelled = True
                    self.call_callback_over()
                    os.kill(self.main_pid, signal.SIGKILL)

    def is_connection_established(self):

        """ Looks for a file created by client, to know if some program is listening on this computation.

        :return: (boolean) whether the file exists or not.
        """

        return self.com_fd is not None

    def send_string(self, message):

        """ Appends a line to the log file.

        :param message: (str) message to append.
        """

        with open(self.fs.log_file, 'a+') as fd:
            fd.write(message)
            fd.flush()

    def send_data(self, results):

        """ Creates and saves pickles dat from user computation results and send the path to client.

        :param results: (list) of outputs of the computed function.
        """

        data_path = os.path.join(self.fs.outputs, f'FINAL_OUTPUT.pkl')
        pickle.dump(results, open(data_path, 'wb'))
        data_dict = {'length': os.stat(data_path).st_size, 'path': data_path}
        self.send_string(messaging._pickle_data(json.dumps(data_dict)) + '\n')

    def send_error(self, message, no_kill=False, delete_dir=False):

        """ Writes error to specific file and send the path to client.

        :param message: (str) error message to send.
        :param no_kill: (boolean) whether client must kill process or not (critical error or not).
        :param delete_dir: (boolean) whether to remove computation directory, usually after user killed computation.
        """

        with open(self.fs.error_file, 'a+') as fd:
            fd.write(message)
            fd.flush()
        self.send_string(messaging._error(self.fs.error_file, no_kill=no_kill, delete_dir=delete_dir) + '\n')

    def send_system_message(self, message):

        """ Send SCORE internal system message.

        :param message: (str) message to send.
        """

        self.send_string(messaging._system_message(message) + '\n')

    def stop(self):

        """ Stop reading communication file. """

        self.carry_on = False
