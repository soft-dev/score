# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import time
import json

from .ClientBase import ClientBase
from ..tools.ssh import SSHHandler, SCPHandler
from ..statics.file_system import ScoreFileSystem
from ..tools import messaging

__all__ = ['ClientSsh']


class ClientSsh(ClientBase):

    """ Extends :py:class:`score.monitoring.ClientBase.ClientBase`\n
    Client SSH class for communication between web server (client) and computation server (worker).\n
    The main idea is to read on remote log file (filled by worker), and handle function on specific regex read.

    .. todo::
        Catch the end of main process if an error occurred instead of waiting endlessly

    :param ip_address: (str) remote server ip address
    :param username: (str) remote server username
    :param remote_path: (str) path to the root folder containing encapsulate.py script
    :param key_file: (str, optional) path of the private key file to use for connection.\n
                     This parameters is optional to stay generic, but should be filled. If not, ssh will try to connect
                     using user ssh key pair (see ssh documentation for more information)
    """

    def __init__(self, ip_address, username, remote_path, key_file=None, computation_id=None):

        """ (1) initializes attributes, and create handlers which will be called when we read specifics regex
            (2) waits for remote computation has began
            (3) handles a process which read a remote log file.
        """

        super().__init__(computation_id=computation_id)

        # SCORE files and directories
        self.fs = ScoreFileSystem(root=remote_path)

        self.ip_address = ip_address
        self.username = username
        self.key_file = key_file
        self.communication_file = ''
        self.cmd_error = None

        self.ssh = SSHHandler(ip_address, username, key_file=key_file)

        self.ssh.set_regex_callbacks((self.pattern_state, self.state_callback),
                                     (self.pattern_error, self.error_callback),
                                     (self.pattern_message, self.user_message_callback),
                                     (self.pattern_system_message, self.system_message_callback),
                                     (self.pattern_data, self.data_callback))

        out, err = self.ssh.simple_command('[ -d {} ] && echo "OK" || echo "NO"'.format(self.fs.root))
        if out == "NO":
            self.show_error(f'The remote directory {self.fs.root} does not exists anymore')
            return

        self.append_log(f"Computation start, remote folder is {remote_path}")
        self.append_log("waiting for the creation of communication file")

        if not self._connect():
            self.show_error(f'The log file {self.fs.log_file} has not been created')
            return

        self.ssh.exec_channel(f'tail -n \+0 -f {self.fs.log_file}')

        self.append_log("Monitoring command : {}".format(self.ssh.cmd_channel))

    def kill_computation(self, delete_directory=False):

        """ Send message to kill main process.

        :param delete_directory: (boolean) whether to delete computation directory or not.
        """

        if not self.computation_over:
            self.killing_wondered = True
            command = f'echo "{messaging._kill_computation(delete_directory)}" >> {self.communication_file}'
            out, err = self.ssh.simple_command(command)
            if err:
                # log an error if ssh command fails, as if it was send by computation server
                self.ssh.analyse_received.data_queue.put(messaging._error('Error while killing the remote computation',
                                                         no_kill=True))

    def user_message_callback(self, mo):

        """ Handler called at user message incoming event : append message to the log list.

        :param mo: (re.MatchObject) from re.match containing group 'message'.
        """

        if not mo:
            return
        msg = mo.group('message').rstrip()
        self.append_log(f'Received : {msg}')

    def state_callback(self, mo):

        """ Handler called at status message incoming event : put status to the dedicated queue.

        :param mo: (re.MatchObject) from re.match, containing groups 'status' and 'message'.
        """

        if not mo:
            return
        self.change_status(int(mo.group('status').rstrip()), mo.group('message').rstrip())

    def data_callback(self, mo):

        """ Handler called when data regex is read : these data from remote server.

        :param mo: (re.MatchObject) from re.match containing group 'message' (local path of data) and optionally
                   'intermediate'.
        """

        if not mo:
            return

        data_path = json.loads(mo.group('message'))['path']

        scp = SCPHandler(self.ip_address, self.username, key_file=self.key_file)
        fs = ScoreFileSystem(create_tree=True)
        received_data_name, ext = os.path.splitext(os.path.basename(data_path))
        new_data_file_name = f'{received_data_name}_{self.computation_id}{ext}'
        local_data_path = os.path.join(fs.outputs, new_data_file_name)
        scp.get(data_path, local_data_path)
        scp.close()

        if mo.group('intermediate'):
            self.queue_intermediate_data.put(local_data_path)
            if self.intermediate_data_observer is not None:
                self.get_intermediate_data()
        else:
            self.queue_final_data.put(local_data_path)
            self.computation_over = True
            if self.final_data_observer is not None:
                self.get_final_data()

    def error_callback(self, mo):

        """ Handler called at error message incoming event :\n
        -> read error text\n
        -> kill process after reading if worker ask for (by adding ?? in message)\n

        :param mo: (re.MatchObject) from re.match containing group 'message', and optionally 'no_kill' and 'delete_dir'.
        """

        if not mo:
            return

        out, err = self.ssh.simple_command('cat {}'.format(mo.group('message').rstrip()))
        self.show_error(out)
        if self.killing_wondered and mo.group('delete_dir'):
            self.append_log("Process over with error !")
            self.computation_over = True
            self.ssh.stop_channel()

            # This sleep time let time for all clients to disconnect, otherwise as clients are reading a file in
            # directory, the directory cannot be removed (BUSY)
            time.sleep(2)

            out, err = self.ssh.simple_command(f'rm -rf {self.fs.root}')
            if not err:
                self.append_log("Computation directory deleted")
            else:
                self.show_error(f"\nRemote computation directory has not been removed : \n{err}")
            self.ssh.kill()
        elif not mo.group('no_kill'):
            self.append_log("Process over with error !")
            self.computation_over = True
            self.ssh.kill()

    def system_message_callback(self, mo):

        """ Handler called at system message incoming event :\n
                message == 'END' : end process by turning computation_over at True\n
                message == 'INIT' : the remote process wait for the creation of a file, which mean that some program
                                    is listening to it

        :param mo: (re.MatchObject) from re.match containing group 'message'.
        """

        if not mo:
            return
        msg = mo.group('message').rstrip()
        if msg == "END":
            self.computation_over = True
            self.append_log('Computation over')
            if self.waiting_code_return.acquire(blocking=False):
                self.waiting_code_return.release()
                self.ssh.kill()
        elif "INIT" in msg:
            if not self.communication_file:
                self.communication_file = msg[4:]
                out, err = self.ssh.simple_command('touch {}'.format(self.communication_file))

        elif msg == "end_code":
            self.waiting_code_return.release()
            if self.computation_over:
                self.ssh.kill()

        else:
            self.append_log(f'SCORE internal message : {msg}')

    def _connect(self):

        """ Look for the log file on the remote server, which means that computation is running.

        :return: (boolean) whether the connexion succeed or not.
        """

        for i in range(5):
            out, err = self.ssh.simple_command('[ -f {} ] && echo "OK" || echo "NO"'.format(self.fs.log_file))
            if out == "OK":
                return True
            time.sleep(1)
        return False
