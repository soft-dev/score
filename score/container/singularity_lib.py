# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
from threading import Thread
from spython.main import Client
from hashlib import md5
import subprocess

from ..tools.ssh import SSHHandler
from ..statics.file_system import ScoreFileSystem


def check_local_simage(simage_file, md5sum):

    """ Check if locale singularity image checksum is the same as expected.

    :param simage_file: (str) path of singularity image file.
    :param md5sum: (str) md5_sum of the singularity image.
    :return: (boolean) whether the image file has the same checksum than the one provided.
    """

    try:
        with open(simage_file, 'rb') as fd:
            simage_md5sum = md5(fd.read()).hexdigest()
    except FileNotFoundError:
        return False

    return simage_md5sum == md5sum


def check_remote_simage(server, username, key_file, simage_name, md5sum):

    """ Check if singularity image is present on remote computation server, in SCORE's configuration directory.

    :param server: (str) IP address or hostname of remote server.
    :param username: (str) username used for the connection.
    :param key_file: (str) path of the private key file to use for connection.
    :param simage_name: (str) name of image file we are looking for.
    :param md5sum: (str) md5_sum of the singularity image, to assure the computation traceability.
    :return: (boolean) whether the image file has been found or not on remote server.
    """

    command = 'md5sum {}/{} | awk \'{{ print $1 }}\''.format(ScoreFileSystem.MAIN_CONFIG_DIR, simage_name)

    remote_md5sum, error = SSHHandler(server, username, key_file=key_file).simple_command(command, close_session=True)
    return not error and remote_md5sum == md5sum


def build_simage(definition_file, image_file, process_list=None):

    """ Builds a singularity image and stream the output to a generator.\n
        This function is a copy of stream_command from spython package, including stderr in stdout, because singularity
        redirects some useful message in stderr.

    :param definition_file: (str) path to a singularity definition file.
    :param image_file: (str) path to the image to build.
    :param process_list: (list, optional) is a list which will contain the subprocess.Popen object after execution.\n
                         This is a list because list are mutable, so we can use it as a pointer. It allows the program
                         using 'build_simage' function to kill the process if necessary.
    :return: (generator) containing each status line of build.
    """

    if not os.path.isdir(os.path.dirname(image_file)):
        os.makedirs(os.path.dirname(image_file))

    command = ['singularity', 'build']

    if os.geteuid() != 0:
        command.append('--fakeroot')

    command.extend(['--force', '--nohttps', image_file, definition_file])

    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    if isinstance(process_list, list):
        process_list.append(process)

    for line in iter(process.stdout.readline, b''):
        yield line.decode('UTF-8')
    process.stdout.close()
    return_code = process.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, command)


class SingularityDetached(Thread):

    """ Useful to run singularity container in detached mode, as it is not allow in python API of singularity.

    :param image: (str) path to singularity image to use.
    :param bind_folder: (str) path to folder to bind in singularity image.
    :param script: (str) path to the script to execute in image.
    :param args: (list) list of arguments specifics to the 'function'.
    """

    def __init__(self, image, bind_folder, script, *args):

        """ Initialize attributes and load image in singularity client. """

        super().__init__()
        Client.load(image)
        self.bind_folder = bind_folder
        self.script = script
        self.args = args

    def run(self):

        """ Execute a specific script using the singularity image environment. """

        command = ['python', self.script]
        for arg in self.args:
            command.append(str(arg))
        Client.execute(command, bind=self.bind_folder)


def launch_singularity(image, port_redirect, src_folder):

    """ Execute a singularity image in a thread, as python API of singularity does not allow detached process.\n
        In this image we will run encapsulate.py script.

    :param image: (str) path to singularity image to use.
    :param port_redirect: (int) USELESS as we don't need to redirect ports in singularity.
    :param src_folder: (str) path to folder containing user application.
    :return: (SingularityDetached) object, which contain information on running container.
    """

    print("Used port = {}".format(port_redirect))
    detached_container = SingularityDetached(image, src_folder, os.path.join(src_folder, "encapsulate.py"), port_redirect)
    SingularityDetached.start(detached_container)
    return detached_container


def build_simage_stream(definition_file, image_file):

    """ Builds a singularity image and stream the build status to a generator.

    :param definition_file: (str) path to a singularity definition file.
    :param image_file: (str) path to the image to build.
    :return: (generator) containing each status line of build.
    """

    if not os.path.isdir(os.path.dirname(image_file)):
        os.makedirs(os.path.dirname(image_file))

    options = ['--force', '--nohttps']
    if os.geteuid() != 0:
        options.append('--fakeroot')

    _, stream = Client.build(recipe=definition_file,
                             image=image_file,
                             options=options,
                             sudo=False,
                             stream=True)
    return stream
