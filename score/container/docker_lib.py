# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
import tarfile
from tempfile import NamedTemporaryFile
from io import BufferedRWPair, DEFAULT_BUFFER_SIZE
from re import compile

import docker

__all__ = ['launch_docker', 'kill_docker', 'image_exists', 'docker_copy']

timeout = 20
token_pattern = r"\?token=([0-9a-f]*)\s"

# regex used to validate and extract data from the src and dest inputs
pathre = compile(r"^(?:(?P<container>\w+):)?(?P<path>\.?(?:/?[\w\-_\.]*)+/?)$")


def kill_docker(container, error_code, client=None):

    """ Stop and remove a running docker container

    TODO: Don't remember why this function remains... remove it

    :param container: (docker.client.container) instanced object
    :param error_code: (str) an error code to print ???
    :param client: (docker.client, optional) if we need an other than environment one
    :return:
    """

    try:
        if client is None:
            client = docker.from_env()
        container.stop()
        container.remove()
        client.close()
    finally:
        print(error_code)


def launch_docker(image, port_redirect, src_folder):

    """ Launch a docker container to execute user code in it, forwarding a socket port if necessary
        Basically, the steps are :
            -> create a new image making a copy of the given one in arguments, and adding the folder containing user
            program
            TODO: find a way to not duplicate identical images, the only way I found to copy a folder in existing image
                  and use the 'run' command
            -> run a container from that new image, and run encapsulate.py script (which will run user function) in it
            -> return the container


    :param image: (str) referring an existing docker image on server
    :param port_redirect: (int) port redirection for external reach on that docker image
    :param src_folder: (str) path to folder containing user application, to copy in new image
    :return: (docker.client.container) running the encapsulate.py script
    """

    client = docker.from_env()

    container = client.containers.create(image)
    docker_copy(client, os.path.join(src_folder, os.curdir), "{}:/opt/work".format(container.id))

    new_image = "{}_{}".format(image, os.path.basename(src_folder))
    container.commit(new_image)
    container.stop()
    container.remove()

    container = client.containers.run(new_image, command="python /opt/work/encapsulate.py",
                                      detach=True, ports={9999: port_redirect})

    for c in client.containers.list():
        if c.id == container.id:
            container = c

    network_settings, = container.attrs['NetworkSettings']['Networks'].values()
    ip = network_settings['IPAddress']
    print("To stop docker : \ndocker container stop {};docker container rm {}\n".format(container.id, container.id))

    client.close()
    # return container, (ip, port_redirect)
    return container


def copy_from_container(container, src, dest, bufsize):
    """Method to copy file from container to local filesystem"""
    tar_name = None
    with NamedTemporaryFile(buffering=bufsize, prefix="dockercp", delete=False) as f:
        tar_name = f.name
        archive = container.get_archive(src)
        buff = BufferedRWPair(archive[0], f, bufsize)
        # read the data (an archive) sent by docker daemon into a temporary file locally
        while True:
            if buff.write(buff.read(bufsize)) < bufsize:
                break
        buff.flush()
    # let's extract the archive into the destination
    with tarfile.open(tar_name, bufsize=bufsize) as tar:
        tar.extractall(path=dest)
    os.remove(tar_name)


def copy_to_container(container, src, dest, bufsize):
    """Method to copy file from local file system into container"""
    # it's necessary create a tar file with src file/directory
    archive = None
    with NamedTemporaryFile(buffering=bufsize, prefix="dockercp", delete=False) as fp:
        with tarfile.open(mode="w:bz2",fileobj=fp, bufsize=bufsize) as tar:
            tar.add(src, arcname=os.path.basename(src))
        archive = fp.name
    # send the tar to the container
    if archive is not None:
        result = False
        with open(archive, "rb") as fp:
            result = container.put_archive(dest, fp)
        os.remove(archive)
        return result
    return False


def docker_copy(client, src, dest, bufsize=DEFAULT_BUFFER_SIZE):
    """Copy the file in src path to dest"""
    src_match = pathre.match(src)
    dest_match = pathre.match(dest)
    if src_match.group("container"):
        # copy from container
        container = client.containers.get(src_match.group("container"))
        copy_from_container(container, src_match.group("path"),
            dest_match.group("path"), bufsize)
    else:
        # copy to container
        container = client.containers.get(dest_match.group("container"))
        copy_to_container(container, src_match.group("path"),
            dest_match.group("path"), bufsize)


def image_exists(image_name):
    return 1


if __name__ == "__main__":
    root_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, os.pardir)
    # print(root_directory)

    container_test, address = launch_docker("score:lmgc90", 8888, os.path.join(root_directory, "src"))

    print("Access notebook at : {}".format(address))
