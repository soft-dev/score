# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import os
try:
    from importlib import resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from hashlib import md5

from ..statics.file_system import ScoreFileSystem
from ..container.singularity_lib import build_simage, check_local_simage


class RunnerBase:

    """ RunnerBase is the main class of SCORE to export computation on remote server.

    :param testing: (boolean) whether it is a test or a real computation.
    :param log_callback: (callable) function to call to return log to Core object.
    :param error_callback: (callable) function to call to return error to Core object.
    """

    monitoring_options = ['local', 'socket', 'ssh', 'slurm']

    def __init__(self, testing=False, status_callback=None, log_callback=None, error_callback=None):

        self.type = 'local'
        self.status = "Initialisation"

        self.testing = testing
        self.status_callback = status_callback
        self.log_callback = log_callback
        self.error_callback = error_callback

        self.ready_to_compute = False

        self.python_path = None

        self.simage_file = ''
        self.simage_md5sum = ''
        self.simage_definition_file = ''

        self.status = "Initialisation"

        # SCORE files and directories
        self.relative_fs = ScoreFileSystem(create_tree=True)

    def compute(self, application_src, storage_path=None):

        """ Launch computation, must be implemented on each child.

        :param application_src: (str) local path to the application to compute.
        :param storage_path: (str, optional) computation server's local path to store data at the end of the
                             computation (to bind the directory in Singularity).\n
                             This must be filled only if the data are to be stored on the server which performs the
                             computation.
        :return: (str) Path to the computation directory.
        """

        raise NotImplementedError

    def set_simage(self, simage_file, simage_md5sum='', simage_definition_file=''):

        """ Set the singularity image to use for computation.\n
        It may not work if the current environment is a singularity environment and the computation run locally
        (it is not possible to run a singularity container from a singularity container).

        :param simage_file: (str) local path of singularity image to use for computation (or to build if necessary).
        :param simage_md5sum: (bytes) expected md5sum of the image.
        :param simage_definition_file: (str) path to singularity definition file, used to build image if it doesn't
                                       exist.\n
                                       The definition file is not kept if the code run in a singularity container, as it
                                       is not possible to build an image from a container.
        """

        simage_file = os.path.expandvars(simage_file)
        simage_definition_file = os.path.expandvars(simage_definition_file)

        if 'SINGULARITY_CONTAINER' in os.environ.keys():
            if not (self.type == 'ssh' or self.type == 'slurm'):
                self._log('You cannot run a singularity container from a singularity container.\n'
                          'To compute locally in a container, you have 2 choices :\n'
                          '\t- import CoreBase object from OS environment, and use "CoreBase.set_simage"\n'
                          '\t- import CoreBase object from the container, it uses by default the current python '
                          'path in the container (sys.executable), or you can setup the CoreBase.python_path')
                return False
            elif (self.type == 'ssh' or self.type == 'slurm') and self.ip_address is None:
                self._log('There is no computation server setup, it will not work locally')

        if simage_definition_file:
            if 'SINGULARITY_CONTAINER' not in os.environ.keys():
                self.simage_definition_file = simage_definition_file
            else:
                self._log('The current program is running in a singularity container, what prevents us to build '
                          'an other singularity image. Therefore the definition file is useless in this case.')

        if simage_md5sum:
            if check_local_simage(simage_file, simage_md5sum):
                self.simage_file = simage_file
                self.simage_md5sum = simage_md5sum
                return True

            else:
                if not self.simage_definition_file:
                    self._log("Singularity image has not been found, and the definition file can't be used (or "
                              "hasn't been provided). The computation is not possible.")
                    return False

                if not os.path.isfile(self.simage_definition_file):
                    self._log("Singularity image has not been found, and the definition file hasn't been found. "
                              "The computation is not possible.")
                    return False

                self._log("Building singularity image")
                self.build_image(self.simage_definition_file, simage_file)

        elif not os.path.isfile(simage_file):
            self._log("Singularity image have to be build before.")
            return False

        self.simage_file = simage_file
        with open(self.simage_file, 'rb') as fd:
            self.simage_md5sum = md5(fd.read()).hexdigest()
        return True

    def build_image(self, definition_file, image_file):

        """ Build a singularity image and update the status variable with the status of build.

        :param definition_file: (str) path to a singularity definition file.
        :param image_file: (str) path to the image to build.
        """

        build_stream = build_simage(definition_file, image_file)
        self._log('Building singularity image')
        self.status = 'Building singularity image'
        for line in build_stream:
            self.status = line

    def _status(self, status, progress=None):

        if callable(self.status_callback):
            self.status_callback(status, progress)

    def _log(self, log):

        """ Function called when some logs are to be wrote.

        :param log: (str) new log.
        """

        if callable(self.log_callback):
            self.log_callback(log)
        else:
            print(log)

    def _error(self, error):

        """ Function called when an error occurred before computation has been launched.

        :param error: (str) error text.
        """

        if callable(self.error_callback):
            self.error_callback(error)
        else:
            print(f'\033[31m{error}\033[0m')

    def get_socket_port(self, application_src, count=3):

        """ If monitoring tools is socket, this function will retrieve the tcp socket port used by local server. """

        pass
