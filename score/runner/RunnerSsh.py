# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import traceback
import time
import os
from threading import Semaphore

from .RunnerBase import RunnerBase
from ..statics.file_system import ScoreFileSystem
from ..tools.ssh import SSHHandler, SCPHandler, test_ssh_connection, create_remote_directory
from ..container.singularity_lib import check_remote_simage


class RunnerSsh(RunnerBase):

    """ Extends :py:class:`score.runner.RunnerBase.RunnerBase`\n
    Class to export computation on remote server using ssh/scp copy files and execute computation.

    :param timeout: (int) time limit to retrieve the remote PID of process ran, before considering the process did not
                    start as expected.
    :param testing: (boolean) whether it is a test or a real computation.
    :param log_callback: (callable) function to call to return log to Core object.
    :param error_callback: (callable) function to call to return error to Core object.
    """

    def __init__(self, timeout=10, **kwargs):

        super().__init__(**kwargs)
        self.type = 'ssh'
        self.ip_address = None
        self.username = None
        self.key_file = None
        self.computation_dir = None
        self.ssh_handler = None
        self.timeout = timeout
        self.copying_simage = Semaphore()

    def compute(self, application_src, storage_path=None):

        """ Run application on remote computation server using SSH.\n
        (1) copy application directory on computation server\n
        (2) checks for remote singularity image, using md5sum\n
            > if not, check for local singularity image, using md5sum\n
                > if not, build the local image (if definition file provided and not running in container)\n
            > copy local image to remote server\n
        (3) remotely executes application using singularity\n
        (4) checks if the process run by looking for its PID.

        :param application_src: (str) local path to the application to compute.
        :param storage_path: (str, optional) computation server's local path to store data at the end of the
                             computation (to bind the directory in Singularity).\n
                             This must be filled only if the data are to be stored on the server which performs the
                             computation.
        :return: (str) path to the remote computation directory.
        """

        remote_path = os.path.join(self.computation_dir, os.path.basename(application_src))

        command = '{} {}/score_compute.py'.format(self.python_path or 'python', remote_path)

        pid = self.ssh_handler.get_pid(command)
        if pid:
            self._log('The exact same process is already running on server')
            return remote_path

        self._remote_copy(application_src, remote_path)

        # if self.make_temp:
        #     rmtree(os.path.dirname(compute_configuration['application_src']))

        self._log('files copied to {}@{}:{}'.format(self.username, self.ip_address, remote_path))
        self.copying_simage.acquire()

        try:
            if self.simage_file:
                if not check_remote_simage(self.ip_address, self.username, self.key_file,
                                           os.path.basename(self.simage_file), self.simage_md5sum):

                    self._log("Copying singularity image on computation server")

                    self._copy_simage_to_computation(self.simage_file)

                bind_folders = f'{remote_path},{self.relative_fs.MAIN_CONFIG_DIR}'
                if storage_path:
                    bind_folders += f',{storage_path}'

                full_command = f'singularity exec --containall --bind {bind_folders} ' \
                               f'{os.path.join(self.relative_fs.MAIN_CONFIG_DIR, os.path.basename(self.simage_file))} ' \
                               f'{command}'
            else:
                full_command = command

        except:
            traceback.print_exc()
            return None
        finally:
            self.copying_simage.release()

        self.ssh_handler.exec_detach(full_command, error_file=os.path.join(remote_path, self.relative_fs.error_file))

        debut = time.time()
        while not pid:
            pid = self.ssh_handler.get_pid(command)
            time.sleep(1)
            if time.time() - debut >= self.timeout:
                break

        if pid:
            return remote_path
        else:
            out, err = self.ssh_handler.simple_command('cat {}'.format(os.path.join(remote_path,
                                                                                    self.relative_fs.error_file)))
            self._error(f'Remote error : \n{out}')
            return None

    def set_server(self, server_ip, username, key_file=None, simage_file=None,
                   remote_computation_directory=ScoreFileSystem.MAIN_COMPUTE_DIR):

        """ Test connection to a computation server, and set it up for future computation.

        :param server_ip: (str) IP address or hostname of remote server we'd like to use for computation.
        :param username: (str) username used for the connection.
        :param key_file: (str, optional) path of the private key file to use for connection.\n
                         This parameters is optional to stay generic, but should be filled. If not, ssh will try to
                         connect using user ssh key pair (see ssh documentation for more information).
        :param simage_file: (str, optional) local path of singularity image to use for computation.
        :param remote_computation_directory: (str, optional) remote path on computation server to process the
                                             computation, allows to process on SSD for faster access for example.
        :return: (boolean) whether the computation server can and will be used for future computation.
        """

        if test_ssh_connection(server_ip, username, key_file):

            if not create_remote_directory(server_ip, username, key_file, self.relative_fs.MAIN_CONFIG_DIR):
                self._log("Error while creating remote config directory, computation could went wrong")

            self.ssh_handler = SSHHandler(server_ip, username, key_file=key_file)
            self.computation_dir, err = self.ssh_handler.simple_command(f'echo {remote_computation_directory}')
            if err:
                print("An error occurred while trying to retrieve the remote full path")
                self._error(err)
                self.computation_dir = None
                return False

            self.ip_address = server_ip
            self.username = username
            self.key_file = key_file

            if simage_file:
                if not self.set_simage(simage_file):
                    self.ready_to_compute = False
                    self._log("Error with singularity image")
                    return False
                else:
                    self.ready_to_compute = True
            elif not self.simage_file:
                print("Singularity image has not been provided, computation will be done in system environment on the "
                      "computation server")
                self.ready_to_compute = True
            return True
        else:
            self._log("Error with computation setting, it seems that you do not have permission to use it.")
            return False

    def _remote_copy(self, application_src, remote_path):

        """ Copies a local directory on the computation server set up.

        :param application_src: (str) path of a directory to copy on computation server.
        :param remote_path: (str) path of the final directory on remote server.
        """

        out, err = self.ssh_handler.simple_command('rm -rf {}'.format(remote_path))

        self.status = "Copying files to {}".format(self.ip_address)
        SCPHandler(self.ip_address,
                   self.username,
                   self.key_file).put_dir(application_src, remote_path, one_shot=True)

    def _copy_simage_to_computation(self, simage_file):

        """ Copy the singularity image to computation server.\n
        ..todo:
            Perform copy in a dedicated thread to allow user to execute other part of the code in the same time.

        :param simage_file: (str) path to the singularity image to copy to the server.
        """

        def copy_status(bytes_copied, bytes_supposed):
            """ Callback function called by paramiko.SFTP.put, and used in our case to know when the singularity image
            is copied on computation server.

            :param bytes_copied: (int) number of bytes copied on server.
            :param bytes_supposed: (int) number of bytes to copy.
            """

            simage_copy_status = round(100. * bytes_copied/bytes_supposed, 1)
            self._status("", progress=simage_copy_status)
            if bytes_copied == bytes_supposed:
                self.ready_to_compute = True

        self._status("Copying environment")
        config_dir, _ = SSHHandler(self.ip_address,
                                   self.username,
                                   key_file=self.key_file
                                   ).simple_command(f'echo {self.relative_fs.MAIN_CONFIG_DIR}', close_session=True)

        scp = SCPHandler(self.ip_address, self.username, key_file=self.key_file)

        scp.put(simage_file,
                os.path.join(config_dir, os.path.basename(simage_file)),
                callback=copy_status,
                one_shot=True)

    def get_socket_port(self, remote_path, count=3):

        """ If monitoring tools is socket, this function will retrieve the remote tcp socket port used by
            computation server.

        :param remote_path: (str) path of the application on the remote computation server.
        :param count: (int) number of attempts before giving up.
        :return: (int) port number, 0 if no result.
        """

        out, err = self.ssh_handler.simple_command('cat {}'.format(os.path.join(remote_path,
                                                                                self.relative_fs.socket_port_file)))

        if err:
            self._error(f"Error while trying to read the socket file :\n{err}")
            return 0
        try:
            return int(out)
        except ValueError:
            if count:
                time.sleep(2)
                return self.get_socket_port(remote_path, count - 1)
            else:
                self._log("Remote socket number not found, no connection possible")
                self._error(f"Remote socket cannot been casting to int. Value : {out}")
                return 0
