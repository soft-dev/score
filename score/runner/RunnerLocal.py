# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import sys
import os
import time
import subprocess

try:
    from importlib import resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from .RunnerBase import RunnerBase


class RunnerLocal(RunnerBase):

    """ Extends :py:class:`score.runner.RunnerBase.RunnerBase`\n
    Class to export computation on local server.

    :param testing: (boolean) whether it is a test or a real computation.
    :param log_callback: (callable) function to call to return log to Core object.
    :param error_callback: (callable) function to call to return error to Core object.
    """

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self.type = 'local'

        self.ready_to_compute = True

    def compute(self, application_src, storage_path=None):

        """ Launch local computation.

        :param application_src: (str) local path to the application to compute.
        :param storage_path: (str, optional) computation server's local path to store data at the end of the
                             computation (to bind the directory in Singularity).\n
                             This must be filled only if the data are to be stored on the server which performs the
                             computation.
        :return: (str) Path to the computation directory.
        """

        python_executable = 'python' if self.simage_file else sys.executable
        command = [self.python_path or python_executable,
                   '{}/score_compute.py'.format(application_src)]

        if self.simage_file:
            bind_folders = f'{application_src}'
            if storage_path:
                bind_folders += f',{storage_path}'

            full_command = ['singularity', 'exec', '--containall', '--bind', bind_folders, self.simage_file]
            full_command.extend(command)

        else:
            full_command = command

        self._log(f"command : {' '.join(full_command)}")
        subprocess.Popen(full_command, start_new_session=True)
        self._log("Command executed")
        return application_src

    def get_socket_port(self, application_src, count=3):

        """ If monitoring tools is socket, this function will retrieve the tcp socket port used by local server.

        :param application_src: (str) path to the application.
        :param count: (int) number of attempts before giving up.
        :return: (int) port number, 0 if no result.
        """

        try:
            with open(os.path.join(application_src, self.relative_fs.socket_port_file), 'r') as fd:
                return int(fd.read())
        except FileNotFoundError:
            if count:
                time.sleep(2)
                return self.get_socket_port(application_src, count - 1)
            else:
                self._log("Socket number not found, no connection possible")
                self._log("Socket file has not been created")
                return 0
