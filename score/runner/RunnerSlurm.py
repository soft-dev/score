# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import traceback
import os

try:
    import radical.saga as rs
    SAGA = True
except ModuleNotFoundError:
    SAGA = False

from .RunnerSsh import RunnerSsh
from ..statics.file_system import ScoreFileSystem
from ..tools.ssh import SSHHandler, test_ssh_connection, create_remote_directory
from ..container.singularity_lib import check_remote_simage


class RunnerSlurm(RunnerSsh):

    """ Extends :py:class:`score.runner.RunnerSsh.RunnerSsh`\n
    Class to export computation on remote server with Slurm as jobs scheduler.

    :param timeout: (int) time limit to retrieve the remote PID of process ran, before considering the process did not
                    start as expected.
    :param testing: (boolean) whether it is a test or a real computation.
    :param log_callback: (callable) function to call to return log to Core object.
    :param error_callback: (callable) function to call to return error to Core object.
    """

    def __new__(cls, *args, **kwargs):

        """ Prevent object creation if all required dependencies are not satisfied """

        if not SAGA:
            raise Exception("You need to install radical-saga to use Slurm job scheduler.")

        instance = super().__new__(cls)
        return instance

    def __init__(self, **kwargs):

        super().__init__(**kwargs)

        self.type = 'slurm'
        self.queue = None
        self.modules = None
        self.job_id = None
        self.execution_hosts = None
        self.slurm_job_service = None

    def compute(self, application_src, storage_path=None):

        """ Launch computation on remote computation server using SSH/SLURM.\n
        (1) creates the SLURM job using radical SAGA\n
        (2) copy application directory on computation server\n
        (3) checks for remote singularity image, using md5sum\n
            > if not, check for local singularity image, using md5sum\n
                > if not, build the local image (if definition file provided and not running in container)\n
            > copy local image to remote server\n
        (4) remotely executes application radical SAGA\n

        :param application_src: (str) local path to the application to compute.
        :param storage_path: (str, optional) computation server's local path to store data at the end of the
                             computation (to bind the directory in Singularity).\n
                             This must be filled only if the data are to be stored on the server which performs the
                             computation.
        :return: (str) path to the remote computation directory.
        """

        remote_path = os.path.join(self.computation_dir, os.path.basename(application_src))

        # create job
        job_description = rs.job.Description()
        job_description.working_directory = remote_path
        job_description.name = 'SCORE_JOB'
        job_description.output = 'SCORE_MANAGE/slurm.out'
        job_description.error = 'SCORE_MANAGE/slurm.err'
        if self.queue:
            job_description.queue = self.queue

        # create job command without singularity
        job_description.executable = f"{self.python_path or 'python'}"
        job_description.arguments = [f"{remote_path}/score_compute.py"]

        # Add modules
        if self.modules:
            job_description.pre_exec = []
            for mod in self.modules:
                job_description.pre_exec.append(f"module load {mod}\n")

        self._remote_copy(application_src, remote_path)

        # if self.make_temp:
        #     rmtree(os.path.dirname(compute_configuration['application_src']))

        self._log('files copied to {}@{}:{}'.format(self.username, self.ip_address, remote_path))
        self.copying_simage.acquire()

        try:
            if self.simage_file:
                if not check_remote_simage(self.ip_address, self.username, self.key_file,
                                           os.path.basename(self.simage_file), self.simage_md5sum):

                    self._log("Copying singularity image on computation server")

                    self._copy_simage_to_computation(self.simage_file)

                bind_folders = f'{remote_path},{self.relative_fs.MAIN_CONFIG_DIR}'
                if storage_path:
                    bind_folders += f',{storage_path}'

                # update job command using singularity
                backup_arguments = [job_description.executable] + job_description.arguments
                job_description.executable = 'singularity'
                job_description.arguments = ["exec", "--containall", "--bind", bind_folders,
                                             os.path.join(self.relative_fs.MAIN_CONFIG_DIR,
                                                          os.path.basename(self.simage_file))] + backup_arguments

        except:
            traceback.print_exc()
            return None
        finally:
            self.copying_simage.release()

        # run job
        job = self.slurm_job_service.create_job(job_description)
        job.run()

        self.job_id = job.id
        self.execution_hosts = job.execution_hosts

        return remote_path

    def set_server(self, server_ip, username, key_file=None, queue=None, modules=None, simage_file=None,
                   remote_computation_directory=ScoreFileSystem.MAIN_COMPUTE_DIR):

        """ Test connection to a computation server, and set it up for future computation.

        :param server_ip: (str) IP address or hostname of remote server we'd like to use for computation.
        :param username: (str) username used for the connection.
        :param key_file: (str, optional) path of the private key file to use for connection.\n
                         This parameters is optional to stay generic, but should be filled. If not, ssh will try to
                         connect using user ssh key pair (see ssh documentation for more information).
        :param queue: (str, optional) name of the queue to use on slurm server (use default if None).
        :param modules: (str or list of str, optional) name of the modules to load on the cluster.
        :param simage_file: (str, optional) local path of singularity image to use for computation.
        :param remote_computation_directory: (str, optional) remote path on computation server to process the
                                             computation, allows to process on SSD for faster access for example.
        :return: (boolean) whether the computation server can and will be used for future computation.
        """

        if test_ssh_connection(server_ip, username, key_file):

            if not create_remote_directory(server_ip, username, key_file, self.relative_fs.MAIN_CONFIG_DIR):
                self._log("Error while creating remote config directory, computation could went wrong")

            self.ssh_handler = SSHHandler(server_ip, username, key_file=key_file)

            slurm_url = "slurm+ssh://" + server_ip
            slurm_context = rs.Context('ssh')
            slurm_context.user_id = username
            if key_file:
                slurm_context.user_key = key_file
            slurm_session = rs.Session()
            slurm_session.add_context(slurm_context)
            self.slurm_job_service = rs.job.Service(slurm_url, slurm_session)

            self.computation_dir, err = self.ssh_handler.simple_command(f'echo {remote_computation_directory}')
            if err:
                print("An error occurred while trying to retrieve the remote full path")
                self.computation_dir = None
                return False

            self.ip_address = server_ip
            self.username = username
            self.key_file = key_file
            self.queue = queue

            if isinstance(modules, list):
                self.modules = [m for m in modules if m]
            elif isinstance(modules, str) and modules:
                self.modules = [modules]

            if simage_file:
                if not self.set_simage(simage_file):
                    self.ready_to_compute = False
                    self._log("Error with singularity image")
                    return False
                else:
                    self.ready_to_compute = True
            elif not self.simage_file:
                print("Singularity image has not been provided, computation will be done in system environment on the "
                      "computation server")
                self.ready_to_compute = True
            return True
        else:
            self._log("Error with computation setting, it seems that you do not have permission to use it.")
            return False
