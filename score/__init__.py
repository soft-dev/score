# -*- coding: utf-8 -*-
#
#       Copyright CNRS - Université de Montpellier
#
#       This file is part of SCORE library.
#
#       Distributed under the GNU Lesser General Public License (LGPL).
#       See accompanying file LICENSE.
#
# ==============================================================================

import warnings
from .Core import Core


# Retro compatibility
class CoreBase:
    def __new__(cls, *args, **kwargs):
        warnings.warn("Use generic score.Core object instead.", DeprecationWarning, stacklevel=2)
        return Core()


class CoreSsh:
    def __new__(cls, *args, **kwargs):
        warnings.warn("Use generic score.Core object instead.", DeprecationWarning, stacklevel=2)
        return Core()


class CoreSlurm:
    def __new__(cls, *args, **kwargs):
        warnings.warn("Use generic score.Core object instead.", DeprecationWarning, stacklevel=2)
        return Core()
