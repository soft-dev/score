# SWAF : user requirement document

## 1. Introduction

### 1.1. Objectifs
Le projet vise à développer un canevas logiciel permettant de générer des interfaces entre un client web et un serveur de calcul exécutant des logiciels scientifiques spécifiques.
Ce canevas doit aussi proposer la gestion de rendus graphiques (2D/3D), de soumission de projets, la gestion des accès à la plate-forme et la sécurité.

### 1.2. Portée
Le framework développé doit être suffisamment générique pour être indépendant des outils de calculs qu'il interface.
Le premier besoin concerne les plate-formes de modélisation et simulation, mais il est aussi envisagé de l'utiliser pour du traitement d'images par exemple.
Les développements pourront s'appuyer sur la plate-forme de modélisation / simulation LMGC90.

### 1.3. Définitions, acronymes et abréviations

### 1.4. Références

### 1.5. Vue d'ensemble

## 2. Description générale

### 2.1. "Positionnement" du produit
La généricité du logiciel fait qu'il ne se positionne pas comme complémentaire à un(des) logiciel(s) en particulier.

### 2.2. Fonctionnalités générales
cf. description projet WIFFIS ?

### 2.3. Contraintes générales
cf. description projet WIFFIS ?
Accès, communication...

### 2.4. Description des utilisateurs
Il existe 2 niveaux d'utilisation du canevas logiciel :

1. Utilisateur connaissant l'outils de calcul (utilisateur avancé)

    * la personne qui met en place l'interface en utilisant le canevas est une personne ayant des compétences minimales en informatique.
    * elle est capable d'écrire ses propres scripts, et ainsi d'utiliser les outils de calculs informatiques sans interface.
    * cette personne n'est pas supposée avoir de compétences en langages web, le framework doit lui permettre de déployer une interface web sans ces connaissances.

2. Utilisateur de l'outil de calcul via l'interface (utilisateur final)

    * cet utilisateur est intéressé par les résultats de l'outil de calcul, résultant des paramètres qu'il a renseigné via l'interface.
    * il n'est pas censé avoir de connaissances en programmation, autant pour le calcul que pour les interfaces utilisateur.
    * il devra pouvoir renseigner les paramètres à partir de l'interface, lancer les calculs, suivre leurs progressions en ligne et visualiser les résultats.

### 2.5. Environnement de fonctionnement
L'idée générale (non imposée) est que les calculs se réalisent sur un serveur dédié (cluster, grille...), et que l'interface web soit sur un autre serveur dédié.
Enfin l'utilisateur ou le développeur travaille sur sa propre machine, et est ainsi connecté au serveur de calcul via l'interface web.

Cela implique de gérer la communication entre (i) le(les) serveur(s) de calculs et le serveur web et (ii) le serveur web et le poste utilisateur.
L'utilisateur / développeur pourra ainsi communiquer avec l'outil de calcul sans se préoccuper de l'installation serveur.

```text
? Ajouter un schéma ?
```

### 2.6. Justification des exigences


## 3. Besoins spécifiques
```text
C'est le cœur de l'URD, c'est ici qu'on entre dans les détails sur les fonctionnalités
```

### 3.1. Fonctionnalités spécifiques

### 3.2. Contraintes spécifiques