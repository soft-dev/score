# SWAF : software requirements document

## 1. Introduction

### 1.1. Objectifs

### 1.2. Portée
Le projet pourrait s'appeler SWAF : Software Web Application Framework

1. Ce logiciel facilitera grandement la réalisation d'interfaces web permettant de paramétrer, exécuter, suivre et analyser des calculs informatiques.
2. Il ne sera pas dépendant du logiciel de calcul qu'il interface.
3. Il pourra être utilisé par des non programmeurs.

### 1.3. Définitions, acronymes et abréviations

### 1.4. Références

### 1.5. Vue d'ensemble

## 2. Description générale

### 2.1. Relation aux projets actuels
Ce projet est indépendant d'autres développements locaux, mais il pourra utiliser des outils open source autant que possible.

### 2.2. Relation aux projets futurs et passés
cf 2.1

### 2.3. Utilité et objectifs
cf 1.2

### 2.4. Environnement de fonctionnement
Le logiciel devra se diviser en deux parties distinctes, l'une installée sur serveur de calcul sous Linux, et l'autre installée sur un serveur web Linux aussi.

Les utilisateurs finaux utiliseront un navigateur web, et n'ont donc pas de dépendance à un OS en particulier (sous réserve de ne pas utiliser IE ou Edge)

Les besoins matériels ne devraient pas être excessifs concernant ce logiciel, mais plutôt concernant ses applications (calcul scientifique). 
Les serveurs devront donc être dimensionnés en fonction (i) des besoins en calcul et (ii) des connexions simultanées possibles (bien que les besoins en calcul limitent probablement le nombre de connexions)

### 2.5. Relation aux autres systèmes
Le logiciel devra être capable d'interagir avec des outils de calcul scientifique, de les exécuter et d'en récupérer les états intermédiaires et les sorties.

Il devra probablement aussi être en mesure de gérer des nœuds de calcul afin d'éviter les crashs sur le serveur de calcul.

Afin de ne pas redévelopper des sous parties existantes, il pourrait être dépendant d'au moins 2 autres logiciels, un pour la gestion des calculs distribués et un pour la création d'interface web.
Il est indispensable de dresser une liste des impératifs auxquels doivent répondre ces logiciels afin d'en faire le bon choix.

1. **Gestionnaire de calculs distribués**  
    Un certain nombre de logiciels existent d'ors et déjà pour la gestion de calculs scientifiques, en local, sur serveur distant, sur grille ou HPC...
    Les plus simples proposent des librairies facilitant l'envoi de commande bash sur serveur distant (eg. execo), et les plus complexes (workflow management system) permettent de créer des worflows, gèrent la traçabilité et la reproductibilité (eg. Galaxy, Taverna, ...)
    Logiquement plus l'outil propose des fonctionnalité avancées, plus il est complexe à prendre en main.  
    Liste des besoins :
    - outil libre : OUI
    - communauté de développement pérenne : OUI
    - possibilité d'intégrer la communauté de développement : OUI
    - possibilité d'exécuter des calculs sur serveurs distant : OUI
    - possibilité d'exécuter des calculs sur HPC, cloud, ... : OUI
    - exécution des calculs dans des containers : OUI
    - API permettant de l'interroger et de lancer des calculs depuis un autre outil (eg. SWAF) : OUI
    - developpé en langage Python : OUI mais pas indispensable
    - interface web propre à l'outil : pas indispensable
    - gestion de workflows : pas indispensable
    - gestion des données : pas indispensable
    - permet de récupérer l'état du calcul en cours : pas indispensable
    - gestion des droits sur les scripts : pas indispensable
    - facilité à prendre en main pour l'utilisateur intermédiaire de SWAF, ou possibilité de l'utiliser de manière totalement transparente : OUI

1. **Outil de création d'interface web**  
    Liste des besoins :
    - outil libre : OUI
    - communauté de développement pérenne : OUI
    - possibilité d'intégrer la communauté de développement : OUI
    - developpé en langage Python : OUI
    - possibilité de générer automatiquement une interface web : OUI
    - possibilité d'intégrer ou de développer un outil **graphique** de mise en page de l'interface : OUI
    - possibilité de l'interfacer avec le gestionnaire de calculs : OUI

 
### 2.6. Contraintes générales
La principale contrainte tient dans la généricité de l'outil, tout en gardant une simplicité d'utilisation.

Une autre contrainte concerne la gestion de utilisateurs et des projets, et donc des accès sur les différentes interfaces et outils liés.

### 2.7. Description du modèle
![Représentation globale](img/2019-04-17_global.png)

Description du schéma : 
1. La plateforme de modélisation (ici LMGC90 comme exemple).
2. Le script de calcul, développé par l'utilisateur avancé indépendamment de SWAF.
3. L'outil de construction d'interface, utilisé par l'utilisateur avancé pour créer une interface sur mesure.
4. L'interface web, généré par le framework et utilisé par l'utilisateur final.
5. La communication "live" entre l'interface et le script de calcul.

Concrètement le projet SWAF concerne les parties 3, 4 et 5.

La plateforme de modélisation (1) ne doit pas être impactée par l'utilisation du framework, afin de conserver la généricité de cet outil.
Ainsi aucune modification des plateformes de modélisation ne peut être demandée.

Le script de calcul (2) peut quant à lui nécessiter quelques adaptations afin d'aider à la génération d'interfaces et d'être "compatible" avec le framework.
Il faudra alors définir des normes de code pour que le framework puisse interpréter ce script.

L'outil de construction d'interface web (3) devra permettre de construire et tester facilement une interface adaptée au script. 
Dans l'idéal cette interface pourrait se construire automatiquement à partir du script de calcul préformaté.
Cet outil générera en sortie une description de l'interface (format DB ou fichier).
Cette description permettra à son tour de générer l'interface utilisateur à la volée pour utilisation ou modification.

L'interface web (4) est générée par les fichiers (ou données) de description, et permet à l'utilisateur final de paramétrer et exécuter le script de calcul.
Cette interface permettra aussi d'afficher l'état des calculs en cours et les sorties de calcul. Ces sorties pourront inclure des images 2D ou 3D.

La communication entre l'interface et le script de calcul (5) se fait sur 3 étapes distinctes :
* Avant le lancement du calcul le paramétrage.
* Durant les calculs pour un renvoyer l'état actuel (voir modifier des paramètres en cours ??).
* À la fin du calculs pour retourner les résultats.

## 3. Exigences spécifiques
Les exigences sont données dans un ordre de priorité.

Une exigence générale concerne le langage de programmation Python pour tous les programmes côté serveurs.

### 3.1. Fonctionnels
1. Paramétrer, exécuter et suivre un calcul sur serveur distant à partir d'une interface web.
1. Faciliter à la génération d'interface web dédiée à du calcul scientifique, permettant le paramétrage et l'exécution des calculs.
1. Permettre la visualisation des résultats de calculs scientifiques via une interface web, comprenant des sortie 2D et 3D.
1. Gérer les utilisateurs et les projets pour le stockage, l'utilisation et la modification des interfaces et des outils de calcul. 
1. Interpréter un script pour en générer un modèle interface préformaté.

### 3.2. Performances
1. Temps de génération d'une interface par un utilisateur averti ?
1. Temps de prise en main de l'outil ?

### 3.3. Interface
#### Software
1. Établissement d'une communication bi-directionnelle entre un serveur de calcul et un serveur web (TCP).
1. Établissement d'une communication bi-directionnelle entre un serveur web et une interface web (Ajax).

#### Database
1. Gestion de données de description d'interface.
1. Gestion de données utilisateurs et projets.
1. Gestion des scripts de calcul.
1. Gestion des données de sortie.

### 3.4. Opérationnels
SWAF est exécuté en mode service (daemon en cours d'exécution permanente), et reste en attente de connexions et de demandes de calculs.

L'interface "utilisateur avancé" doit permettre de choisir des inputs de paramétrage (text input, numerical input, sliders...) 
ainsi que des affichages de sorties de calcul (tableau, 3D...).
Il faudra que cet utilisateur puisse lier ces input/output à des variables de scripts, afin que l'utilisateur final n'ait plus qu'à réaliser la saisie des paramètres.
Enfin il faudra probablement des outils de test afin de garantir que la configuration est fonctionnelle.

L'interface "utilisateur final" devra être la plus épurée possible afin que cet utilisateur n'ait accès qu'aux input / output.
Il faudra permettre de récupérer les output en local pour des traitements hors interface.

### 3.5. Ressources
Il n'y a pas de limitation en ressources concernant cette interface côté serveur.

En revanche côté client, une utilisation standard de l'application web ne doit pas nécessiter de puissance particulière.

### 3.6. Vérifications
Des outils de vérification doivent être mis en place pour chaque organe indépendant de la solution complète.

### 3.7. Tests de validation
Les tests de validation concerneront tout d'abord l'expérience utilisateurs lors de l'utilisation de SWAF, autant pour la partie génération d'interface qu'exécution des calculs.

Il faudra réaliser des tests unitaire sur :
* le fonctionnement des éléments de l'interface.
* l'intégrité des données suite à leur communication et leur sauvegarde.
* la communication "live" des informations de calculs.
* la gestion des processus sur le serveur de calcul.

### 3.8. Documentation
Une documentation devra être rédigée pour 3 niveaux :
* documentation d'installation et de maintenance de l'application.
* documentation d'utilisation avancée de l'outil pour créer des interfaces.
* documentation générale permettant de comprendre le fonctionnement globale et les bonnes pratiques à tous les niveaux, même utilisateur final.

### 3.9. Sécurité des données
Identification des utilisateurs avec chiffrement de type sha1.
L'authentification pourra se faire via un serveur LDAP.
L'authentification limitera les accès des utilisateurs :
1. simple utilisation ou création d'interface.
2. liste des interfaces disponibles en utilisation ou modification (en fonction des droits ci-dessus)
3. nombre de noeuds de calculs disponibles.

### 3.10. Portabilité
Le logiciel devra être facilement portable et ré-installable, ce qui impose :
* utilisation d'environnement d'exécution (docker, conda, venv ...) avec une traçabilité des environnements fonctionnels.
* utilisation de fichier de configuration pour tout ce qui peut être amené à changer (adresses IP, port, nombre de cœurs de calcul disponibles, ...).

Côté utilisateur, tout navigateur web en norme W3C devra être en mesure d'utiliser l'interface, indépendamment du système d'exploitation.
 
### 3.11. Qualité
Le programme devra être compris et réutilisé, et devra donc respecter certaines normes de qualité :
* utilisation d'une norme de style pour le code (PEP-8 pour python)
* les commentaires de code doivent aussi respecter un norme, de manière à pouvoir générer une documentation standard et automatique (type readthedoc)
* utilisation de librairies standards dès que possible

### 3.12. Fiabilité
L'outil étant une interface entre des outils de calcul et des utilisateurs, sa principale obligation de fiabilité est la garantie de l'intégrité des données durant la communication de ces données.

### 3.13. Maintenance
L'outil devra pouvoir être maintenu fonctionnel et évoluer sans risque. 
Pour cela les développeurs utiliserons une forge logiciels (gitlab) pour le suivi de version.
Ils mettront en place aussi un système d'intégration continue afin de garantir le fonctionnement de chaque version et la non régression entre elles.

### 3.14. Sécurité matérielle et humaine