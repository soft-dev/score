How does it work ?
==================

Here is a diagram giving a description of the operations happening when a user run a computation using SCORE, each step is described
below.

.. image:: /img/SCORE_operation.png
    :target: _images/SCORE_operation.png

Running
-------

[1] **Request computation**
    | That part is performed by user from a script, shell or Jupyter interface, using the method
      :py:meth:`score.Core.Core.compute`.
    | This part assumes that the computation server, the singularity image and possibly the storage server are already
      set up (:py:meth:`score.Core.Core.set_computation_server`, :py:meth:`score.Core.Core.set_simage` and
      :py:meth:`score.Core.Core.set_storage`).

[2] **Create application**
    To create the application, there are several steps :

    - Retrieve the path to the module containing the computation function.

    - Create a temporary directory containing a copy of that module and the complete file tree files at the same location. As
      Python can import everything contained in the subdirectory, it allows the user to import other module from its
      function, as well as other static data if necessary (JSON, image...).

    - Add pickle archive of the input data in the directory.

    - Create a script (*score_compute.py*) which will be the entry point of the computation.

    - Create a JSON file containing all necessary information to execute the computation on a remote server (main module
      and function to call, storage informations, monitoring tool to use...).

[3] **Copy application and Singularity image**
    | That copy uses SFTP, and assumes that the SSH key file provided allows to connect to the session on the
      computation server.
    | If there is one, the Singularity image is copied on the remote user home directory, in the directory
      *$HOME/.score*. Before copying, it checks if the image is not already there (using the name of the file, and the
      md5 print).
    | The application directory is also copied in the user home directory, in the directory *$HOME/score_computation* by
      default. The user can change that default directory when setting up the computation server.
    | During the copy (which can be quite long due to Singularity image), nothing can be done in the
      Python interface.


[4] **Run script remotely**
    | Running the computation consists in running the script *score_compute.py* (created before) using SSH, in a detached
      session.
    | If a Singularity image is provided, it will be used as environment.
    | If user has specified a Python path (using Singularity image or not), this executable will be used, otherwise it
      assumes that *python* command run the correct executable.
    | Once the computation is running, the current Python interface (on the user computer) can be closed without
      impacting the computation.

[5] **Create client**
    The *Core* object creates a *client*, which will wait for the *worker* to be ready for connection, and then will
    wait for information from the computation. It uses also SSH to connect to the computation server.

[6] **Initialize computation**
    Before the computation really start, *score_compute.py* script will instantiate a *worker* object to manage the
    computation. Its first action is to create all the necessary objects for communication, then set up the computation :

    - Read the configuration file to retrieve all the necessary information about how to run the computation.

    - Create a *log* file where all the communication from the computation to the the *clients* will be written.

    - Create a *com* file where all the communication from *clients* to *worker* will be written (right now the only
      existing communication from *client* to *worker* is to kill the computation).

    - Create a thread used to read the *com* file, in an infinite loop, to catch any message from the *clients*.

    - 'unpickle' the input data.

    - Download new data from a web service if requested.

    - Call the *start* callback function if requested.

[7] **Run function**
    The computation function has already been imported at the initialization step, so it just runs it using a context
    manager to catch (and append to the log file) all the prints from the computation script.

Monitoring
----------

[8] **Connect to worker**
    The client simply waits for the creation of the *log* file to know if the computation is started or not. The first
    message it must read is the path to the *com* file, since it is where it will be able to communicate to the computation server.

.. _return_function_to_worker:

[9] **Return information from function to worker**
    | A specific syntax is used for communication (see :py:mod:`score.tools.messaging` module), depending if the
      information is a user message, a status, a data (intermediate or final), a system message or an error.
    | **User messages**, **status** and **system message** are written in the *log* file as they are only string data.
    | The **data** are archived using pickle, and the path to that data are filled in the *log* file as data.
    | To report **error**, all the previous and next actions to the *compute function*  are called in a try/except
      statement from the *score_compute.py* script. The error (multiline) is written in an other file, and the path to that
      file is filled in the *log* file as an error.

.. _send_worker_to_client:

[10] **Send information from worker (computation server) to client (user computer)**
    Send is a misnomer as the worker only log information locally in its own file system. The client is perpetually
    watching the log file to retrieve any new information. There is a specific behaviour for each kind of information :

    - **User messages** are stored in a *log* list of the client object.

    - **Status** is stored as a tuple (progress (int), message (str)) in the client object.

    - **System messages** will produce an action depending on the input value.
      For now only 2 kinds of system messages exists : 'INIT' and 'END', which
      inform about the beginning and the end of the computation.

    - **Data** is read as a path to a pickle file on the computation server. It is retrieved using SFTP by the user
      server. The local path is then stored in one of the 2 dedicated Queue, depending if it is an intermediate or final
      data. The intermediates data are overwriting each other, as we assumes that there is no need to store all of them,
      only the last one.

    - **Error** is read as a path to a text file on the computation server, so it is read using SSH, and stored in a
      variable in the client object. The client also is informed if the error is fatal (and the computation has
      stopped), so it can stop listening to it.

.. _return_client_to_python:

[11] **Return information from client to Python interface**
    The client is an object instantiated in the Python interface, so all its attributes can be freely accessed,
    including the logs list (user message), the status, and the error. There are methods to get intermediate and final
    data, which have to be 'unpickled'.

    There are also listener on each information's incoming event, callback functions can be added to each event.
    Details can be found :ref:`here <Listening to the computation>`.

[12] **Return final data from function to worker**
    Same operation as :ref:`[9] <return_function_to_worker>` above.

[13] **Send information from worker (computation server) to client (user computer)**
    Same operation as :ref:`[10] <send_worker_to_client>` above.

[14] **Return information from client to Python interface**
    Same operation as :ref:`[11] <return_client_to_python>` above.

[15] **Save data**
    That operation implies that there is a SSH key pair on the computation server allowing to connect to the
    storage server (details :ref:`here <Optional steps>`).

    Then it uses SFTP protocole to save the whole computation directory (containing Python module/package, configuration
    file, final data and log/error files) on the storage server.

