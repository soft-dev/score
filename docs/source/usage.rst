How to use it ?
===============

Compute on a remote server
--------------------------

Here is an example on how, from a python script, run a function on a remote server, using a Singularity image.

Minimal steps
*************

Creating a Core object :
    .. code-block:: python

        from score import Core as ScoreCore

        score_core = ScoreCore()

Set up a computation server in the `score_core` object:
    A local SSH key file must allow you to connect to the computation server.

    .. code-block:: python

        score_core.set_computation_server(server_ip='localhost',
                                         username='username',
                                         key_file='local/path/to/ssh_key_file',
                                         simage='local/path/to/singularity_image')

    The minimal set up is done, as the Core object has been initialized with:

    - connection information to remote server (*server_ip*, *username* and *key_file*)
    - the Singularity image in which to execute the computation (*simage*)

Launching computation
    In that example, we'll use the function *test_compute(arg1, arg2)*, in the module file *test_module.py*.

    .. code-block:: python

        from test_module import test_compute
        # Arguments can be anything, as soon as there are 'pickable'
        var1 = "tralala"
        var2 = [1, dict({'a':1})]
        score_core.compute(test_compute, arg1=var1, arg2=var2)

Optional steps
**************

Saving data on the computation server, in a specific directory.
    In case the user wants to access to some data they can be moved in a specific directory at the end of the computation:

    .. code-block:: python

        score_core.set_storage(data_path='path/to/folder/on/computation/folder')

Saving the data on a separate storage server, again using an *SSH* key file.
    .. code-block:: python

        # Create a SSH key on the computation server
        from score.tools.ssh import create_remote_ssh_key
        key = create_remote_ssh_key(server='IP address of computation server',
                                    username='username on computation',
                                    key_file='local/path/to/ssh_key_file'
                                    )

        # Set up the storage server
        score_core.set_storage(data_path='path/to/folder/on/storage/server',
                              key_file='local/path/to/ssh_key_file',
                              remote={'ip_address': 'IP address of storage server',
                                      'username': 'username on storage',
                                      'computation_key': key.decode('utf8')
                                      }
                              )

Using a Python installation instead of Singularity image (conda, virtualenv, ...)
    .. code-block:: python

        score_core.set_computation_server(server_ip='localhost',
                                         username='username',
                                         key_file='local/path/to/ssh_key_file'
                                         )
        score_core.python_path = 'path/to/python/executable'

Running the computation in a specific directory
    It can be useful to perform the computation in a specific directory on the computation server, for faster file
    writing or better disk size control if the computation produce a lot of files.

    The :py:meth:`score.Core.Core.set_computation_server()` has the argument :code:`remote_computation_directory` for
    that feature.

Settings
*********

All the settings step can be done using a settings file (JSON), which must be located in the directory
*./SCORE_MANAGE/core_config.json*, and must exists before instancing Core object.

More details can be found at  :py:class:`score.statics.file_system.ScoreFileSystem` and
:py:mod:`score.tools.core_configuration`.

Here is an example of a quite full config file, every part are optional :

.. code-block:: json

    {
      "computation": {
        "ip_address": "(str) IP address of computation server",
        "username": "(str) username on computation",
        "computation_directory": "(str, optional) remote/path/to/computation_directory"
      },
      "storage" : {
        "data_path": "(str) path/to/folder/on/storage/server",
        "ip_address": "(str, optional) IP address of storage server",
        "username": "(str, optional) username on storage",
        "computation_key": "(str, optional) computation session public key value"
      },
      "token": "(str, optional) token of the user, to be able to download data of the user on a data server",
      "key_file": "(str, optional) local/path/to/ssh_key_file",
      "simage_file": "(str) local/path/to/singularity_image",
      "locked": "(boolean, optional) to lock or not current settings"
    }

Monitor a computation
---------------------

| Monitoring a computation consists in creating a communication channel between a **client** (user side) and a
  **worker** (computation side).
| As soon as a computation has been launched using a Core object, it embeds a worker object to manage that
  computation's steps and the communication.
| On user side, a client can be instantiated to communicate with each worker, and thereby with each computation.

Connection to computation using the Core object
***********************************************

The Core object will automatically create a list of *Client* objects, each monitoring a launched computation.

So if the current session has not been closed, a monitor is available for each *Client* in :code:`score_core.monitors[X]`.

If the session has been closed, it is still possible to *reconnect()* a new Core object:

.. code-block:: python

    from score import Core as ScoreCore

    score_core = ScoreCore()
    score_core.reconnect()

This will create all the monitors as if you just launched the computations.

.. warning::
    The session must be in the same directory than the previous one since all configuration files are stored in there.

Connection to the computation using the Client object
*****************************************************

It is also possible to connect back to a running computation from anywhere by using the *ClientSsh* object with only the connection parameters
(host, username, directory path and SSH key):

.. code-block:: python

    from score.monitoring import ClientSsh
    monitor = ClientSsh(ip_address='localhost',
                        username='username',
                        remote_path='remote/path/to/computation_folder',
                        key_file='local/path/to/ssh_key_file')

Monitoring the computation
**************************

The monitoring is done with attributs of the :code:`monitor` objects on
the user side and with functions of the :py:mod:`score.tools.messaging` module.

**Get Status**
    | The current status of the computation can be retrieved using :code:`monitor.status` variable.
    | On the computation function side, the best way to send log is to use :py:func:`score.tools.messaging.send_status()`
      function.

**Get logs**
    | All the logs can be retrieved from the list :code:`monitor.logs`.
    | On the computation function side, the best way to send log is to use
      :py:func:`score.tools.messaging.send_message()` function.

**Get data**
    | As soon as the computation is over (:code:`monitor.computation_over`), the data can be retrieved with
      :code:`monitor.get_final_data()` method. If this method is called before the end of the computation then
      it is blocking until its end.
    | It is also possible to retrieve intermediate data using :code:`monitor.get_intermediate_data()`. That feature can
      be useful to display the evolution of the computation without waiting the end, or to check if it works as
      expected. Intermediate data are temporary as they are overwriting each other, there is no intent to save that data.

**Get errors**
    If an exception occurred, that will be reported in a text variable :code:`monitor.error`.

**Kill the computation**
    Any remote computation can be killed using :code:`monitor.kill()` method, with an option whether to delete, or not,
    the remote computation directory.

Listening to the computation
****************************

Observers can be set up on the Client object, which will call dedicated callback function each time it receives
information from the computation through the `monitor`/`messaging` system.

Status, logs and error incoming events can be bound to several callback functions. The methods
:code:`monitor.bind_to_status(fc)`, :code:`monitor.bind_to_logs(fc)` or :code:`monitor.bind_to_error(fc)` will append a
new function to each observers list.

| Intermediate and final data incoming events can be bound each to one callback function, using the functions
  :code:`monitor.bind_to_intermediate_data(fc)` and :code:`monitor.bind_to_final_data(fc)`.
| These observers can also be set up when launching the computation :

    .. code-block:: python

        # let's say the compute function is test_compute(arg1, arg2)

        score_core.compute(test_compute,
                          callback_intermediate_data=fc1,
                          callback_final_data=fc2,
                          arg1=var1,
                          arg2=var2)


Each callback function must have a specific signature :

- **status** :

    .. code-block:: python

        def callback_status(progress:int, status_message:str)

- **logs** :

    .. code-block:: python

        def callback_logs(log:str)

- **error** :

    .. code-block:: python

        def callback_error(error:str)

- **intermediate data** :

    .. code-block:: python

        def callback_intermediate_data(monitor:score.monitor.ClientSsh, data:tuple(of whatever))

- **final data** :

    .. code-block:: python

        def callback_final_data(monitor:score.monitor.ClientSsh, data:tuple(of whatever))

Sending information from computation function
*********************************************

| **Final data** and **errors** are essential information to monitor a remote computation. Consequently, this information is send automatically at the end of the computation.
| **Status**, **logs** and **intermediate data** are defined by function. To send that information from the computation
  function, the module :py:mod:`score.tools.messaging` can be used.

.. code-block:: python

    from score.tools import messaging

    def compute(arguments):
        messaging.send_message('The computation is beginning')

        # INIT COMPUTATION

        loop_count = 500

        for i in range(loop_count):
            progress = i * 100 / 500
            messaging.send_status(progress, f'computation is at step {i}')

            # DO SOME STUFF

            messaging.send_data_ssh(data)

        messaging.send_message('The computation is over')

Jupyter Notebook integration
----------------------------

| Using SCORE in Jupyter is useful to construct a user web interface over a Python application.
| Once the Notebook is created, the final user only need to modify the arguments of the compute function to be able to
  perform remote computation, in a pre-built environment (Singularity).
| Combined with `Voila-dashboard <https://voila.readthedocs.io/en/stable/>`_ will easily create a web application for
  complex and long computation program [ref SWAF].

| SCORE provides a homemade widget (based on `IPywidgets <https://ipywidgets.readthedocs.io/en/stable/#>`_), to make the
  use of SCORE much easier: :py:func:`score.tools.notebook_lib.ComputeWidget`.
| A `ComputeWidget` is included in the Core object : :code:`Core.compute_widget`.
| To use it, it simply has to be displayed: :code:`Core.display_widget(fc_to_run:callable)`.

Let's use the first example in Jupyter.

.. code-block:: python

    # import Core
    from score import Core as ScoreCore

    # import the computation function
    from test_module import test_compute

    # Create some global variable that a user may be able to modify using widgets for instance
    var1 = "tralala"
    var2 = [1, dict({'a':1})]

    # instantiate the Core object and set it up for remote computation
    score_core = ScoreCore()
    score_core.set_computation_server(server_ip='localhost',
                                     username='username',
                                     key_file='local/path/to/ssh_key_file',
                                     simage='local/path/to/singularity_image')

    def launch_computation():
        # some stuff can be done here to format and check arguments to the compute function
        score_core.compute(test_compute, arg1=var1, arg2=var2)

    # display the widget
    score_core.display_compute_widget(launch_computation)

This will display the compute widget:

.. image:: /img/compute_widget.png

| Pressing the compute button will execute :code:`launch_computation()`, which will launch the
  computation on the remote server.
| It will also automatically create a status widget, linked to the corresponding computation:

.. image:: /img/compute_status_widget.png

Each status widget will retrieve automatically its computation status, logs and errors.
It also allows to kill the computation with a *STOP* button.

.. warning::
    | The function used as argument of :py:meth:`score.Core.Core.display_compute_widget()` method must not be mistaken with
      the function used as argument of :code:`Core.compute()`. One is used by the widget on the user side, whereas the
      second one is run on the computation server side.
    | Keep in mind that this usage is event-driven, so :code:`Core.display_compute_widget()` method may be called
      before the compute function arguments are set up.
    | That's why :code:`launch_computation()` must not accept any arguments, and why global variables are used.

| Jupyter Notebook communicates with a kernel on the server. So if the kernel reboots (reloading the page, stopping the
  server...) the connection to the computation would be lost.
| To avoid that behaviour, the compute widget constructor has a `reconnect_fn` parameter, a function which will be
  called at the end of :code:`Core.display_compute_widget()`.
| The compute widget created in Core object called :code:`Core.reconnect()`, which will create again all the status
  widgets connected to each computation.
