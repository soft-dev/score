
SCORE : Supervise computation remotely
======================================

.. include:: abstract.rst

Content
-------

.. toctree::

   installation

.. toctree::

    usage

.. toctree::

    operation

.. toctree::
    :maxdepth: 3

    api

Licence
-------

.. toctree::

   licence

Authors
-------

.. include:: ../../AUTHORS.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
