SCORE package provides tools to execute and monitor computation (using Python) on remote servers.

Files and informations are easily sent and received using *SSH* and computation are run in a dedicated *Singularity* environment. You can also run computation on your computer.

From a Python console or Notebook on your computer you can:

Choose where to compute:
    - **locally** : it will run on a new process locally.
    - **remotely**: it will run on a computation server. All you need is to provide an IP address, a username and a SSH
      key which allows you to connect to the provided session.

    Once the computation is launched you can close your current session, and get back to the computation whenever you want.

Choose your environment:
    You can perform the computation:

    - in an existing **Conda**, **Virtualenv** or **system wide** environment. All you need is a full path to the Python
      executable.
    - in a **Singularity** environment. To use it, the Singularity image must be build on the local computer and
      Singularity installed on the remote system which will run computation. In case Singularity is installed on your computer,
      you can provide a Singularity definition file, and SCORE will build the corresponding image for you.

Save the output data:
    You can use SCORE to save all the computation data (the created files and the console outputs) on a storage server.
    To use this feature, the computation server needs an SSH key allowing to connect to the storage server.
    SCORE provides tools to set this up almost automatically.

Monitor your computation:
    When a computation is running or has run, it is easy to retrieve:

    - the computation status
    - the printed outputs
    - the intermediate or final data (data must be 'pickable')
    - the error messages if there are any

