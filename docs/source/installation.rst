Installation
============

User install
------------

.. code-block:: bash

    pip install .

Optional installation
---------------------

Cluster using SLURM job scheduler
*********************************


.. code-block:: bash

    sudo apt-get install gcc
    pip install .[slurm]

Developer install
-----------------

.. code-block:: bash

    pip install -e .[dev]

Using a conda environment
-------------------------
Install conda by following that steps : https://conda.io/projects/conda/en/latest/user-guide/install/index.html

Then create a dedicated environment and install SCORE in there :

.. code-block:: bash

    conda create --name score python
    conda activate score

    # for user install
    pip install .

    # or for application development
    pip install -e .

