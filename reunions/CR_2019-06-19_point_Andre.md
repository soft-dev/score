
CR réunion 20/06/2019
=====================

Présents : André Chrysochoos, Nicolas Brichet (auteur)

Présentation du projet OEHM
---------------------------
Le projet concerne l'habitat méditerranéen, mais pourra très certainement s'étendre.
Ses objectifs principaux sont :
- diminution du coût énergétique
- maintien du confort (hygrométrie / température)
- garantie de tenue mécanique

Pour ça une première partie consiste à modéliser les différents matériaux à une échelle fine d'abord, puis à une échelle macro, matériaux "assemblés".
Cette modélisation est multi-physique, elle tient compte de plusieurs aspects de l'environnement (température, humidité, lumière...), associés à la description des matériaux (inertie, resistance...).

Dans un deuxième temps il s'agira de développer des outils de calcul numérique de ces modèles, et de leurs associer des outils graphiques.
Ces outils graphique inclue la création de bâtiments 3D, la paramétrisation des modèles et la visualisation des résultats.

Le CSTB (partenaire / prestataire / on ne sait pas trop) doivent fournir le code source de leur outil.  
Il faudra étudier dans quelle mesure on peut s'appuyer sur cet outil pour fournir une interface similaire pour les futurs utilisateurs.
