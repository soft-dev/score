
CR réunion 19/06/2019
=====================

Présents : Alexandre Dehne-Garcia (correspondant calculs scientifiques INRA), Rémy Mozuls, Nicolas Brichet (auteur)

Alexandre soutient l'idée qu'il faut développer le moins possible et maintenir le moins possible :
- des outils existent qui font potentiellement ce qu'on souhaite (type Galaxy, ou autres gestionnaires de dataflow).
- il faut absolument s'appuyer sur les infrastructures de services existantes (type France Grille) pour le déploiement d'une application web.

Dans tous les cas, si c'est un outil que l'on souhaite déployer et rendre accessible à une large communauté :
- il faut se poser dès maintenant la question de la maintenance et de l'admin. sys.
- il faudra créer une communauté, et donc faire de la com. autour de l'outil.

À voir:
-------
- Galaxy Europe, qui serait un projet européen permettant d'utiliser Galaxy (avec nos projets) et des serveurs de calcul associés.  
Les échos négatifs qu'il a eu concernent principalement la gestion des droits sur les données, voir les codes.
 
- Guix, un outil qui "remplace" conda, à priori mieux gaulé pour la reproductibilité des calculs.
Il semblerait qu'il inclut aussi des compilateurs.

Infos en vrac :
---------------
- **containers** : le gros avantage de singularity sur docker concerne le réseau, singularity voit et utilise le réseau type infiniband, spécifique au cluster/HPC.  
De plus le container est vraiment vu comme une appli, avec les mêmes droits utilisateur que le serveur.
- **communication de données** : ce n'est pas tous les cluster qui accepte l'ouverture d'un socket pour recevoir des connexions.  
Un outil souvent utilisé serait sshfs, file system sur ssh.
