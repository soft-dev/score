
CR réunion 14/06/2019
=====================

Présents : Paul Taforel, Nicolas Brichet (auteur)

Interrogations sur l'outil de gestion des calculs :
- Comment récupérer des données intermédiaires, durant le calcul ?  
Cas typique de LMGC90 où les données sont écrites dans le dossier OUTBOX (entre autres), et qu'il faut pouvoir accéder à ces données durant le calcul.
- Comment exécuter du code sur le serveur qui exécute le calcul (container en l'occurrence) en parallèle du calcul ?
- Comment paramétrer les ressources disponibles pour le container ? Est-ce une option pour l'utilisateur final ?

Interrogations sur la base de données et les droits :
- Est-il possible de mettre des droits différents sur les données issues d'un même calcul (typiquement en option d'une application) ?
- De même est-il possible de mettre des droits sur des parties de l'interface, avec options visibles mais non utilisables ?  
 Un possibilité pour ça pourrait être de découpler l'interface du programme de calcul, ce qui permettrait d'avoir des droits sur différentes interfaces sans dupliquer le programme lui même...
- Interrogations sur la flexibilité de l'interface, jusqu'au on va dans la paramétrisation "esthétique" ?