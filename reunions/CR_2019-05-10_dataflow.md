
CR réunion 10/05/2019
=====================

Présents : Rémy Mozul, Christophe Pradal, Nicolas Brichet (auteur)

Présentation :
--------------

Christophe a présenté les outils qu'il développe, à savoir :
* Openalea  pour la gestion de dataflow
* Visualea, une interface graphique pour Openalea
* OALab, un laboratoire virtuel pour Openalea  

Ces outils sont développés quasiment intégralement en python, le format d'enregistrement des workflow est un pickle python.

Actuellement la plate-forme est surtout dédiée aux plantes, mais Christophe ne voit pas de limitation à LMGC90.

Christophe est pret à passer un peu de temps (type coding sprint sur 2 ou 3 jours) pour qu'on adapte un exemple de LMGC90 à la plateforme OpenAlea.
Cela permettrait de faire des essais avec les outils internes et connus, et ainsi de voir s'ils correspondent à nos besoins.

À noter qu'OpenAlea a acquit un status de consortium entre INRIA, INRA et CIRAD, ce qui laisse espérer une certaine pérennité au projet.

RM : j'ai trouvé l'outil OpenAlea avec la visualisation associée impressionant.
en l'essence, on se retrouve avec un interface graphique qui permet d'aller piocher
dans les fonctions de tout les modules python disponible dans l'environnement pour
en faire des boîtes et d'y brancher, en les typant et en y associant un composant
graphique permettant de le remplir, les différentes entrées/sorties.

Ca permet en fait de définir un 'dataflow', ou en plus simple, un 'pipeline' de donnée
et d'exporter une vue statique de de ce truc dans une fenêtre et l'utilisateur peut
changer les données d'entrées et récupérer sa sortie. Il y a même un type 'sortie graphique'
permettant de récupérer une fenêtre 2D/3D.

OpenAlea est donc un outil qui permet de travailler sur des workflow d'objets python
et offre une correspondance (bijective) entre du code python et une representation graphique
d'une fonction dans un workflow.

Enfin il y a de l'inférence sur les signatures des fonctions python d'un module,
il faut donc avoir des docstrings à jour.

 
Info en vrac :
--------------

* Il faut absolument qu'on fasse un package conda de LMGC90 avant tout, si possible intégré à conda-forge
* Il est important qu'on se pose la question de l'objet à visualiser en sortie de calcul.
  Est-ce qu'on mise tout sur la représentation 3D ou est-ce qu'on veut pouvoir visualiser d'autre type de données facilement, et auquel cas comment ?
* Voir NGPhylogenie.fr, un site pour paramétrer et exécuter des workflows qui s'appuit sur le dataflow galaxy
* Voir K3D pour la visualisation 3D dans Jupyter
* Un pipeline est un dataflow linéaire. Le dataflow permet donc de gérer des évenement là ou le
  pipeline ne peut pas forcément le faire.


     