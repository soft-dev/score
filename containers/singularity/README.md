Singularity
===========
Par défaut singularity donne accès (bind) au HOME directory depuis le container.

Petite nuance avec docker, singularity récupère les utilisateurs de la machine, et une partie de leur environnement...
### Installation (not working in conda env because of root privileges to build image)
[installation instructions](https://www.sylabs.io/guides/3.2/user-guide/installation.html)

### Construction of image :
Definition file is swaf_singularity.def, image output is swaf_singularity.sif
```bash
mkdir -p ~/singularity_images
sudo singularity build ~/singularity_images/swaf_lmgc90.sif swaf_lmgc90.def
```

### Running a container 
To be done for a server which have deamon in %runscript part
```bash
singularity run ~/singularity_images/swaf_lmgc90.sif
```

### Binding server path into image and running container
```bash
singularity run --bind /temp/$MODULE_PATH:$HOME/$MODULE_PATH
```

### Running a container and executing a command
```bash
singularity run ~/singularity_images/swaf_lmgc90.sif which python
singularity exec ~/singularity_images/swaf_lmgc90.sif which python
```
In this case it not pass by %runscript part of definition file

Python API
----------
```bash
conda install spython -c conda-forge
```
```python
from os import path
from spython.main import Client
Client.load(path.join(path.expanduser('~'), 'singularity_images/swaf_lmgc90.sif'))
print(Client.execute(['conda', '-h']))
```
**Stream possible**, ce qui signifie que tout ce qui sort du docker est mis en liste

**Attention** le cache est très bizarrement utilisé :
- J'ai modifié swaf, je refait conda build (qui lui même contient la commande setup.py de swaf)
- L'installation de swaf ne s'est pas faite, sans message... alors que le projet a bien été mis à jour, seul le setup.py n'a rien fait.  
-> Possiblement un problème de cache de setup.py, car je copie le dossier de travail, et pas un projet vierge !! My fault !