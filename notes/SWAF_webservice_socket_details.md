Détails du fonctionnement de l'utilisation du web service pour lancer les calculs et des socket pour la supervision
========================================

À priori il y a peu de chances qu'une de ces solutions soit utilisée sur cluster de calcul, pour des raisons de sécurité d'ouverture de ports.


![SWAF_calcul_step_by_step](img/2019-06-13_swaf_calculation_step_by_step.png)

**serveur de calcul** = celui qui exécute les dockers.

*serveur script* = celui sur lequel est installée l'interface (jupyter dans un premier temps).

<br />

**serveur de calcul** : lance un service web en écoute sur le port 24999.

*serveur script* : exécute *launch_calculation(function, arg1...)*

*serveur script - launch_calculation* :
- recherche le dossier du module qui contient la fonction et copie le dossier dans un dossier temporaire.
- crée un pickle pour chaque argument et le met à la racine du dossier temporaire.
- ajoute le script *encapsulate.py*, issu de *create_script()*, dans ce meme dossier temporaire.
- créer une archive de ce dossier et l'envoie au serveur de calcul via le service web. 
Lors de l'envoi, il renseigne aussi l'outil à utiliser (ici lmgc90), qui doit correspondre à un docker existant sur le serveur web (nom exact *swaf:lmgc90*). 

**serveur de calcul - service web** : 
- récupère et décompresse l'archive.
- exécute *launch_docker()* avec pour paramètre le nom de l'image  (ici *swaf:lmgc90*), le port de communication et le chemin vers l'archive décompressée.

**serveur de calcul - launch_docker**
- crée un container à partir de l'image sans l'exécuter, copie les fichiers dans l'image, et enregistre ce container comme une nouvelle image et le ferme.

   TODO : c'est un truc à améliorer, mais on ne peut pas copier de fichier dans un container non lancé, et si je lance le container sans fonction, il me rend la main directement... Il faudrait créer une fonction sleep, mais c'est pas très propre.
- lance le nouveau container en exécutant le script encapsulate.py.
- récupère l'ip et le port de ce container, et le renvoi au service web, avec l'ID du container.

**serveur de calcul - service web** : 
- renvoie l'ip et le port du container à launch_calculation.

**serveur de calcul - docker - encapsulate** :
- crée un socket côté serveur, et attend la connexion d'un client avant de lancer les calculs.

*serveur script - launch_calculation* :
- crée un socket côté client avec les infos de connexion au serveur.

À partir de maintenant les communications s'établissent entre le **socket_serveur** et le *socket_client*.

**serveur de calcul - docker - socket_serveur** :
- crée un socket TCP et attend un connexion.

*serveur script - socket_client* : 
- crée un socket UDP.
- se connecte au socket TCP du serveur, et lui envoi les infos sur son socket UDP.

**serveur de calcul - docker - socket_serveur** :
- accepte la connexion TCP.
- se connecte au socket UDP du client.
- se remet en attente d'une potentielle autre communication TCP.

**serveur de calcul - docker - encapsulate** :
- lance les calculs (le module utilisateur), maintenant qu'au moins un client est connecté.
- tous les prints du scripts sont catchés par le socket serveur qui les renvoie à tous ses clients via leur socket UDP.

*serveur script - socket_client* :
- parse les messages reçus sur le port UDP, avec 4 types de messages possibles :
    - "##*pourcentage*##*message*" représente un état d'avancement : le pourcentage d'avancement entre les dièses, suivi d'une description de l'état.
    Cet état est alors ajouter à une queue  qui peut être lue par le client.
    
      NOTE : C'est une contrainte légère au script, il faut formatter les prints pour qu'ils soient vus comme un état d'avancement, type "##2##Chargement des librairies"   
    - "!!*message*" représente une erreur python côté serveur.
    On arrête tout et on affiche le message d'erreur, qui peut être dû à SWAF ou au script utilisateur.
    - ">>*message*" représente un message du socket serveur au socket client.
    Pour le moment seul "END" existe pour signaler au client que le calcul est terminé.
    - "??*message*" correspond à un futur envoi de données.
    *message* contient alors la taille en octets des données attendues (données au format pickle).
    Le socket client attend alors un certain nombre d'octets correspondant à la donnée, qu'il décompresse et met dans une queue spécifique.
    L'utilisateur peut ainsi récupérer ces données via la méthode *get_data()*.
- le socket client peut aussi renvoyer des messages au serveur, reste à voir comment l'interpréter pour modifier le calcul.
 