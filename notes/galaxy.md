Galaxy
=======
Galaxy permet d'interfacer et d'exécuter des calculs sur serveur distants, et permet une visualisation des données.
Il est très orienté génétique, et contient beaucoup d'outils dédiés à cette discipline.

Tutoriel rapide qui permet de se faire une idée des fonctionnalités : https://galaxyproject.org/tutorials/g101/

Interface
---------
![galaxy_main](img/galaxy_main.png)

Trois panneaux verticaux:
1. outils 
1. visualisation et édition
1. historique et données

### Outils :
Principalement des outils dédiés à l'analyse génétique, avec en plus quelques outils génériques :
- gestion de données (récupération ou enregistrement de données)
- transformation de données, plutôt pour des données tableau à priori
- statistiques

On peut récupérer des données publiques. 
Dans ce cas on trouve un lien vers un formulaire de récupération, qui pointent visiblement vers une base de données.
Lorsqu'on valide, cela génère une demande de récupération dans galaxy, et les données sont accessible dès que le processus est exécuté.

### Historique et données
Chaque action est enregistrée dans un "historique".  
On peut créer autant d'historique que l'on souhaite, rajouter des actions ou des données à un historique existant, transférer des données de l'un à l'autre...  
Finalement **il faut voir l'historique comme une exécution de workflow**, avec ses données d'entrée, intermédiaires et de sortie.  
Il peut contenir des actions de tous types : récupération de données, transformation, calcul, workflow...  
On peut **typer les données de sortie**, et ainsi faire le lien avec des outils de visualisation.
Des liens vers des outils de visualisation externes à l'interface de Galaxy sont alors proposés.  

**On peut extraire un workflow à partir d'un historique**.
On peut ensuite :
- choisir les données intermédiaires que l'on souhaite sauvegarder
- renommer les input/output
- choisir les input fixées et ceux variables, que l'utilisateur devra alors saisir
- bien sur exécuter un workflow dans un nouvel historique avec de nouvelles données

Enfin on peut importer les historiques / workflows d'autres utilisateurs.

Sous le capot
-------------
### Données
Toutes les données semblent être des fichiers, ce qui implique que les programmes gère écriture et lecture de fichiers.
Ou de créer une librairie qui fait la transformation.  
Gestion des droits sur les données théoriquement possible (non testé).  
Théoriquement aussi on peut créer ses propres datatypes, et ainsi possiblement ses propres visualisateurs...
Ça n'a pas l'air simple mais on peut faire les tests.


La gestion des données et des historiques n'est pas très claire, tu n'es jamais sûr que la données a été effacée, d'ailleurs des fois elle ne l'est pas...
### Jupyter
On peut théoriquement inclure Notebook dans Galaxy (ils appellent ça GIE pour Galaxy Interactive Environment),
 mais ils conseillent d'être costaud en sys admin 
 [you’ll need to be a fairly competent SysAdmin to debug all of the possible problems](https://docs.galaxyproject.org/en/master/admin/interactive_environments.html).
Un tuto de formation peut être essayé : [tuto](https://github.com/galaxyproject/dagobah-training/blob/2017-melbourne/sessions/21-gie/ex1-jupyter.md)

### Serveurs distants 
voir [Pulsar](https://galaxyproject.github.io/training-material/topics/admin/tutorials/heterogeneous-compute/tutorial.html)

### workflow
Gestion des workflow parfois pas très claire... du moins à la configuration.  
Il faut visiblement respecter certaines règles non explicitées, comme par exemple ne pas passer des données par "collection" ou dossiers 
(pour qu'une données input soit reliée à un output, il faut qu'elle soit totalement indépendante et déclarée).  
Et il n'y a pas de message pour te signaler le problème, quand tu exécutes le workflow il te dit OK mais ne fait rien... 

### API
Il existe une API ([Python](https://bioblend.readthedocs.io/en/latest/api_docs/galaxy/all.html#) entre autres), 
qui permet de gérer beaucoup de fonctionnalités : 
exécution de tools et workflow, management des historiques (données)... et sûrement beaucoup plus.  
On peut donc imaginer utiliser Galaxy comme gestionnaire des processus / workflows et de données, et refaire une interface utilisateur plus spécifique.

Résumé
------
Cet outil permettra de gérer les jobs et de faire des interfaces simples.
Il est moins évident qu'il permette de faire les interfaces spécifiques, et de la visualisation de données complexes.


À voir :
--------
- gestion de données : quels formats, quels droits, quelle flexibilité
- gestion des droits sur les outils
- plugins : est-ce qu'il est possible développer ses propres plugins de visualisation
- facilité d'utilisation pour l'ajout d'outils, de format de données... par des utilisateurs intermédiaires
- personnalisation de l'interface en fonction des projets
- <s>possibilité de l'utiliser en arrière plan pour les utilisateurs finaux, du coup API ?</s> 