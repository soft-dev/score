Containerisation : Singularity / Docker
====================
Les deux techno semblent assez similaires dans leur utilisation, 

Pourquoi singularity est préféré sur HPC :
- meilleure gestion des droits dans le container, là où docker a tendance à privilégier le superutilisateur, 
singularity privilégie le mode user, sans privilèges élevés.
- Gestion de processus (CPU, RAM, temps d'exécution) plus simple sur Singularity, car le container est lancé comme un processus, alors que pour Docker c'est un deamon qui lance le container.  
Singularity est ainsi compatible avec Slurm ou UGE (gestion de file de calcul).

[Comparaison entre singularity, docker et shifter.](http://geekyap.blogspot.com/2016/11/docker-vs-singularity-vs-shifter-in-hpc.html)
