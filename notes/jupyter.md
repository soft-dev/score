Jupyter
=======
Masquer le code dans le notebook
--------------------------------
Pour cacher le code, mais avec la possibilité de l'afficher à nouveau, il faut aller à http://localhost:8888/tree#nbextensions_configurator, et cocher l'extension *Hide input*.

Application web : [appmode](https://github.com/oschuett/appmode)
---------------
Pour faire une application web à partir du notebook (plus de possibilité de voir le code, code exécuté automatiquement au chargement...), il faut installe AppMode.
```cmd
conda install -c conda-forge appmode
```
Et pour enlever des bouton dans le mode application, il faut modifier le fichier de configuration de jupyter (généré avec `jupyter notebook --generate-config` : ~/.jupyter/jupyter_notebook_config.py), et rajouter à la fin :
```text
## Appmode
c.Appmode.show_edit_button = False
c.Appmode.show_other_buttons = False
```

[Dashboard](https://jupyter-dashboards-layout.readthedocs.io/en/latest/using.html) sous notebook
-----------------------
(attention il faut être en notebook <= 5.5)
```bash
conda install notebook==5.5
conda install jupyter_dashboards -c conda-forge
jupyter nbextension enable jupyter_dashboards --py --sys-prefix
```
Ensuite au même titre que l'appmode, on peut créer des dashboard dans l'environnement

[Dashboard](https://github.com/plotly/jupyterlab-dash) in jupyter lab, en utilisant [plotly dash](https://dash.plot.ly/?_ga=2.67004781.2142878770.1562074549-1215964753.1562074549)
-----------
**Plotly dash** permet de créer des dashboards en python, avec flask en serveur web.  
Une extension permet de l'utiliser nativement dans jupyterlab, 
et ainsi de faire tous ses tests de code + interface dans le même environnement.  
Théoriquement, l'application web ainsi construite est indépendante car elle tourne sur flask, et peut donc être embarqué sur un serveur web...

[**Bokeh**](https://bokeh.pydata.org/en/latest/) est une autre librairie de dashboard en python, qui  [s'inclue dans Jupyterlab](https://github.com/bokeh/jupyterlab_bokeh) aussi.
[Ce comparatif](https://blog.sicara.com/bokeh-dash-best-dashboard-framework-python-shiny-alternative-c5b576375f7f) a l'air de dire que plotly dash est meilleur et plus simple à prendre en main.
  