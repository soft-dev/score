Ce texte est un résumé des réflexions et essais début août 2019 concernant le projet SWAF.  
Il contient :
- un état des lieux simpliste du fonctionnement pour les 2 niveaux d'utilisateurs.
- un découpage des différents outils qu'il faut développer ou réutiliser pour ce projet.

Fonctionnement
==============

Utilisateur intermédiaire
-------------------------
Il se connecte sur une interface web de gestion, et accède à un tableau de bord sur lequel il peut :  
- gérer son compte
- créer une infrastructure (serveur + mode de connexion)
- créer un nouveau service
- accéder à une interface de service précédemment créée
- donner les droits sur les services à d'autres utilisateurs
- visualiser ses données de calcul

Pour la création d'un nouveau service, il faudrait 
- importer un script ou un package.  
- l'associer à une infrastructure de calcul.
- choisir des options (containerisation, communication...).
- lancer un interpréteur (outil à développer) qui en ferait un Notebook (ou page web), avec les inputs / outputs décrits dans le script lui-même.  
- accéder au Notebook pour apporter des modifications et faire des essais (si Notebook).
- lorsque le Notebook est fonctionnel, l'utilisateur peut alors aller sur le "composer" pour arranger visuellement la page web.

Utilisateur final
-----------------
Cet utilisateur se connecte aussi à l'interface web de gestion, et peut :
- gérer son compte.
- accéder à des interfaces de calcul sur lesquelles il a les droits et les exécuter.
- visualiser ses données de calcul.
    
Outils à développer (ou intégrer)
================================
1\. Système d'information de gestion
------------------------------------
Une interface web de gestion des utilisateurs, des dashboards, des droits, des calculs en cours et des données.  
Cette interface web sera forcément liée à une base de données, qu'il faut aussi concevoir pour ce besoin.  
On y retrouve les 2 niveaux d'utilisateurs, avec :
- les utilisateurs intermédiaires qui peuvent uploader des scripts, modifier le dashboard, donner des droits aux autres utilisateurs et des accès aux ressources de calcul.
- les utilisateurs finaux qui ont des droits d'exécution sur des dashboards, qui peuvent consulter leurs calculs en cours et leurs données.

Il faudra faire des liens entre cette interface (SWAF) et l'interface de création / modification / exécution de dashboard qui sera probablement un autre outil (eg. Jupyter / Voilà)

#### Outils existants

Pas d'outil existant.

##### Qu'est-ce qu'on a ?
Rien en dehors de la théorie.

**Serveur** : Pour rester en python on peut utiliser le framework Tornado pour le serveur et la gestion des pages web.  
**Authentification** : Pour la partie authentification, il faut regarder du côté LDAP (normalement c'est intégré à Tornado), Renater pour toucher plus large (voir l'équivalent européen), et SAML pour l'authentification unique.  
**Web design** : Concernant le design il n'y a rien pour l'instant   
**Base de données** : Pour la base de données il y a cet exemple : [database exemple](../notes/databases/2_database_mixed_json_relational.md) qui combine la base de gestion et de calculs.
 

2\. Un interpréteur...
----------------------
...permettant de passer du script à une version de dashboard.  
Cet outil est intégré à l'interface web SWAF, et permet de créer une 1<sup>ère</sup> version de dashboard automatiquement, dès que le script est uploadé.  
Son utilisation implique forcément des règles dans l'écriture du script, avec des commentaires formatés pour être interprétés.   
On peut imaginer un script dans ce genre :
```python
import module1
from module2 import function1

''' inputs
param1: int {"min":0, "max":100}
param2: str {"max_len": 50}
'''
''' output
out: int display save
'''

param1 = 0
param2 = '0'

''' remote '''
out = function1(param1, param2)
''' end remote '''
```
Dans un cas comme ça, il suffit alors de parser les commentaires pour en déduire les inputs et outputs attendus (enregistrés, visibles sur l'interface ou les 2).  
En fonction des calculs cela peut être assez laborieux pour l'utilisateur, il faut voir si c'est plus intéressant que de faire soit même l'interface et de la relier aux variables.

Dans tous les cas les types d'outputs visibles dans l'interface seront forcément imposés, dépendant des widgets d'affichage qu'on aura développé.

Cet interpréteur inclue les outils permettant de gérer les calculs sur serveur distant.
Ainsi la(les) fonction(s) déclarée(s) comme *remote* sera automatiquement exécutées sur serveur distant.

#### Outils existants
Je n'ai rien trouvé dans ce sens, il faudra probablement faire notre protocole et le programme d'interprétation.

##### Qu'est-ce qu'on a ?
Pour l'instant je n'ai pas étudié cet aspect, ça ne me semblait pas être la priorité. 

3\. Un "composer"
-----------------
Cet outil permet de réorganiser et designer le dashboard. 
C'est un outil visuel accessible depuis l'interface web, avec une utilisation majoritairement (voir uniquement) à la souris.

On peut imaginer que c'est ici aussi qu'on configure la gestion des calculs, à savoir le serveur distant, la containerisation, les ressources matérielles...

#### Outils existants
Les 3 options que je vois ici (en restant en python) sont, dans l'ordre de préférence :
- Jupyter + Voilà
- Bokeh
- Plotly Dash

J'ai fait des essais avec Dash et Jupyter / Voilà.

J'opte très clairement pour une solution basée sur **Jupyter**, qui rassemble beaucoup d'avantages :
- une grande communauté de développeurs pour le maintenir et le faire évoluer (à laquelle on doit pouvoir intégrer)
- le noyau qui permet d'exécuter du code depuis l'interface web
- la [quantité de langages](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels) qui peuvent être interprétés par Jupyter
(je n'ai pas essayé, je ne suis pas sûr que tous les kernels soient aussi bien maintenus que pour Python et R).
- les ipywidgets qui intègrent entre autres des outils de visualisation 3D (ipyvolume), de plot (matplotlib)
- les autres développements qui s'intègrent à Jupyter (plotly, bokeh, K3D...)

Concrètement une grosse partie du besoin me semble remplie par Jupyter.

**Voilà** pourrait finaliser la partie application pour en faire un dashboard.
L'idée est l'exécution du code contenu dans un Notebook sans donner l'accès à une console, et donc l'utilisateur final ne peut pas exécuter son code.
- Le cœur de Voilà permet de faire une application à partir du Notebook, avec le même style d'affichage.
- On peut rajouter un "plugin", Voilà-gridstack, qui permet de modifier l'affichage des widgets (position et taille).

Pour l'instant le projet est balbutiant mais prometteur. 
Il faut espérer qu'il soit soutenu.

##### Qu'est-ce qu'on a ?
Dans les essais, il y a un dashboard fonctionnel utilisant **plotly dash**. 
Il est fonctionnel dans le sens qu'il permet de saisir des inputs et d'exécuter du code avec.
Je ne suis pas allé jusqu'à l'affichage des résultats qui n'est pas trivial (mais faisable en théorie).  
Pour cet essai j'ai utilisé l'intégration de dash dans JupyterLab.  
On peut le trouver dans le dossier dashboard/plotly_jupyterlab (branche *client_server_docker*), le README.md explique comment le tester.

Concernant **Jupyter**, je suis allé plus loin avec les inputs, l'exécution de calcul et l'affichage des résultats (2D ou 3D).
  
Concernant **Voilà** j'ai rajouté du code pour faire une application en modifiant Voilà-gridstack.  
Le projet étant en développement, ils n'ont pas cherché du tout à le finaliser, 
du coup la partie gridstack ne permettait pas d'enregistrer la position des éléments, ni de figer un dashboard.  
J'ai donc rajouté ces 2 fonctionnalités (à ma sauce, je ne pense pas que ce serait validé si on en faisait un merge request).  
Ces projets sont directement sur mon [GitHub](https://github.com/brichet). 
Pour l'installer il faut cloner les 2 dépôts et faire un `python -m pip install . -vv` de Voilà puis de Voilà-gridstack. 
 

4\. Exécution du dashboard
--------------------------
Un outils pour exécuter le dashboard sur le serveur web, afin de traduire les inputs et transférer le calcul sur serveur distant.

#### Outils existants
À priori l'outil composer s'appuyera sur un outil permettant d'exécuter les dashboards, ça va de paire.

##### Qu'est-ce qu'on a ?
Dans le cas où on utilise le combo Jupyter / Voilà la question ne se pose pas.

J'ai séparé cet outils pour le cas où on choisirai une technologie autre et qu'il faille exécuter du code python (par exemple) depuis des widgets javascript.

5\. Gestionnaire de calcul
--------------------------
Un outil pour transférer et exécuter les calculs sur un serveur / HPC dédié.
Il faut donc qu'il puisse : 
- transférer des commandes (eg. SSH).
- transférer des données dans les 2 sens (eg. SCP, RSYNC si on reste sur des fichiers).
- faire un script bash interprétable par un gestionnaire de queue le cas échéant.
- communiquer avec une base de donnée pour stocker les résultats et avec l'interface web pour de l'intérection en live.

#### Outils existants
Il y en a théoriquement une flopée, lourds à assimiler et pas forcément adaptés.
Pour l'instant je vois 3 options (du moins évolué au plus évolué) :
- un service web maison
- Execo, une librairie qui permet de faire des commandes en ssh
- WAVES qui gère les infrastructures, utilise le gestionnaire de queues de l'infrastructure et fournit une API (Rest) pour envoyer les jobs.

J'ai testé surtout les 2 premières solutions, pour WAVES je me suis contenté du tuto pour le moment.

Je pense que si on arrive à intégrer **WAVES** ce sera une bonne solution.
L'outil est maintenu par le LIRMM, et il abstrait le gestionnaire de queue présent sur le cluster.
Il faut voir s'il permet de faire ce qu'on souhaite, et si ce n'est pas le cas à quel coût on peut le faire évoluer.

**Execo** est aussi une bonne alternative dans le sens qu'il simplifie l'usage du SSH et SCP, qui sont les standards de communication avec tout serveur de calcul.

Enfin un **service web** est assez simple et peut faire le boulot, le problème est qu'il nécessite :
- l'exécution d'un programme côté serveur
- l'ouverture d'un port de communication pour appeler le service

Je pense que ces 2 conditions rende l'outil difficilement utilisable sur cluster, et à priori pas sur MUSE@LR.

##### Qu'est-ce qu'on a ?
Les solutions **service web** et **Execo** sont fonctionnelles (bien sûr non finalisées).  
On est capable depuis la librairie de SWAF d'envoyer un calcul sur un serveur distant en utilisant l'une ou l'autre de ces solutions.  
La condition pour utiliser ces solutions pour l'instant est d'appeler une fonction particulière en utilisant SWAF. 
On ne lui passe donc pas un script, mais on appelle une fonction avec des arguments.
 
L'appel est relativement transparent dans les 2 cas.

Pour faire l'équivalent de ça :
```python
from calculation import calculation

load_parameters = "blabla"

points_X_pre, points_Y_pre = calculation(load_parameters)
```
Il suffit de faire ça avec SWAF :
```python
from calculation import calculation
from swaf import SwafCoreExeco as SwafCore # pour utiliser Execo
# ou from swaf import SwafCoreWebService as SwafCore pour utiliser le web service

load_parameters = "blabla"

swaf_core = SwafCore()
swaf_core.compute(calculation, load_parameters=load_parameters)
points_X_pre, points_Y_pre = swaf_core.socket.get_data()
```

**Que se passe-t-il quand on fait launch_calculation() :**  
Le début est le même dans les 2 cas (service web ou Execo):
- récupération du chemin du module (script qui contient la fonction).
- création d'un dossier temporaire.
- copie de toute l'arborescence du script (tous les fichiers et dossiers au même niveau) dans le dossier temporaire.
Ceci permet de récupérer toutes les dépendances et données nécessaire mais non préalablement installées. 
- "picklisation" des arguments dans ce même dossier temporaire.
- création d'un script (toujours dans ce même dossier) qui a pour mission de (i) "dépickliser" les arguments, (ii) lancer le calcul avec la supervison choisie. 

Dans le cas du **service web** (Explication détaillée d'une [solution combinant web service + socket](SWAF_webservice_socket_details.md)): 
- création d'une archive avec ce dossier temporaire
- envoi de l'archive via le service web
- côté serveur de calcul : désarchivage puis  exécution du script



Dans le cas de **Execo** :
- envoi du dossier temporaire par SCP
- envoi de la commande bash d'exécution du script par SSH

6\. Supervision des calculs en cours
------------------------------------
Un outil pour superviser les calculs en cours :
- récupération des données en fin de calcul
- arrêter le calcul
- avoir un état d'avancement
- visualiser des données intermédiaires
- changer des paramètres en cours de calcul ? 

#### Outils existants

Je n'ai rien trouvé qui fasse tout ça.
En revanche tous les outils "gestionnaires de calculs" un peu avancés (WAVES, Galaxy, etc) font au moins la récupération des données finales et l'annulation du calcul. 

Les réflexions pour faire cet outil étaient initialement l'utilisation de socket, ce qu'il faudra probablement exclure dans le cas de cluster, pour les même raisons qu'on exclurait le web service (problème d'ouverture de ports).
Un autre option que j'explore est l'utilisation de SSH (Execo à nouveau) pour faire ça.

##### Qu'est-ce qu'on a ?
Une solution de communication par **sockets** est fonctionnelle (non finalisée, pas stable du tout) 
elle permet :
- de récupérer en temps réel un état d'avancement du calcul (états d'avancement évalués par le développeur).   
- de récupérer les données de sortie (format pickle, mais ça pourrait être n'importe quoi en fichier).
- d'exécuter du code python dans le même environnement que le calcul (mais je n'ai pas réussi à accéder aux variables de calcul).

Explication détaillée d'une [solution combinant web service + socket](SWAF_webservice_socket_details.md)

Si on reprend l'exemple précédent, il est possible de récupérer des états intermédiaires dans l'interface de cette façon : 

```python
import time
from calculation import calculation
from swaf import SwafCoreExeco as SwafCore

load_parameters = "blabla"

swaf_core = SwafCore()
swaf_core.compute(calculation, load_parameters=load_parameters)

# do whatever you want waiting for end of calculation
# for example get calculation status
while not swaf_core.socket.computation_over:
    if swaf_core.socket.queue_status.empty():
        time.sleep(0.01)
    else:
        state, progress = swaf_core.socket.queue_status.get()
        print("{}% : {}".format(progress, state))
    
# and get you data back when calculation is over
points_X_pre, points_Y_pre = swaf_core.socket.get_data()

```

Une solution **SSH - Execo** est en cours de développement. 
On peut déjà récupérer les états intermédiaires, mais je ne maîtrise pas encore complètement l'outil et j'ai des bugs à corriger.
En revanche il semblerait que ce soit déjà bien moins réactif que par l'utilisation de sockets.


7\. Base de données de calculs
------------------------------
En plus de la base de données de gestion, il faudra une base de données de calculs qui stockerait les exécutions de calculs (version, input...) et les résultats.   

#### Outils existants
Je ne crois pas non plus qu'il y ait des base données spécifiques à ce besoin, mais il en existe probablement desquelles on peut s'inspirer.

##### Qu'est-ce qu'on a ?
Rien excepté de la théorie :
[database exemple](../notes/databases/2_database_mixed_json_relational.md) qui combine la base de gestion et de calculs.

Schéma des outils
-----------------
![MCD](img/global_tools.png)