Compilation des différentes remarques et questionnement sur le projet
=====================================================================

Général
-------
- Définir plus précisément les workpackages, à savoir les sous-parties du projet
- Se renseigner sur l'authentification LDAP
- Se renseigner sur Singularity en complément / remplacement de docker
- Regarder [Kubernetes](kubernetes.md) pour la gestion des containers(../notes/kubernetes.md)
- est-ce que le moteur de Gitlab peut être une option permettant de stocker les états, les projets...

Gestion de données
------------------
- Se renseigner sur ce qui existe en terme de dataflow / gestion de données de calculs
- Réfléchir à la notion de projet, permettant d'englober l'ensemble d'une application :
    - Sécurisation : seules les personnes autorisées ont accès aux données / scripts
    - Enregistrement / récupération du projet en l'état, à n'importe quelle phase (parametrisation, calculs, post traitement)
- Se poser la question des formats de données possibles, et aussi d'un modèle de base de données qui puisse correspondre (relationnel, NoSQL...)
- comment inclure les ontologies aux données ?... de la manière la plus simple et transparente possible...
- voir ce qui existe déjà en base de données pour ce type de problème (gestion des dockers, des versions de code, des droits)  
Voir les JDEV...
- Est-il possible de mettre des droits différents sur les données issues d'un même calcul (typiquement en option d'une application) ?
- De même est-il possible de mettre des droits sur des parties de l'interface, avec options visibles mais non utilisables ?  
Une possibilité pour ça pourrait être de découpler l'interface du programme de calcul, ce qui permettrait d'avoir des droits sur différentes interfaces sans dupliquer le programme lui même...

Calcul
------
- Possibilité de modification de variables en cours de calcul ?
- Voir du côté des décorateurs python pour renvoyer l'état du calcul.
- Regarder les contraintes hardware des dockers (GPU, CPU, RAM…)
- Voir comment allumer un docker à distance via notebook (cf Kubernetes)
- Être capable de retrouver l'état du calcul pour chaque données, et de reproduire ce calcul :
    - association de paramètres d'entrée et d'un script / module / package de calcul pour chaque donnée de sortie.
    - faire un commit à chaque lancement d'un calcul ?
    
Interface
---------
- Voir si QtDesigner ou équivalent peut permettre de générer une interface web simplement
- Étudier les framework python / web, en particulier Django. Voir si Tornado inclus des droits.
- Voir les outils d'interface web :
    - FreeFen++
    - Zebulon -> Zset -> livezset
- Interrogations sur la flexibilité de l'interface, jusqu'au on va dans la paramétrisation "esthétique" ?

[Jupyter](jupyter.md) 
-------
- Regarder les besoins pour l'installation d'un serveur JupyterHub
- Est-ce que l'instance de Jupyter Hub doit être sur le serveur de calcul ou sur un serveur distant ?
- Est-ce qu'il est possible de complètement cacher le code et avoir juste une interface clique bouton ?
- Est-ce qu'on est au moins sûr que le code des modules appelés n'est pas lisible ?
- Étudier la gestion des droits pour voir jusqu'où on peut aller (et voir aussi avec connexion LDAP)
- Voir si on peut intégrer le notebook à une autre page web, ou au moins "décorer" la page avec une charte graphique (cf jupyter theme)
- Est-ce que le développement de widgets "maison" est accessibles, au cas où ?

Dockers
-------
- Regarder les contraintes hardware des dockers (GPU, CPU, RAM…)
- Voir comment allumer un docker à distance via notebook (cf Kubernetes)
- Gestion des processeurs sur docker pour des applications multiprocesseurs