Base de donnée entièrement en JSON, type MongoDB
================================================
Remarques :
-----------
Ce document a servi d'aide à la reflexion sur les possibilités d'une base de données orientée documents.
À priori les propositions faites ici ne seront pas retenues, du moins pas dans l'intégralité. 

Collections
-----------

### 1. Data

Ce format doit être le plus ouvert possible pour accepter différents types de données, si possible sans devenir une usine à gaz.
Une option est de s'appuyersur  des ontologies pour décrire le type, et dans ce cas sur des ontologies sur les données numériques.

L'idée pourrait être d'imposer soit du format générique (int, float, string), soit une données composée (list, dict) de génériques, soit une donnée "fichier" type image, point cloud...
Dans le cas de donnée de type "fichier", le document ne contient que le lien permettant d'y accéder.

```json5
{
  "uri": "http://www.swaf.fr/data/**type**/**id**", 
  "date": "2019-03-14 18:49:56.781879",
  "author": "http://www.swaf.fr/users/**login**",
  "provenance": "manual, calculated or measured",
  "type": "uri_ontology_data_type",
  "data": { // data fields should be type dependant
    
    // FILE, complex data : image, point cloud, pickle, document..
    "file_name" : "d4215326-c9b4-40e4-bc1e-c1a99659360d.png",
    "server_path" : "URL vers serveur contenant l'image",
    
    // Numerical / string data :
    "value": "string", // or int, float...
    
    // List of numerical / string data :
    "value": ["string", 100, ],
    
    // Dict of numerical / string data :
    "value": {
      "key1": "value1",
      "key2": 100,
    },
              
  },
  
  "metadata": {
    /* metadata fields should be type dependant
      It contains useful information for user or calculation
      There is a risk that it becomes user / system / calculation specific
    */
    
    // For exemple for a camera image
    "target_types": ["uri_ontology_to_any_object_type"], 
    "rotation_angle": 30,
    "camera_coordinates": {
      "x" : 2500,
      "y" : 512,
      "z" : 577
      }
    }
}
```

### 2. Programme / module / script

On pourrait s'appuyer sur git pour la gestion d'un programme, peu importe sa complexité.
Ainsi chaque nouveau programme correspond à un projet sur un serveur Gitlab.

```json5
{
  "uri": "http://www.swaf.fr/programs/**program_name**",
  "author":  "http://www.swaf.fr/users/**login**",
  "git_clone_http": "https://git-xen.lmgc.univ-montp2.fr/soft-dev/swaf.git",
  "git_clone_ssh": "git@git-xen.lmgc.univ-montp2.fr:soft-dev/swaf.git",
  // "git_branch_name": "branch name",  
  "dependencies": "http://www.swaf.fr/yamls/**id**",
  "description" : "Interfacing, managing and saving calculation process on remote server and whatever else...",
  "input_type" :  {
    "arg_name1": "uri_ontology_data_type", 
    "arg_name2": "uri_ontology_data_type",
    "arg_nameX": "...",
  },
  "output_type" : {
    "output_name1": "uri_ontology_data_type", 
    "output_name2": "uri_ontology_data_type",
    "output_nameX": "...",
  },
}
```
Le git_branch_name est volontairement commenté car j'imagine que ce n'est pas une bonne idée, la branche peut être supprimée.
Il vaut donc mieux travailler uniquement sur la branche master lorsqu'on est en prod (et donc qu'on sauvegarde les données).
Les calculs sur branche ne sont donc pas sauvegardés dans la DB.

### 3. Process / workflow execution

Le process execution correspond à l'exécution d'un programme.
Il est donc lié à un programme (et son commit / tag), des data "input", des data "output", pourquoi pas un serveur ?

Il faut alors de faire un nouveau *git add \* / git commit / git push* à chaque exécution du code.
S'il n'y a pas eu de changement depuis le dernier commit, alors on ne fait rien bien sûr.

Pour les ontologies liées à la provenance, voir [prov](https://www.w3.org/ns/prov).
Il faudrait probablement qu'on s'appuit sur leurs concepts.

```json5
{
    "uri" : "http://www.swaf.fr/process/**process_id**",
    "program_uri": "http://www.swaf.fr/programs/**program_name**",
    "agent":  "http://www.swaf.fr/users/**login**", /*prov:Agent or foaf:Agent*/
    "start_time" : "2019-05-23 17:25:14", /*prov:Start*/
    "end_time": "2019-05-23 17:26:14", /*prov:endedAtTime*/
    "git_commit": "commit_id", /* ou git tag, voir le mieux */
    "input_uri" : { /*prov:used URIs (dont peut etre datasets / images)*/
      "arg1": "http://www.swaf.fr/data/**type**/**id**", 
      "arg2": "http://www.swaf.fr/data/**type**/**id**",
      "argX": "...", 
    },
    "output_uri" : { /*http://www.w3.org/ns/prov#generated*/
      "output1": "http://www.swaf.fr/data/**type**/**id**", 
      "output2": "http://www.swaf.fr/data/**type**/**id**",
      "outpuX": "...", 
    },
    "execution_server_uri": "http://www.swaf.fr/server/**server_uri**",
    "description": "Specific description of this execution ?? "
}
```

### 4. Calculation servers

Cette collection permet de décrire les serveurs de calcul, et de les lier à l'exécution de process.
On pourrait aussi se servir de cette collection pour la gestion / repartition des calculs.


```json5
{
  "uri": "http://www.swaf.fr/servers/**id**",
  "dockers": [
    "http://www.swaf.fr/dockers/**id**",
    "http://www.swaf.fr/dockers/**id**",
  ],
  "hardware": {
    "CPU": 8,
    "memory": 64,
    "...": "...",
  }
  
}
```

### 5. Docker images
Data de type fichier, avec une uri en "htt<span />p://w<span />ww.swaf.fr/dockerfiles/\*\*id**"

### 6. Yaml files
Data de type fichier, avec une uri en "htt<span />p://w<span />ww.swaf.fr/yaml/\*\*id**"

### 7. Utilisateurs
```json5
{
  "uri": "http://www.swaf.fr/users/**login**",
  "login": "tartempion",
  "password": "SHA1 password",
  "description": {
    "lastname": "tartempion",
    "firstname": "tintin",
    "...": "...",
  }
}
```

### 8. Permissions
```json5
{
  "uri": "http://www.swaf.fr/permissions/**id**",
  "type": "ontology to permission if exists",
  "description": "This permission allow..."
}
```

### 9. Droits des utilisateurs
```json5
{
  "uri": "http://www.swaf.fr/users_permissions/**id**",
  "user_uri": "http://www.swaf.fr/users/**login**",
  "object_uri": "http://www.swaf.fr/[data | programs | servers]/**id**",
  "permission_uri": "http://www.swaf.fr/permissions/**id**"
}
```

### Sensors ??

Cette base n'est probablement pas dédiée à la gestion des données issues de capteurs.
Du coup elle n'a pas de raison d'inclure la description de capteurs, sinon juste un lien vers une base dédiée.

Représentation MCD
----------
Le schéma ci-dessous est une représentation de la base de donnée avec les liens entre les éléments.
Le schéma différencie les éléments relatifs au calcul et ceux relatifs aux utilisateurs et droits. 

![MCD](img/1_database_full_json_mcd.png)

Limitations / alternatives
--------------------------
### 1. Gestion des utilisateurs et des droits
À priori ça ne semble pas adapté à une base NoSQL par documents, les bases relationnelles gèrent à priori très bien les utilisateurs.

### 2. Général NoSQL

Dans ce schéma il y a trop de liens entre documents.

Une idée centrale du NoSQL est l'autonomie des documents, en minimisant au maximum les liens entre eux.
Il vaut mieux faire de la redondance de données entre les documents que de faire des liens, c'est ce qui permet une bonne distribution. 

Alternatives :
- Un seul format de json (une seule collection) contenant toute l'info sur un calcul, à priori *Process* augmenté.
- Combinaison base relationnelle / NoSQL, voir aussi web sémantique ou différentes techno NoSQL.
- pas d'utilisation de documents, avec le risque de perdre en flexibilité... voir du coté des NoSQL orientés colonnes.

### 3. Data
- Initialement j'imaginais inclure l'uri du process dans la provenance, mais on se retrouve avec des tables qui se font références l'une l'autre.
*Process* pointe sur des URI de *Data* dans son champ output, et *Data* pointe sur des uri de *Process* dans provenance... c'est le serpent qui se mord la queue.

    Alternatives :
    - Ce n'est pas gênant d'avoir des références croisées, on met l'uri du *Process* dans *Data*. 
    - Pas d'output dans *Process*, seul *Data* fait le lien. 
Ça implique soit de n'avoir qu'un seul output par *Program*, soit de ne pas nommer les outputs, et donc ne pas les identifier.
    - Inclure les *Data* au *Process* à nouveau.

- Les metadata sont hors contrôle, ce qui est le but, mais aussi le risque. 
Il n'y a aucune ontologie ou description obligatoire des metadata, ce qui permet de rendre l'outil très souple.
En revanche le risque est que ces metadata finissent par contenir de la donnée par simplicité d'utilisation.

    Alternatives:
    - On s'en fout, chacun fait ce qu'il veut avec ses données.
    - Pas de metadata, mais ça représente probablement une perte importante, et un des intérêt majeur de la DB orientée document.
    - Encadrer au mieux les metadata utilisable.

### 4. Description des programmes
Cette description fige les inputs et outputs d'un programme... 
ce qui limite les évolutions de code, et qui implique dans ce schéma de créer un nouveau projet quand les inputs / outputs évoluent pour une même fonctionnalité.

Alternatives :
- Pas de description des inputs et outputs d'un programme.
La conséquence est probablement de rendre l'utilisation en *mode dataflow* impossible.
- Le document *Program* prend en plus un git tag, et on impose un tag lorsqu'il y a modification des inputs / outputs.
Un nouveau document *Program* par tag, et donc par modification de la description du programme.

### 5. Description des serveurs
La description des serveurs est vouée à évoluer, autant le hard ou le soft, du coup une description figée ne semble pas être la meilleure formule.

Alternatives :
- Un json par version, avec un nouvel ID même si c'est le même serveur. 
Le document ne représente pas seulement le serveur mais l'ensemble serveur plus configuration.
- Faire comme pour un projet git, avec du versionnement des descriptions, qui peuvent rester au format JSON.
Dans ce cas on rajouterai un champs *git_commit_server* à la collection *process_execution*.
Il faudrait aussi toujours un projet genre *SWAF_server* sur le gitlab.
