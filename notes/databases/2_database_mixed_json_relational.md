Base de donnée mixte entre JSON (type MongoDB) et relationnelle
==============================================================
Ceci est une ébauche de base de données mixte entre base relationnelle et NoSQL.  
En gros l'idée générale est d'utiliser une base relationnelle classique (type postgreSQL ou MySQL) pour :
- ce qui est interactif avec une interface web
- ce qui est amené à changer régulièrement, à s'incrémenter, mais dans des volumes de données raisonnables

En parallèle on utilise une base NoSQL documents pour :
- les données pour lesquelles on souhaite garder de la flexibilité
- ce qui ne va jamais bouger
- ce qu'on peut vouloir extraire en masse pour analyse

**En résumé, on retrouve dans la base relationnelle :**
- les utilisateurs et les groupes
- les programmes dans l'état actuel, avec les dépendances et les inputs / outputs actuels
- les dockerfiles existants dans SWAF dans l'état actuel
- les serveurs dans leur configuration courante

**et dans la base NoSQL :**
- l'historiques des programmes
- l'historique des dockerfiles
- l'historique des serveurs
- les exécutions de programme
- les exécutions de dataflows
- (les données ??)

Base de données relationnelle
-----------------------------

### Gestion des droits
Les droits sont attribués par groupe, et chaque utilisateur appartient au moins à un groupe, crée en même temps que lui (comme linux).  
Il y a 2 grands types de droits :
- les droits en lecture / récupération des données (issues de calculs en général)
- les droits de lecture (visualisation), écriture (modification, suppression) et exécution (utilisation) des programmes de calcul, voir des serveurs et autres.

### Les programmes de calculs
La base relationnelle contient une description sommaire des programmes de calcul, le lien vers vers le git, et le lien vers une description plus complète de l'état actuel en NoSQL.   
Pour chaque programme on associe une liste de dépendances ainsi qu'une description des types de données en input et en output.

### Les dockerfiles
Les dockerfiles dans leur état actuel sont aussi décris, avec pour chacun la liste des dépendances qu'ils satisfont.

### Les serveurs
De même les serveurs sont aussi décris sommairement, avec les infos qui sont utiles pour les utilisateurs (RAM, CPUS, ...)  
Un lien entre dockerfiles et serveur est crée, qui peut être intéressant dans le cas de programmes spécifiques windows.

NoSQL : Collections de documents
------------------------
### Gestion des données
Si on part sur une base de données orientée documents, l'idée est de garder le maximum d'autonomie à chaque document.
Donc lorsque l'on souhaite requêter sur des données, il est important de se poser la question de ce qu'il indispensable d'embarquer avec et de ce qu'on mettre dans des documents à part.
L'idée est donc que ce qui est en documents à part soit là pour la traçabilité et la reproductibilité, mais que ce ne soit pas des données d'analyse.

Partant de là, 3 options semblent envisageables :
- **Les données ont une vie propre, et sont utilisables seules.**  
Dans ce cas les données sont des documents, et les process sont des documents à part.
Et on peut se retrouver avec un conflit (l'histoire de la poule et l'œuf) : 
est-ce que le process contient l'URI des données produites ou est-ce que les données produites contienne l'URI du process ?   
Pas sûr que les 2 ensembles soient possible.
- **Les données ne sont intéressantes que liées au calcul.**  
Dans ce cas il vaut mieux avoir un gros document qui contient l'ensemble des données input et des données output, avec les info sur le programme et sa version.
Dans ce cas si on souhaite que des données de sorties soient en entrée d'un autre processus (type dataflow), on duplique entièrement la donnée, pour chaque document soit indépendant.
- **On s'intéresse au process et à la valeur des données input et output.**  
Une nuance possible par rapport aux versions ci-dessus, pourrait être que les données soient décrites indépendamment, avec le process qui les a produite le cas échéant.
Afin que le document de process garde une autonomie, il pourrait dupliquer seulement les valeurs des données, avec un lien vers la données originale pour les metadata.
Cette stratégie présente plusieurs avantages :
    - on peut requêter facilement par processus ou par données, chaque document est autonome.
    - ça permet de ne pas donner plus d'importance aux metadata qu'aux données (cf. point 2 [limitations](./1_database_full_json.md#3-data))
    - la traçabilité est garantie par le lien existant entre le process et la données.  
    Ainsi si une donnée d'output est en input d'un autre process, on peut le tracer.
Par contre il y a aussi des inconvénients :
    - demande plus d'espace disque
    - redondance de données, donc risque de perte d'intégrité
    
### Gestion des versions...
1. **...des programmes**  
Chaque donnée produite est liée à un programme d'une manière ou d'une autre.
Il est donc indispensable de tracer ces programmes, et pour ça une option est d'utiliser gitlab pour le versionnement.  
Ainsi on pourrait considérer qu'on fait un nouveau document lorsque la description du programme, des input, des output ou les dépendances changent.
On pourrait alors imaginer qu'un changement majeur entraîne la création d'un tag sur git, et que pour chaque tag on a un document json.  
Pour la version exacte, on laisse alors le document de process enregistrer le commit par exemple.

2.**...des serveurs**  
De même que pour les programmes, il est probablement important d'avoir un historique des composant hard et soft des serveur lors de l'exécution d'un calcul.
On peut imaginer ici aussi un nouveau document pour le changement sur le serveur, ainsi on peut lier le process à la configuration sur laquelle il a tourné.

3.**...des dockerfiles**
Et enfin dans la même idée on pourrait sauvegarder l'état des dockerfiles ou singularity pour connaître aussi la version exacte de chaque dépendance.


Représentation MCD
----------
Le schéma ci-dessous est une représentation de la base de donnée avec les liens entre les éléments.

![MCD](img/2_database_mixed_json_relational_mcd.png)

Limitations / alternatives
--------------------------
