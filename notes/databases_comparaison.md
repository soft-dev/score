Databases
=========
Brève description
-----------------
Deux grandes familles : relationnelles et NoSQL.
En résumé simpliste, on peut retenir pour chacun les avantages et inconvénients ci dessous.

**Relationnelle :**  
**+** Pas de redondance (du moins c'est l'objectif)  
**+** Intégrité des données, du fait de la non redondance, du typage et de l'atomicité des données  
**+** Langage de requête stable et éprouvé, avec des jointures possibles entre les tables  
**-** Pas de flexibilité sur le noms des colonnes, et donc sur les données  
**-** Données atomique, donc un tableau se traduit par une nouvelle table avec autant de lignes que d'éléments  
**-** Pas adapté au big data

**NoSQL :**  
**+** Flexibilité des données, pas de champ obligatoire  
**+** Accepte les données complexes (dictionnaires, tableaux)  
**+** Adapté au traitement de données en big data  
**-** Pas d'intégrité de données, on peut mettre ce qu'on veut  
**-** Requêtes entre documents pas adapté, donc il vaut mieux que chaque document vive indépendamment des autres  
**-** Redondance de données, du fait de l'autonomie des documents, donc risque fort d'erreurs
**-** Mise à jour de documents pas du tout adaptée, en général on écrase lorsqu'on veut modifier

**En résumé**, le NoSQL n'est préconisé que dans certains cas très spécifiques :
- une masse considérable de données à traiter
- un format de données que l'on souhaite flexible
- on ne fait pas de mises à jour sur les données (type log)

Analyse dans le cas du projet SWAF
----------------------------------
Dans le cas du projet SWAF, il pourrait y avoir les 2 familles de base de données qui cohabitent :

**Relationnelle comme base de travail :**
- utilisateurs
- groupes
- droits
- serveurs opérationnels
- programmes opérationnels, avec la description des inputs / outputs actuels
- capacité à faire tourner un programme sur un serveur

**NoSQL comme base de log et de gestion des données :**
- Données avec un format flexible
- Description de dataflows, avec les liens entre les inputs et outputs des différents programmes
- Log d'exécution des programmes en lien avec les données, une configuration serveur et une configuration programme
- Log d'exécution de dataflows, liens avec les logs d'exécution des programmes
- Historique des configurations des serveurs
- Historique des modifications des programmes (particulièrement les inputs / outputs)

Il reste à savoir comment on compte exploiter les données, ce qui peut changer la structure des documents.

Par exemple on peut imaginer qu'une donnée n'a de sens que si elle est inclue dans son dataflow.
Dans ce cas le mieux est que le dataflow contienne les données pour éviter de faire constamment des jointures entre les collections.

