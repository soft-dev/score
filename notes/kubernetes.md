Kubernetes (k8s)
==================
cf. [article simple expliquant la complémentarité entre docker et kubernetes](https://containerjournal.com/2019/01/14/kubernetes-vs-docker-a-primer/)

cf. [JupyterHub + Kubernetes](https://z2jh.jupyter.org/en/latest/)

Concept
-------
* Développé par/pour Google, et laissé à la communauté en 2015
* Permet de gérer des containers : dockers mais aussi autres environnements virtuels, par contre on ne parle pas de singularity dans la doc
* Gestion de ces containers sur plusieurs nœuds d'un cluster en simple ligne de commande, ou via API python à priori
* Un serveur sert de Master Node Kubernetes, qui gère des Workers Nodes 
* Notion de PODS : ensembles de containers fonctionnant systématiquement ensemble. 
Dans le cas simple un seul container par POD, mais on peut imaginer un serveur web lié à un serveur d'authentification par exemple.
* Il existe une version locale pour faire des essais [minikube](https://kubernetes.io/docs/setup/minikube/)

Utilisation possible pour SWAF
------------------------------
On peut imaginer un serveur kubernetes qui lance un POD {serveur web - serveur d'authentification}.
Le serveur web peut autant être un site classique qu'un jupyterhub, et peut être sur la même machine physique que kubernetes.

Ce kubernetes connaît alors le cluster de calcul, sur lequel il va lancer les dockers de calcul à la demande de l'interface web (API python).