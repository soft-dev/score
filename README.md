SCORE : Supervise Computation Remotely
======================================

SCORE package provides tools to execute and monitor computation (using Python) on remote servers.\n
It make it easy by using *SSH* to send files and data and retrieving information from the running computation, and
allowing to run the computation in a *Singularity* environment.

Let's say you are in a Python console or Notebook on your computer, and want to run a quite long computation.

### Choose where to compute :

- **locally** : it will run in its own process
- **remotely**: it will run on a computation server. All you need is to provide an IP address, an username and SSH
  key which is allowed to connect to the provided session.

In each case you can close your current session, and get back to the computation whenever you want.

### Choose your environment :

You can perform the computation :

- in an existing **Conda**, **Virtualenv** or **system wide** environment. All you need is a full path to the Python
  executable.
- in a **Singularity** environment. You need for that a built Singularity image on the local computer, and
  Singularity installed on the system which will run computation. In case Singularity is installed on your computer,
  you can provide a Singularity definition file, and SCORE will build it for you.

### Save the output data :

You can use SCORE to save all the computation data (the file system, and the Python output data) on storage server.
For tha feature you will need a SSH key on the computation server, which is allowed to connect to the storage server.
SCORE provides tools to make it easy.

### Monitor your computation :

When a computation is running or has run, you can retrieve easily :

- the computation status
- the printed outputs
- the intermediate or final data (data must be 'pickable')
- the error messages if there are any

### Documentation

To generate the documentation you need python whith the 'sphinx' and 'sphinx-rdt-theme'
packages installed. Then you can generate the HTML documentation by going in the
`doc` directory and running (in Linux):
```shell
make -f Makefile html
```
Then open the `build/html/index.html` file with your web browser.

